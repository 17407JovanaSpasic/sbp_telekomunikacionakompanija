﻿using System;
using System.Collections.Generic;
using System.Linq;

using NHibernate;

using TelekomunikacionaKompanija.DTOs.Device;
using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.DTOManagers
{
    public class DeviceDTOManager
    {
        #region Device Method(s)
        public static List<DeviceDTO> GetAllDevices()
        {
            var devicesList = new List<DeviceDTO>();
            try
            {
                var session = DataLayer.GetSession();
                var devicesEntities = (from devices in session.Query<Device>() select devices);
                foreach (var device in devicesEntities)
                {
                    IQuery q = session.CreateQuery("select m.PIB, m.Name from Manufacturer m "
                                                    + "where m.PIB like " + device.PIBManufacturer.PIB);

                    IList<object[]> results = q.List<object[]>();
                    foreach (object[] o in results)
                    {
                        string pib = (string)o[0];
                        string name = (string)o[1];

                        devicesList.Add(new DeviceDTO(device.SerialNumber, device.StartOfUseDate, device.ServiceDate, device.ReasonForServicing, pib, name));
                    }
                }
                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return devicesList;
        }

        public static bool DeleteDevice(string serialNumber)
        {
            try
            {
                var session = DataLayer.GetSession();
                var device = session.Get<Device>(serialNumber);

                session.Delete(device);
                session.Flush();
                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        #endregion Device Method(s)

        #region Main Station Method(s)
        public static List<MainStationDTO> GetAllMainStations()
        {
            var mainList = new List<MainStationDTO>();
            try
            {
                var session = DataLayer.GetSession();
                var mainEntities = (from main in session.Query<MainStation>() select main);

                foreach (var main in mainEntities)
                {
                    mainList.Add(new MainStationDTO(main.SerialNumber, main.NumberOfNodes));
                }
                session.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return mainList;
        }

        public static bool CreateMainStation(MainStationDTO mainStationObj, Manufacturer pib)
        {
            try
            {
                var session = DataLayer.GetSession();
                var mainStationEntity = new MainStation();
                mainStationEntity.SerialNumber = mainStationObj.SerialNumber;
                mainStationEntity.StartOfUseDate = mainStationObj.StartOfUseDate;
                mainStationEntity.ServiceDate = mainStationObj.ServiceDate;
                mainStationEntity.ReasonForServicing = mainStationObj.ReasonForServicing;
                mainStationEntity.NumberOfNodes = mainStationObj.NumberOfNodes;
                mainStationEntity.PRegionalHub = 0;
                mainStationEntity.PIBManufacturer = pib;

                session.SaveOrUpdate(mainStationEntity);
                session.Flush();
                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion Main Station Method(s)

        #region Regional Hub Method(s)
        public static List<RegionalHubDTO> GetAllRegionalHubs()
        {
            var regionalList = new List<RegionalHubDTO>();
            try
            {
                var session = DataLayer.GetSession();
                var regionalEntities = (from reg in session.Query<MainStation>()
                                        where (reg.PRegionalHub == 1)
                                        select reg);

                foreach (var regional in regionalEntities)
                {
                    regionalList.Add(new RegionalHubDTO(regional.SerialNumber, regional.StartOfUseDate, regional.ServiceDate, regional.ReasonForServicing, regional.NumberOfNodes, regional.NameOfRegion));
                }
                session.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return regionalList;
        }

        public static bool CreateRegionalHub(RegionalHubDTO regionalHubObj, Manufacturer pib)
        {
            try
            {
                var session = DataLayer.GetSession();
                var regionalHubEntity = new MainStation();
                regionalHubEntity.SerialNumber = regionalHubObj.SerialNumber;
                regionalHubEntity.StartOfUseDate = regionalHubObj.StartOfUseDate;
                regionalHubEntity.ServiceDate = regionalHubObj.ServiceDate;
                regionalHubEntity.ReasonForServicing = regionalHubObj.ReasonForServicing;
                regionalHubEntity.NumberOfNodes = regionalHubObj.NumberOfNodes;
                regionalHubEntity.PRegionalHub = 1;
                regionalHubEntity.NameOfRegion = regionalHubObj.NameOfRegion;
                regionalHubEntity.PIBManufacturer = pib;

                session.SaveOrUpdate(regionalHubEntity);
                session.Flush();
                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        #endregion Regional Hub Method(s)

        #region Communication Node Method(s)
        public static List<CommunicationNodeDTO> GetAllCommNodes()
        {
            var commList = new List<CommunicationNodeDTO>();
            try
            {
                var session = DataLayer.GetSession();
                var commEntities = (from comm in session.Query<CommunicationNode>() select comm);

                foreach (var comm in commEntities)
                {
                    commList.Add(new CommunicationNodeDTO(comm.SerialNumber, comm.Address, comm.LocationNumber, comm.Description));
                }
                session.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return commList;
        }
        public static bool CreateCommunicationNode(CommunicationNodeDTO commNodeObj, Manufacturer Pib, MainStation serialNum)
        {
            try
            {
                var session = DataLayer.GetSession();
                var commStationEntity = new CommunicationNode();
                commStationEntity.SerialNumber = commNodeObj.SerialNumber;
                commStationEntity.StartOfUseDate = commNodeObj.StartOfUseDate;
                commStationEntity.ServiceDate = commNodeObj.ServiceDate;
                commStationEntity.ReasonForServicing = commNodeObj.ReasonForServicing;
                commStationEntity.PIBManufacturer = Pib;
                commStationEntity.Address = commNodeObj.Address;
                commStationEntity.LocationNumber = commNodeObj.LocationNumber;
                commStationEntity.Description = commNodeObj.Description;
                commStationEntity.SerialNumberOfMainStation = serialNum;

                session.SaveOrUpdate(commStationEntity);
                session.Flush();
                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion Communication Node Method(s)
    }
}
