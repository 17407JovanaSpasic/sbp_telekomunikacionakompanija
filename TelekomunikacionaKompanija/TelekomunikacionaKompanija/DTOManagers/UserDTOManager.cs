﻿using System;
using System.Collections.Generic;
using System.Linq;

using NHibernate;

using TelekomunikacionaKompanija.DTOs.Contract;
using TelekomunikacionaKompanija.DTOs.Contract.Service;
using TelekomunikacionaKompanija.DTOs.Package;
using TelekomunikacionaKompanija.DTOs.User;
using TelekomunikacionaKompanija.Entities;
using TelekomunikacionaKompanija.DTOs.Device;

namespace TelekomunikacionaKompanija.DTOManagers
{
    public class UserDTOManager
    {
        #region Mutual Method(s)

        public static bool AddContractToTheUser(int userId, string contractId)
        {
            try
            {
                var s = DataLayer.GetSession();

                var contract = s.Get<Entities.Contract>(contractId);
                var user = s.Get<Entities.User>(userId);

                if (contract == null || user == null)
                    return false;

                user.UsersContracts.Add(contract);

                s.SaveOrUpdate(user);
                s.Flush();
                s.Close();
                return true;
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in ContractDTOManager(AddPaymentToTheContract). \n" + ex.Message); return false; }
        }
        public static bool CheckIfExists(string contactPhone)
        {
            try
            {
                var session = DataLayer.GetSession();

                IQuery que = session.CreateQuery("select u from User u where u.ContactPhone = " + "'" + contactPhone + "'");
                var user = que.UniqueResult<User>();

                if (user == null)
                    return false;
            }
            catch(Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(CheckIfExists). \n" + ex.Message); return false; }

            return true;
        }
        public static string GetCommunicationNodeID()
        {
            string nodeSerialNumber;
            try
            {
                List<CommunicationNodeDTO> listOfNodes = new List<CommunicationNodeDTO>();
                listOfNodes = DeviceDTOManager.GetAllCommNodes();

                Random randNum = new Random();
                int index = randNum.Next(0, listOfNodes.Count - 1);

                CommunicationNodeDTO node = listOfNodes.ElementAt(index);
                nodeSerialNumber = node.SerialNumber;
                return nodeSerialNumber;
            }
            catch(Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(GetCommunicationNodeID). \n" + ex.Message); return null; }
            
        }
        public static void AddContract(int id, Entities.Contract con)
        {
            try
            {
                var s = DataLayer.GetSession();

                var user = s.Get<User>(id);
                user.UsersContracts.Add(con);

                s.SaveOrUpdate(user);
                s.Flush();
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(AddContract). \n" + ex.Message); }
        }

        public static bool DeleteUser(int userId)
        {
            try
            {
                var session = DataLayer.GetSession();
                var user = session.Get<User>(userId);
                session.Delete(user);
                session.Flush();
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(DeleteUser). \n" + ex.Message); return false; }
            return true;
        }

        public static bool UpdateUser(UserDTO obj)
        {
            try
            {
                var session = DataLayer.GetSession();
                var user = session.Get<Entities.User>(obj.Id);

                user.Name = obj.Name;
                user.LastName = obj.LastName;
                user.Address = obj.Address;
                user.AddressNumber = obj.AddressNumber;
                user.City = obj.City;
                user.ContactPhone = obj.ContactPhone;
                user.Email = obj.Email;

                if (obj.Individual == 1)
                {
                    user.Individual = 1;
                    user.IndividualJMBG = (obj as IndividualDTO).IndividualJMBG;
                    user.LegalEntity = 0;
                }
                else
                {
                    user.Individual = 0;
                    user.LegalEntity = 1;
                    user.LegalEntityFaxNumber = (obj as LegalEntityDTO).LegalEntityFaxNumber;
                    user.LegalEntityPIB = (obj as LegalEntityDTO).LegalEntityPIB;
                }
                session.SaveOrUpdate(user);
                session.Flush();
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(UpdateUsers). \n" + ex.Message); return false; }
            return true;
        }

        public static void GetUserByContactPhoneForPayment(string phoneNumber, string packageName, ref UserDTO obj, ref ContractDTO contractObj)
        {
            try
            {
                var session = DataLayer.GetSession();

                IQuery que = session.CreateQuery("select u from User u where u.ContactPhone = " + "'" + phoneNumber + "'");

                var user = que.UniqueResult<User>();

                obj.Id = user.Id; 
                obj.Name = user.Name;
                obj.LastName = user.LastName;
                obj.Address = user.Address;
                obj.AddressNumber = user.AddressNumber;
                obj.City = user.City;
                obj.ContactPhone = user.ContactPhone;
                obj.Email = user.Email;

                foreach (var p in user.UsersContracts)
                {
                    if (p.PackageName.Name == packageName || packageName == "")
                    {
                        var package = new PackageDTO(p.PackageName.Name, p.PackageName.Price, p.PackageName.AdditionalPackagePrice,
                                                     p.PackageName.InternetSpeed, p.PackageName.PricePerMB, p.PackageName.PricePerAddress,
                                                     p.PackageName.NumberOfMinutes);

                        var service = new ServiceDTO(p.ServiceId.ServiceID);

                        contractObj = new ContractDTO(p.ContractId, p.ContractStartDate, p.ContractEndDate, obj, package, service);
                    }
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(GetInformationUsersByContactPhoneForPayment). \n" + ex.Message); }
        }

        public static void GetUserByContactPhoneForContract(string phoneNumber, ref int userId, ref UserDTO obj)
        {
            try
            {
                var session = DataLayer.GetSession();

                IQuery que = session.CreateQuery("select u from User u where u.ContactPhone = " + "'" + phoneNumber + "'");

                var user = que.UniqueResult<User>();

                obj.Id = user.Id;
                userId = user.Id;
                obj.Name = user.Name;
                obj.LastName = user.LastName;
                obj.Address = user.Address;
                obj.AddressNumber = user.AddressNumber;
                obj.City = user.City;
                obj.ContactPhone = user.ContactPhone;
                obj.Email = user.Email;

                ContractDTO contractObj;
                foreach (var p in user.UsersContracts)
                {
                    var package = new PackageDTO(p.PackageName.Name, p.PackageName.Price, p.PackageName.AdditionalPackagePrice,
                                                 p.PackageName.InternetSpeed, p.PackageName.PricePerMB, p.PackageName.PricePerAddress,
                                                 p.PackageName.NumberOfMinutes);

                    var service = new ServiceDTO(p.ServiceId.ServiceID);

                    contractObj = new ContractDTO(p.ContractId, p.ContractStartDate, p.ContractEndDate, obj, package, service);

                    obj.UsersContracts.Add(new ContractDTO(p.ContractId, p.ContractStartDate, p.ContractEndDate, obj, package, service));
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(GetInformationUsersByContactPhoneForPayment). \n" + ex.Message); }
        }

        public static UserDTO GetUserByID(int userID)
        {
            var user = new UserDTO();

            try
            {
                var session = DataLayer.GetSession();

                var userEntity = session.Query<User>().Where(x => x.Id == userID).FirstOrDefault();

                if (userEntity.Individual == 1)
                {
                    user = new IndividualDTO(userEntity.Id, userEntity.Name, userEntity.LastName,
                        userEntity.Address, userEntity.AddressNumber, userEntity.City,
                        userEntity.ContactPhone, userEntity.Email, userEntity.IndividualJMBG);
                }
                else
                {
                    user = new LegalEntityDTO(userEntity.Id, userEntity.Name, userEntity.LastName,
                        userEntity.Address, userEntity.AddressNumber, userEntity.City,
                        userEntity.ContactPhone, userEntity.Email, userEntity.LegalEntityFaxNumber,
                        userEntity.LegalEntityPIB);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return user;
        }
        #endregion Mutual Method(s)

        #region Individual Method(s)
        public static List<IndividualDTO> GetAllIndividualUsers()
        {
            List<IndividualDTO> listOfUsers = new List<IndividualDTO>();
            try
            {
                ISession s = DataLayer.GetSession();

                var users = (from u in s.Query<User>()
                             where (u.Individual == 1)
                             select u);

                foreach (var u in users)
                {
                    listOfUsers.Add(new IndividualDTO(u.Id, u.Name, u.LastName, u.Address, u.AddressNumber,
                        u.City, u.ContactPhone, u.Email, u.IndividualJMBG));
                }
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(GetAllIndividualUsers). \n" + ex.Message); }

            return listOfUsers;
        }

        public static bool CreateIndividualUser(IndividualDTO obj)
        {
            try
            {
                var s = DataLayer.GetSession();

                var IndividualEntity = new User();
                IndividualEntity.Name = obj.Name;
                IndividualEntity.LastName = obj.LastName;
                IndividualEntity.Address = obj.Address;
                IndividualEntity.AddressNumber = obj.AddressNumber;
                IndividualEntity.City = obj.City;
                IndividualEntity.ContactPhone = obj.ContactPhone;
                IndividualEntity.Email = obj.Email;

                IndividualEntity.Individual = 1;
                IndividualEntity.IndividualJMBG = obj.IndividualJMBG;

                string nodeSerialNumber = UserDTOManager.GetCommunicationNodeID();

                var ComChanel = s.Get<CommunicationNode>(nodeSerialNumber);
                IndividualEntity.NodeSerialNumber = ComChanel;

                s.SaveOrUpdate(IndividualEntity);
                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error has occured in UserDTOManager(CreateIndividualUser). \n" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion Individual Method(s)

        #region Legal Entity Method(s)
        public static List<LegalEntityDTO> GetAllLegalEntityUsers()
        {
            List<LegalEntityDTO> listOfUsers = new List<LegalEntityDTO>();
            try
            {
                ISession s = DataLayer.GetSession();

                var users = (from u in s.Query<User>()
                             where (u.Individual == 0)
                             select u);

                foreach (var u in users)
                {
                    listOfUsers.Add(new LegalEntityDTO(u.Id, u.Name, u.LastName, u.Address, u.AddressNumber,
                        u.City, u.ContactPhone, u.Email, u.LegalEntityFaxNumber, u.LegalEntityPIB));
                }
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(GetAllLegalEntityUsers). \n" + ex.Message); }

            return listOfUsers;
        }

        public static bool CreateLegalEntityUser(LegalEntityDTO obj)
        {
            try
            {
                var s = DataLayer.GetSession();

                var legalEntity = new User();
                legalEntity.Name = obj.Name;
                legalEntity.LastName = obj.LastName;
                legalEntity.Address = obj.Address;
                legalEntity.AddressNumber = obj.AddressNumber;
                legalEntity.City = obj.City;
                legalEntity.ContactPhone = obj.ContactPhone;
                legalEntity.Email = obj.Email;

                legalEntity.LegalEntity = 1;
                legalEntity.LegalEntityFaxNumber = obj.LegalEntityFaxNumber;
                legalEntity.LegalEntityPIB = obj.LegalEntityPIB;

                string nodeSerialNumber = UserDTOManager.GetCommunicationNodeID();
                var ComChanel = s.Get<CommunicationNode>(nodeSerialNumber);

                legalEntity.NodeSerialNumber = ComChanel;

                s.SaveOrUpdate(legalEntity);
                s.Flush();
                s.Close();
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error has occured in UserDTOManager(CreateLegalEntityUser). \n" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion Legal Entity Method(s)
    }
}
