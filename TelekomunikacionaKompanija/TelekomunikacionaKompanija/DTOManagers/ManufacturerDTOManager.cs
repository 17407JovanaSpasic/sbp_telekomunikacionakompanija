﻿using System;
using System.Collections.Generic;
using System.Linq;

using TelekomunikacionaKompanija.DTOs.Manufacturer;
using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.DTOManagers
{
    public class ManufacturerDTOManager
    {
        #region Manufacturer Method(s)
        public static List<ManufacturerDTO> GetAllManufactures()
        {
            var manufacturerList = new List<ManufacturerDTO>();
            try
            {
                var session = DataLayer.GetSession();
                var manufacturerEntities = (from manufacturer in session.Query<Manufacturer>() select manufacturer);

                foreach (var manufacturer in manufacturerEntities)
                {
                    manufacturerList.Add(new ManufacturerDTO(manufacturer.PIB, manufacturer.Name));
                }
                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return manufacturerList;
        }

        public static ManufacturerDTO GetManufacturerByPIB(string pib)
        {
            var manufacturer = new ManufacturerDTO();

            try
            {
                var session = DataLayer.GetSession();
                var manufacturerEntity = session.Query<Manufacturer>().Where(x => x.PIB == pib)
                    .FirstOrDefault();

                manufacturer.PIB = manufacturerEntity.PIB;
                manufacturer.Name = manufacturerEntity.Name;

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return manufacturer;
        }

        public static bool CreateManufacturer(ManufacturerDTO manufacturerDTO)
        {
            try
            {
                var session = DataLayer.GetSession();
                var manufacturerEntity = new Manufacturer
                {
                    PIB = manufacturerDTO.PIB,
                    Name = manufacturerDTO.Name
                };

                session.SaveOrUpdate(manufacturerEntity);
                session.Flush();
                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        public static bool UpdateManufacturer(ManufacturerDTO manufacturer)
        {
            try
            {
                var session = DataLayer.GetSession();

                var manufacturerEntity = new Manufacturer
                {
                    PIB = manufacturer.PIB,
                    Name = manufacturer.Name
                };

                session.SaveOrUpdate(manufacturerEntity);
                session.Flush();
                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }

        public static bool DeleteManufacturer(string pib)
        {
            try
            {
                var session = DataLayer.GetSession();

                var manufacturerEntity = session.Get<Manufacturer>(pib);

                session.Delete(manufacturerEntity);
                session.Flush();
                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion Manufacturer Method(s)
    }
}
