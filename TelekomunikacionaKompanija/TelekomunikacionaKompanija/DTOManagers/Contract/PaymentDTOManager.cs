﻿using System;
using System.Collections.Generic;
using System.Linq;

using NHibernate;

using TelekomunikacionaKompanija.DTOs.Contract.Payment;
using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.DTOManagers.Contract
{
    public class PaymentDTOManager
    {
        #region Payment Method(s)
        public static List<PaymentDTO> GetAllPayments()
        {
            List<PaymentDTO> listOfAllpayments = new List<PaymentDTO>();
            try
            {
                var session = DataLayer.GetSession();
                var payments = (from p in session.Query<Payment>() select p);

                foreach (var p in payments)
                {
                    IQuery q = session.CreateQuery("select u.Name, u.LastName, u.ContactPhone from User u " +
                                                    "where u.Id like " + p.ContractNumber.UserId.Id);
                    IList<object[]> results = q.List<object[]>();

                    foreach (object[] o in results)
                    {
                        string ime = (string)o[0];
                        string prezime = (string)o[1];
                        string kontaktTelefon = (string)o[2];

                        string brojUgovora = p.ContractNumber.ContractId;
                        string nazivPakte = p.ContractNumber.PackageName.Name;
                        listOfAllpayments.Add(new PaymentDTO(p.PaymentNumber, p.PaymentDate, p.AdditionalPackagePrice, p.TotalCost,
                        ime, prezime, kontaktTelefon, brojUgovora, nazivPakte));
                    }
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PayementDTOManger(GetAllPayments). \n" + ex.Message); }
            return listOfAllpayments;
        }

        public static bool CreatePayment(FlatRateInternetDTO flateRateIObj, RealizedInternetFlowDTO realizedInternetFlowObj)
        {
            string idContract;
            int idPayment;
            try
            {
                var s = DataLayer.GetSession();
                
                
                if (flateRateIObj != null)
                {
                    var contract = s.Get<Entities.Contract>(flateRateIObj.ContractNumber.ContractId);
                    var newFlateRateObj = new FlatRateInternet();
                    newFlateRateObj.PaymentDate = flateRateIObj.PaymentDate;
                    newFlateRateObj.AdditionalPackagePrice = flateRateIObj.AdditionalPackagePrice;
                    newFlateRateObj.TotalCost = flateRateIObj.TotalCost;
                    newFlateRateObj.ContractNumber = contract;
                    newFlateRateObj.Cost = flateRateIObj.Cost;
                    
                    s.SaveOrUpdate(newFlateRateObj);
                    s.Flush();
                    s.Close();

                    idContract = newFlateRateObj.ContractNumber.ContractId;
                    idPayment = newFlateRateObj.PaymentNumber;

                    ContractDTOManager.AddPaymentToTheContract(idContract, idPayment);

                }
                else
                {
                    var contract = s.Get<Entities.Contract>(realizedInternetFlowObj.ContractNumber.ContractId);
                    var newRealizedInternetFlowObj = new RealizedInternetFlow();
                    newRealizedInternetFlowObj.PaymentDate = realizedInternetFlowObj.PaymentDate;
                    newRealizedInternetFlowObj.AdditionalPackagePrice = realizedInternetFlowObj.AdditionalPackagePrice;
                    newRealizedInternetFlowObj.TotalCost = realizedInternetFlowObj.TotalCost;
                    newRealizedInternetFlowObj.ContractNumber = contract;
                    newRealizedInternetFlowObj.FlowAmount = realizedInternetFlowObj.FlowAmount;
                    newRealizedInternetFlowObj.CostPerFlow = realizedInternetFlowObj.CostPerFlow;

                    s.SaveOrUpdate(newRealizedInternetFlowObj);
                    s.Flush();
                    s.Close();

                    idContract = newRealizedInternetFlowObj.ContractNumber.ContractId;
                    idPayment = newRealizedInternetFlowObj.PaymentNumber;

                    ContractDTOManager.AddPaymentToTheContract(idContract, idPayment);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error has occured in PaymentDTOManager(CreateOrUpadatePayment). \n" + ex.Message);
                return false;
            }
            return true;
        }

        public static bool DeletePayement(int paymentNumber)
        {
            try
            {
                var session = DataLayer.GetSession();
                var user = session.Get<Payment>(paymentNumber);
                session.Delete(user);
                session.Flush();
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PaymentDTOManager(DeletePayment). \n" + ex.Message); return false; }
            return true;
        }
        #endregion Payment Method(s)

        #region Flat-Rate Internet Method(s)
        public static List<RealizedInternetFlowDTO> GetAllRealizedInternetFlow()
        {
            List<RealizedInternetFlowDTO> realizedInternetFlows = new List<RealizedInternetFlowDTO>();
            try
            {
                var session = DataLayer.GetSession();
                var payements = (from p in session.Query<RealizedInternetFlow>() select p);

                foreach (var p in payements)
                {
                    realizedInternetFlows.Add(new RealizedInternetFlowDTO(p.PaymentNumber, p.PaymentDate, p.AdditionalPackagePrice, p.TotalCost,
                        p.ContractNumber, p.FlowAmount, p.CostPerFlow));
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PayementDTOManger(GetAllRealizedInternetFlow). \n" + ex.Message); }
            return realizedInternetFlows;
        }
        #endregion Flat-Rate Internet Method(s)

        #region Realized Internet Flow Method(s)
        public static List<FlatRateInternetDTO> GetAllFlatRateInternet()
        {
            List<FlatRateInternetDTO> flatRateInternets = new List<FlatRateInternetDTO>();
            try
            {
                var session = DataLayer.GetSession();
                var payements = (from p in session.Query<FlatRateInternet>() select p);

                foreach (var p in payements)
                {
                    flatRateInternets.Add(new FlatRateInternetDTO(p.PaymentNumber, p.PaymentDate, p.AdditionalPackagePrice, p.TotalCost,
                        p.ContractNumber, p.Cost));
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PayementDTOManger(GetAllRealizedInternetFlow). \n" + ex.Message); }
            return flatRateInternets;
        }
        #endregion Realized Internet Flow Method(s)
    }
}
