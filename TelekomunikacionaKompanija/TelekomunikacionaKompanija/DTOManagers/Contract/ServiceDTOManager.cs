﻿using System;
using System.Collections.Generic;
using System.Linq;

using TelekomunikacionaKompanija.DTOs.Contract.Service;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Internet;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Telephony;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Television;
using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.DTOManagers.Contract
{
    public class ServiceDTOManager
    {
        #region Service Method(s)
        public static int CreateService(ServiceDTO service)
        {
            var serviceEntity = new Service
            {
                ContainsTelephony = service.ContainsTelephony,
                ContainsTelevision = service.ContainsTelevision,
                ContainsInternet = service.ContainsInternet
            };

            if (service.ContainsInternet)
            {
                serviceEntity.Address1 = service.Internet.Address1;
                serviceEntity.Address2 = service.Internet.Address2;

                if (service.Internet.InternetType == InternetType.Prepaid)
                {
                    serviceEntity.IsPrepaidInternet = true;
                    serviceEntity.IsPostpaidInternet = false;

                    serviceEntity.PaymentDate = null;
                }
                else
                {
                    serviceEntity.IsPrepaidInternet = false;
                    serviceEntity.IsPostpaidInternet = true;
                }
            }

            try
            {
                var session = DataLayer.GetSession();

                session.Save(serviceEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return -1;
            }

            if (service.ContainsTelephony && service.PhoneNumbers.Count != 0)
            {
                ServiceDTOManager.CreatePhoneNumbers(serviceEntity.ServiceID, service.PhoneNumbers);
            }

            if (service.ContainsTelevision && service.AdditionalChannelPackages.Count != 0)
            {
                ServiceDTOManager.CreateAdditionalChannelPackages(serviceEntity.ServiceID,
                    service.AdditionalChannelPackages);
            }

            return serviceEntity.ServiceID;
        }

        public static ServiceDTO GetServiceByID(int serviceID)
        {
            var service = new ServiceDTO(serviceID);

            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Load<Service>(serviceID);

                service.ContainsTelephony = serviceEntity.ContainsTelephony;
                service.ContainsTelevision = serviceEntity.ContainsTelevision;
                service.ContainsInternet = serviceEntity.ContainsInternet;

                foreach (var phoneNumberEntity in serviceEntity.PhoneNumbers)
                {
                    var phoneNumber = new PhoneNumberDTO(phoneNumberEntity.ID.Number,
                        phoneNumberEntity.NumberOfMinutes);

                    service.PhoneNumbers.Add(phoneNumber);
                }

                foreach (var additionalChannelPackageEntity in serviceEntity.AdditionalChannelPackages)
                {
                    var additionalChannelPackage = new AdditionalChannelPackageDTO(
                        additionalChannelPackageEntity.ID.AdditionalChannelPackageName);

                    service.AdditionalChannelPackages.Add(additionalChannelPackage);
                }

                if (serviceEntity.ContainsInternet)
                {
                    if (serviceEntity.IsPrepaidInternet)
                    {
                        service.Internet = new PrepaidInternetDTO(serviceEntity.Address1,
                            serviceEntity.Address2, InternetType.Prepaid, serviceEntity.PaymentDate,
                            serviceEntity.Balance);
                    }
                    else
                    {
                        service.Internet = new PostpaidInternetDTO(serviceEntity.Address1,
                            serviceEntity.Address2, InternetType.Postpaid);
                    }
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return service;
        }

        public static bool UpdateInternetService(int serviceID, InternetDTO internet)
        {
            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Load<Service>(serviceID);

                serviceEntity.Address1 = internet.Address1;
                serviceEntity.Address2 = internet.Address2;

                if (internet.InternetType == InternetType.Prepaid)
                {
                    var prepaidInternet = internet as PrepaidInternetDTO;

                    serviceEntity.PaymentDate = prepaidInternet.PaymentDate;
                    serviceEntity.Balance = prepaidInternet.Balance;
                }

                session.Update(serviceEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }
        #endregion Service Method(s)

        #region Telephony Service Method(s)
        private static void CreatePhoneNumbers(int serviceID, IList<PhoneNumberDTO> phoneNumbers)
        {
            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Load<Service>(serviceID);

                foreach (var phoneNumber in phoneNumbers)
                {
                    var phoneNumberEntity = new PhoneNumber
                    {
                        ID = new PhoneNumberID
                        {
                            Service = serviceEntity,
                            Number = phoneNumber.PhoneNumber
                        },
                        NumberOfMinutes = phoneNumber.NumberOfMinutes
                    };

                    session.Save(phoneNumberEntity);
                }

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static bool CreatePhoneNumber(int serviceID, PhoneNumberDTO phoneNumber)
        {
            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Load<Service>(serviceID);

                var phoneNumberEntity = new PhoneNumber
                {
                    ID = new PhoneNumberID
                    {
                        Service = serviceEntity,
                        Number = phoneNumber.PhoneNumber
                    },
                    NumberOfMinutes = phoneNumber.NumberOfMinutes
                };

                session.Save(phoneNumberEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }

        public static bool DeletePhoneNumber(int serviceID, string phoneNumber)
        {
            try
            {
                var session = DataLayer.GetSession();

                var phoneNumberEntity = session.Query<PhoneNumber>()
                    .Where(x => x.ID.Service.ServiceID == serviceID && x.ID.Number == phoneNumber)
                    .FirstOrDefault();

                if (phoneNumberEntity != null)
                {
                    session.Delete(phoneNumberEntity);

                    session.Flush();

                    session.Close();
                }
                else
                {
                    session.Close();

                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }

        public static bool UpdateNumberOfMinutes(int serviceID, PhoneNumberDTO phoneNumber)
        {
            try
            {
                var session = DataLayer.GetSession();

                var phoneNumberEntity = session.Query<PhoneNumber>()
                    .Where(x => x.ID.Service.ServiceID == serviceID && x.ID.Number == phoneNumber.PhoneNumber)
                    .FirstOrDefault();

                if (phoneNumberEntity != null)
                {
                    phoneNumberEntity.NumberOfMinutes = phoneNumber.NumberOfMinutes;

                    session.Update(phoneNumberEntity);

                    session.Flush();

                    session.Close();
                }
                else
                {
                    session.Close();

                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }
        #endregion Telephony Service Method(s)

        #region Television Service Method(s)
        public static void CreateAdditionalChannelPackages(int serviceID,
            IList<AdditionalChannelPackageDTO> additionalChannelPackages)
        {
            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Load<Service>(serviceID);

                foreach (var additionalChannelPackage in additionalChannelPackages)
                {
                    var additionalChannelPackageEntity = new AdditionalChannelPackage
                    {
                        ID = new AdditionalChannelPackageID
                        {
                            Service = serviceEntity,
                            AdditionalChannelPackageName = additionalChannelPackage.Name
                        }
                    };

                    session.Save(additionalChannelPackageEntity);
                }

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void DeleteAdditionalChannelPackages(int serviceID,
            IList<AdditionalChannelPackageDTO> additionalChannelPackages)
        {
            try
            {
                var session = DataLayer.GetSession();

                foreach (var additionalChannelPackage in additionalChannelPackages)
                {
                    var additionalChannelPackageEntity = session.Query<AdditionalChannelPackage>()
                        .Where(x => x.ID.Service.ServiceID == serviceID &&
                            x.ID.AdditionalChannelPackageName == additionalChannelPackage.Name)
                        .FirstOrDefault();

                    if (additionalChannelPackageEntity != null)
                    {
                        session.Delete(additionalChannelPackageEntity);
                    }
                }

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion Television Service Method(s)
    }
}
