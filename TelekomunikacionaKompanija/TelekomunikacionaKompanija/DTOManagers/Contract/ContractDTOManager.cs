﻿using NHibernate;

using System;
using System.Collections.Generic;
using System.Linq;

using TelekomunikacionaKompanija.DTOs.Contract;
using TelekomunikacionaKompanija.DTOs.Contract.Service;
using TelekomunikacionaKompanija.DTOs.Package;
using TelekomunikacionaKompanija.DTOs.User;
using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.DTOManagers.Contract
{
    public class ContractDTOManager
    {
        #region Contract Method(s)
        public static void AddPaymentToTheContract(string contractId, int idPayment)
        {
            try
            {
                var s = DataLayer.GetSession();

                var contract = s.Get<Entities.Contract>(contractId);
                var payment = s.Get<Entities.Payment>(idPayment);

                if (contract == null || payment == null)
                    return;

                contract.ServicePayment.Add(payment);

                s.SaveOrUpdate(contract);
                s.Flush();
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in ContractDTOManager(AddPaymentToTheContract). \n" + ex.Message); }
        }

        public static bool CreateContract(ContractDTO con, int serviceId)
        {
            try
            {
                var s = DataLayer.GetSession();

                var newContract = new Entities.Contract();
                newContract.ContractStartDate = con.ContractStartDate;
                newContract.ContractEndDate = con.ContractEndDate;
                var newUser = s.Get<Entities.User>(con.UserId.Id);
                var package = s.Get<Entities.Package>(con.PackageName.Name);
                var service = s.Get<Entities.Service>(serviceId);

                newContract.UserId = newUser;
                newContract.PackageName = package;
                newContract.ServiceId = service;

                s.SaveOrUpdate(newContract);
                s.Flush();
                s.Close();
                string id = newContract.ContractId;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error has occured in ContractDTOManager(CreateContract). \n" + ex.Message);
                return false;
            }
        }

        public static Entities.Contract GetContractById(string id)
        {
            try
            {
                var s = DataLayer.GetSession();
                var contract = s.Get<Entities.Contract>(id);
                s.Close();
                return contract;
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public static void GetInformationUsersByContactPhoneForPayment(string phoneNumber, string packageName, ref UserDTO obj, ref ContractDTO contractObj)
        {
            try
            {
                var session = DataLayer.GetSession();

                IQuery que = session.CreateQuery("select u from User u where u.ContactPhone = " + "'" + phoneNumber + "'");

                var user = que.UniqueResult<User>();

                obj.Id = user.Id;
                obj.Name = user.Name;
                obj.LastName = user.LastName;
                obj.Address = user.Address;
                obj.AddressNumber = user.AddressNumber;
                obj.City = user.City;
                obj.ContactPhone = user.ContactPhone;
                obj.Email = user.Email;

                foreach (var c in user.UsersContracts)
                {
                    if (c.PackageName.Name == packageName)
                    {
                        contractObj.ContractId = c.ContractId;
                        contractObj.ContractStartDate = c.ContractStartDate;
                        contractObj.ContractEndDate = c.ContractEndDate;
                        var package = new PackageDTO(c.PackageName.Name, c.PackageName.Price, c.PackageName.AdditionalPackagePrice, c.PackageName.InternetSpeed, c.PackageName.PricePerMB, c.PackageName.PricePerAddress, c.PackageName.NumberOfMinutes);
                        contractObj.PackageName = package;
                        contractObj.UserId = obj;

                        var serviceDto = new ServiceDTO(c.ServiceId.ServiceID);
                        contractObj.ServiceId = serviceDto;
                        break;
                    }
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(GetInformationUsersByContactPhoneForPayment). \n" + ex.Message); }

        }

        public static List<ContractByUserDTO> GetContractsByUsersPhoneNumber(string phoneNumber)
        {
            var contracts = new List<ContractByUserDTO>();

            try
            {
                var session = DataLayer.GetSession();

                var contractEntities = session.Query<Entities.Contract>()
                    .Where(x => x.UserId.ContactPhone == phoneNumber).ToList();

                foreach (var contractEntity in contractEntities)
                {
                    var contract = new ContractByUserDTO(contractEntity.ContractId,
                        contractEntity.ContractStartDate, contractEntity.ContractEndDate,
                        contractEntity.PackageName.Name, contractEntity.ServiceId.ServiceID);

                    contracts.Add(contract);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return contracts;
        }
        #endregion Contract Method(s)
    }
}
