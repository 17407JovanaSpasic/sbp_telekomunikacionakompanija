﻿using System;
using System.Collections.Generic;
using System.Linq;

using TelekomunikacionaKompanija.DTOs.Package;
using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.DTOManagers
{
    public class PackageDTOManager
    {
        #region Package Method(s)
        public static List<PackageDTO> GetAllPackages()
        {
            var packagesList = new List<PackageDTO>();

            try
            {
                var session = DataLayer.GetSession();

                var packageEntities = (from package in session.Query<Package>()
                                       select package);

                foreach (var package in packageEntities)
                {
                    packagesList.Add(new PackageDTO(package.Name, package.Price,
                        package.AdditionalPackagePrice, package.InternetSpeed, package.PricePerMB,
                        package.PricePerAddress, package.NumberOfMinutes));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return packagesList;
        }

        public static bool CreateOrUpdatePackage(PackageDTO package, List<ChannelDTO> channels,
            List<AdditionalPackageDTO> additionalPackages)
        {
            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = new Package
                {
                    Name = package.Name,
                    Price = package.Price,
                    AdditionalPackagePrice = package.AdditionalPackagePrice,
                    InternetSpeed = package.InternetSpeed,
                    PricePerMB = package.PricePerMB,
                    PricePerAddress = package.PricePerAddress,
                    NumberOfMinutes = package.NumberOfMinutes
                };

                session.SaveOrUpdate(packageEntity);

                foreach (var channel in channels)
                {
                    var channelEntity = new Channel
                    {
                        ID = new ChannelID
                        {
                            Package = packageEntity,
                            ChannelName = channel.Name
                        }
                    };

                    session.SaveOrUpdate(channelEntity);
                }

                foreach (var additionalPackage in additionalPackages)
                {
                    var additionalPackageEntity = new AdditionalPackage
                    {
                        ID = new AdditionalPackageID
                        {
                            Package = packageEntity,
                            AdditionalPackageName = additionalPackage.Name
                        }
                    };

                    session.SaveOrUpdate(additionalPackageEntity);
                }

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }

        public static PackageDTO GetPackageByName(string packageName)
        {
            var package = new PackageDTO();

            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = session.Load<Package>(packageName);

                package.Name = packageEntity.Name;
                package.Price = packageEntity.Price;
                package.AdditionalPackagePrice = packageEntity.AdditionalPackagePrice;
                package.InternetSpeed = packageEntity.InternetSpeed;
                package.PricePerMB = packageEntity.PricePerMB;
                package.PricePerAddress = packageEntity.PricePerAddress;
                package.NumberOfMinutes = packageEntity.NumberOfMinutes;

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return package;
        }

        public static bool DeletePackage(string packageName)
        {
            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = session.Load<Package>(packageName);

                session.Delete(packageEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }

        public static bool CheckIfPackageContainsTelevision(string packageName)
        {
            try
            {
                var session = DataLayer.GetSession();

                var channelEntity = session.Query<Channel>().Where(x => x.ID.Package.Name == packageName)
                    .FirstOrDefault();

                session.Close();

                if (channelEntity is null)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }
        #endregion Package Method(s)

        #region Channel Method(s)
        public static List<ChannelDTO> GetChannelsByPackage(string packageName)
        {
            var channelsList = new List<ChannelDTO>();

            try
            {
                var session = DataLayer.GetSession();

                var channelNames = (from channel in session.Query<Channel>()
                                    where channel.ID.Package.Name == packageName
                                    select channel.ID.ChannelName);

                foreach (var name in channelNames)
                {
                    channelsList.Add(new ChannelDTO(name));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return channelsList;
        }

        public static List<ChannelDTO> GetAllChannels()
        {
            var channelsList = new List<ChannelDTO>();

            try
            {
                var session = DataLayer.GetSession();

                var channelNames = (from channel in session.Query<Channel>()
                                    select channel.ID.ChannelName).Distinct();

                foreach (var name in channelNames)
                {
                    channelsList.Add(new ChannelDTO(name));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return channelsList;
        }

        public static bool DeleteSelectedChannelsByPackage(string packageName,
            List<ChannelDTO> channels)
        {
            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = session.Load<Package>(packageName);

                foreach (var channel in channels)
                {
                    var channelID = new ChannelID
                    {
                        Package = packageEntity,
                        ChannelName = channel.Name
                    };

                    var channelEntity = session.Load<Channel>(channelID);

                    session.Delete(channelEntity);
                }

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }
        #endregion Channel Method(s)

        #region Additional Package Method(s)
        public static List<AdditionalPackageDTO> GetAdditionalPackagesByPackage(string packageName)
        {
            var additionalPackagesList = new List<AdditionalPackageDTO>();

            try
            {
                var session = DataLayer.GetSession();

                var additionalPackageNames = (from additionalPackage in session.Query<AdditionalPackage>()
                                              where additionalPackage.ID.Package.Name == packageName
                                              select additionalPackage.ID.AdditionalPackageName);

                foreach (var name in additionalPackageNames)
                {
                    additionalPackagesList.Add(new AdditionalPackageDTO(name));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return additionalPackagesList;
        }

        public static List<AdditionalPackageDTO> GetAllAdditionalPackages()
        {
            var additionalPackagesList = new List<AdditionalPackageDTO>();

            try
            {
                var session = DataLayer.GetSession();

                var additionalPackageNames = (from additionalPackage in session.Query<AdditionalPackage>()
                                              select additionalPackage.ID.AdditionalPackageName).Distinct();

                foreach (var name in additionalPackageNames)
                {
                    additionalPackagesList.Add(new AdditionalPackageDTO(name));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return additionalPackagesList;
        }

        public static bool DeleteSelectedAdditionalPackagesByPackage(string packageName,
            List<AdditionalPackageDTO> additionalPackages)
        {
            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = session.Load<Package>(packageName);

                foreach (var additionalPackage in additionalPackages)
                {
                    var additionalPackageID = new AdditionalPackageID
                    {
                        Package = packageEntity,
                        AdditionalPackageName = additionalPackage.Name
                    };

                    var additionalPackageEntity = session.Load<AdditionalPackage>(additionalPackageID);

                    session.Delete(additionalPackageEntity);
                }

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }
        #endregion Additional Package Method(s)
    }
}
