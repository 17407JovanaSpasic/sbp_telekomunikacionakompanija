﻿using FluentNHibernate.Mapping;

using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.Mappings
{
    public class AdditionalPackageMapping : ClassMap<AdditionalPackage>
    {
        #region Constructor(s)
        public AdditionalPackageMapping()
        {
            this.Table("DODATNI_PAKET");

            this.CompositeId(x => x.ID)
                .KeyReference(x => x.Package, "Naziv_Paketa")
                .KeyProperty(x => x.AdditionalPackageName, "Dodatni_Paket");
        }
        #endregion Constructor(s)
    }
}
