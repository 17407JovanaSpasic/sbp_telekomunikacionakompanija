﻿using FluentNHibernate.Mapping;

using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.Mappings
{
    public class MainStationMapping : SubclassMap<MainStation>
    {
        #region Constructor(s)
        public MainStationMapping()
        {
            Table("GLAVNA_STANICA");

            KeyColumn("Serijski_Broj");

            Map(x => x.NumberOfNodes, "Broj_Cvorova");
            Map(x => x.PRegionalHub, "PRegionalni_Hub");
            Map(x => x.NameOfRegion, "Naziv_Regiona");

            HasMany(x => x.CommunicationNodes).KeyColumn("Serijski_Broj_Glavne_Stanice").Cascade.All().Inverse();
        }
        #endregion Constructor(s)
    }
}
