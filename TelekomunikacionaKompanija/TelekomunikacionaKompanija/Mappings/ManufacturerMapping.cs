﻿using FluentNHibernate.Mapping;

using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.Mappings
{
    public class ManufacturerMapping : ClassMap<Manufacturer>
    {
        #region Constructor(s)
        public ManufacturerMapping()
        {
            Table("PROIZVODJAC");

            Id(x => x.PIB, "PIB").GeneratedBy.Assigned();

            Map(x => x.Name, "Naziv");

            HasMany(x => x.Devices).KeyColumn("PIB_Proizvodjaca").Cascade.All().Inverse();
        }
        #endregion Constructor(s)
    }
}
