﻿using FluentNHibernate.Mapping;

using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.Mappings
{
    public class FlatRateInternetMapping : SubclassMap<FlatRateInternet>
    {
        #region Constructor(s)
        public FlatRateInternetMapping()
        {
            Table("FLAT_RATE_INTERNET");

            KeyColumn("Broj_Racuna");

            Map(x => x.Cost, "Flat_Rate_Cena");
        }
        #endregion Constructor(s)
    }
}
