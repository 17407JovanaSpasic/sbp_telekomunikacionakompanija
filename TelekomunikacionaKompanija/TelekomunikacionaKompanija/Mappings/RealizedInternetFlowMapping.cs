﻿using FluentNHibernate.Mapping;

using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.Mappings
{
    public class RealizedInternetFlowMapping : SubclassMap<RealizedInternetFlow>
    {
        #region Constructor(s)
        public RealizedInternetFlowMapping()
        {
            Table("OSTVARENI_PROTOK_INTERNETA");

            KeyColumn("Broj_Racuna");

            Map(x => x.FlowAmount, "Kolicina");
            Map(x => x.CostPerFlow, "Cena_Po_Protoku");
        }
        #endregion Constructor(s)
    }
}
