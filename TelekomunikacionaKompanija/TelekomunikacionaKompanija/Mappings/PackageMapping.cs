﻿using FluentNHibernate.Mapping;

using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.Mappings
{
    public class PackageMapping : ClassMap<Package>
    {
        #region Constructor(s)
        public PackageMapping()
        {
            this.Table("PAKET");

            this.Id(x => x.Name).Column("Naziv").GeneratedBy.Assigned();

            this.Map(x => x.Price).Column("Cena");
            this.Map(x => x.AdditionalPackagePrice).Column("Cena_Dodatnog_Paketa");
            this.Map(x => x.InternetSpeed).Column("Brzina_Interneta");
            this.Map(x => x.PricePerMB).Column("Cena_Po_MB");
            this.Map(x => x.PricePerAddress).Column("Cena_Po_Adresi");
            this.Map(x => x.NumberOfMinutes).Column("Broj_Minuta");

            this.HasMany(x => x.Channels).KeyColumn("Naziv_Paketa").Inverse().Cascade.All();
            this.HasMany(x => x.AdditionalPackages).KeyColumn("Naziv_Paketa").Inverse().Cascade.All();
            this.HasMany(x => x.Contracts).KeyColumn("Naziv_Paketa").Inverse().Cascade.All();
        }
        #endregion Constructor(s)
    }
}
