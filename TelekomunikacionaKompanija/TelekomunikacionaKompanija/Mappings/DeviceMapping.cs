﻿using FluentNHibernate.Mapping;

using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.Mappings
{
    public class DeviceMapping : ClassMap<Device>
    {
        #region Constructor(s)
        public DeviceMapping()
        {
            Table("UREDJAJ");

            Id(x => x.SerialNumber, "Serijski_Broj").GeneratedBy.Assigned();

            Map(x => x.StartOfUseDate, "Datum_Pocetka_Upotrebe");
            Map(x => x.ServiceDate, "Datum_Servisiranja");
            Map(x => x.ReasonForServicing, "Razlog_Servisiranja");

            References(x => x.PIBManufacturer).Column("PIB_Proizvodjaca").LazyLoad();
        }
        #endregion Constructor(s)
    }
}
