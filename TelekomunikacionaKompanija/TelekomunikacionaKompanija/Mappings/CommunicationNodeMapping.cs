﻿using FluentNHibernate.Mapping;

using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.Mappings
{
    public class CommunicationNodeMapping : SubclassMap<CommunicationNode>
    {
        #region Constructor(s)
        public CommunicationNodeMapping()
        {
            Table("KOMUNIKACIONI_CVOR");

            KeyColumn("Serijski_Broj");

            Map(x => x.Address, "Adresa");
            Map(x => x.LocationNumber, "Broj_Lokacije");
            Map(x => x.Description, "Opis");

            References(x => x.SerialNumberOfMainStation).Column("Serijski_Broj_Glavne_Stanice").LazyLoad();
        }
        #endregion Constructor(s)
    }
}
