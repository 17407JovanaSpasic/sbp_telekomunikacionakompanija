﻿using System;
using System.Windows.Forms;

using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

using NHibernate;

using TelekomunikacionaKompanija.Mappings;

namespace TelekomunikacionaKompanija
{
    public class DataLayer
    {
        #region Properties
        private static ISessionFactory factory = null;
        private static readonly object objectLock = new object();
        #endregion Properties

        #region Method(s)
        public static ISession GetSession()
        {
            if (DataLayer.factory is null)
            {
                lock (DataLayer.objectLock)
                {
                    if (DataLayer.factory is null)
                    {
                        DataLayer.factory = DataLayer.CreateSessionFactory();
                    }
                }
            }

            return DataLayer.factory.OpenSession();
        }

        public static ISessionFactory CreateSessionFactory()
        {
            try
            {
                var config = OracleManagedDataClientConfiguration.Oracle10.ShowSql()
                    .ConnectionString(connStr =>
                    connStr.Is("Data Source=gislab-oracle.elfak.ni.ac.rs:1521/SBP_PDB;User Id=S17406;Password=Dulepo99"));

                return Fluently.Configure().Database(config)
                    .Mappings(mappings => mappings.FluentMappings
                    .AddFromAssemblyOf<UserMapping>()).BuildSessionFactory();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                return null;
            }
        }
        #endregion Method(s)
    }
}
