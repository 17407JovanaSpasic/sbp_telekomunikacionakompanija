﻿using System.Collections.Generic;

namespace TelekomunikacionaKompanija.Entities
{
    public class MainStation : Device
    {
        #region Properties
        public virtual int NumberOfNodes { get; set; }
        public virtual int PRegionalHub { get; set; }
        public virtual string NameOfRegion { get; set; }

        public virtual IList<CommunicationNode> CommunicationNodes { get; set; }
        #endregion Properties

        #region Constructor(s)
        public MainStation()
        {
            CommunicationNodes = new List<CommunicationNode>();
        }
        #endregion Constructor(s)
    }
}
