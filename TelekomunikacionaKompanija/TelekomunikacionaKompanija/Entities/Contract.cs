﻿using System;
using System.Collections.Generic;

namespace TelekomunikacionaKompanija.Entities
{
    public class Contract
    {
        #region Properties
        public virtual string ContractId { get; protected set; }
        public virtual DateTime ContractStartDate { get; set; }
        public virtual DateTime ContractEndDate { get; set; }
        public virtual User UserId { get; set; }
        public virtual Package PackageName { get; set; }
        public virtual Service ServiceId { get; set; }

        public virtual IList<Payment> ServicePayment { get; set; }
        #endregion Properties

        #region Constructor(s)
        public Contract()
        {
            ServicePayment = new List<Payment>();
        }
        #endregion Constructor(s)
    }
}
