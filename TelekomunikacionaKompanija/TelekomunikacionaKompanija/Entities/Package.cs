﻿using System.Collections.Generic;

namespace TelekomunikacionaKompanija.Entities
{
    public class Package
    {
        #region Properties
        public virtual string Name { get; set; }
        public virtual float Price { get; set; }
        public virtual float AdditionalPackagePrice { get; set; }
        public virtual int InternetSpeed { get; set; }
        public virtual float PricePerMB { get; set; }
        public virtual float PricePerAddress { get; set; }
        public virtual int NumberOfMinutes { get; set; }

        public virtual IList<Channel> Channels { get; set; }
        public virtual IList<AdditionalPackage> AdditionalPackages { get; set; }
        public virtual IList<Contract> Contracts { get; set; }
        #endregion Properties

        #region Constructor(s)
        public Package()
        {
            this.Channels = new List<Channel>();
            this.AdditionalPackages = new List<AdditionalPackage>();
            this.Contracts = new List<Contract>();
        }
        #endregion Constructor(s)
    }
}
