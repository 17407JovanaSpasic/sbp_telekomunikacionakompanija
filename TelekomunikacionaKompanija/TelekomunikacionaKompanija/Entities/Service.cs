﻿using System;
using System.Collections.Generic;

namespace TelekomunikacionaKompanija.Entities
{
    public class Service
    {
        #region Properties
        public virtual int ServiceID { get; protected set; }
        public virtual bool ContainsTelephony { get; set; }
        public virtual bool ContainsTelevision { get; set; }
        public virtual bool ContainsInternet { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual bool IsPrepaidInternet { get; set; }
        public virtual DateTime? PaymentDate { get; set; }
        public virtual float Balance { get; set; }
        public virtual bool IsPostpaidInternet { get; set; }

        public virtual IList<PhoneNumber> PhoneNumbers { get; set; }
        public virtual IList<AdditionalChannelPackage> AdditionalChannelPackages { get; set; }

        public virtual Contract Contract { get; set; }
        #endregion Properties

        #region Constructor(s)
        public Service()
        {
            this.PhoneNumbers = new List<PhoneNumber>();
            this.AdditionalChannelPackages = new List<AdditionalChannelPackage>();
        }
        #endregion Constructor(s)
    }
}
