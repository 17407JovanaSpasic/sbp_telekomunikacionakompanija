﻿namespace TelekomunikacionaKompanija.Entities
{
    public class CommunicationNode : Device
    {
        #region Properties
        public virtual string Address { get; set; }
        public virtual int LocationNumber { get; set; }
        public virtual string Description { get; set; }
        public virtual MainStation SerialNumberOfMainStation { get; set; }
        #endregion Properties
    }
}
