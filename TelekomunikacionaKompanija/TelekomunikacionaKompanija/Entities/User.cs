﻿using System.Collections.Generic;

namespace TelekomunikacionaKompanija.Entities
{
    public class User
    {
        #region Properties
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Address { get; set; }
        public virtual int AddressNumber { get; set; }
        public virtual string City { get; set; }
        public virtual string ContactPhone { get; set; }
        public virtual string Email { get; set; }
        public virtual CommunicationNode NodeSerialNumber { get; set; }

        public virtual IList<Contract> UsersContracts { get; set; }

        //Fizicko lice
        public virtual int Individual { get; set; }
        public virtual string IndividualJMBG { get; set; }

        //Pravno lice
        public virtual int LegalEntity { get; set; }
        public virtual string LegalEntityFaxNumber { get; set; }
        public virtual string LegalEntityPIB { get; set; }
        #endregion Properties

        #region Constructor(s)
        public User()
        {
            UsersContracts = new List<Contract>();
        }
        #endregion Constructor(s)
    }
}
