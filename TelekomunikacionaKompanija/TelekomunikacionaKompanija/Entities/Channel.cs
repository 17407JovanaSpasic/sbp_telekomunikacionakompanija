﻿namespace TelekomunikacionaKompanija.Entities
{
    #region Channel
    public class Channel
    {
        #region Properties
        public virtual ChannelID ID { get; set; }
        #endregion Properties

        #region Constructor(s)
        public Channel()
        {
            this.ID = new ChannelID();
        }
        #endregion Constructor(s)
    }
    #endregion Channel

    #region ChannelID
    public class ChannelID
    {
        #region Properties
        public virtual Package Package { get; set; }
        public virtual string ChannelName { get; set; }
        #endregion Properties

        #region Method(s)
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != typeof(ChannelID))
            {
                return false;
            }

            var channelIDObj = obj as ChannelID;

            return (this.Package.Name == channelIDObj.Package.Name) &&
                (this.ChannelName == channelIDObj.ChannelName);
        }

        public override int GetHashCode() => base.GetHashCode();
        #endregion Method(s)
    }
    #endregion ChannelID
}
