﻿namespace TelekomunikacionaKompanija.Entities
{
    public class FlatRateInternet : Payment
    {
        #region Properties
        public virtual float Cost { get; set; }
        #endregion Properties
    }
}
