﻿using System.Collections.Generic;

namespace TelekomunikacionaKompanija.Entities
{
    public class Manufacturer
    {
        #region Properties
        public virtual string PIB { get; set; }
        public virtual string Name { get; set; }

        public virtual IList<Device> Devices { get; set; }
        #endregion Properties

        #region Constructor(s)
        public Manufacturer()
        {
            Devices = new List<Device>();
        }
        #endregion Constructor(s)
    }
}
