﻿using System;

namespace TelekomunikacionaKompanija.Entities
{
    public class Device
    {
        #region Properties
        public virtual string SerialNumber { get; set; }
        public virtual DateTime StartOfUseDate { get; set; }
        public virtual DateTime ServiceDate { get; set; }
        public virtual string ReasonForServicing { get; set; }
        public virtual Manufacturer PIBManufacturer { get; set; }
        #endregion Properties
    }
}
