﻿using System;

namespace TelekomunikacionaKompanija.Entities
{
    public class Payment
    {
        #region Properties
        public virtual int PaymentNumber { get; set; }
        public virtual DateTime PaymentDate { get; set; }
        public virtual float AdditionalPackagePrice { get; set; }
        public virtual float TotalCost { get; set; }
        public virtual Contract ContractNumber { get; set; }
        #endregion Properties
    }
}
