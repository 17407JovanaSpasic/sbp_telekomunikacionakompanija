﻿using System;
using System.Windows.Forms;

using TelekomunikacionaKompanija.Forms.Contract;
using TelekomunikacionaKompanija.Forms.Contract.Payment;
using TelekomunikacionaKompanija.Forms.Device;
using TelekomunikacionaKompanija.Forms.Manufacturer;
using TelekomunikacionaKompanija.Forms.Package;
using TelekomunikacionaKompanija.Forms.User;

namespace TelekomunikacionaKompanija.Forms
{
    public partial class frmMain : Form
    {
        #region Constructor(s)
        public frmMain()
        {
            this.InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void btnShowManufacturers_Click(object sender, EventArgs e)
        {
            var manufacturerForm = new frmManufacturer();

            manufacturerForm.Show();
        }

        private void btnShowDevices_Click(object sender, EventArgs e)
        {
            var deviceForm = new frmDevice();

            deviceForm.Show();
        }

        private void btnShowPackages_Click(object sender, EventArgs e)
        {
            var packageForm = new frmPackage();

            packageForm.Show();
        }

        private void btnShowIndividualUsers_Click(object sender, EventArgs e)
        {
            var individualUserForm = new frmIndividual();

            individualUserForm.Show();
        }

        private void btnShowLegalUsers_Click(object sender, EventArgs e)
        {
            var legalUserForm = new frmLegalEntity();

            legalUserForm.Show();
        }

        private void btnShowContracts_Click(object sender, EventArgs e)
        {
            var contractForm = new frmContract();

            contractForm.Show();
        }

        private void btnShowContractsByUser_Click(object sender, EventArgs e)
        {
            var showContractsByUserForm = new frmShowContractsByUser();

            showContractsByUserForm.Show();
        }

        private void btnShowPayments_Click(object sender, EventArgs e)
        {
            var paymentForm = new frmPayment();

            paymentForm.Show();
        }
        #endregion Form Event Method(s)
    }
}
