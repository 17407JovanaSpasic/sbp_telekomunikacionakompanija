﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.Manufacturer;

namespace TelekomunikacionaKompanija.Forms.Manufacturer
{
    public partial class frmManufacturer : Form
    {
        #region Attribute(s)
        private List<ManufacturerDTO> manufacturerList;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmManufacturer()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Method(s)
        private void RefreshList()
        {
            this.lstManufacturers.Items.Clear();

            this.manufacturerList = ManufacturerDTOManager.GetAllManufactures();
            foreach (var i in manufacturerList)
            {
                var item = new ListViewItem(new string[] { i.PIB, i.Name });

                this.lstManufacturers.Items.Add(item);
            }
            this.lstManufacturers.Refresh();
        }
        #endregion Method(s)

        #region Form Event Method(s)
        private void linkMainForm_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void frmManufacturer_Load(object sender, EventArgs e)
        {
            this.RefreshList();

            this.btnUpdateManufacturer.Enabled = false;
            this.btnDeleteManufacturer.Enabled = false;
        }

        private void btnNewManufacturer_Click(object sender, EventArgs e)
        {
            frmNewManufacturer newManufacturerForm = new frmNewManufacturer();
            newManufacturerForm.ShowDialog();

            this.RefreshList();
        }

        private void btnUpdateManufacturer_Click(object sender, EventArgs e)
        {
            string pib = this.lstManufacturers.SelectedItems[0].SubItems[0].Text;

            var UpdateManufacturer = new frmUpdateManufacturer()
            {
                pib = pib
            };
            UpdateManufacturer.ShowDialog();

            this.RefreshList();
        }

        private void btnDeleteManufacturer_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Da li ste sigurni da želite da obrišete proizvođača?",
                "Potvrda brisanja", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                string pib = this.lstManufacturers.SelectedItems[0].SubItems[0].Text;
                if (ManufacturerDTOManager.DeleteManufacturer(pib))
                {
                    MessageBox.Show("Uspešno obrisan proizvođač!", "Obaveštenje",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.RefreshList();
                }
                else
                {
                    MessageBox.Show("Greška prilikom brisanja proizvođača!", "Greška",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                this.btnUpdateManufacturer.Enabled = false;
                this.btnDeleteManufacturer.Enabled = false;
            }
        }

        private void lstManufacturers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lstManufacturers.SelectedItems.Count == 0)
            {
                this.btnUpdateManufacturer.Enabled = false;
                this.btnDeleteManufacturer.Enabled = false;
            }
            else
            {
                this.btnUpdateManufacturer.Enabled = true;
                this.btnDeleteManufacturer.Enabled = true;
            }
        }
        #endregion #region Form Event Method(s)
    }
}
