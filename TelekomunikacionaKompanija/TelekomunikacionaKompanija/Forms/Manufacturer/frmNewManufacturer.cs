﻿using System;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOs.Manufacturer;
namespace TelekomunikacionaKompanija.Forms.Manufacturer
{
    public partial class frmNewManufacturer : Form
    {
        #region Constructor(s)
        public frmNewManufacturer()
        {
            InitializeComponent();
        }
        #endregion

        #region Form Event Method(s)
        private void btnKreiraj_Click(object sender, EventArgs e)
        {
            if (this.txtPib.Text == string.Empty || this.txtName.Text == string.Empty)
            {
                MessageBox.Show("Sva polja moraju biti popunjena!", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string pib = this.txtPib.Text;
            string name = this.txtName.Text;

            var manufacturer = new ManufacturerDTO(pib, name);

            if (DTOManagers.ManufacturerDTOManager.CreateManufacturer(manufacturer))
            {
                MessageBox.Show("Uspešno kreiran proizvođač!", "Obaveštenje", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                this.Close();
            }
            else
            {
                MessageBox.Show("Kreiranje proizvođača nije uspešno!", "Greška", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        #endregion Form Event Method(s)
    }
}
