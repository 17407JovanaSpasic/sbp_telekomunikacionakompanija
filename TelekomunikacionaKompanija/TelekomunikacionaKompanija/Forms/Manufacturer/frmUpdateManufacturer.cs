﻿using System;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.Manufacturer;

namespace TelekomunikacionaKompanija.Forms.Manufacturer
{
    public partial class frmUpdateManufacturer : Form
    {
        #region Attribute(s)
        public string pib;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmUpdateManufacturer()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var manufacturer = new ManufacturerDTO
            {
                PIB = this.txtPib.Text,
                Name = this.txtName.Text
            };

            if (!ManufacturerDTOManager.UpdateManufacturer(manufacturer))
            {
                MessageBox.Show("Greška prilikom ažuriranja proizvođača", "Greška",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show("Uspešno ste izmenili podatke proizvođača!", "Obaveštenje",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Close();
        }

        private void frmUpdateManufacturer_Load(object sender, EventArgs e)
        {
            this.txtPib.Enabled = false;

            var manufacturer = ManufacturerDTOManager.GetManufacturerByPIB(this.pib);

            if (manufacturer != null)
            {
                this.txtPib.Text = manufacturer.PIB;
                this.txtName.Text = manufacturer.Name;
            }
        }
        #endregion Form Event Method(s)
    }
}
