﻿
namespace TelekomunikacionaKompanija.Forms.Manufacturer
{
    partial class frmManufacturer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpManufacturers = new System.Windows.Forms.GroupBox();
            this.btnDeleteManufacturer = new System.Windows.Forms.Button();
            this.btnUpdateManufacturer = new System.Windows.Forms.Button();
            this.btnNewManufacturer = new System.Windows.Forms.Button();
            this.lstManufacturers = new System.Windows.Forms.ListView();
            this.columnHeaderPIB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderNaziv = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.linkMainForm = new System.Windows.Forms.LinkLabel();
            this.grpManufacturers.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpManufacturers
            // 
            this.grpManufacturers.Controls.Add(this.btnDeleteManufacturer);
            this.grpManufacturers.Controls.Add(this.btnUpdateManufacturer);
            this.grpManufacturers.Controls.Add(this.btnNewManufacturer);
            this.grpManufacturers.Controls.Add(this.lstManufacturers);
            this.grpManufacturers.Location = new System.Drawing.Point(5, 41);
            this.grpManufacturers.Margin = new System.Windows.Forms.Padding(2);
            this.grpManufacturers.Name = "grpManufacturers";
            this.grpManufacturers.Padding = new System.Windows.Forms.Padding(2);
            this.grpManufacturers.Size = new System.Drawing.Size(494, 206);
            this.grpManufacturers.TabIndex = 1;
            this.grpManufacturers.TabStop = false;
            this.grpManufacturers.Text = "Proizvođači";
            // 
            // btnDeleteManufacturer
            // 
            this.btnDeleteManufacturer.Enabled = false;
            this.btnDeleteManufacturer.Location = new System.Drawing.Point(357, 143);
            this.btnDeleteManufacturer.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeleteManufacturer.Name = "btnDeleteManufacturer";
            this.btnDeleteManufacturer.Size = new System.Drawing.Size(118, 28);
            this.btnDeleteManufacturer.TabIndex = 3;
            this.btnDeleteManufacturer.Text = "Obrišite proizvođača";
            this.btnDeleteManufacturer.UseVisualStyleBackColor = true;
            this.btnDeleteManufacturer.Click += new System.EventHandler(this.btnDeleteManufacturer_Click);
            // 
            // btnUpdateManufacturer
            // 
            this.btnUpdateManufacturer.Enabled = false;
            this.btnUpdateManufacturer.Location = new System.Drawing.Point(357, 88);
            this.btnUpdateManufacturer.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdateManufacturer.Name = "btnUpdateManufacturer";
            this.btnUpdateManufacturer.Size = new System.Drawing.Size(118, 36);
            this.btnUpdateManufacturer.TabIndex = 2;
            this.btnUpdateManufacturer.Text = "Ažurirajte proizvođača";
            this.btnUpdateManufacturer.UseVisualStyleBackColor = true;
            this.btnUpdateManufacturer.Click += new System.EventHandler(this.btnUpdateManufacturer_Click);
            // 
            // btnNewManufacturer
            // 
            this.btnNewManufacturer.Location = new System.Drawing.Point(357, 43);
            this.btnNewManufacturer.Margin = new System.Windows.Forms.Padding(2);
            this.btnNewManufacturer.Name = "btnNewManufacturer";
            this.btnNewManufacturer.Size = new System.Drawing.Size(118, 28);
            this.btnNewManufacturer.TabIndex = 1;
            this.btnNewManufacturer.Text = "Novi proizvođač";
            this.btnNewManufacturer.UseVisualStyleBackColor = true;
            this.btnNewManufacturer.Click += new System.EventHandler(this.btnNewManufacturer_Click);
            // 
            // lstManufacturers
            // 
            this.lstManufacturers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderPIB,
            this.columnHeaderNaziv});
            this.lstManufacturers.FullRowSelect = true;
            this.lstManufacturers.GridLines = true;
            this.lstManufacturers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstManufacturers.HideSelection = false;
            this.lstManufacturers.Location = new System.Drawing.Point(9, 26);
            this.lstManufacturers.Margin = new System.Windows.Forms.Padding(2);
            this.lstManufacturers.MultiSelect = false;
            this.lstManufacturers.Name = "lstManufacturers";
            this.lstManufacturers.Size = new System.Drawing.Size(322, 162);
            this.lstManufacturers.TabIndex = 0;
            this.lstManufacturers.UseCompatibleStateImageBehavior = false;
            this.lstManufacturers.View = System.Windows.Forms.View.Details;
            this.lstManufacturers.SelectedIndexChanged += new System.EventHandler(this.lstManufacturers_SelectedIndexChanged);
            // 
            // columnHeaderPIB
            // 
            this.columnHeaderPIB.Text = "PIB";
            this.columnHeaderPIB.Width = 150;
            // 
            // columnHeaderNaziv
            // 
            this.columnHeaderNaziv.Text = "Naziv proizvođača";
            this.columnHeaderNaziv.Width = 150;
            // 
            // linkMainForm
            // 
            this.linkMainForm.AutoSize = true;
            this.linkMainForm.Location = new System.Drawing.Point(11, 9);
            this.linkMainForm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkMainForm.Name = "linkMainForm";
            this.linkMainForm.Size = new System.Drawing.Size(79, 13);
            this.linkMainForm.TabIndex = 0;
            this.linkMainForm.TabStop = true;
            this.linkMainForm.Text = "Početna strana";
            this.linkMainForm.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkMainForm_LinkClicked);
            // 
            // frmManufacturer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 258);
            this.Controls.Add(this.linkMainForm);
            this.Controls.Add(this.grpManufacturers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmManufacturer";
            this.Text = "Prikaz svih proizvođača";
            this.Load += new System.EventHandler(this.frmManufacturer_Load);
            this.grpManufacturers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpManufacturers;
        private System.Windows.Forms.LinkLabel linkMainForm;
        private System.Windows.Forms.ListView lstManufacturers;
        private System.Windows.Forms.ColumnHeader columnHeaderPIB;
        private System.Windows.Forms.ColumnHeader columnHeaderNaziv;
        private System.Windows.Forms.Button btnDeleteManufacturer;
        private System.Windows.Forms.Button btnUpdateManufacturer;
        private System.Windows.Forms.Button btnNewManufacturer;
    }
}