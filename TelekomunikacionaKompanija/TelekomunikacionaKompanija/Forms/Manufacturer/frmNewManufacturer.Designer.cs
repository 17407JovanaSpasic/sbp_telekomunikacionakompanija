﻿
namespace TelekomunikacionaKompanija.Forms.Manufacturer
{
    partial class frmNewManufacturer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpNewManufacturer = new System.Windows.Forms.GroupBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtPib = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblPib = new System.Windows.Forms.Label();
            this.grpNewManufacturer.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpNewManufacturer
            // 
            this.grpNewManufacturer.Controls.Add(this.btnCreate);
            this.grpNewManufacturer.Controls.Add(this.txtName);
            this.grpNewManufacturer.Controls.Add(this.txtPib);
            this.grpNewManufacturer.Controls.Add(this.lblName);
            this.grpNewManufacturer.Controls.Add(this.lblPib);
            this.grpNewManufacturer.Location = new System.Drawing.Point(11, 11);
            this.grpNewManufacturer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpNewManufacturer.Name = "grpNewManufacturer";
            this.grpNewManufacturer.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpNewManufacturer.Size = new System.Drawing.Size(249, 184);
            this.grpNewManufacturer.TabIndex = 0;
            this.grpNewManufacturer.TabStop = false;
            this.grpNewManufacturer.Text = "Proizvođač";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(124, 119);
            this.btnCreate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(76, 37);
            this.btnCreate.TabIndex = 4;
            this.btnCreate.Text = "Kreirajte proizvođača";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnKreiraj_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(110, 77);
            this.txtName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtName.MaxLength = 30;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(108, 20);
            this.txtName.TabIndex = 3;
            // 
            // txtPib
            // 
            this.txtPib.Location = new System.Drawing.Point(110, 38);
            this.txtPib.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPib.MaxLength = 9;
            this.txtPib.Name = "txtPib";
            this.txtPib.Size = new System.Drawing.Size(108, 20);
            this.txtPib.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(22, 80);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(37, 13);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Naziv:";
            // 
            // lblPib
            // 
            this.lblPib.AutoSize = true;
            this.lblPib.Location = new System.Drawing.Point(22, 41);
            this.lblPib.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPib.Name = "lblPib";
            this.lblPib.Size = new System.Drawing.Size(27, 13);
            this.lblPib.TabIndex = 0;
            this.lblPib.Text = "PIB:";
            // 
            // frmNewManufacturer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 206);
            this.Controls.Add(this.grpNewManufacturer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewManufacturer";
            this.Text = "Novi proizvođač";
            this.grpNewManufacturer.ResumeLayout(false);
            this.grpNewManufacturer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpNewManufacturer;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtPib;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblPib;
    }
}