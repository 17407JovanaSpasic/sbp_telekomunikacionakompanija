﻿
namespace TelekomunikacionaKompanija.Forms.Package
{
    partial class frmPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkMainForm = new System.Windows.Forms.LinkLabel();
            this.grpPackage = new System.Windows.Forms.GroupBox();
            this.btnDeletePackage = new System.Windows.Forms.Button();
            this.btnUpdatePackage = new System.Windows.Forms.Button();
            this.cboSearch = new System.Windows.Forms.ComboBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.btnNewPackage = new System.Windows.Forms.Button();
            this.lstPackages = new System.Windows.Forms.ListView();
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderAdditionalPackagePrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderInternetSpeed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPricePerMB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPricePerAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderNumberOfMinutes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpChannels = new System.Windows.Forms.GroupBox();
            this.lstAdditionalPackages = new System.Windows.Forms.ListView();
            this.columnHeaderAdditionalPackageName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstChannels = new System.Windows.Forms.ListView();
            this.columnHeaderChannelName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpPackage.SuspendLayout();
            this.grpChannels.SuspendLayout();
            this.SuspendLayout();
            // 
            // linkMainForm
            // 
            this.linkMainForm.AutoSize = true;
            this.linkMainForm.Location = new System.Drawing.Point(12, 9);
            this.linkMainForm.Name = "linkMainForm";
            this.linkMainForm.Size = new System.Drawing.Size(79, 13);
            this.linkMainForm.TabIndex = 0;
            this.linkMainForm.TabStop = true;
            this.linkMainForm.Text = "Početna strana";
            this.linkMainForm.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkMainForm_LinkClicked);
            // 
            // grpPackage
            // 
            this.grpPackage.Controls.Add(this.btnDeletePackage);
            this.grpPackage.Controls.Add(this.btnUpdatePackage);
            this.grpPackage.Controls.Add(this.cboSearch);
            this.grpPackage.Controls.Add(this.lblSearch);
            this.grpPackage.Controls.Add(this.btnNewPackage);
            this.grpPackage.Controls.Add(this.lstPackages);
            this.grpPackage.Location = new System.Drawing.Point(12, 34);
            this.grpPackage.Name = "grpPackage";
            this.grpPackage.Size = new System.Drawing.Size(860, 220);
            this.grpPackage.TabIndex = 1;
            this.grpPackage.TabStop = false;
            this.grpPackage.Text = "Paketi";
            // 
            // btnDeletePackage
            // 
            this.btnDeletePackage.Enabled = false;
            this.btnDeletePackage.Location = new System.Drawing.Point(764, 144);
            this.btnDeletePackage.Name = "btnDeletePackage";
            this.btnDeletePackage.Size = new System.Drawing.Size(75, 35);
            this.btnDeletePackage.TabIndex = 5;
            this.btnDeletePackage.Text = "Obrišite paket";
            this.btnDeletePackage.UseVisualStyleBackColor = true;
            this.btnDeletePackage.Click += new System.EventHandler(this.btnDeletePackage_Click);
            // 
            // btnUpdatePackage
            // 
            this.btnUpdatePackage.Enabled = false;
            this.btnUpdatePackage.Location = new System.Drawing.Point(764, 103);
            this.btnUpdatePackage.Name = "btnUpdatePackage";
            this.btnUpdatePackage.Size = new System.Drawing.Size(75, 35);
            this.btnUpdatePackage.TabIndex = 4;
            this.btnUpdatePackage.Text = "Ažurirajte paket";
            this.btnUpdatePackage.UseVisualStyleBackColor = true;
            this.btnUpdatePackage.Click += new System.EventHandler(this.btnUpdatePackage_Click);
            // 
            // cboSearch
            // 
            this.cboSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearch.FormattingEnabled = true;
            this.cboSearch.Items.AddRange(new object[] {
            "Svi paketi",
            "Cena <= 4000",
            "Cena > 4000"});
            this.cboSearch.Location = new System.Drawing.Point(106, 22);
            this.cboSearch.Name = "cboSearch";
            this.cboSearch.Size = new System.Drawing.Size(121, 21);
            this.cboSearch.TabIndex = 1;
            this.cboSearch.SelectedIndexChanged += new System.EventHandler(this.cboSearch_SelectedIndexChanged);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(6, 25);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(94, 13);
            this.lblSearch.TabIndex = 0;
            this.lblSearch.Text = "Kriterijum pretrage:";
            // 
            // btnNewPackage
            // 
            this.btnNewPackage.Location = new System.Drawing.Point(764, 74);
            this.btnNewPackage.Name = "btnNewPackage";
            this.btnNewPackage.Size = new System.Drawing.Size(75, 23);
            this.btnNewPackage.TabIndex = 3;
            this.btnNewPackage.Text = "Novi paket";
            this.btnNewPackage.UseVisualStyleBackColor = true;
            this.btnNewPackage.Click += new System.EventHandler(this.btnNewPackage_Click);
            // 
            // lstPackages
            // 
            this.lstPackages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderName,
            this.columnHeaderPrice,
            this.columnHeaderAdditionalPackagePrice,
            this.columnHeaderInternetSpeed,
            this.columnHeaderPricePerMB,
            this.columnHeaderPricePerAddress,
            this.columnHeaderNumberOfMinutes});
            this.lstPackages.FullRowSelect = true;
            this.lstPackages.GridLines = true;
            this.lstPackages.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstPackages.HideSelection = false;
            this.lstPackages.Location = new System.Drawing.Point(6, 49);
            this.lstPackages.MultiSelect = false;
            this.lstPackages.Name = "lstPackages";
            this.lstPackages.Size = new System.Drawing.Size(739, 165);
            this.lstPackages.TabIndex = 2;
            this.lstPackages.UseCompatibleStateImageBehavior = false;
            this.lstPackages.View = System.Windows.Forms.View.Details;
            this.lstPackages.SelectedIndexChanged += new System.EventHandler(this.lstPackages_SelectedIndexChanged);
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Naziv paketa";
            this.columnHeaderName.Width = 100;
            // 
            // columnHeaderPrice
            // 
            this.columnHeaderPrice.Text = "Cena";
            // 
            // columnHeaderAdditionalPackagePrice
            // 
            this.columnHeaderAdditionalPackagePrice.Text = "Cena dodatnog paketa kanala";
            this.columnHeaderAdditionalPackagePrice.Width = 160;
            // 
            // columnHeaderInternetSpeed
            // 
            this.columnHeaderInternetSpeed.Text = "Brzina interneta";
            this.columnHeaderInternetSpeed.Width = 113;
            // 
            // columnHeaderPricePerMB
            // 
            this.columnHeaderPricePerMB.Text = "Cena po MB";
            this.columnHeaderPricePerMB.Width = 96;
            // 
            // columnHeaderPricePerAddress
            // 
            this.columnHeaderPricePerAddress.Text = "Cena po adresi";
            this.columnHeaderPricePerAddress.Width = 116;
            // 
            // columnHeaderNumberOfMinutes
            // 
            this.columnHeaderNumberOfMinutes.Text = "Broj minuta";
            this.columnHeaderNumberOfMinutes.Width = 80;
            // 
            // grpChannels
            // 
            this.grpChannels.Controls.Add(this.lstAdditionalPackages);
            this.grpChannels.Controls.Add(this.lstChannels);
            this.grpChannels.Location = new System.Drawing.Point(12, 260);
            this.grpChannels.Name = "grpChannels";
            this.grpChannels.Size = new System.Drawing.Size(860, 194);
            this.grpChannels.TabIndex = 2;
            this.grpChannels.TabStop = false;
            this.grpChannels.Text = "Kanali";
            // 
            // lstAdditionalPackages
            // 
            this.lstAdditionalPackages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderAdditionalPackageName});
            this.lstAdditionalPackages.FullRowSelect = true;
            this.lstAdditionalPackages.GridLines = true;
            this.lstAdditionalPackages.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstAdditionalPackages.HideSelection = false;
            this.lstAdditionalPackages.Location = new System.Drawing.Point(464, 19);
            this.lstAdditionalPackages.MultiSelect = false;
            this.lstAdditionalPackages.Name = "lstAdditionalPackages";
            this.lstAdditionalPackages.Size = new System.Drawing.Size(390, 169);
            this.lstAdditionalPackages.TabIndex = 1;
            this.lstAdditionalPackages.UseCompatibleStateImageBehavior = false;
            this.lstAdditionalPackages.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderAdditionalPackageName
            // 
            this.columnHeaderAdditionalPackageName.Text = "Naziv dodatnog paketa kanala";
            this.columnHeaderAdditionalPackageName.Width = 170;
            // 
            // lstChannels
            // 
            this.lstChannels.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderChannelName});
            this.lstChannels.FullRowSelect = true;
            this.lstChannels.GridLines = true;
            this.lstChannels.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstChannels.HideSelection = false;
            this.lstChannels.Location = new System.Drawing.Point(6, 19);
            this.lstChannels.MultiSelect = false;
            this.lstChannels.Name = "lstChannels";
            this.lstChannels.Size = new System.Drawing.Size(390, 169);
            this.lstChannels.TabIndex = 0;
            this.lstChannels.UseCompatibleStateImageBehavior = false;
            this.lstChannels.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderChannelName
            // 
            this.columnHeaderChannelName.Text = "Naziv kanala";
            this.columnHeaderChannelName.Width = 120;
            // 
            // frmPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 464);
            this.Controls.Add(this.grpChannels);
            this.Controls.Add(this.grpPackage);
            this.Controls.Add(this.linkMainForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPackage";
            this.Text = "Prikaz svih paketa";
            this.Load += new System.EventHandler(this.frmPackage_Load);
            this.grpPackage.ResumeLayout(false);
            this.grpPackage.PerformLayout();
            this.grpChannels.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkMainForm;
        private System.Windows.Forms.GroupBox grpPackage;
        private System.Windows.Forms.Button btnDeletePackage;
        private System.Windows.Forms.Button btnUpdatePackage;
        private System.Windows.Forms.ComboBox cboSearch;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.Button btnNewPackage;
        private System.Windows.Forms.ListView lstPackages;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.ColumnHeader columnHeaderPrice;
        private System.Windows.Forms.ColumnHeader columnHeaderAdditionalPackagePrice;
        private System.Windows.Forms.ColumnHeader columnHeaderInternetSpeed;
        private System.Windows.Forms.ColumnHeader columnHeaderPricePerMB;
        private System.Windows.Forms.ColumnHeader columnHeaderPricePerAddress;
        private System.Windows.Forms.ColumnHeader columnHeaderNumberOfMinutes;
        private System.Windows.Forms.GroupBox grpChannels;
        private System.Windows.Forms.ListView lstChannels;
        private System.Windows.Forms.ColumnHeader columnHeaderChannelName;
        private System.Windows.Forms.ListView lstAdditionalPackages;
        private System.Windows.Forms.ColumnHeader columnHeaderAdditionalPackageName;
    }
}