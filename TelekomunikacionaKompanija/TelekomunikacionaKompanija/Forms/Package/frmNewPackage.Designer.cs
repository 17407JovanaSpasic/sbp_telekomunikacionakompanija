﻿
namespace TelekomunikacionaKompanija.Forms.Package
{
    partial class frmNewPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblAdditionalPackagePrice = new System.Windows.Forms.Label();
            this.txtAdditionalPackagePrice = new System.Windows.Forms.TextBox();
            this.lblInternetSpeed = new System.Windows.Forms.Label();
            this.txtInternetSpeed = new System.Windows.Forms.TextBox();
            this.lblPricePerMB = new System.Windows.Forms.Label();
            this.txtPricePerMB = new System.Windows.Forms.TextBox();
            this.lblPricePerAddress = new System.Windows.Forms.Label();
            this.txtPricePerAddress = new System.Windows.Forms.TextBox();
            this.lblNumberOfMinutes = new System.Windows.Forms.Label();
            this.txtNumberOfMinutes = new System.Windows.Forms.TextBox();
            this.grpChannels = new System.Windows.Forms.GroupBox();
            this.txtNewAdditionalPackages = new System.Windows.Forms.TextBox();
            this.lblNewAdditionalPackages = new System.Windows.Forms.Label();
            this.txtNewChannels = new System.Windows.Forms.TextBox();
            this.lblNewChannels = new System.Windows.Forms.Label();
            this.lstAdditionalPackages = new System.Windows.Forms.ListView();
            this.columnHeaderAdditionalPackageName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstChannels = new System.Windows.Forms.ListView();
            this.columnHeaderChannelName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnCreateNewPackage = new System.Windows.Forms.Button();
            this.grpChannels.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(256, 15);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(37, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Naziv:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(416, 12);
            this.txtName.MaxLength = 15;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 1;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(256, 41);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(35, 13);
            this.lblPrice.TabIndex = 2;
            this.lblPrice.Text = "Cena:";
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(416, 38);
            this.txtPrice.MaxLength = 7;
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(100, 20);
            this.txtPrice.TabIndex = 3;
            // 
            // lblAdditionalPackagePrice
            // 
            this.lblAdditionalPackagePrice.AutoSize = true;
            this.lblAdditionalPackagePrice.Location = new System.Drawing.Point(256, 67);
            this.lblAdditionalPackagePrice.Name = "lblAdditionalPackagePrice";
            this.lblAdditionalPackagePrice.Size = new System.Drawing.Size(154, 13);
            this.lblAdditionalPackagePrice.TabIndex = 4;
            this.lblAdditionalPackagePrice.Text = "Cena dodatnog paketa kanala:";
            // 
            // txtAdditionalPackagePrice
            // 
            this.txtAdditionalPackagePrice.Location = new System.Drawing.Point(416, 64);
            this.txtAdditionalPackagePrice.MaxLength = 6;
            this.txtAdditionalPackagePrice.Name = "txtAdditionalPackagePrice";
            this.txtAdditionalPackagePrice.Size = new System.Drawing.Size(100, 20);
            this.txtAdditionalPackagePrice.TabIndex = 5;
            // 
            // lblInternetSpeed
            // 
            this.lblInternetSpeed.AutoSize = true;
            this.lblInternetSpeed.Location = new System.Drawing.Point(256, 93);
            this.lblInternetSpeed.Name = "lblInternetSpeed";
            this.lblInternetSpeed.Size = new System.Drawing.Size(83, 13);
            this.lblInternetSpeed.TabIndex = 6;
            this.lblInternetSpeed.Text = "Brzina interneta:";
            // 
            // txtInternetSpeed
            // 
            this.txtInternetSpeed.Location = new System.Drawing.Point(416, 90);
            this.txtInternetSpeed.MaxLength = 3;
            this.txtInternetSpeed.Name = "txtInternetSpeed";
            this.txtInternetSpeed.Size = new System.Drawing.Size(100, 20);
            this.txtInternetSpeed.TabIndex = 7;
            // 
            // lblPricePerMB
            // 
            this.lblPricePerMB.AutoSize = true;
            this.lblPricePerMB.Location = new System.Drawing.Point(256, 119);
            this.lblPricePerMB.Name = "lblPricePerMB";
            this.lblPricePerMB.Size = new System.Drawing.Size(69, 13);
            this.lblPricePerMB.TabIndex = 8;
            this.lblPricePerMB.Text = "Cena po MB:";
            // 
            // txtPricePerMB
            // 
            this.txtPricePerMB.Location = new System.Drawing.Point(416, 116);
            this.txtPricePerMB.MaxLength = 6;
            this.txtPricePerMB.Name = "txtPricePerMB";
            this.txtPricePerMB.Size = new System.Drawing.Size(100, 20);
            this.txtPricePerMB.TabIndex = 9;
            // 
            // lblPricePerAddress
            // 
            this.lblPricePerAddress.AutoSize = true;
            this.lblPricePerAddress.Location = new System.Drawing.Point(256, 145);
            this.lblPricePerAddress.Name = "lblPricePerAddress";
            this.lblPricePerAddress.Size = new System.Drawing.Size(81, 13);
            this.lblPricePerAddress.TabIndex = 10;
            this.lblPricePerAddress.Text = "Cena po adresi:";
            // 
            // txtPricePerAddress
            // 
            this.txtPricePerAddress.Location = new System.Drawing.Point(416, 142);
            this.txtPricePerAddress.MaxLength = 6;
            this.txtPricePerAddress.Name = "txtPricePerAddress";
            this.txtPricePerAddress.Size = new System.Drawing.Size(100, 20);
            this.txtPricePerAddress.TabIndex = 11;
            // 
            // lblNumberOfMinutes
            // 
            this.lblNumberOfMinutes.AutoSize = true;
            this.lblNumberOfMinutes.Location = new System.Drawing.Point(256, 171);
            this.lblNumberOfMinutes.Name = "lblNumberOfMinutes";
            this.lblNumberOfMinutes.Size = new System.Drawing.Size(62, 13);
            this.lblNumberOfMinutes.TabIndex = 12;
            this.lblNumberOfMinutes.Text = "Broj minuta:";
            // 
            // txtNumberOfMinutes
            // 
            this.txtNumberOfMinutes.Location = new System.Drawing.Point(416, 168);
            this.txtNumberOfMinutes.MaxLength = 5;
            this.txtNumberOfMinutes.Name = "txtNumberOfMinutes";
            this.txtNumberOfMinutes.Size = new System.Drawing.Size(100, 20);
            this.txtNumberOfMinutes.TabIndex = 13;
            // 
            // grpChannels
            // 
            this.grpChannels.Controls.Add(this.txtNewAdditionalPackages);
            this.grpChannels.Controls.Add(this.lblNewAdditionalPackages);
            this.grpChannels.Controls.Add(this.txtNewChannels);
            this.grpChannels.Controls.Add(this.lblNewChannels);
            this.grpChannels.Controls.Add(this.lstAdditionalPackages);
            this.grpChannels.Controls.Add(this.lstChannels);
            this.grpChannels.Location = new System.Drawing.Point(12, 194);
            this.grpChannels.Name = "grpChannels";
            this.grpChannels.Size = new System.Drawing.Size(760, 302);
            this.grpChannels.TabIndex = 14;
            this.grpChannels.TabStop = false;
            this.grpChannels.Text = "Kanali";
            // 
            // txtNewAdditionalPackages
            // 
            this.txtNewAdditionalPackages.Location = new System.Drawing.Point(544, 191);
            this.txtNewAdditionalPackages.Multiline = true;
            this.txtNewAdditionalPackages.Name = "txtNewAdditionalPackages";
            this.txtNewAdditionalPackages.Size = new System.Drawing.Size(210, 100);
            this.txtNewAdditionalPackages.TabIndex = 5;
            // 
            // lblNewAdditionalPackages
            // 
            this.lblNewAdditionalPackages.AutoSize = true;
            this.lblNewAdditionalPackages.Location = new System.Drawing.Point(401, 191);
            this.lblNewAdditionalPackages.Name = "lblNewAdditionalPackages";
            this.lblNewAdditionalPackages.Size = new System.Drawing.Size(137, 13);
            this.lblNewAdditionalPackages.TabIndex = 4;
            this.lblNewAdditionalPackages.Text = "Novi dodatni paketi kanala:";
            // 
            // txtNewChannels
            // 
            this.txtNewChannels.Location = new System.Drawing.Point(75, 191);
            this.txtNewChannels.Multiline = true;
            this.txtNewChannels.Name = "txtNewChannels";
            this.txtNewChannels.Size = new System.Drawing.Size(281, 100);
            this.txtNewChannels.TabIndex = 3;
            // 
            // lblNewChannels
            // 
            this.lblNewChannels.AutoSize = true;
            this.lblNewChannels.Location = new System.Drawing.Point(6, 191);
            this.lblNewChannels.Name = "lblNewChannels";
            this.lblNewChannels.Size = new System.Drawing.Size(63, 13);
            this.lblNewChannels.TabIndex = 2;
            this.lblNewChannels.Text = "Novi kanali:";
            // 
            // lstAdditionalPackages
            // 
            this.lstAdditionalPackages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderAdditionalPackageName});
            this.lstAdditionalPackages.FullRowSelect = true;
            this.lstAdditionalPackages.GridLines = true;
            this.lstAdditionalPackages.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstAdditionalPackages.HideSelection = false;
            this.lstAdditionalPackages.Location = new System.Drawing.Point(404, 19);
            this.lstAdditionalPackages.Name = "lstAdditionalPackages";
            this.lstAdditionalPackages.Size = new System.Drawing.Size(350, 160);
            this.lstAdditionalPackages.TabIndex = 1;
            this.lstAdditionalPackages.UseCompatibleStateImageBehavior = false;
            this.lstAdditionalPackages.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderAdditionalPackageName
            // 
            this.columnHeaderAdditionalPackageName.Text = "Naziv dodatnog paketa kanala";
            this.columnHeaderAdditionalPackageName.Width = 170;
            // 
            // lstChannels
            // 
            this.lstChannels.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderChannelName});
            this.lstChannels.FullRowSelect = true;
            this.lstChannels.GridLines = true;
            this.lstChannels.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstChannels.HideSelection = false;
            this.lstChannels.Location = new System.Drawing.Point(6, 19);
            this.lstChannels.Name = "lstChannels";
            this.lstChannels.Size = new System.Drawing.Size(350, 160);
            this.lstChannels.TabIndex = 0;
            this.lstChannels.UseCompatibleStateImageBehavior = false;
            this.lstChannels.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderChannelName
            // 
            this.columnHeaderChannelName.Text = "Naziv kanala";
            this.columnHeaderChannelName.Width = 120;
            // 
            // btnCreateNewPackage
            // 
            this.btnCreateNewPackage.Location = new System.Drawing.Point(322, 502);
            this.btnCreateNewPackage.Name = "btnCreateNewPackage";
            this.btnCreateNewPackage.Size = new System.Drawing.Size(125, 23);
            this.btnCreateNewPackage.TabIndex = 15;
            this.btnCreateNewPackage.Text = "Kreirajte novi paket";
            this.btnCreateNewPackage.UseVisualStyleBackColor = true;
            this.btnCreateNewPackage.Click += new System.EventHandler(this.btnCreateNewPackage_Click);
            // 
            // frmNewPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 535);
            this.Controls.Add(this.btnCreateNewPackage);
            this.Controls.Add(this.grpChannels);
            this.Controls.Add(this.txtNumberOfMinutes);
            this.Controls.Add(this.lblNumberOfMinutes);
            this.Controls.Add(this.txtPricePerAddress);
            this.Controls.Add(this.lblPricePerAddress);
            this.Controls.Add(this.txtPricePerMB);
            this.Controls.Add(this.lblPricePerMB);
            this.Controls.Add(this.txtInternetSpeed);
            this.Controls.Add(this.lblInternetSpeed);
            this.Controls.Add(this.txtAdditionalPackagePrice);
            this.Controls.Add(this.lblAdditionalPackagePrice);
            this.Controls.Add(this.txtPrice);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewPackage";
            this.Text = "Novi paket";
            this.Load += new System.EventHandler(this.frmNewPackage_Load);
            this.grpChannels.ResumeLayout(false);
            this.grpChannels.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblAdditionalPackagePrice;
        private System.Windows.Forms.TextBox txtAdditionalPackagePrice;
        private System.Windows.Forms.Label lblInternetSpeed;
        private System.Windows.Forms.TextBox txtInternetSpeed;
        private System.Windows.Forms.Label lblPricePerMB;
        private System.Windows.Forms.TextBox txtPricePerMB;
        private System.Windows.Forms.Label lblPricePerAddress;
        private System.Windows.Forms.TextBox txtPricePerAddress;
        private System.Windows.Forms.Label lblNumberOfMinutes;
        private System.Windows.Forms.TextBox txtNumberOfMinutes;
        private System.Windows.Forms.GroupBox grpChannels;
        private System.Windows.Forms.ListView lstAdditionalPackages;
        private System.Windows.Forms.ListView lstChannels;
        private System.Windows.Forms.ColumnHeader columnHeaderChannelName;
        private System.Windows.Forms.TextBox txtNewAdditionalPackages;
        private System.Windows.Forms.Label lblNewAdditionalPackages;
        private System.Windows.Forms.TextBox txtNewChannels;
        private System.Windows.Forms.Label lblNewChannels;
        private System.Windows.Forms.ColumnHeader columnHeaderAdditionalPackageName;
        private System.Windows.Forms.Button btnCreateNewPackage;
    }
}