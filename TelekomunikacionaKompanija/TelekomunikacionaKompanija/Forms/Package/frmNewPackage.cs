﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.Package;

namespace TelekomunikacionaKompanija.Forms.Package
{
    public partial class frmNewPackage : Form
    {
        #region Constructor(s)
        public frmNewPackage()
        {
            this.InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmNewPackage_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Već postojeće kanale, koje je moguće dodati u paket, treba samo select-ovati.\r\n"
                + "Prilikom unosa novih kanala i dodatnih paketa kanala, svaki unositi u novom redu.",
                "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.lstChannels.Items.Clear();
            this.lstAdditionalPackages.Items.Clear();

            var channelsList = PackageDTOManager.GetAllChannels();
            var additionalPackagesList = PackageDTOManager.GetAllAdditionalPackages();

            foreach (var channel in channelsList)
            {
                var item = new ListViewItem(new string[] { channel.Name });

                this.lstChannels.Items.Add(item);
            }

            foreach (var additionalPackage in additionalPackagesList)
            {
                var item = new ListViewItem(new string[] { additionalPackage.Name });

                this.lstAdditionalPackages.Items.Add(item);
            }

            this.lstChannels.Refresh();
            this.lstAdditionalPackages.Refresh();
        }

        private void btnCreateNewPackage_Click(object sender, EventArgs e)
        {
            string packageName = this.txtName.Text;
            if (packageName == string.Empty)
            {
                MessageBox.Show("Paket mora da ima naziv.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            float packagePrice;
            if (this.txtPrice.Text == string.Empty ||
                !Single.TryParse(this.txtPrice.Text, out packagePrice))
            {
                MessageBox.Show("Paket mora da ima validnu cenu.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            float additionalPackagePrice = 0.00f;
            if (this.txtAdditionalPackagePrice.Text != string.Empty &&
                !Single.TryParse(this.txtAdditionalPackagePrice.Text, out additionalPackagePrice))
            {
                MessageBox.Show("Cena dodatnog paketa kanala mora biti validna.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            int internetSpeed = 0;
            if (this.txtInternetSpeed.Text != string.Empty &&
                !Int32.TryParse(this.txtInternetSpeed.Text, out internetSpeed))
            {
                MessageBox.Show("Brzina interneta mora biti validna.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            float pricePerMB = 0.00f;
            if (this.txtPricePerMB.Text != string.Empty &&
                !Single.TryParse(this.txtPricePerMB.Text, out pricePerMB))
            {
                MessageBox.Show("Cena po MB mora biti validna.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            float pricePerAddress = 0.00f;
            if (this.txtPricePerAddress.Text != string.Empty &&
                !Single.TryParse(this.txtPricePerAddress.Text, out pricePerAddress))
            {
                MessageBox.Show("Cena po adresi mora biti validna.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            int numberOfMinutes = 0;
            if (this.txtNumberOfMinutes.Text != string.Empty &&
                !Int32.TryParse(this.txtNumberOfMinutes.Text, out numberOfMinutes))
            {
                MessageBox.Show("Broj minuta mora biti validan.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            var newPackage = new PackageDTO(packageName, packagePrice, additionalPackagePrice,
                internetSpeed, pricePerMB, pricePerAddress, numberOfMinutes);

            var newPackageChannels = new List<ChannelDTO>();
            foreach (ListViewItem item in this.lstChannels.SelectedItems)
            {
                newPackageChannels.Add(new ChannelDTO(item.SubItems[0].Text));
            }

            if (this.txtNewChannels.Text != string.Empty)
            {
                string[] newChannelNames = this.txtNewChannels.Text.Split('\n');
                foreach (var name in newChannelNames)
                {
                    newPackageChannels.Add(new ChannelDTO(name));
                }
            }

            var newPackageAdditionalPackages = new List<AdditionalPackageDTO>();
            foreach (ListViewItem item in this.lstAdditionalPackages.SelectedItems)
            {
                newPackageAdditionalPackages.Add(new AdditionalPackageDTO(item.SubItems[0].Text));
            }

            if (this.txtNewAdditionalPackages.Text != string.Empty)
            {
                string[] newAdditionalPackageNames = this.txtNewAdditionalPackages.Text.Split('\n');
                foreach (var name in newAdditionalPackageNames)
                {
                    newPackageAdditionalPackages.Add(new AdditionalPackageDTO(name));
                }
            }

            if (PackageDTOManager.CreateOrUpdatePackage(newPackage, newPackageChannels,
                newPackageAdditionalPackages))
            {
                MessageBox.Show("Paket je uspešno kreiran.", "Obaveštenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                var packageForm = new frmPackage();

                packageForm.Show();

                this.Close();
            }
            else
            {
                MessageBox.Show("Paket nije kreiran.", "Greška",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion Form Event Method(s)
    }
}
