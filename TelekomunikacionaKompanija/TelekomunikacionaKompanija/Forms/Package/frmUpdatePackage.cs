﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.Package;

namespace TelekomunikacionaKompanija.Forms.Package
{
    public partial class frmUpdatePackage : Form
    {
        #region Properties
        public string PackageName { get; set; }
        #endregion Properties

        #region Constructor(s)
        public frmUpdatePackage()
        {
            this.InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmUpdatePackage_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Već postojeće kanale, koje je moguće dodati u paket, treba samo select-ovati.\r\n"
                + "Prilikom unosa novih kanala i dodatnih paketa kanala, svaki unositi u novom redu.",
                "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

            var package = PackageDTOManager.GetPackageByName(this.PackageName);

            this.txtName.Text = package.Name;
            this.txtPrice.Text = package.Price.ToString("N2");
            this.txtAdditionalPackagePrice.Text = package.AdditionalPackagePrice.ToString("N2");
            this.txtInternetSpeed.Text = package.InternetSpeed.ToString();
            this.txtPricePerMB.Text = package.PricePerMB.ToString("N2");
            this.txtPricePerAddress.Text = package.PricePerAddress.ToString("N2");
            this.txtNumberOfMinutes.Text = package.NumberOfMinutes.ToString();

            this.lstChannels.Items.Clear();
            this.lstAdditionalPackages.Items.Clear();

            var channelsList = PackageDTOManager.GetChannelsByPackage(this.PackageName);
            var additionalPackagesList =
                PackageDTOManager.GetAdditionalPackagesByPackage(this.PackageName);

            foreach (var channel in channelsList)
            {
                var item = new ListViewItem(new string[] { channel.Name });

                this.lstChannels.Items.Add(item);
            }

            foreach (var additionalPackage in additionalPackagesList)
            {
                var item = new ListViewItem(new string[] { additionalPackage.Name });

                this.lstAdditionalPackages.Items.Add(item);
            }

            this.RefreshChannelsListView();
            this.RefreshAdditionalPackagesListView();

            this.btnDeleteChannels.Enabled = false;
            this.btnDeleteAdditionalPackages.Enabled = false;
        }

        private void lstChannels_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lstChannels.SelectedItems.Count != 0)
            {
                this.btnDeleteChannels.Enabled = true;
            }
            else
            {
                this.btnDeleteChannels.Enabled = false;
            }
        }

        private void lstAdditionalPackages_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lstAdditionalPackages.SelectedItems.Count != 0)
            {
                this.btnDeleteAdditionalPackages.Enabled = true;
            }
            else
            {
                this.btnDeleteAdditionalPackages.Enabled = false;
            }
        }

        private void btnDeleteChannels_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li želite da obrišete izabrane kanale iz paketa?", "Potvrda brisanja",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var selectedChannelsList = new List<ChannelDTO>();

                foreach (ListViewItem item in this.lstChannels.SelectedItems)
                {
                    selectedChannelsList.Add(new ChannelDTO(item.SubItems[0].Text));
                }

                if (PackageDTOManager.DeleteSelectedChannelsByPackage(this.PackageName,
                    selectedChannelsList))
                {
                    MessageBox.Show("Kanali su uspešno obrisani.", "Obaveštenje",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.RefreshChannelsListView();

                    this.btnDeleteChannels.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Kanali nisu obrisani.", "Greška",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnDeleteAdditionalPackages_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li želite da obrišete izabrane dodatne pakete kanala?", "Potvrda brisanja",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var selectedAdditionalPackagesList = new List<AdditionalPackageDTO>();

                foreach (ListViewItem item in this.lstAdditionalPackages.SelectedItems)
                {
                    selectedAdditionalPackagesList.Add(new AdditionalPackageDTO(item.SubItems[0].Text));
                }

                if (PackageDTOManager.DeleteSelectedAdditionalPackagesByPackage(this.PackageName,
                    selectedAdditionalPackagesList))
                {
                    MessageBox.Show("Dodatni paketi kanala su uspešno obrisani.", "Obaveštenje",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.RefreshAdditionalPackagesListView();

                    this.btnDeleteAdditionalPackages.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Dodatni paketi kanala nisu obrisani.", "Greška",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnUpdatePackage_Click(object sender, EventArgs e)
        {
            string packageName = this.txtName.Text;
            if (packageName == string.Empty)
            {
                MessageBox.Show("Paket mora da ima naziv.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            float packagePrice;
            if (this.txtPrice.Text == string.Empty ||
                !Single.TryParse(this.txtPrice.Text, out packagePrice))
            {
                MessageBox.Show("Paket mora da ima validnu cenu.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            float additionalPackagePrice = 0.00f;
            if (this.txtAdditionalPackagePrice.Text != string.Empty &&
                !Single.TryParse(this.txtAdditionalPackagePrice.Text, out additionalPackagePrice))
            {
                MessageBox.Show("Cena dodatnog paketa kanala mora biti validna.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            int internetSpeed = 0;
            if (this.txtInternetSpeed.Text != string.Empty &&
                !Int32.TryParse(this.txtInternetSpeed.Text, out internetSpeed))
            {
                MessageBox.Show("Brzina interneta mora biti validna.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            float pricePerMB = 0.00f;
            if (this.txtPricePerMB.Text != string.Empty &&
                !Single.TryParse(this.txtPricePerMB.Text, out pricePerMB))
            {
                MessageBox.Show("Cena po MB mora biti validna.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            float pricePerAddress = 0.00f;
            if (this.txtPricePerAddress.Text != string.Empty &&
                !Single.TryParse(this.txtPricePerAddress.Text, out pricePerAddress))
            {
                MessageBox.Show("Cena po adresi mora biti validna.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            int numberOfMinutes = 0;
            if (this.txtNumberOfMinutes.Text != string.Empty &&
                !Int32.TryParse(this.txtNumberOfMinutes.Text, out numberOfMinutes))
            {
                MessageBox.Show("Broj minuta mora biti validan.", "Upozorenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            var package = new PackageDTO(packageName, packagePrice, additionalPackagePrice,
                internetSpeed, pricePerMB, pricePerAddress, numberOfMinutes);

            var packageChannels = new List<ChannelDTO>();
            foreach (ListViewItem item in this.lstChannels.SelectedItems)
            {
                packageChannels.Add(new ChannelDTO(item.SubItems[0].Text));
            }

            if (this.txtNewChannels.Text != string.Empty)
            {
                string[] newChannelNames = this.txtNewChannels.Text.Split('\n');
                foreach (var name in newChannelNames)
                {
                    packageChannels.Add(new ChannelDTO(name));
                }
            }

            var packageAdditionalPackages = new List<AdditionalPackageDTO>();
            foreach (ListViewItem item in this.lstAdditionalPackages.SelectedItems)
            {
                packageAdditionalPackages.Add(new AdditionalPackageDTO(item.SubItems[0].Text));
            }

            if (this.txtNewAdditionalPackages.Text != string.Empty)
            {
                string[] newAdditionalPackageNames = this.txtNewAdditionalPackages.Text.Split('\n');
                foreach (var name in newAdditionalPackageNames)
                {
                    packageAdditionalPackages.Add(new AdditionalPackageDTO(name));
                }
            }

            if (PackageDTOManager.CreateOrUpdatePackage(package, packageChannels,
                packageAdditionalPackages))
            {
                MessageBox.Show("Paket je uspešno ažuriran.", "Obaveštenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                var packageForm = new frmPackage();

                packageForm.Show();

                this.Close();
            }
            else
            {
                MessageBox.Show("Paket nije ažuriran.", "Greška",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion Form Event Method(s)

        #region Helper Method(s)
        private void RefreshChannelsListView()
        {
            this.lstChannels.Items.Clear();

            var channelsList = PackageDTOManager.GetChannelsByPackage(this.PackageName);

            foreach (var channel in channelsList)
            {
                var item = new ListViewItem(new string[] { channel.Name });

                this.lstChannels.Items.Add(item);
            }

            this.lstChannels.Refresh();
        }

        private void RefreshAdditionalPackagesListView()
        {
            this.lstAdditionalPackages.Items.Clear();

            var additionalPackagesList =
                PackageDTOManager.GetAdditionalPackagesByPackage(this.PackageName);

            foreach (var additionalPackage in additionalPackagesList)
            {
                var item = new ListViewItem(new string[] { additionalPackage.Name });

                this.lstAdditionalPackages.Items.Add(item);
            }

            this.lstAdditionalPackages.Refresh();
        }
        #endregion Helper Method(s)
    }
}
