﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.Package;

namespace TelekomunikacionaKompanija.Forms.Package
{
    public partial class frmPackage : Form
    {
        #region Attribute(s)
        private List<PackageDTO> packagesList;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmPackage()
        {
            this.InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmPackage_Load(object sender, EventArgs e)
        {
            this.packagesList = PackageDTOManager.GetAllPackages();

            this.cboSearch.SelectedIndex = 0;

            this.btnUpdatePackage.Enabled = false;
            this.btnDeletePackage.Enabled = false;
        }

        private void linkMainForm_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            => this.Close();

        private void cboSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lstPackages.Items.Clear();

            var filteredPackagesList = new List<PackageDTO>();

            switch (this.cboSearch.SelectedIndex)
            {
                case 0:
                    filteredPackagesList = this.packagesList;
                    break;
                case 1:
                    filteredPackagesList = this.packagesList.Where(x => x.Price <= 4000.00f).ToList();
                    break;
                default:
                    filteredPackagesList = this.packagesList.Where(x => x.Price > 4000.00f).ToList();
                    break;
            }

            foreach (var package in filteredPackagesList)
            {
                var item = new ListViewItem(new string[] { package.Name, package.Price.ToString("N2"),
                    package.AdditionalPackagePrice.ToString("N2"), package.InternetSpeed.ToString(),
                    package.PricePerMB.ToString("N2"), package.PricePerAddress.ToString("N2"),
                    package.NumberOfMinutes.ToString() });

                this.lstPackages.Items.Add(item);
            }

            this.lstPackages.Refresh();

            this.btnUpdatePackage.Enabled = false;
            this.btnDeletePackage.Enabled = false;

            this.lstChannels.Items.Clear();
            this.lstAdditionalPackages.Items.Clear();
        }

        private void lstPackages_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lstPackages.SelectedItems.Count != 0)
            {
                this.btnUpdatePackage.Enabled = true;
                this.btnDeletePackage.Enabled = true;

                var selectedPackageName = this.lstPackages.SelectedItems[0].SubItems[0].Text;

                var selectedPackageChannels = PackageDTOManager.GetChannelsByPackage(selectedPackageName);
                var selectedPackageAdditionalPackages =
                    PackageDTOManager.GetAdditionalPackagesByPackage(selectedPackageName);

                this.lstChannels.Items.Clear();
                this.lstAdditionalPackages.Items.Clear();

                foreach (var channel in selectedPackageChannels)
                {
                    var item = new ListViewItem(new string[] { channel.Name });

                    this.lstChannels.Items.Add(item);
                }

                foreach (var additionalPackage in selectedPackageAdditionalPackages)
                {
                    var item = new ListViewItem(new string[] { additionalPackage.Name });

                    this.lstAdditionalPackages.Items.Add(item);
                }

                this.lstChannels.Refresh();
                this.lstAdditionalPackages.Refresh();
            }
            else
            {
                this.btnUpdatePackage.Enabled = false;
                this.btnDeletePackage.Enabled = false;

                this.lstChannels.Items.Clear();
                this.lstAdditionalPackages.Items.Clear();
            }
        }

        private void btnNewPackage_Click(object sender, EventArgs e)
        {
            var newPackageForm = new frmNewPackage();

            newPackageForm.Show();

            this.Close();
        }

        private void btnUpdatePackage_Click(object sender, EventArgs e)
        {
            var updatePackageForm = new frmUpdatePackage
            {
                PackageName = this.lstPackages.SelectedItems[0].SubItems[0].Text
            };

            updatePackageForm.Show();

            this.Close();
        }

        private void btnDeletePackage_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li želite da obrišete izabrani paket?", "Potvrda brisanja",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string packageName = this.lstPackages.SelectedItems[0].SubItems[0].Text;

                if (PackageDTOManager.DeletePackage(packageName))
                {
                    MessageBox.Show("Paket je uspešno obrisan.", "Obaveštenje",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.lstPackages.Items.Clear();

                    this.packagesList = PackageDTOManager.GetAllPackages();

                    foreach (var package in this.packagesList)
                    {
                        var item = new ListViewItem(new string[] { package.Name, package.Price.ToString("N2"),
                            package.AdditionalPackagePrice.ToString("N2"), package.InternetSpeed.ToString(),
                            package.PricePerMB.ToString("N2"), package.PricePerAddress.ToString("N2"),
                            package.NumberOfMinutes.ToString() });

                        this.lstPackages.Items.Add(item);
                    }

                    this.lstPackages.Refresh();

                    this.cboSearch.SelectedIndex = 0;

                    this.btnUpdatePackage.Enabled = false;
                    this.btnDeletePackage.Enabled = false;

                    this.lstChannels.Items.Clear();
                    this.lstAdditionalPackages.Items.Clear();
                }
                else
                {
                    MessageBox.Show("Paket nije obrisan.", "Greška",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion Form Event Method(s)
    }
}
