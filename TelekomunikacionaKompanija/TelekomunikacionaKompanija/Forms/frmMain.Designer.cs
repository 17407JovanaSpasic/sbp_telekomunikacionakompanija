﻿
namespace TelekomunikacionaKompanija.Forms
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnShowManufacturers = new System.Windows.Forms.Button();
            this.btnShowDevices = new System.Windows.Forms.Button();
            this.btnShowPackages = new System.Windows.Forms.Button();
            this.btnShowIndividualUsers = new System.Windows.Forms.Button();
            this.btnShowContracts = new System.Windows.Forms.Button();
            this.btnShowPayments = new System.Windows.Forms.Button();
            this.txtInformation = new System.Windows.Forms.TextBox();
            this.btnShowLegalUsers = new System.Windows.Forms.Button();
            this.btnShowContractsByUser = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnShowManufacturers
            // 
            this.btnShowManufacturers.Location = new System.Drawing.Point(322, 42);
            this.btnShowManufacturers.Name = "btnShowManufacturers";
            this.btnShowManufacturers.Size = new System.Drawing.Size(100, 35);
            this.btnShowManufacturers.TabIndex = 1;
            this.btnShowManufacturers.Text = "Prikaz svih proizvođača";
            this.btnShowManufacturers.UseVisualStyleBackColor = true;
            this.btnShowManufacturers.Click += new System.EventHandler(this.btnShowManufacturers_Click);
            // 
            // btnShowDevices
            // 
            this.btnShowDevices.Location = new System.Drawing.Point(322, 83);
            this.btnShowDevices.Name = "btnShowDevices";
            this.btnShowDevices.Size = new System.Drawing.Size(100, 35);
            this.btnShowDevices.TabIndex = 2;
            this.btnShowDevices.Text = "Prikaz svih uređaja";
            this.btnShowDevices.UseVisualStyleBackColor = true;
            this.btnShowDevices.Click += new System.EventHandler(this.btnShowDevices_Click);
            // 
            // btnShowPackages
            // 
            this.btnShowPackages.Location = new System.Drawing.Point(322, 124);
            this.btnShowPackages.Name = "btnShowPackages";
            this.btnShowPackages.Size = new System.Drawing.Size(100, 35);
            this.btnShowPackages.TabIndex = 3;
            this.btnShowPackages.Text = "Prikaz svih paketa";
            this.btnShowPackages.UseVisualStyleBackColor = true;
            this.btnShowPackages.Click += new System.EventHandler(this.btnShowPackages_Click);
            // 
            // btnShowIndividualUsers
            // 
            this.btnShowIndividualUsers.Location = new System.Drawing.Point(322, 165);
            this.btnShowIndividualUsers.Name = "btnShowIndividualUsers";
            this.btnShowIndividualUsers.Size = new System.Drawing.Size(100, 35);
            this.btnShowIndividualUsers.TabIndex = 4;
            this.btnShowIndividualUsers.Text = "Prikaz svih fizičkih lica";
            this.btnShowIndividualUsers.UseVisualStyleBackColor = true;
            this.btnShowIndividualUsers.Click += new System.EventHandler(this.btnShowIndividualUsers_Click);
            // 
            // btnShowContracts
            // 
            this.btnShowContracts.Location = new System.Drawing.Point(322, 247);
            this.btnShowContracts.Name = "btnShowContracts";
            this.btnShowContracts.Size = new System.Drawing.Size(100, 35);
            this.btnShowContracts.TabIndex = 6;
            this.btnShowContracts.Text = "Kreiranje ugovora";
            this.btnShowContracts.UseVisualStyleBackColor = true;
            this.btnShowContracts.Click += new System.EventHandler(this.btnShowContracts_Click);
            // 
            // btnShowPayments
            // 
            this.btnShowPayments.Location = new System.Drawing.Point(322, 329);
            this.btnShowPayments.Name = "btnShowPayments";
            this.btnShowPayments.Size = new System.Drawing.Size(100, 35);
            this.btnShowPayments.TabIndex = 8;
            this.btnShowPayments.Text = "Prikaz svih plaćanja";
            this.btnShowPayments.UseVisualStyleBackColor = true;
            this.btnShowPayments.Click += new System.EventHandler(this.btnShowPayments_Click);
            // 
            // txtInformation
            // 
            this.txtInformation.Enabled = false;
            this.txtInformation.Location = new System.Drawing.Point(12, 42);
            this.txtInformation.Multiline = true;
            this.txtInformation.Name = "txtInformation";
            this.txtInformation.Size = new System.Drawing.Size(304, 322);
            this.txtInformation.TabIndex = 0;
            this.txtInformation.Text = resources.GetString("txtInformation.Text");
            // 
            // btnShowLegalUsers
            // 
            this.btnShowLegalUsers.Location = new System.Drawing.Point(322, 206);
            this.btnShowLegalUsers.Name = "btnShowLegalUsers";
            this.btnShowLegalUsers.Size = new System.Drawing.Size(100, 35);
            this.btnShowLegalUsers.TabIndex = 5;
            this.btnShowLegalUsers.Text = "Prikaz svih pravnih lica";
            this.btnShowLegalUsers.UseVisualStyleBackColor = true;
            this.btnShowLegalUsers.Click += new System.EventHandler(this.btnShowLegalUsers_Click);
            // 
            // btnShowContractsByUser
            // 
            this.btnShowContractsByUser.Location = new System.Drawing.Point(322, 288);
            this.btnShowContractsByUser.Name = "btnShowContractsByUser";
            this.btnShowContractsByUser.Size = new System.Drawing.Size(100, 35);
            this.btnShowContractsByUser.TabIndex = 7;
            this.btnShowContractsByUser.Text = "Prikaz ugovora za korisnika";
            this.btnShowContractsByUser.UseVisualStyleBackColor = true;
            this.btnShowContractsByUser.Click += new System.EventHandler(this.btnShowContractsByUser_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 411);
            this.Controls.Add(this.btnShowContractsByUser);
            this.Controls.Add(this.btnShowLegalUsers);
            this.Controls.Add(this.txtInformation);
            this.Controls.Add(this.btnShowPayments);
            this.Controls.Add(this.btnShowContracts);
            this.Controls.Add(this.btnShowIndividualUsers);
            this.Controls.Add(this.btnShowPackages);
            this.Controls.Add(this.btnShowDevices);
            this.Controls.Add(this.btnShowManufacturers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.Text = "Početna strana";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnShowManufacturers;
        private System.Windows.Forms.Button btnShowDevices;
        private System.Windows.Forms.Button btnShowPackages;
        private System.Windows.Forms.Button btnShowIndividualUsers;
        private System.Windows.Forms.Button btnShowContracts;
        private System.Windows.Forms.Button btnShowPayments;
        private System.Windows.Forms.TextBox txtInformation;
        private System.Windows.Forms.Button btnShowLegalUsers;
        private System.Windows.Forms.Button btnShowContractsByUser;
    }
}