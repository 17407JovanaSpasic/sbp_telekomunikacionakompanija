﻿
namespace TelekomunikacionaKompanija.Forms.User
{
    partial class frmUpdateUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIme = new System.Windows.Forms.Label();
            this.lblPrezime = new System.Windows.Forms.Label();
            this.lblAdresa = new System.Windows.Forms.Label();
            this.lblBroj = new System.Windows.Forms.Label();
            this.lblGrad = new System.Windows.Forms.Label();
            this.lblKontaktTel = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblSetIme = new System.Windows.Forms.Label();
            this.lblSetPrezime = new System.Windows.Forms.Label();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtGrad = new System.Windows.Forms.TextBox();
            this.txtKontaktTel = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.numBroj = new System.Windows.Forms.NumericUpDown();
            this.grpPravnoLice = new System.Windows.Forms.GroupBox();
            this.txtPIB = new System.Windows.Forms.TextBox();
            this.txtBrojFaksa = new System.Windows.Forms.TextBox();
            this.lblPIB = new System.Windows.Forms.Label();
            this.lblBrojFaksa = new System.Windows.Forms.Label();
            this.lblPocetnaStr = new System.Windows.Forms.LinkLabel();
            this.btnIzmeni = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numBroj)).BeginInit();
            this.grpPravnoLice.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(11, 45);
            this.lblIme.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(27, 13);
            this.lblIme.TabIndex = 1;
            this.lblIme.Text = "Ime:";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Location = new System.Drawing.Point(11, 72);
            this.lblPrezime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(47, 13);
            this.lblPrezime.TabIndex = 3;
            this.lblPrezime.Text = "Prezime:";
            // 
            // lblAdresa
            // 
            this.lblAdresa.AutoSize = true;
            this.lblAdresa.Location = new System.Drawing.Point(11, 101);
            this.lblAdresa.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAdresa.Name = "lblAdresa";
            this.lblAdresa.Size = new System.Drawing.Size(43, 13);
            this.lblAdresa.TabIndex = 5;
            this.lblAdresa.Text = "Adresa:";
            // 
            // lblBroj
            // 
            this.lblBroj.AutoSize = true;
            this.lblBroj.Location = new System.Drawing.Point(11, 132);
            this.lblBroj.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBroj.Name = "lblBroj";
            this.lblBroj.Size = new System.Drawing.Size(28, 13);
            this.lblBroj.TabIndex = 7;
            this.lblBroj.Text = "Broj:";
            // 
            // lblGrad
            // 
            this.lblGrad.AutoSize = true;
            this.lblGrad.Location = new System.Drawing.Point(11, 168);
            this.lblGrad.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGrad.Name = "lblGrad";
            this.lblGrad.Size = new System.Drawing.Size(33, 13);
            this.lblGrad.TabIndex = 9;
            this.lblGrad.Text = "Grad:";
            // 
            // lblKontaktTel
            // 
            this.lblKontaktTel.AutoSize = true;
            this.lblKontaktTel.Location = new System.Drawing.Point(11, 202);
            this.lblKontaktTel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblKontaktTel.Name = "lblKontaktTel";
            this.lblKontaktTel.Size = new System.Drawing.Size(82, 13);
            this.lblKontaktTel.TabIndex = 11;
            this.lblKontaktTel.Text = "Kontakt telefon:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(11, 235);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 13;
            this.lblEmail.Text = "E-mail:";
            // 
            // lblSetIme
            // 
            this.lblSetIme.AutoSize = true;
            this.lblSetIme.Location = new System.Drawing.Point(109, 45);
            this.lblSetIme.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSetIme.Name = "lblSetIme";
            this.lblSetIme.Size = new System.Drawing.Size(62, 13);
            this.lblSetIme.TabIndex = 2;
            this.lblSetIme.Text = "labelSetIme";
            // 
            // lblSetPrezime
            // 
            this.lblSetPrezime.AutoSize = true;
            this.lblSetPrezime.Location = new System.Drawing.Point(109, 72);
            this.lblSetPrezime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSetPrezime.Name = "lblSetPrezime";
            this.lblSetPrezime.Size = new System.Drawing.Size(82, 13);
            this.lblSetPrezime.TabIndex = 4;
            this.lblSetPrezime.Text = "labelSetPrezime";
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(112, 98);
            this.txtAdresa.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtAdresa.MaxLength = 30;
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(102, 20);
            this.txtAdresa.TabIndex = 6;
            // 
            // txtGrad
            // 
            this.txtGrad.Location = new System.Drawing.Point(112, 165);
            this.txtGrad.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtGrad.MaxLength = 15;
            this.txtGrad.Name = "txtGrad";
            this.txtGrad.Size = new System.Drawing.Size(102, 20);
            this.txtGrad.TabIndex = 10;
            // 
            // txtKontaktTel
            // 
            this.txtKontaktTel.Location = new System.Drawing.Point(112, 199);
            this.txtKontaktTel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtKontaktTel.MaxLength = 10;
            this.txtKontaktTel.Name = "txtKontaktTel";
            this.txtKontaktTel.Size = new System.Drawing.Size(102, 20);
            this.txtKontaktTel.TabIndex = 12;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(112, 232);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(103, 20);
            this.txtEmail.TabIndex = 14;
            // 
            // numBroj
            // 
            this.numBroj.Location = new System.Drawing.Point(112, 130);
            this.numBroj.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numBroj.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numBroj.Name = "numBroj";
            this.numBroj.Size = new System.Drawing.Size(102, 20);
            this.numBroj.TabIndex = 8;
            // 
            // grpPravnoLice
            // 
            this.grpPravnoLice.Controls.Add(this.txtPIB);
            this.grpPravnoLice.Controls.Add(this.txtBrojFaksa);
            this.grpPravnoLice.Controls.Add(this.lblPIB);
            this.grpPravnoLice.Controls.Add(this.lblBrojFaksa);
            this.grpPravnoLice.Location = new System.Drawing.Point(260, 45);
            this.grpPravnoLice.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpPravnoLice.Name = "grpPravnoLice";
            this.grpPravnoLice.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpPravnoLice.Size = new System.Drawing.Size(184, 126);
            this.grpPravnoLice.TabIndex = 15;
            this.grpPravnoLice.TabStop = false;
            this.grpPravnoLice.Text = "Pravno lice";
            // 
            // txtPIB
            // 
            this.txtPIB.Location = new System.Drawing.Point(76, 79);
            this.txtPIB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPIB.MaxLength = 9;
            this.txtPIB.Name = "txtPIB";
            this.txtPIB.Size = new System.Drawing.Size(90, 20);
            this.txtPIB.TabIndex = 3;
            // 
            // txtBrojFaksa
            // 
            this.txtBrojFaksa.Location = new System.Drawing.Point(76, 39);
            this.txtBrojFaksa.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtBrojFaksa.MaxLength = 10;
            this.txtBrojFaksa.Name = "txtBrojFaksa";
            this.txtBrojFaksa.Size = new System.Drawing.Size(90, 20);
            this.txtBrojFaksa.TabIndex = 1;
            // 
            // lblPIB
            // 
            this.lblPIB.AutoSize = true;
            this.lblPIB.Location = new System.Drawing.Point(14, 82);
            this.lblPIB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPIB.Name = "lblPIB";
            this.lblPIB.Size = new System.Drawing.Size(27, 13);
            this.lblPIB.TabIndex = 2;
            this.lblPIB.Text = "PIB:";
            // 
            // lblBrojFaksa
            // 
            this.lblBrojFaksa.AutoSize = true;
            this.lblBrojFaksa.Location = new System.Drawing.Point(14, 42);
            this.lblBrojFaksa.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrojFaksa.Name = "lblBrojFaksa";
            this.lblBrojFaksa.Size = new System.Drawing.Size(57, 13);
            this.lblBrojFaksa.TabIndex = 0;
            this.lblBrojFaksa.Text = "Broj faksa:";
            // 
            // lblPocetnaStr
            // 
            this.lblPocetnaStr.AutoSize = true;
            this.lblPocetnaStr.Location = new System.Drawing.Point(11, 9);
            this.lblPocetnaStr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPocetnaStr.Name = "lblPocetnaStr";
            this.lblPocetnaStr.Size = new System.Drawing.Size(79, 13);
            this.lblPocetnaStr.TabIndex = 0;
            this.lblPocetnaStr.TabStop = true;
            this.lblPocetnaStr.Text = "Početna strana";
            this.lblPocetnaStr.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblPocetnaStr_LinkClicked);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(294, 199);
            this.btnIzmeni.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(105, 53);
            this.btnIzmeni.TabIndex = 16;
            this.btnIzmeni.Text = "Ažurirajte podatke";
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // frmUpdateUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 268);
            this.Controls.Add(this.btnIzmeni);
            this.Controls.Add(this.lblPocetnaStr);
            this.Controls.Add(this.grpPravnoLice);
            this.Controls.Add(this.numBroj);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtKontaktTel);
            this.Controls.Add(this.txtGrad);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.lblSetPrezime);
            this.Controls.Add(this.lblSetIme);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblKontaktTel);
            this.Controls.Add(this.lblGrad);
            this.Controls.Add(this.lblBroj);
            this.Controls.Add(this.lblAdresa);
            this.Controls.Add(this.lblPrezime);
            this.Controls.Add(this.lblIme);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUpdateUser";
            this.Text = "Ažuriranje podataka o korisniku";
            this.Load += new System.EventHandler(this.frmUpdateUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numBroj)).EndInit();
            this.grpPravnoLice.ResumeLayout(false);
            this.grpPravnoLice.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIme;
        private System.Windows.Forms.Label lblPrezime;
        private System.Windows.Forms.Label lblAdresa;
        private System.Windows.Forms.Label lblBroj;
        private System.Windows.Forms.Label lblGrad;
        private System.Windows.Forms.Label lblKontaktTel;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblSetIme;
        private System.Windows.Forms.Label lblSetPrezime;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtGrad;
        private System.Windows.Forms.TextBox txtKontaktTel;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.NumericUpDown numBroj;
        private System.Windows.Forms.GroupBox grpPravnoLice;
        private System.Windows.Forms.TextBox txtPIB;
        private System.Windows.Forms.TextBox txtBrojFaksa;
        private System.Windows.Forms.Label lblPIB;
        private System.Windows.Forms.Label lblBrojFaksa;
        private System.Windows.Forms.LinkLabel lblPocetnaStr;
        private System.Windows.Forms.Button btnIzmeni;
    }
}