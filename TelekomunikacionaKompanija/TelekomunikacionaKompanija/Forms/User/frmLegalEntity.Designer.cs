﻿
namespace TelekomunikacionaKompanija.Forms.User
{
    partial class frmLegalEntity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDodajPravnoLice = new System.Windows.Forms.Button();
            this.btnObrisiPravnoLice = new System.Windows.Forms.Button();
            this.btnIzmeniPravnoLice = new System.Windows.Forms.Button();
            this.lstPravnaLica = new System.Windows.Forms.ListView();
            this.clmId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmIme = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmPrezime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmAdresa = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmBroj = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmGrad = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmKontaktTelefon = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmBrojFax = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmPib = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblPovratakNaPocetnuStranu = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // btnDodajPravnoLice
            // 
            this.btnDodajPravnoLice.Location = new System.Drawing.Point(12, 101);
            this.btnDodajPravnoLice.Margin = new System.Windows.Forms.Padding(2);
            this.btnDodajPravnoLice.Name = "btnDodajPravnoLice";
            this.btnDodajPravnoLice.Size = new System.Drawing.Size(76, 43);
            this.btnDodajPravnoLice.TabIndex = 1;
            this.btnDodajPravnoLice.Text = "Kreirajte pravno lice";
            this.btnDodajPravnoLice.UseVisualStyleBackColor = true;
            this.btnDodajPravnoLice.Click += new System.EventHandler(this.btnDodajPravnoLice_Click);
            // 
            // btnObrisiPravnoLice
            // 
            this.btnObrisiPravnoLice.Enabled = false;
            this.btnObrisiPravnoLice.Location = new System.Drawing.Point(12, 195);
            this.btnObrisiPravnoLice.Margin = new System.Windows.Forms.Padding(2);
            this.btnObrisiPravnoLice.Name = "btnObrisiPravnoLice";
            this.btnObrisiPravnoLice.Size = new System.Drawing.Size(76, 43);
            this.btnObrisiPravnoLice.TabIndex = 3;
            this.btnObrisiPravnoLice.Text = "Obrišite pravno lice";
            this.btnObrisiPravnoLice.UseVisualStyleBackColor = true;
            this.btnObrisiPravnoLice.Click += new System.EventHandler(this.btnObrisiPravnoLice_Click);
            // 
            // btnIzmeniPravnoLice
            // 
            this.btnIzmeniPravnoLice.Enabled = false;
            this.btnIzmeniPravnoLice.Location = new System.Drawing.Point(12, 148);
            this.btnIzmeniPravnoLice.Margin = new System.Windows.Forms.Padding(2);
            this.btnIzmeniPravnoLice.Name = "btnIzmeniPravnoLice";
            this.btnIzmeniPravnoLice.Size = new System.Drawing.Size(76, 43);
            this.btnIzmeniPravnoLice.TabIndex = 2;
            this.btnIzmeniPravnoLice.Text = "Ažurirajte pravno lice";
            this.btnIzmeniPravnoLice.UseVisualStyleBackColor = true;
            this.btnIzmeniPravnoLice.Click += new System.EventHandler(this.lblIzmeniPravnoLice_Click);
            // 
            // lstPravnaLica
            // 
            this.lstPravnaLica.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clmId,
            this.clmIme,
            this.clmPrezime,
            this.clmAdresa,
            this.clmBroj,
            this.clmGrad,
            this.clmKontaktTelefon,
            this.clmEmail,
            this.clmBrojFax,
            this.clmPib});
            this.lstPravnaLica.FullRowSelect = true;
            this.lstPravnaLica.GridLines = true;
            this.lstPravnaLica.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstPravnaLica.HideSelection = false;
            this.lstPravnaLica.Location = new System.Drawing.Point(104, 41);
            this.lstPravnaLica.Margin = new System.Windows.Forms.Padding(2);
            this.lstPravnaLica.MultiSelect = false;
            this.lstPravnaLica.Name = "lstPravnaLica";
            this.lstPravnaLica.Size = new System.Drawing.Size(855, 276);
            this.lstPravnaLica.TabIndex = 3;
            this.lstPravnaLica.UseCompatibleStateImageBehavior = false;
            this.lstPravnaLica.View = System.Windows.Forms.View.Details;
            this.lstPravnaLica.SelectedIndexChanged += new System.EventHandler(this.lstPravnaLica_SelectedIndexChanged);
            // 
            // clmId
            // 
            this.clmId.Text = "ID korisnika";
            this.clmId.Width = 88;
            // 
            // clmIme
            // 
            this.clmIme.Text = "Ime";
            // 
            // clmPrezime
            // 
            this.clmPrezime.Text = "Prezime";
            this.clmPrezime.Width = 70;
            // 
            // clmAdresa
            // 
            this.clmAdresa.Text = "Adresa";
            this.clmAdresa.Width = 75;
            // 
            // clmBroj
            // 
            this.clmBroj.Text = "Broj";
            this.clmBroj.Width = 47;
            // 
            // clmGrad
            // 
            this.clmGrad.Text = "Grad";
            this.clmGrad.Width = 88;
            // 
            // clmKontaktTelefon
            // 
            this.clmKontaktTelefon.Text = "Kontakt telefon";
            this.clmKontaktTelefon.Width = 105;
            // 
            // clmEmail
            // 
            this.clmEmail.Text = "E-mail";
            this.clmEmail.Width = 119;
            // 
            // clmBrojFax
            // 
            this.clmBrojFax.Text = "Broj faksa";
            this.clmBrojFax.Width = 89;
            // 
            // clmPib
            // 
            this.clmPib.Text = "PIB";
            this.clmPib.Width = 102;
            // 
            // lblPovratakNaPocetnuStranu
            // 
            this.lblPovratakNaPocetnuStranu.AutoSize = true;
            this.lblPovratakNaPocetnuStranu.Location = new System.Drawing.Point(11, 9);
            this.lblPovratakNaPocetnuStranu.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPovratakNaPocetnuStranu.Name = "lblPovratakNaPocetnuStranu";
            this.lblPovratakNaPocetnuStranu.Size = new System.Drawing.Size(79, 13);
            this.lblPovratakNaPocetnuStranu.TabIndex = 0;
            this.lblPovratakNaPocetnuStranu.TabStop = true;
            this.lblPovratakNaPocetnuStranu.Text = "Početna strana";
            this.lblPovratakNaPocetnuStranu.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblPovratakNaPocetnuStranu_LinkClicked);
            // 
            // frmLegalEntity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 328);
            this.Controls.Add(this.lblPovratakNaPocetnuStranu);
            this.Controls.Add(this.lstPravnaLica);
            this.Controls.Add(this.btnIzmeniPravnoLice);
            this.Controls.Add(this.btnObrisiPravnoLice);
            this.Controls.Add(this.btnDodajPravnoLice);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLegalEntity";
            this.Text = "Prikaz svih pravnih lica";
            this.Load += new System.EventHandler(this.frmLegalEntity_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDodajPravnoLice;
        private System.Windows.Forms.Button btnObrisiPravnoLice;
        private System.Windows.Forms.Button btnIzmeniPravnoLice;
        private System.Windows.Forms.ListView lstPravnaLica;
        private System.Windows.Forms.ColumnHeader clmId;
        private System.Windows.Forms.ColumnHeader clmIme;
        private System.Windows.Forms.ColumnHeader clmPrezime;
        private System.Windows.Forms.ColumnHeader clmAdresa;
        private System.Windows.Forms.ColumnHeader clmBroj;
        private System.Windows.Forms.ColumnHeader clmGrad;
        private System.Windows.Forms.ColumnHeader clmKontaktTelefon;
        private System.Windows.Forms.ColumnHeader clmEmail;
        private System.Windows.Forms.ColumnHeader clmBrojFax;
        private System.Windows.Forms.ColumnHeader clmPib;
        private System.Windows.Forms.LinkLabel lblPovratakNaPocetnuStranu;
    }
}