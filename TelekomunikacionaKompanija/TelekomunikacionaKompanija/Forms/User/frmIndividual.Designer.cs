﻿
namespace TelekomunikacionaKompanija.Forms.User
{
    partial class frmIndividual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnDeleteUser = new System.Windows.Forms.Button();
            this.btnIzmeniKorisnika = new System.Windows.Forms.Button();
            this.lstKorisnici = new System.Windows.Forms.ListView();
            this.clmId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmLastname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmAddres = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmAddresNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmCity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmPhoneNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmJmbg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(11, 9);
            this.linkLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(79, 13);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Početna strana";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_MainFormClicked);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(11, 88);
            this.btnCreate.Margin = new System.Windows.Forms.Padding(2);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(76, 43);
            this.btnCreate.TabIndex = 1;
            this.btnCreate.Text = "Kreirajte fizičko lice";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDeleteUser
            // 
            this.btnDeleteUser.Enabled = false;
            this.btnDeleteUser.Location = new System.Drawing.Point(11, 185);
            this.btnDeleteUser.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeleteUser.Name = "btnDeleteUser";
            this.btnDeleteUser.Size = new System.Drawing.Size(75, 42);
            this.btnDeleteUser.TabIndex = 3;
            this.btnDeleteUser.Text = "Obrišite fizičko lice";
            this.btnDeleteUser.UseVisualStyleBackColor = true;
            this.btnDeleteUser.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnIzmeniKorisnika
            // 
            this.btnIzmeniKorisnika.Enabled = false;
            this.btnIzmeniKorisnika.Location = new System.Drawing.Point(11, 135);
            this.btnIzmeniKorisnika.Margin = new System.Windows.Forms.Padding(2);
            this.btnIzmeniKorisnika.Name = "btnIzmeniKorisnika";
            this.btnIzmeniKorisnika.Size = new System.Drawing.Size(76, 46);
            this.btnIzmeniKorisnika.TabIndex = 2;
            this.btnIzmeniKorisnika.Text = "Ažurirajte fizičko lice";
            this.btnIzmeniKorisnika.UseVisualStyleBackColor = true;
            this.btnIzmeniKorisnika.Click += new System.EventHandler(this.button3_Click);
            // 
            // lstKorisnici
            // 
            this.lstKorisnici.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clmId,
            this.clmName,
            this.clmLastname,
            this.clmAddres,
            this.clmAddresNumber,
            this.clmCity,
            this.clmPhoneNum,
            this.clmEmail,
            this.clmJmbg});
            this.lstKorisnici.FullRowSelect = true;
            this.lstKorisnici.GridLines = true;
            this.lstKorisnici.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstKorisnici.HideSelection = false;
            this.lstKorisnici.Location = new System.Drawing.Point(100, 42);
            this.lstKorisnici.Margin = new System.Windows.Forms.Padding(2);
            this.lstKorisnici.MultiSelect = false;
            this.lstKorisnici.Name = "lstKorisnici";
            this.lstKorisnici.Size = new System.Drawing.Size(814, 247);
            this.lstKorisnici.TabIndex = 4;
            this.lstKorisnici.UseCompatibleStateImageBehavior = false;
            this.lstKorisnici.View = System.Windows.Forms.View.Details;
            this.lstKorisnici.SelectedIndexChanged += new System.EventHandler(this.lstKorisnici_SelectedIndexChanged);
            // 
            // clmId
            // 
            this.clmId.Text = "ID korisnika";
            this.clmId.Width = 90;
            // 
            // clmName
            // 
            this.clmName.Text = "Ime";
            this.clmName.Width = 62;
            // 
            // clmLastname
            // 
            this.clmLastname.Text = "Prezime";
            this.clmLastname.Width = 85;
            // 
            // clmAddres
            // 
            this.clmAddres.Text = "Adresa";
            this.clmAddres.Width = 116;
            // 
            // clmAddresNumber
            // 
            this.clmAddresNumber.Text = "Broj";
            this.clmAddresNumber.Width = 39;
            // 
            // clmCity
            // 
            this.clmCity.Text = "Grad";
            this.clmCity.Width = 81;
            // 
            // clmPhoneNum
            // 
            this.clmPhoneNum.Text = "Kontakt telefon";
            this.clmPhoneNum.Width = 104;
            // 
            // clmEmail
            // 
            this.clmEmail.Text = "E-mail";
            this.clmEmail.Width = 122;
            // 
            // clmJmbg
            // 
            this.clmJmbg.Text = "JMBG";
            this.clmJmbg.Width = 135;
            // 
            // frmIndividual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 300);
            this.Controls.Add(this.lstKorisnici);
            this.Controls.Add(this.btnIzmeniKorisnika);
            this.Controls.Add(this.btnDeleteUser);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.linkLabel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmIndividual";
            this.Text = "Prikaz svih fizičkih lica";
            this.Load += new System.EventHandler(this.frmIndividual_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnDeleteUser;
        private System.Windows.Forms.Button btnIzmeniKorisnika;
        private System.Windows.Forms.ListView lstKorisnici;
        private System.Windows.Forms.ColumnHeader clmId;
        private System.Windows.Forms.ColumnHeader clmName;
        private System.Windows.Forms.ColumnHeader clmLastname;
        private System.Windows.Forms.ColumnHeader clmAddres;
        private System.Windows.Forms.ColumnHeader clmAddresNumber;
        private System.Windows.Forms.ColumnHeader clmCity;
        private System.Windows.Forms.ColumnHeader clmPhoneNum;
        private System.Windows.Forms.ColumnHeader clmEmail;
        private System.Windows.Forms.ColumnHeader clmJmbg;
    }
}