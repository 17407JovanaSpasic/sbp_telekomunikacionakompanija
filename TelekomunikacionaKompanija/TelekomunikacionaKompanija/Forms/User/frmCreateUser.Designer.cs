﻿
namespace TelekomunikacionaKompanija.Forms.User
{
    partial class frmCreateUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIme = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblPrezime = new System.Windows.Forms.Label();
            this.lblAdresa = new System.Windows.Forms.Label();
            this.lblBroj = new System.Windows.Forms.Label();
            this.lblGrad = new System.Windows.Forms.Label();
            this.lblKonaktTelefon = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtPrezime = new System.Windows.Forms.TextBox();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtGrad = new System.Windows.Forms.TextBox();
            this.txtKonaktTelefon = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.grpIndividual = new System.Windows.Forms.GroupBox();
            this.txtJMBG = new System.Windows.Forms.TextBox();
            this.lblJmbg = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numBroj = new System.Windows.Forms.NumericUpDown();
            this.grpLegalEntity = new System.Windows.Forms.GroupBox();
            this.txtPIB = new System.Windows.Forms.TextBox();
            this.txtFaxNumber = new System.Windows.Forms.TextBox();
            this.lblPIB = new System.Windows.Forms.Label();
            this.lblFaxNumber = new System.Windows.Forms.Label();
            this.btnCreateUser = new System.Windows.Forms.Button();
            this.grpIndividual.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBroj)).BeginInit();
            this.grpLegalEntity.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(136, 40);
            this.txtIme.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtIme.MaxLength = 15;
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(89, 20);
            this.txtIme.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(27, 43);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(27, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Ime:";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Location = new System.Drawing.Point(27, 74);
            this.lblPrezime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(47, 13);
            this.lblPrezime.TabIndex = 2;
            this.lblPrezime.Text = "Prezime:";
            // 
            // lblAdresa
            // 
            this.lblAdresa.AutoSize = true;
            this.lblAdresa.Location = new System.Drawing.Point(27, 109);
            this.lblAdresa.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAdresa.Name = "lblAdresa";
            this.lblAdresa.Size = new System.Drawing.Size(43, 13);
            this.lblAdresa.TabIndex = 4;
            this.lblAdresa.Text = "Adresa:";
            // 
            // lblBroj
            // 
            this.lblBroj.AutoSize = true;
            this.lblBroj.Location = new System.Drawing.Point(27, 145);
            this.lblBroj.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBroj.Name = "lblBroj";
            this.lblBroj.Size = new System.Drawing.Size(28, 13);
            this.lblBroj.TabIndex = 6;
            this.lblBroj.Text = "Broj:";
            // 
            // lblGrad
            // 
            this.lblGrad.AutoSize = true;
            this.lblGrad.Location = new System.Drawing.Point(27, 181);
            this.lblGrad.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGrad.Name = "lblGrad";
            this.lblGrad.Size = new System.Drawing.Size(33, 13);
            this.lblGrad.TabIndex = 8;
            this.lblGrad.Text = "Grad:";
            // 
            // lblKonaktTelefon
            // 
            this.lblKonaktTelefon.AutoSize = true;
            this.lblKonaktTelefon.Location = new System.Drawing.Point(27, 217);
            this.lblKonaktTelefon.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblKonaktTelefon.Name = "lblKonaktTelefon";
            this.lblKonaktTelefon.Size = new System.Drawing.Size(82, 13);
            this.lblKonaktTelefon.TabIndex = 10;
            this.lblKonaktTelefon.Text = "Kontakt telefon:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(27, 253);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 12;
            this.lblEmail.Text = "E-mail:";
            // 
            // txtPrezime
            // 
            this.txtPrezime.Location = new System.Drawing.Point(136, 71);
            this.txtPrezime.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPrezime.MaxLength = 15;
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.Size = new System.Drawing.Size(89, 20);
            this.txtPrezime.TabIndex = 3;
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(136, 106);
            this.txtAdresa.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtAdresa.MaxLength = 30;
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(89, 20);
            this.txtAdresa.TabIndex = 5;
            // 
            // txtGrad
            // 
            this.txtGrad.Location = new System.Drawing.Point(136, 178);
            this.txtGrad.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtGrad.MaxLength = 15;
            this.txtGrad.Name = "txtGrad";
            this.txtGrad.Size = new System.Drawing.Size(89, 20);
            this.txtGrad.TabIndex = 9;
            // 
            // txtKonaktTelefon
            // 
            this.txtKonaktTelefon.Location = new System.Drawing.Point(136, 214);
            this.txtKonaktTelefon.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtKonaktTelefon.MaxLength = 10;
            this.txtKonaktTelefon.Name = "txtKonaktTelefon";
            this.txtKonaktTelefon.Size = new System.Drawing.Size(89, 20);
            this.txtKonaktTelefon.TabIndex = 11;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(136, 250);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(89, 20);
            this.txtEmail.TabIndex = 13;
            // 
            // grpIndividual
            // 
            this.grpIndividual.Controls.Add(this.txtJMBG);
            this.grpIndividual.Controls.Add(this.lblJmbg);
            this.grpIndividual.Location = new System.Drawing.Point(306, 11);
            this.grpIndividual.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpIndividual.Name = "grpIndividual";
            this.grpIndividual.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpIndividual.Size = new System.Drawing.Size(168, 72);
            this.grpIndividual.TabIndex = 1;
            this.grpIndividual.TabStop = false;
            this.grpIndividual.Text = "Fizičko lice";
            // 
            // txtJMBG
            // 
            this.txtJMBG.Location = new System.Drawing.Point(82, 27);
            this.txtJMBG.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtJMBG.MaxLength = 13;
            this.txtJMBG.Name = "txtJMBG";
            this.txtJMBG.Size = new System.Drawing.Size(76, 20);
            this.txtJMBG.TabIndex = 1;
            // 
            // lblJmbg
            // 
            this.lblJmbg.AutoSize = true;
            this.lblJmbg.Location = new System.Drawing.Point(4, 30);
            this.lblJmbg.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblJmbg.Name = "lblJmbg";
            this.lblJmbg.Size = new System.Drawing.Size(39, 13);
            this.lblJmbg.TabIndex = 0;
            this.lblJmbg.Text = "JMBG:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtEmail);
            this.groupBox2.Controls.Add(this.txtKonaktTelefon);
            this.groupBox2.Controls.Add(this.txtGrad);
            this.groupBox2.Controls.Add(this.numBroj);
            this.groupBox2.Controls.Add(this.txtAdresa);
            this.groupBox2.Controls.Add(this.txtPrezime);
            this.groupBox2.Controls.Add(this.lblEmail);
            this.groupBox2.Controls.Add(this.lblKonaktTelefon);
            this.groupBox2.Controls.Add(this.lblGrad);
            this.groupBox2.Controls.Add(this.lblBroj);
            this.groupBox2.Controls.Add(this.lblAdresa);
            this.groupBox2.Controls.Add(this.lblPrezime);
            this.groupBox2.Controls.Add(this.lblName);
            this.groupBox2.Controls.Add(this.txtIme);
            this.groupBox2.Location = new System.Drawing.Point(11, 11);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(256, 313);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opšte informacije korisnika";
            // 
            // numBroj
            // 
            this.numBroj.Location = new System.Drawing.Point(136, 143);
            this.numBroj.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numBroj.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numBroj.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numBroj.Name = "numBroj";
            this.numBroj.Size = new System.Drawing.Size(89, 20);
            this.numBroj.TabIndex = 7;
            this.numBroj.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // grpLegalEntity
            // 
            this.grpLegalEntity.Controls.Add(this.txtPIB);
            this.grpLegalEntity.Controls.Add(this.txtFaxNumber);
            this.grpLegalEntity.Controls.Add(this.lblPIB);
            this.grpLegalEntity.Controls.Add(this.lblFaxNumber);
            this.grpLegalEntity.Location = new System.Drawing.Point(306, 107);
            this.grpLegalEntity.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpLegalEntity.Name = "grpLegalEntity";
            this.grpLegalEntity.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpLegalEntity.Size = new System.Drawing.Size(168, 140);
            this.grpLegalEntity.TabIndex = 2;
            this.grpLegalEntity.TabStop = false;
            this.grpLegalEntity.Text = "Pravno lice";
            // 
            // txtPIB
            // 
            this.txtPIB.Location = new System.Drawing.Point(82, 86);
            this.txtPIB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPIB.MaxLength = 9;
            this.txtPIB.Name = "txtPIB";
            this.txtPIB.Size = new System.Drawing.Size(76, 20);
            this.txtPIB.TabIndex = 3;
            // 
            // txtFaxNumber
            // 
            this.txtFaxNumber.Location = new System.Drawing.Point(82, 46);
            this.txtFaxNumber.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtFaxNumber.MaxLength = 10;
            this.txtFaxNumber.Name = "txtFaxNumber";
            this.txtFaxNumber.Size = new System.Drawing.Size(76, 20);
            this.txtFaxNumber.TabIndex = 1;
            // 
            // lblPIB
            // 
            this.lblPIB.AutoSize = true;
            this.lblPIB.Location = new System.Drawing.Point(4, 89);
            this.lblPIB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPIB.Name = "lblPIB";
            this.lblPIB.Size = new System.Drawing.Size(27, 13);
            this.lblPIB.TabIndex = 2;
            this.lblPIB.Text = "PIB:";
            // 
            // lblFaxNumber
            // 
            this.lblFaxNumber.AutoSize = true;
            this.lblFaxNumber.Location = new System.Drawing.Point(4, 51);
            this.lblFaxNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFaxNumber.Name = "lblFaxNumber";
            this.lblFaxNumber.Size = new System.Drawing.Size(57, 13);
            this.lblFaxNumber.TabIndex = 0;
            this.lblFaxNumber.Text = "Broj faksa:";
            // 
            // btnCreateUser
            // 
            this.btnCreateUser.Location = new System.Drawing.Point(306, 274);
            this.btnCreateUser.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCreateUser.Name = "btnCreateUser";
            this.btnCreateUser.Size = new System.Drawing.Size(168, 50);
            this.btnCreateUser.TabIndex = 3;
            this.btnCreateUser.Text = "Kreirajte novog korisnika";
            this.btnCreateUser.UseVisualStyleBackColor = true;
            this.btnCreateUser.Click += new System.EventHandler(this.btnCreateUser_Click);
            // 
            // frmCreateUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 335);
            this.Controls.Add(this.btnCreateUser);
            this.Controls.Add(this.grpLegalEntity);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.grpIndividual);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCreateUser";
            this.Text = "Novi korisnik";
            this.Load += new System.EventHandler(this.frmCreateUser_Load);
            this.grpIndividual.ResumeLayout(false);
            this.grpIndividual.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBroj)).EndInit();
            this.grpLegalEntity.ResumeLayout(false);
            this.grpLegalEntity.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblPrezime;
        private System.Windows.Forms.Label lblAdresa;
        private System.Windows.Forms.Label lblBroj;
        private System.Windows.Forms.Label lblGrad;
        private System.Windows.Forms.Label lblKonaktTelefon;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtPrezime;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtGrad;
        private System.Windows.Forms.TextBox txtKonaktTelefon;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.GroupBox grpIndividual;
        private System.Windows.Forms.Label lblJmbg;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numBroj;
        private System.Windows.Forms.GroupBox grpLegalEntity;
        private System.Windows.Forms.Label lblPIB;
        private System.Windows.Forms.Label lblFaxNumber;
        private System.Windows.Forms.Button btnCreateUser;
        private System.Windows.Forms.TextBox txtJMBG;
        private System.Windows.Forms.TextBox txtPIB;
        private System.Windows.Forms.TextBox txtFaxNumber;
    }
}