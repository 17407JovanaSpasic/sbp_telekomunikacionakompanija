﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.User;

namespace TelekomunikacionaKompanija.Forms.User
{
    public partial class frmIndividual : Form
    {
        #region Attribute(s)
        private List<IndividualDTO> listOfIndividuals;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmIndividual()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void linkLabel1_MainFormClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void frmIndividual_Load(object sender, EventArgs e)
        {
            this.lstKorisnici.Items.Clear();
            this.listOfIndividuals = UserDTOManager.GetAllIndividualUsers();

            foreach (var i in listOfIndividuals)
            {
                var item = new ListViewItem(new string[] { i.Id.ToString(), i.Name, i.LastName, i.Address,
                        i.AddressNumber.ToString(), i.City, i.ContactPhone.ToString(), i.Email,
                        i.IndividualJMBG.ToString() });

                this.lstKorisnici.Items.Add(item);
            }
            this.lstKorisnici.Refresh();

            this.btnIzmeniKorisnika.Enabled = false;
            this.btnDeleteUser.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCreateUser newCreateForm = new frmCreateUser()
            {
                isItIndividual = true
            };

            newCreateForm.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Da li ste sigurni da želite da obrišete korisnika?",
                "Potvrda brisanja", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No)
                return;

            int usersId = int.Parse(this.lstKorisnici.SelectedItems[0].SubItems[0].Text);
            if (UserDTOManager.DeleteUser(usersId))
            {
                MessageBox.Show("Uspešno obrisan korisnik!", "Obaveštenje", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                var newFrm = new frmIndividual();
                newFrm.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Dogodila se greška prilikom brisanja korisnika, molimo Vas pokušajte operaciju kasnije!",
                    "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //getting indetification for user
            int id = int.Parse(this.lstKorisnici.SelectedItems[0].SubItems[0].Text);

            var newFrmUpdate = new frmUpdateUser()
            {
                id = id,
                individualFlag = true
            };
            newFrmUpdate.Show();

            this.Close();
        }

        private void lstKorisnici_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lstKorisnici.SelectedItems.Count == 0)
            {
                this.btnIzmeniKorisnika.Enabled = false;
                this.btnDeleteUser.Enabled = false;
            }
            else
            {
                this.btnIzmeniKorisnika.Enabled = true;
                this.btnDeleteUser.Enabled = true;
            }
        }
        #endregion Form Event Method(s)
    }
}
