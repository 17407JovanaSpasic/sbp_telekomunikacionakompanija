﻿using System;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.User;

namespace TelekomunikacionaKompanija.Forms.User
{
    public partial class frmUpdateUser : Form
    {
        #region Attribute(s)
        public int id;
        public bool individualFlag;

        //public IndividualDTO individualObj;
        //private LegalEntityDTO legalEntityObj;
        #endregion Attribute(s)

        #region Properties
        public int Id { get; set; }
        public bool IndividualFlag { get; set; }
        //public IndividualDTO IndividualObj {get; set;}
        //public LegalEntityDTO LegalEntityObj { get; set; }
        #endregion Properties

        #region Constructor(s)
        public frmUpdateUser()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmUpdateUser_Load(object sender, EventArgs e)
        {
            var userObj = UserDTOManager.GetUserByID(this.id);

            if (userObj != null)
            {
                this.lblSetIme.Text = userObj.Name;
                this.lblSetPrezime.Text = userObj.LastName;
                this.txtAdresa.Text = userObj.Address;
                this.numBroj.Value = userObj.AddressNumber;
                this.txtGrad.Text = userObj.City;
                this.txtKontaktTel.Text = userObj.ContactPhone;
                this.txtEmail.Text = userObj.Email;
            }
            if (this.individualFlag == true && userObj.Individual == 1)
            {
                this.grpPravnoLice.Visible = false;
                this.lblBrojFaksa.Visible = false;
                this.txtBrojFaksa.Visible = false;
                this.lblPIB.Visible = false;
                this.txtPIB.Visible = false;
            }
            else if (this.individualFlag == false && userObj.LegalEntity == 1)
            {
                this.txtBrojFaksa.Text = (userObj as LegalEntityDTO).LegalEntityFaxNumber;
                this.txtPIB.Text = (userObj as LegalEntityDTO).LegalEntityPIB;
            }
        }

        private void lblPocetnaStr_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            var userObj = UserDTOManager.GetUserByID(this.id);

            userObj.Address = this.txtAdresa.Text;
            userObj.AddressNumber = (int)this.numBroj.Value;
            userObj.City = this.txtGrad.Text;
            userObj.ContactPhone = this.txtKontaktTel.Text;
            userObj.Email = this.txtEmail.Text;

            if (userObj.LegalEntity == 1 && individualFlag == false)
            {
                (userObj as LegalEntityDTO).LegalEntityFaxNumber = this.txtBrojFaksa.Text;
                (userObj as LegalEntityDTO).LegalEntityPIB = this.txtPIB.Text;
            }
            if (!UserDTOManager.UpdateUser(userObj))
            {
                MessageBox.Show("Dogodila se greška prilikom izmene podataka korisnika, molimo Vas pokušajte sa izmenom kasnije.",
                    "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show("Uspešno ste izmenili podatke korisnika!", "Obaveštenje",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            if(userObj.LegalEntity==1)
            {
                var newLegalEntityfrm = new frmLegalEntity();
                newLegalEntityfrm.Show();
            }
            else
            {
                var newIndividualfrm = new frmIndividual();
                newIndividualfrm.Show();
            }

            this.Close();
        }
        #endregion Form Event Method(s)
    }
}
