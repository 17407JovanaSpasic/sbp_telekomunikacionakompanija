﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.User;

namespace TelekomunikacionaKompanija.Forms.User
{
    public partial class frmLegalEntity : Form
    {
        #region Attribute(s)
        private List<LegalEntityDTO> listOfLegalEntities;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmLegalEntity()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmLegalEntity_Load(object sender, EventArgs e)
        {
            this.lstPravnaLica.Items.Clear();
            this.listOfLegalEntities = UserDTOManager.GetAllLegalEntityUsers();

            foreach (var i in listOfLegalEntities)
            {
                var item = new ListViewItem(new string[] { i.Id.ToString(), i.Name, i.LastName, i.Address,
                        i.AddressNumber.ToString(), i.City, i.ContactPhone.ToString(), i.Email,
                        i.LegalEntityFaxNumber, i.LegalEntityPIB });

                this.lstPravnaLica.Items.Add(item);
            }
            this.lstPravnaLica.Refresh();

            this.btnIzmeniPravnoLice.Enabled = false;
            this.btnObrisiPravnoLice.Enabled = false;
        }

        private void btnDodajPravnoLice_Click(object sender, EventArgs e)
        {
            frmCreateUser newCreateForm = new frmCreateUser()
            {
                isItIndividual = false
            };
            newCreateForm.Show();
            this.Close();
        }

        private void btnObrisiPravnoLice_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Da li ste sigurni da želite da obrišete korisnika?",
                "Potvrda brisanja", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No)
                return;

            int usersId = int.Parse(this.lstPravnaLica.SelectedItems[0].SubItems[0].Text);
            if (UserDTOManager.DeleteUser(usersId))
            {
                MessageBox.Show("Uspešno obrisan korisnik!", "Obaveštenje", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                //uraditi logiku da se refresuje strana
                var newFrm = new frmLegalEntity();
                newFrm.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Dogodila se greška prilikom brisanja korisnika, molimo Vas pokušajte operaciju kasnije!",
                    "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void lblIzmeniPravnoLice_Click(object sender, EventArgs e)
        {
            //getting indetification for user
            int id = int.Parse(this.lstPravnaLica.SelectedItems[0].SubItems[0].Text);

            var newFrmUpdate = new frmUpdateUser()
            {
                id = id,
                individualFlag = false
            };
            newFrmUpdate.Show();

            this.Close();
        }

        private void lblPovratakNaPocetnuStranu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void lstPravnaLica_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lstPravnaLica.SelectedItems.Count == 0)
            {
                this.btnIzmeniPravnoLice.Enabled = false;
                this.btnObrisiPravnoLice.Enabled = false;
            }
            else
            {
                this.btnIzmeniPravnoLice.Enabled = true;
                this.btnObrisiPravnoLice.Enabled = true;
            }
        }
        #endregion Form Event Method(s)
    }
}
