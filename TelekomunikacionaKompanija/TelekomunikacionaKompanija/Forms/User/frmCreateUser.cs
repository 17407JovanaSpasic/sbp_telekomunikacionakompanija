﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.User;

namespace TelekomunikacionaKompanija.Forms.User
{
    public partial class frmCreateUser : Form
    {
        #region Attribute(s)
        public bool isItIndividual;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmCreateUser()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmCreateUser_Load(object sender, EventArgs e)
        {
            if (isItIndividual == false)
            {
                //THIS BLOCK IS FOR LEGAL ENTITY
                this.lblJmbg.ForeColor = Color.Gray;
                this.grpIndividual.ForeColor = Color.Gray;
                this.txtJMBG.Enabled = false;
                this.Refresh();
            }
            else
            {
                //THIS BLOCK IS FOR INDIVIDUAL PERSONS
                this.lblFaxNumber.ForeColor = Color.Gray;
                this.txtFaxNumber.Enabled = false;
                this.lblPIB.ForeColor = Color.Gray;
                this.txtPIB.Enabled = false;
                this.grpLegalEntity.ForeColor = Color.Gray;
                this.Refresh();
            }
        }

        private void btnCreateUser_Click(object sender, EventArgs e)
        {
            if (this.txtIme.Text == string.Empty || this.txtPrezime.Text == string.Empty ||
                this.txtAdresa.Text == string.Empty || this.txtGrad.Text == string.Empty ||
                this.txtKonaktTelefon.Text == string.Empty || this.numBroj.Value <= 0)
            {
                MessageBox.Show("Proverite ispravnost unetih opštih podataka, ne možete ostaviti prazna polja"
                    + " odnosno negativnu vrednost za adresu.", "Upozorenje", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            string name = this.txtIme.Text;
            string lname = this.txtPrezime.Text;
            string address = this.txtAdresa.Text;
            int addressNumber = (int)this.numBroj.Value;
            string city = this.txtGrad.Text;
            string contactPhone = this.txtKonaktTelefon.Text;
            string email = this.txtEmail.Text;

            if(UserDTOManager.CheckIfExists(contactPhone)== true)
            {
                MessageBox.Show("Pokusali ste da kreirate korisnika koji vec postoji!.", "Upozorenje", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                this.Close();
                return;
            }

            //Legal Entity
            if (isItIndividual == false)
            {
                string faxNumber = this.txtFaxNumber.Text;
                string pib = this.txtPIB.Text;

                var legalEntityObj = new LegalEntityDTO(name, lname, address, addressNumber, city,
                    contactPhone, email, faxNumber, pib);

                if (DTOManagers.UserDTOManager.CreateLegalEntityUser(legalEntityObj))
                {
                    MessageBox.Show("Uspešno kreiran korisnik!", "Obaveštenje", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    var legalEntityForm = new frmLegalEntity();

                    legalEntityForm.Show();

                    this.Close();
                }
            }
            else
            {
                string jmbg = this.txtJMBG.Text;
                var individualObj = new IndividualDTO(name, lname, address, addressNumber, city,
                    contactPhone, email, jmbg);

                if (DTOManagers.UserDTOManager.CreateIndividualUser(individualObj))
                {
                    MessageBox.Show("Uspešno kreiran korisnik!", "Obaveštenje", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    var individualForm = new frmIndividual();

                    individualForm.Show();

                    this.Close();
                }
            }
        }
        #endregion Form Event Method(s)
    }
}
