﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOs.Device;
using TelekomunikacionaKompanija.DTOs.Manufacturer;
using TelekomunikacionaKompanija.DTOManagers;

namespace TelekomunikacionaKompanija.Forms.Device.MainStation
{
    public partial class frmNewMainStation : Form
    {
        #region Attribute(s)
        private List<ManufacturerDTO> manufacturerList;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmNewMainStation()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmNewMainStation_Load(object sender, EventArgs e)
        {
            //Manufacturers
            this.lstManufacturers.Items.Clear();

            this.manufacturerList = ManufacturerDTOManager.GetAllManufactures();
            foreach (var i in manufacturerList)
            {
                var item = new ListViewItem(new string[] { i.PIB, i.Name });

                this.lstManufacturers.Items.Add(item);
            }
            this.lstManufacturers.Refresh();
        }
        private void btnAddNewMainStation_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lstManufacturers.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Morate selektovati proizvođača!", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (this.txtSerijskiBroj.Text == string.Empty || this.txtReason.Text == string.Empty)
                {
                    MessageBox.Show("Morate popuniti sva polja", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var session = DataLayer.GetSession();
                string pib = this.lstManufacturers.SelectedItems[0].SubItems[0].Text;
                var Pib = session.Get<TelekomunikacionaKompanija.Entities.Manufacturer>(pib);

                string serialNumber = this.txtSerijskiBroj.Text;
                DateTime startDate = this.dtpStartOfUseDate.Value;
                DateTime serviceDate = this.dtpServiceDate.Value;
                string reason = this.txtReason.Text;
                int numOfNodes = (int)this.numNumberOfNodes.Value;

                var newMainStation = new MainStationDTO(serialNumber, startDate, serviceDate, reason, Pib, numOfNodes, 0);

                if (DTOManagers.DeviceDTOManager.CreateMainStation(newMainStation, Pib))
                {
                    MessageBox.Show("Uspešno ste kreirali glavnu stanicu", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Glavna stanica nije kreirana, pokušajte sa drugim serijskim brojem!", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion Form Event Method(s)
    }
}

