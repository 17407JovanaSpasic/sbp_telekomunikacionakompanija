﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.Device;
using TelekomunikacionaKompanija.Forms.Device.RegionalHub;
using TelekomunikacionaKompanija.Forms.Device.MainStation;
using TelekomunikacionaKompanija.Forms.Device.CommunicationNode;

namespace TelekomunikacionaKompanija.Forms.Device
{
    public partial class frmDevice : Form
    {
        #region Attribute(s)
        private List<DeviceDTO> devicesList;
        private List<MainStationDTO> mainList;
        private List<CommunicationNodeDTO> commList;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmDevice()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Method(s)
        private void RefreshDeviceList()
        {
            this.lstDevices.Items.Clear();
            this.devicesList = DeviceDTOManager.GetAllDevices();

            foreach (var devices in devicesList)
            {
                var item = new ListViewItem(new string[]
                {
                    devices.SerialNumber,
                    devices.StartOfUseDate.ToString(),
                    devices.ServiceDate.ToString(),
                    devices.ReasonForServicing,
                    devices.Pib,
                    devices.NameP
                });
                this.lstDevices.Items.Add(item);
            }
            this.lstDevices.Refresh();
        }

        private void RefreshMainStationList()
        {
            this.lstMainStations.Items.Clear();

            this.mainList = DeviceDTOManager.GetAllMainStations();
            foreach (var i in mainList)
            {
                var item = new ListViewItem(new string[] {
                    i.SerialNumber,
                    i.NumberOfNodes.ToString()
                });

                this.lstMainStations.Items.Add(item);
            }
            this.lstMainStations.Refresh();
        }

        private void RefreshCommunicationNodeList()
        {
            this.lstCommunicationNodes.Items.Clear();

            this.commList = DeviceDTOManager.GetAllCommNodes();
            foreach (var i in commList)
            {
                var item = new ListViewItem(new string[] {
                    i.SerialNumber,
                    i.Address,
                    i.LocationNumber.ToString(),
                    i.Description
                });

                this.lstCommunicationNodes.Items.Add(item);
            }
            this.lstCommunicationNodes.Refresh();
        }
        #endregion Method(s)

        #region Form Event Method(s)
        private void linkMainForm_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }
        private void frmDevice_Load(object sender, EventArgs e)
        {
            this.RefreshDeviceList();
            this.RefreshMainStationList();
            this.RefreshCommunicationNodeList();
        }
        private void btnRegionalHubs_Click(object sender, EventArgs e)
        {
            frmRegionalHub regionalForm = new frmRegionalHub();
            regionalForm.ShowDialog();
            this.RefreshDeviceList();
            this.RefreshMainStationList();
        }
        private void btnNewMainStation_Click(object sender, EventArgs e)
        {
            frmNewMainStation mainStationForm = new frmNewMainStation();
            mainStationForm.ShowDialog();
            this.RefreshDeviceList();
            this.RefreshMainStationList();
        }
        private void btnDeleteMainStation_Click(object sender, EventArgs e)
        {
            if (this.lstMainStations.SelectedItems.Count == 0)
            {
                MessageBox.Show("Morate selektovati glavnu stanicu!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            DialogResult result = MessageBox.Show("Da li ste sigurni da želite da obrišete glavnu stanicu?", "Upozorenje", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.No)
                return;

            string serialNumber = this.lstMainStations.SelectedItems[0].SubItems[0].Text;
            if (DeviceDTOManager.DeleteDevice(serialNumber))
            {
                MessageBox.Show("Uspešno obrisana glavna stanica!", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.RefreshDeviceList();
                this.RefreshMainStationList();
            }
            else
            {
                MessageBox.Show("Glavna stanica nije obrisana!", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnNewCommunicationNode_Click(object sender, EventArgs e)
        {
            frmNewCommunicationNode commForm = new frmNewCommunicationNode();
            commForm.ShowDialog();
            this.RefreshDeviceList();
            this.RefreshCommunicationNodeList();
        }
        private void btnDeleteCommNode_Click(object sender, EventArgs e)
        {
            if (this.lstCommunicationNodes.SelectedItems.Count == 0)
            {
                MessageBox.Show("Morate selektovati komunikacioni čvor!", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            DialogResult result = MessageBox.Show("Da li ste sigurni da želite da obrišete komunikacioni čvor?", "Upozorenje", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.No)
                return;

            string serialNumber = this.lstCommunicationNodes.SelectedItems[0].SubItems[0].Text;
            if (DeviceDTOManager.DeleteDevice(serialNumber))
            {
                MessageBox.Show("Uspešno obrisan komunikacioni čvor!", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.RefreshDeviceList();
                this.RefreshCommunicationNodeList();
            }
            else
            {
                MessageBox.Show("Komunikacioni čvor nije obrisan!", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void cboSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lstMainStations.Items.Clear();

            var filteredMainStationList = new List<MainStationDTO>();

            switch (this.cboSelect.SelectedIndex)
            {
                case 0:
                    filteredMainStationList = this.mainList;
                    break;
                case 1:
                    filteredMainStationList = this.mainList.Where(x => x.NumberOfNodes <= 1).ToList();
                    break;
                default:
                    filteredMainStationList = this.mainList.Where(x => x.NumberOfNodes > 1).ToList();
                    break;
            }
            foreach (var i in filteredMainStationList)
            {
                var item = new ListViewItem(new string[] {
                    i.SerialNumber,
                    i.NumberOfNodes.ToString()
            });

                this.lstMainStations.Items.Add(item);
            }

            this.lstMainStations.Refresh();
        }
        #endregion Form Event Method(s)
    }
}
