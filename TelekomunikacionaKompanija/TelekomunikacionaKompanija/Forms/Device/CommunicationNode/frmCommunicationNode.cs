﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelekomunikacionaKompanija.DTOs.Device;

namespace TelekomunikacionaKompanija.Forms.Device.CommunicationNode
{
    public partial class frmNewCommunicationNode : Form
    {
        #region Attribute(s)
        private List<TelekomunikacionaKompanija.DTOs.Manufacturer.ManufacturerDTO> manufacturerList;
        private List<TelekomunikacionaKompanija.DTOs.Device.MainStationDTO> mainStationList;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmNewCommunicationNode()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmNewCommunicationNode_Load(object sender, EventArgs e)
        {
            //Manufacturers
            this.lstManufacturers.Items.Clear();

            this.manufacturerList = TelekomunikacionaKompanija.DTOManagers.ManufacturerDTOManager.GetAllManufactures();
            foreach (var i in manufacturerList)
            {
                var item = new ListViewItem(new string[] { i.PIB, i.Name });

                this.lstManufacturers.Items.Add(item);
            }
            this.lstManufacturers.Refresh();

            //Main Stations
            this.lstMainStation.Items.Clear();

            this.mainStationList = TelekomunikacionaKompanija.DTOManagers.DeviceDTOManager.GetAllMainStations();
            foreach (var i in mainStationList)
            {
                var item = new ListViewItem(new string[] {
                    i.SerialNumber,
                    i.NumberOfNodes.ToString()
                });

                this.lstMainStation.Items.Add(item);
            }
            this.lstMainStation.Refresh();
        }
        private void btnAddNewCommunicationNode_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lstManufacturers.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Morate selektovati proizvođača!", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (this.lstMainStation.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Morate selektovati glavnu stanicu!", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (this.txtSerijskiBroj.Text == string.Empty ||
                    this.txtAddress.Text == string.Empty || this.txtDescription.Text == string.Empty)
                {
                    MessageBox.Show("Morate popuniti sva polja", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var session = DataLayer.GetSession();

                string pib = this.lstManufacturers.SelectedItems[0].SubItems[0].Text;
                var Pib = session.Get<TelekomunikacionaKompanija.Entities.Manufacturer>(pib);

                string serialnum = this.lstMainStation.SelectedItems[0].SubItems[0].Text;
                var serialNum = session.Get<TelekomunikacionaKompanija.Entities.MainStation>(serialnum);

                string serialNumber = this.txtSerijskiBroj.Text;
                DateTime startDate = this.dtpStartOfUseDate.Value;
                DateTime serviceDate = this.dtpServiceDate.Value;
                string reason = this.txtReason.Text;
                string address = this.txtAddress.Text;
                int locationNumber = (int)this.numLocationNumber.Value;
                string description = this.txtDescription.Text;

                var newCommNode = new CommunicationNodeDTO(serialNumber, startDate, serviceDate, reason, Pib, address, locationNumber, description, serialNum);

                if (DTOManagers.DeviceDTOManager.CreateCommunicationNode(newCommNode, Pib, serialNum))
                {
                    MessageBox.Show("Uspesno ste kreirali komunikacioni čvor", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Komunikacioni čvor nije kreiran, pokušajte sa drugim serijskim brojem!", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion Form Event Method(s)
    }
}
