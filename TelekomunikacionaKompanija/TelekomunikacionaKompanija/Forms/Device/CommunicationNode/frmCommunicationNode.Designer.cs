﻿
namespace TelekomunikacionaKompanija.Forms.Device.CommunicationNode
{
    partial class frmNewCommunicationNode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddNewCommunicationNode = new System.Windows.Forms.Button();
            this.dtpServiceDate = new System.Windows.Forms.DateTimePicker();
            this.dtpStartOfUseDate = new System.Windows.Forms.DateTimePicker();
            this.txtReason = new System.Windows.Forms.TextBox();
            this.txtSerijskiBroj = new System.Windows.Forms.TextBox();
            this.lblReasonForServicing = new System.Windows.Forms.Label();
            this.lblServiceDate = new System.Windows.Forms.Label();
            this.lblStartOfUseDate = new System.Windows.Forms.Label();
            this.lblSerialNumber = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblLocationNumber = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.numLocationNumber = new System.Windows.Forms.NumericUpDown();
            this.lstManufacturers = new System.Windows.Forms.ListView();
            this.columnHeaderPIB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblManufacturer = new System.Windows.Forms.Label();
            this.lstMainStation = new System.Windows.Forms.ListView();
            this.columnHeaderSerialNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderNumberOfNodes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblMainStation = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numLocationNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddNewCommunicationNode
            // 
            this.btnAddNewCommunicationNode.Location = new System.Drawing.Point(182, 288);
            this.btnAddNewCommunicationNode.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAddNewCommunicationNode.Name = "btnAddNewCommunicationNode";
            this.btnAddNewCommunicationNode.Size = new System.Drawing.Size(74, 37);
            this.btnAddNewCommunicationNode.TabIndex = 34;
            this.btnAddNewCommunicationNode.Text = "Dodaj";
            this.btnAddNewCommunicationNode.UseVisualStyleBackColor = true;
            this.btnAddNewCommunicationNode.Click += new System.EventHandler(this.btnAddNewCommunicationNode_Click);
            // 
            // dtpServiceDate
            // 
            this.dtpServiceDate.Location = new System.Drawing.Point(171, 86);
            this.dtpServiceDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpServiceDate.Name = "dtpServiceDate";
            this.dtpServiceDate.Size = new System.Drawing.Size(151, 20);
            this.dtpServiceDate.TabIndex = 33;
            // 
            // dtpStartOfUseDate
            // 
            this.dtpStartOfUseDate.Location = new System.Drawing.Point(171, 55);
            this.dtpStartOfUseDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpStartOfUseDate.Name = "dtpStartOfUseDate";
            this.dtpStartOfUseDate.Size = new System.Drawing.Size(151, 20);
            this.dtpStartOfUseDate.TabIndex = 32;
            // 
            // txtReason
            // 
            this.txtReason.Location = new System.Drawing.Point(171, 117);
            this.txtReason.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtReason.MaxLength = 1000;
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(151, 45);
            this.txtReason.TabIndex = 31;
            // 
            // txtSerijskiBroj
            // 
            this.txtSerijskiBroj.Location = new System.Drawing.Point(171, 25);
            this.txtSerijskiBroj.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtSerijskiBroj.MaxLength = 10;
            this.txtSerijskiBroj.Name = "txtSerijskiBroj";
            this.txtSerijskiBroj.Size = new System.Drawing.Size(102, 20);
            this.txtSerijskiBroj.TabIndex = 30;
            // 
            // lblReasonForServicing
            // 
            this.lblReasonForServicing.AutoSize = true;
            this.lblReasonForServicing.Location = new System.Drawing.Point(22, 117);
            this.lblReasonForServicing.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReasonForServicing.Name = "lblReasonForServicing";
            this.lblReasonForServicing.Size = new System.Drawing.Size(98, 13);
            this.lblReasonForServicing.TabIndex = 28;
            this.lblReasonForServicing.Text = "Razlog servisiranja:";
            // 
            // lblServiceDate
            // 
            this.lblServiceDate.AutoSize = true;
            this.lblServiceDate.Location = new System.Drawing.Point(22, 86);
            this.lblServiceDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblServiceDate.Name = "lblServiceDate";
            this.lblServiceDate.Size = new System.Drawing.Size(96, 13);
            this.lblServiceDate.TabIndex = 27;
            this.lblServiceDate.Text = "Datum servisiranja:";
            // 
            // lblStartOfUseDate
            // 
            this.lblStartOfUseDate.AutoSize = true;
            this.lblStartOfUseDate.Location = new System.Drawing.Point(22, 55);
            this.lblStartOfUseDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStartOfUseDate.Name = "lblStartOfUseDate";
            this.lblStartOfUseDate.Size = new System.Drawing.Size(101, 13);
            this.lblStartOfUseDate.TabIndex = 26;
            this.lblStartOfUseDate.Text = "Početak korišćenja:";
            // 
            // lblSerialNumber
            // 
            this.lblSerialNumber.AutoSize = true;
            this.lblSerialNumber.Location = new System.Drawing.Point(22, 25);
            this.lblSerialNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(63, 13);
            this.lblSerialNumber.TabIndex = 25;
            this.lblSerialNumber.Text = "Serijski broj:";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(22, 175);
            this.lblAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(43, 13);
            this.lblAddress.TabIndex = 35;
            this.lblAddress.Text = "Adresa:";
            // 
            // lblLocationNumber
            // 
            this.lblLocationNumber.AutoSize = true;
            this.lblLocationNumber.Location = new System.Drawing.Point(22, 206);
            this.lblLocationNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLocationNumber.Name = "lblLocationNumber";
            this.lblLocationNumber.Size = new System.Drawing.Size(67, 13);
            this.lblLocationNumber.TabIndex = 36;
            this.lblLocationNumber.Text = "Broj lokacije:";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(22, 244);
            this.lblDescription.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(31, 13);
            this.lblDescription.TabIndex = 37;
            this.lblDescription.Text = "Opis:";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(171, 175);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtAddress.MaxLength = 15;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(102, 20);
            this.txtAddress.TabIndex = 38;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(171, 240);
            this.txtDescription.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtDescription.MaxLength = 1000;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(102, 20);
            this.txtDescription.TabIndex = 39;
            // 
            // numLocationNumber
            // 
            this.numLocationNumber.Location = new System.Drawing.Point(171, 206);
            this.numLocationNumber.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numLocationNumber.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numLocationNumber.Name = "numLocationNumber";
            this.numLocationNumber.Size = new System.Drawing.Size(100, 20);
            this.numLocationNumber.TabIndex = 40;
            // 
            // lstManufacturers
            // 
            this.lstManufacturers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderPIB,
            this.columnHeaderName});
            this.lstManufacturers.FullRowSelect = true;
            this.lstManufacturers.GridLines = true;
            this.lstManufacturers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstManufacturers.HideSelection = false;
            this.lstManufacturers.Location = new System.Drawing.Point(342, 58);
            this.lstManufacturers.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lstManufacturers.Name = "lstManufacturers";
            this.lstManufacturers.Size = new System.Drawing.Size(247, 104);
            this.lstManufacturers.TabIndex = 42;
            this.lstManufacturers.UseCompatibleStateImageBehavior = false;
            this.lstManufacturers.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderPIB
            // 
            this.columnHeaderPIB.Text = "PIB";
            this.columnHeaderPIB.Width = 90;
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Naziv proizvođača";
            this.columnHeaderName.Width = 130;
            // 
            // lblManufacturer
            // 
            this.lblManufacturer.AutoSize = true;
            this.lblManufacturer.Location = new System.Drawing.Point(340, 25);
            this.lblManufacturer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblManufacturer.Name = "lblManufacturer";
            this.lblManufacturer.Size = new System.Drawing.Size(116, 13);
            this.lblManufacturer.TabIndex = 41;
            this.lblManufacturer.Text = "Odaberite prozvođača:";
            // 
            // lstMainStation
            // 
            this.lstMainStation.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderSerialNumber,
            this.columnHeaderNumberOfNodes});
            this.lstMainStation.FullRowSelect = true;
            this.lstMainStation.GridLines = true;
            this.lstMainStation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstMainStation.HideSelection = false;
            this.lstMainStation.Location = new System.Drawing.Point(342, 208);
            this.lstMainStation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lstMainStation.Name = "lstMainStation";
            this.lstMainStation.Size = new System.Drawing.Size(247, 92);
            this.lstMainStation.TabIndex = 44;
            this.lstMainStation.UseCompatibleStateImageBehavior = false;
            this.lstMainStation.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderSerialNumber
            // 
            this.columnHeaderSerialNumber.Text = "Serijski broj";
            this.columnHeaderSerialNumber.Width = 90;
            // 
            // columnHeaderNumberOfNodes
            // 
            this.columnHeaderNumberOfNodes.Text = "Broj čvorova";
            this.columnHeaderNumberOfNodes.Width = 130;
            // 
            // lblMainStation
            // 
            this.lblMainStation.AutoSize = true;
            this.lblMainStation.Location = new System.Drawing.Point(340, 175);
            this.lblMainStation.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMainStation.Name = "lblMainStation";
            this.lblMainStation.Size = new System.Drawing.Size(128, 13);
            this.lblMainStation.TabIndex = 43;
            this.lblMainStation.Text = "Odaberite glavnu stanicu:";
            // 
            // frmNewCommunicationNode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 342);
            this.Controls.Add(this.lstMainStation);
            this.Controls.Add(this.lblMainStation);
            this.Controls.Add(this.lstManufacturers);
            this.Controls.Add(this.lblManufacturer);
            this.Controls.Add(this.numLocationNumber);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblLocationNumber);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.btnAddNewCommunicationNode);
            this.Controls.Add(this.dtpServiceDate);
            this.Controls.Add(this.dtpStartOfUseDate);
            this.Controls.Add(this.txtReason);
            this.Controls.Add(this.txtSerijskiBroj);
            this.Controls.Add(this.lblReasonForServicing);
            this.Controls.Add(this.lblServiceDate);
            this.Controls.Add(this.lblStartOfUseDate);
            this.Controls.Add(this.lblSerialNumber);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewCommunicationNode";
            this.Text = "Novi komunikacioni čvor";
            this.Load += new System.EventHandler(this.frmNewCommunicationNode_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numLocationNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnAddNewCommunicationNode;
        private System.Windows.Forms.DateTimePicker dtpServiceDate;
        private System.Windows.Forms.DateTimePicker dtpStartOfUseDate;
        private System.Windows.Forms.TextBox txtReason;
        private System.Windows.Forms.TextBox txtSerijskiBroj;
        private System.Windows.Forms.Label lblReasonForServicing;
        private System.Windows.Forms.Label lblServiceDate;
        private System.Windows.Forms.Label lblStartOfUseDate;
        private System.Windows.Forms.Label lblSerialNumber;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblLocationNumber;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.NumericUpDown numLocationNumber;
        private System.Windows.Forms.ListView lstManufacturers;
        private System.Windows.Forms.ColumnHeader columnHeaderPIB;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.Label lblManufacturer;
        private System.Windows.Forms.ListView lstMainStation;
        private System.Windows.Forms.ColumnHeader columnHeaderSerialNumber;
        private System.Windows.Forms.ColumnHeader columnHeaderNumberOfNodes;
        private System.Windows.Forms.Label lblMainStation;
    }
}