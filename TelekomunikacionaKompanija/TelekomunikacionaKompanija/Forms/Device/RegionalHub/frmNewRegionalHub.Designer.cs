﻿
namespace TelekomunikacionaKompanija.Forms.Device.RegionalHub
{
    partial class frmNewRegionalHub
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSerialNumber = new System.Windows.Forms.Label();
            this.lblStartOfUseDate = new System.Windows.Forms.Label();
            this.lblServiceDate = new System.Windows.Forms.Label();
            this.lblReasonForServicing = new System.Windows.Forms.Label();
            this.lblNumberOfNodes = new System.Windows.Forms.Label();
            this.lblNameOfRegion = new System.Windows.Forms.Label();
            this.txtSerijskiBroj = new System.Windows.Forms.TextBox();
            this.txtReason = new System.Windows.Forms.TextBox();
            this.txtNameOfRegion = new System.Windows.Forms.TextBox();
            this.dtpStartOfUseDate = new System.Windows.Forms.DateTimePicker();
            this.dtpServiceDate = new System.Windows.Forms.DateTimePicker();
            this.btnAddNewRegionalHub = new System.Windows.Forms.Button();
            this.numNumberOfNodes = new System.Windows.Forms.NumericUpDown();
            this.lstManufacturers = new System.Windows.Forms.ListView();
            this.columnHeaderPIB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblManufacturer = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numNumberOfNodes)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSerialNumber
            // 
            this.lblSerialNumber.AutoSize = true;
            this.lblSerialNumber.Location = new System.Drawing.Point(32, 32);
            this.lblSerialNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(63, 13);
            this.lblSerialNumber.TabIndex = 0;
            this.lblSerialNumber.Text = "Serijski broj:";
            // 
            // lblStartOfUseDate
            // 
            this.lblStartOfUseDate.AutoSize = true;
            this.lblStartOfUseDate.Location = new System.Drawing.Point(32, 62);
            this.lblStartOfUseDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStartOfUseDate.Name = "lblStartOfUseDate";
            this.lblStartOfUseDate.Size = new System.Drawing.Size(101, 13);
            this.lblStartOfUseDate.TabIndex = 1;
            this.lblStartOfUseDate.Text = "Pocetak korišćenja:";
            // 
            // lblServiceDate
            // 
            this.lblServiceDate.AutoSize = true;
            this.lblServiceDate.Location = new System.Drawing.Point(32, 93);
            this.lblServiceDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblServiceDate.Name = "lblServiceDate";
            this.lblServiceDate.Size = new System.Drawing.Size(96, 13);
            this.lblServiceDate.TabIndex = 2;
            this.lblServiceDate.Text = "Datum servisiranja:";
            // 
            // lblReasonForServicing
            // 
            this.lblReasonForServicing.AutoSize = true;
            this.lblReasonForServicing.Location = new System.Drawing.Point(32, 124);
            this.lblReasonForServicing.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReasonForServicing.Name = "lblReasonForServicing";
            this.lblReasonForServicing.Size = new System.Drawing.Size(98, 13);
            this.lblReasonForServicing.TabIndex = 3;
            this.lblReasonForServicing.Text = "Razlog servisiranja:";
            // 
            // lblNumberOfNodes
            // 
            this.lblNumberOfNodes.AutoSize = true;
            this.lblNumberOfNodes.Location = new System.Drawing.Point(32, 188);
            this.lblNumberOfNodes.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumberOfNodes.Name = "lblNumberOfNodes";
            this.lblNumberOfNodes.Size = new System.Drawing.Size(70, 13);
            this.lblNumberOfNodes.TabIndex = 4;
            this.lblNumberOfNodes.Text = "Broj čvorova:";
            // 
            // lblNameOfRegion
            // 
            this.lblNameOfRegion.AutoSize = true;
            this.lblNameOfRegion.Location = new System.Drawing.Point(32, 224);
            this.lblNameOfRegion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNameOfRegion.Name = "lblNameOfRegion";
            this.lblNameOfRegion.Size = new System.Drawing.Size(75, 13);
            this.lblNameOfRegion.TabIndex = 5;
            this.lblNameOfRegion.Text = "Naziv regiona:";
            // 
            // txtSerijskiBroj
            // 
            this.txtSerijskiBroj.Location = new System.Drawing.Point(181, 32);
            this.txtSerijskiBroj.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtSerijskiBroj.MaxLength = 10;
            this.txtSerijskiBroj.Name = "txtSerijskiBroj";
            this.txtSerijskiBroj.Size = new System.Drawing.Size(102, 20);
            this.txtSerijskiBroj.TabIndex = 6;
            // 
            // txtReason
            // 
            this.txtReason.Location = new System.Drawing.Point(181, 124);
            this.txtReason.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtReason.MaxLength = 1000;
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(151, 45);
            this.txtReason.TabIndex = 8;
            // 
            // txtNameOfRegion
            // 
            this.txtNameOfRegion.Location = new System.Drawing.Point(181, 224);
            this.txtNameOfRegion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtNameOfRegion.MaxLength = 50;
            this.txtNameOfRegion.Name = "txtNameOfRegion";
            this.txtNameOfRegion.Size = new System.Drawing.Size(102, 20);
            this.txtNameOfRegion.TabIndex = 9;
            // 
            // dtpStartOfUseDate
            // 
            this.dtpStartOfUseDate.Location = new System.Drawing.Point(181, 62);
            this.dtpStartOfUseDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpStartOfUseDate.Name = "dtpStartOfUseDate";
            this.dtpStartOfUseDate.Size = new System.Drawing.Size(151, 20);
            this.dtpStartOfUseDate.TabIndex = 10;
            // 
            // dtpServiceDate
            // 
            this.dtpServiceDate.Location = new System.Drawing.Point(181, 93);
            this.dtpServiceDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpServiceDate.Name = "dtpServiceDate";
            this.dtpServiceDate.Size = new System.Drawing.Size(151, 20);
            this.dtpServiceDate.TabIndex = 11;
            // 
            // btnAddNewRegionalHub
            // 
            this.btnAddNewRegionalHub.Location = new System.Drawing.Point(434, 224);
            this.btnAddNewRegionalHub.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAddNewRegionalHub.Name = "btnAddNewRegionalHub";
            this.btnAddNewRegionalHub.Size = new System.Drawing.Size(74, 37);
            this.btnAddNewRegionalHub.TabIndex = 12;
            this.btnAddNewRegionalHub.Text = "Dodaj";
            this.btnAddNewRegionalHub.UseVisualStyleBackColor = true;
            this.btnAddNewRegionalHub.Click += new System.EventHandler(this.btnAddNewRegionalHub_Click);
            // 
            // numNumberOfNodes
            // 
            this.numNumberOfNodes.Location = new System.Drawing.Point(181, 188);
            this.numNumberOfNodes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numNumberOfNodes.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numNumberOfNodes.Name = "numNumberOfNodes";
            this.numNumberOfNodes.Size = new System.Drawing.Size(100, 20);
            this.numNumberOfNodes.TabIndex = 13;
            // 
            // lstManufacturers
            // 
            this.lstManufacturers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderPIB,
            this.columnHeaderName});
            this.lstManufacturers.FullRowSelect = true;
            this.lstManufacturers.GridLines = true;
            this.lstManufacturers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstManufacturers.HideSelection = false;
            this.lstManufacturers.Location = new System.Drawing.Point(352, 65);
            this.lstManufacturers.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lstManufacturers.Name = "lstManufacturers";
            this.lstManufacturers.Size = new System.Drawing.Size(247, 142);
            this.lstManufacturers.TabIndex = 28;
            this.lstManufacturers.UseCompatibleStateImageBehavior = false;
            this.lstManufacturers.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderPIB
            // 
            this.columnHeaderPIB.Text = "PIB";
            this.columnHeaderPIB.Width = 90;
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Naziv proizvođača";
            this.columnHeaderName.Width = 130;
            // 
            // lblManufacturer
            // 
            this.lblManufacturer.AutoSize = true;
            this.lblManufacturer.Location = new System.Drawing.Point(350, 32);
            this.lblManufacturer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblManufacturer.Name = "lblManufacturer";
            this.lblManufacturer.Size = new System.Drawing.Size(116, 13);
            this.lblManufacturer.TabIndex = 27;
            this.lblManufacturer.Text = "Odaberite prozvođača:";
            // 
            // frmNewRegionalHub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 281);
            this.Controls.Add(this.lstManufacturers);
            this.Controls.Add(this.lblManufacturer);
            this.Controls.Add(this.numNumberOfNodes);
            this.Controls.Add(this.btnAddNewRegionalHub);
            this.Controls.Add(this.dtpServiceDate);
            this.Controls.Add(this.dtpStartOfUseDate);
            this.Controls.Add(this.txtNameOfRegion);
            this.Controls.Add(this.txtReason);
            this.Controls.Add(this.txtSerijskiBroj);
            this.Controls.Add(this.lblNameOfRegion);
            this.Controls.Add(this.lblNumberOfNodes);
            this.Controls.Add(this.lblReasonForServicing);
            this.Controls.Add(this.lblServiceDate);
            this.Controls.Add(this.lblStartOfUseDate);
            this.Controls.Add(this.lblSerialNumber);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frmNewRegionalHub";
            this.Text = "Novi regionalni hub";
            this.Load += new System.EventHandler(this.frmNewRegionalHub_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numNumberOfNodes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSerialNumber;
        private System.Windows.Forms.Label lblStartOfUseDate;
        private System.Windows.Forms.Label lblServiceDate;
        private System.Windows.Forms.Label lblReasonForServicing;
        private System.Windows.Forms.Label lblNumberOfNodes;
        private System.Windows.Forms.Label lblNameOfRegion;
        private System.Windows.Forms.TextBox txtSerijskiBroj;
        private System.Windows.Forms.TextBox txtReason;
        private System.Windows.Forms.TextBox txtNameOfRegion;
        private System.Windows.Forms.DateTimePicker dtpStartOfUseDate;
        private System.Windows.Forms.DateTimePicker dtpServiceDate;
        private System.Windows.Forms.Button btnAddNewRegionalHub;
        private System.Windows.Forms.NumericUpDown numNumberOfNodes;
        private System.Windows.Forms.ListView lstManufacturers;
        private System.Windows.Forms.ColumnHeader columnHeaderPIB;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.Label lblManufacturer;
    }
}