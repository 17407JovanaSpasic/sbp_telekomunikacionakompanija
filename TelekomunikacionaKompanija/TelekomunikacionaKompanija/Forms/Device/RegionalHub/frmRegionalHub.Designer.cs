﻿
namespace TelekomunikacionaKompanija.Forms.Device.RegionalHub
{
    partial class frmRegionalHub
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpRegionalHubs = new System.Windows.Forms.GroupBox();
            this.lstRegionalHubs = new System.Windows.Forms.ListView();
            this.columnHeaderSerialNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStartDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderServiceDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderReason = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderNumberOfNodes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnNewRegionalHub = new System.Windows.Forms.Button();
            this.btnDeleteRegionalHub = new System.Windows.Forms.Button();
            this.grpRegionalHubs.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpRegionalHubs
            // 
            this.grpRegionalHubs.Controls.Add(this.lstRegionalHubs);
            this.grpRegionalHubs.Location = new System.Drawing.Point(15, 14);
            this.grpRegionalHubs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpRegionalHubs.Name = "grpRegionalHubs";
            this.grpRegionalHubs.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpRegionalHubs.Size = new System.Drawing.Size(993, 234);
            this.grpRegionalHubs.TabIndex = 0;
            this.grpRegionalHubs.TabStop = false;
            this.grpRegionalHubs.Text = "Regionalni hub-ovi";
            // 
            // lstRegionalHubs
            // 
            this.lstRegionalHubs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderSerialNum,
            this.columnHeaderName,
            this.columnHeaderStartDate,
            this.columnHeaderServiceDate,
            this.columnHeaderReason,
            this.columnHeaderNumberOfNodes});
            this.lstRegionalHubs.FullRowSelect = true;
            this.lstRegionalHubs.GridLines = true;
            this.lstRegionalHubs.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstRegionalHubs.HideSelection = false;
            this.lstRegionalHubs.Location = new System.Drawing.Point(5, 21);
            this.lstRegionalHubs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstRegionalHubs.MultiSelect = false;
            this.lstRegionalHubs.Name = "lstRegionalHubs";
            this.lstRegionalHubs.Size = new System.Drawing.Size(982, 207);
            this.lstRegionalHubs.TabIndex = 0;
            this.lstRegionalHubs.UseCompatibleStateImageBehavior = false;
            this.lstRegionalHubs.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderSerialNum
            // 
            this.columnHeaderSerialNum.Text = "Serijski broj";
            this.columnHeaderSerialNum.Width = 85;
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Naziv hub-a";
            this.columnHeaderName.Width = 100;
            // 
            // columnHeaderStartDate
            // 
            this.columnHeaderStartDate.Text = "Datum početka upotrebe";
            this.columnHeaderStartDate.Width = 160;
            // 
            // columnHeaderServiceDate
            // 
            this.columnHeaderServiceDate.Text = "Datum servisiranja";
            this.columnHeaderServiceDate.Width = 140;
            // 
            // columnHeaderReason
            // 
            this.columnHeaderReason.Text = "Razlog servisiranja";
            this.columnHeaderReason.Width = 150;
            // 
            // columnHeaderNumberOfNodes
            // 
            this.columnHeaderNumberOfNodes.Text = "Broj čvorova";
            this.columnHeaderNumberOfNodes.Width = 88;
            // 
            // btnNewRegionalHub
            // 
            this.btnNewRegionalHub.Location = new System.Drawing.Point(324, 264);
            this.btnNewRegionalHub.Margin = new System.Windows.Forms.Padding(4);
            this.btnNewRegionalHub.Name = "btnNewRegionalHub";
            this.btnNewRegionalHub.Size = new System.Drawing.Size(161, 59);
            this.btnNewRegionalHub.TabIndex = 1;
            this.btnNewRegionalHub.Text = "Novi regionalni hub";
            this.btnNewRegionalHub.UseVisualStyleBackColor = true;
            this.btnNewRegionalHub.Click += new System.EventHandler(this.btnNewRegionalHub_Click);
            // 
            // btnDeleteRegionalHub
            // 
            this.btnDeleteRegionalHub.Location = new System.Drawing.Point(541, 264);
            this.btnDeleteRegionalHub.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteRegionalHub.Name = "btnDeleteRegionalHub";
            this.btnDeleteRegionalHub.Size = new System.Drawing.Size(161, 59);
            this.btnDeleteRegionalHub.TabIndex = 3;
            this.btnDeleteRegionalHub.Text = "Obrišite regionalni hub";
            this.btnDeleteRegionalHub.UseVisualStyleBackColor = true;
            this.btnDeleteRegionalHub.Click += new System.EventHandler(this.btnDeleteRegionalHub_Click);
            // 
            // frmRegionalHub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 352);
            this.Controls.Add(this.btnDeleteRegionalHub);
            this.Controls.Add(this.btnNewRegionalHub);
            this.Controls.Add(this.grpRegionalHubs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRegionalHub";
            this.Text = "Prikaz svih regionalnih hub-ova";
            this.Load += new System.EventHandler(this.frmRegionalHub_Load);
            this.grpRegionalHubs.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpRegionalHubs;
        private System.Windows.Forms.ListView lstRegionalHubs;
        private System.Windows.Forms.ColumnHeader columnHeaderSerialNum;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.ColumnHeader columnHeaderStartDate;
        private System.Windows.Forms.ColumnHeader columnHeaderServiceDate;
        private System.Windows.Forms.ColumnHeader columnHeaderReason;
        private System.Windows.Forms.ColumnHeader columnHeaderNumberOfNodes;
        private System.Windows.Forms.Button btnNewRegionalHub;
        private System.Windows.Forms.Button btnDeleteRegionalHub;
    }
}