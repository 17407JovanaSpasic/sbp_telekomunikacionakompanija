﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOs.Device;

namespace TelekomunikacionaKompanija.Forms.Device.RegionalHub
{
    public partial class frmNewRegionalHub : Form
    {
        #region Attribute(s)
        private List<TelekomunikacionaKompanija.DTOs.Manufacturer.ManufacturerDTO> manufacturerList;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmNewRegionalHub()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmNewRegionalHub_Load(object sender, EventArgs e)
        {
            //Manufacturers
            this.lstManufacturers.Items.Clear();

            this.manufacturerList = TelekomunikacionaKompanija.DTOManagers.ManufacturerDTOManager.GetAllManufactures();
            foreach (var i in manufacturerList)
            {
                var item = new ListViewItem(new string[] { i.PIB, i.Name });

                this.lstManufacturers.Items.Add(item);
            }
            this.lstManufacturers.Refresh();
        }
        private void btnAddNewRegionalHub_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lstManufacturers.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Morate selektovati proizvođača!", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (this.txtSerijskiBroj.Text == string.Empty || this.txtReason.Text == string.Empty ||
                    this.txtNameOfRegion.Text == string.Empty)
                {
                    MessageBox.Show("Morate popuniti sva polja", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var session = DataLayer.GetSession();
                string pib = this.lstManufacturers.SelectedItems[0].SubItems[0].Text;
                var Pib = session.Get<TelekomunikacionaKompanija.Entities.Manufacturer>(pib);

                string serialNumber = this.txtSerijskiBroj.Text;
                DateTime startDate = this.dtpStartOfUseDate.Value;
                DateTime serviceDate = this.dtpServiceDate.Value;
                string reason = this.txtReason.Text;
                int numOfNodes = (int)this.numNumberOfNodes.Value;
                string nameOfRegion = this.txtNameOfRegion.Text;

                var newRegionalHub = new RegionalHubDTO(serialNumber, startDate, serviceDate, reason, Pib, numOfNodes, nameOfRegion);

                if (DTOManagers.DeviceDTOManager.CreateRegionalHub(newRegionalHub, Pib))
                {
                    MessageBox.Show("Uspešno ste kreirali regionalni hub", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Regionalni hub nije kreiran, pokušajte sa drugim serijskim brojem!", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion Form Event Method(s)
    }
}
