﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOs.Device;

namespace TelekomunikacionaKompanija.Forms.Device.RegionalHub
{
    public partial class frmRegionalHub : Form
    {
        #region Attribute(s)
        private List<RegionalHubDTO> regionalList;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmRegionalHub()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Method(s)
        private void RefreshList()
        {
            this.lstRegionalHubs.Items.Clear();

            this.regionalList = DeviceDTOManager.GetAllRegionalHubs();
            foreach (var i in regionalList)
            {
                var item = new ListViewItem(new string[] {

                        i.SerialNumber, i.NameOfRegion, i.StartOfUseDate.ToString(), i.ServiceDate.ToString(), 

                        i.NameOfRegion, i.SerialNumber, i.StartOfUseDate.ToString(), i.ServiceDate.ToString(),

                        i.ReasonForServicing, i.NumberOfNodes.ToString()});

                this.lstRegionalHubs.Items.Add(item);
            }
            this.lstRegionalHubs.Refresh();
        }
        #endregion

        #region Form Event Method(s)
        private void frmRegionalHub_Load(object sender, EventArgs e)
        {
            this.RefreshList();
        }
       

        private void btnNewRegionalHub_Click(object sender, EventArgs e)
        {
            frmNewRegionalHub newRegionalHub = new frmNewRegionalHub();
            newRegionalHub.ShowDialog();
            this.RefreshList();
        }

        private void btnDeleteRegionalHub_Click(object sender, EventArgs e)
        {
            if (this.lstRegionalHubs.SelectedItems.Count==0)
            {
                MessageBox.Show("Morate selektovati regionalni hub!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            DialogResult result = MessageBox.Show("Da li ste sigurni da zelite da obrisete regionalni hub?", "Upozorenje", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.No)
                return;

            string serialNumber = this.lstRegionalHubs.SelectedItems[0].SubItems[0].Text;
            if (DeviceDTOManager.DeleteDevice(serialNumber))
            {
                MessageBox.Show("Uspesno obrisan regionalni hub!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.RefreshList();
            }
            else
            {
                MessageBox.Show("Regionalni hub nije obrisan!", "Greska", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion Form Event Method(s)

    }
}
