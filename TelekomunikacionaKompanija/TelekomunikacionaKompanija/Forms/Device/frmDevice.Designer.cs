﻿
namespace TelekomunikacionaKompanija.Forms.Device
{
    partial class frmDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkMainForm = new System.Windows.Forms.LinkLabel();
            this.grpDevices = new System.Windows.Forms.GroupBox();
            this.lstDevices = new System.Windows.Forms.ListView();
            this.columnHeaderSerialNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStartDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderServiceDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderReason = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPib = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnRegionalHubs = new System.Windows.Forms.Button();
            this.grpMainStations = new System.Windows.Forms.GroupBox();
            this.lblPretraga = new System.Windows.Forms.Label();
            this.lstMainStations = new System.Windows.Forms.ListView();
            this.columnHeaderSerialNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderNumberOfNodes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cboSelect = new System.Windows.Forms.ComboBox();
            this.grpCommunicationNodes = new System.Windows.Forms.GroupBox();
            this.lstCommunicationNodes = new System.Windows.Forms.ListView();
            this.columnHeaderSerialNu = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderLocationNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnNewMainStation = new System.Windows.Forms.Button();
            this.btnDeleteMainStation = new System.Windows.Forms.Button();
            this.btnDeleteCommNode = new System.Windows.Forms.Button();
            this.btnNewCommunicationNode = new System.Windows.Forms.Button();
            this.grpDevices.SuspendLayout();
            this.grpMainStations.SuspendLayout();
            this.grpCommunicationNodes.SuspendLayout();
            this.SuspendLayout();
            // 
            // linkMainForm
            // 
            this.linkMainForm.AutoSize = true;
            this.linkMainForm.Location = new System.Drawing.Point(15, 11);
            this.linkMainForm.Name = "linkMainForm";
            this.linkMainForm.Size = new System.Drawing.Size(104, 17);
            this.linkMainForm.TabIndex = 0;
            this.linkMainForm.TabStop = true;
            this.linkMainForm.Text = "Početna strana";
            this.linkMainForm.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkMainForm_LinkClicked);
            // 
            // grpDevices
            // 
            this.grpDevices.Controls.Add(this.lstDevices);
            this.grpDevices.Location = new System.Drawing.Point(15, 43);
            this.grpDevices.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpDevices.Name = "grpDevices";
            this.grpDevices.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpDevices.Size = new System.Drawing.Size(1084, 176);
            this.grpDevices.TabIndex = 1;
            this.grpDevices.TabStop = false;
            this.grpDevices.Text = "Uređaji";
            // 
            // lstDevices
            // 
            this.lstDevices.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderSerialNumber,
            this.columnHeaderStartDate,
            this.columnHeaderServiceDate,
            this.columnHeaderReason,
            this.columnHeaderPib,
            this.columnHeaderName});
            this.lstDevices.FullRowSelect = true;
            this.lstDevices.GridLines = true;
            this.lstDevices.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstDevices.HideSelection = false;
            this.lstDevices.Location = new System.Drawing.Point(20, 21);
            this.lstDevices.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstDevices.MultiSelect = false;
            this.lstDevices.Name = "lstDevices";
            this.lstDevices.Size = new System.Drawing.Size(1044, 143);
            this.lstDevices.TabIndex = 0;
            this.lstDevices.UseCompatibleStateImageBehavior = false;
            this.lstDevices.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderSerialNumber
            // 
            this.columnHeaderSerialNumber.Text = "Serijski broj";
            this.columnHeaderSerialNumber.Width = 85;
            // 
            // columnHeaderStartDate
            // 
            this.columnHeaderStartDate.Text = "Datum početka upotrebe";
            this.columnHeaderStartDate.Width = 160;
            // 
            // columnHeaderServiceDate
            // 
            this.columnHeaderServiceDate.Text = "Datum servisiranja";
            this.columnHeaderServiceDate.Width = 130;
            // 
            // columnHeaderReason
            // 
            this.columnHeaderReason.Text = "Razlog servisiranja";
            this.columnHeaderReason.Width = 130;
            // 
            // columnHeaderPib
            // 
            this.columnHeaderPib.Text = "PIB Proizvođača";
            this.columnHeaderPib.Width = 115;
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Naziv proizvođača";
            this.columnHeaderName.Width = 128;
            // 
            // btnRegionalHubs
            // 
            this.btnRegionalHubs.Location = new System.Drawing.Point(925, 271);
            this.btnRegionalHubs.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegionalHubs.Name = "btnRegionalHubs";
            this.btnRegionalHubs.Size = new System.Drawing.Size(174, 54);
            this.btnRegionalHubs.TabIndex = 2;
            this.btnRegionalHubs.Text = "Regionalni hub-ovi";
            this.btnRegionalHubs.UseVisualStyleBackColor = true;
            this.btnRegionalHubs.Click += new System.EventHandler(this.btnRegionalHubs_Click);
            // 
            // grpMainStations
            // 
            this.grpMainStations.Controls.Add(this.lblPretraga);
            this.grpMainStations.Controls.Add(this.lstMainStations);
            this.grpMainStations.Controls.Add(this.cboSelect);
            this.grpMainStations.Location = new System.Drawing.Point(18, 234);
            this.grpMainStations.Margin = new System.Windows.Forms.Padding(4);
            this.grpMainStations.Name = "grpMainStations";
            this.grpMainStations.Padding = new System.Windows.Forms.Padding(4);
            this.grpMainStations.Size = new System.Drawing.Size(870, 220);
            this.grpMainStations.TabIndex = 4;
            this.grpMainStations.TabStop = false;
            this.grpMainStations.Text = "Glavne stanice";
            // 
            // lblPretraga
            // 
            this.lblPretraga.AutoSize = true;
            this.lblPretraga.Location = new System.Drawing.Point(24, 32);
            this.lblPretraga.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPretraga.Name = "lblPretraga";
            this.lblPretraga.Size = new System.Drawing.Size(125, 17);
            this.lblPretraga.TabIndex = 2;
            this.lblPretraga.Text = "Kriterijum pretrage";
            // 
            // lstMainStations
            // 
            this.lstMainStations.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderSerialNum,
            this.columnHeaderNumberOfNodes});
            this.lstMainStations.FullRowSelect = true;
            this.lstMainStations.GridLines = true;
            this.lstMainStations.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstMainStations.HideSelection = false;
            this.lstMainStations.Location = new System.Drawing.Point(27, 65);
            this.lstMainStations.Margin = new System.Windows.Forms.Padding(4);
            this.lstMainStations.Name = "lstMainStations";
            this.lstMainStations.Size = new System.Drawing.Size(831, 143);
            this.lstMainStations.TabIndex = 0;
            this.lstMainStations.UseCompatibleStateImageBehavior = false;
            this.lstMainStations.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderSerialNum
            // 
            this.columnHeaderSerialNum.Text = "Serijski broj";
            this.columnHeaderSerialNum.Width = 150;
            // 
            // columnHeaderNumberOfNodes
            // 
            this.columnHeaderNumberOfNodes.Text = "Broj čvorova";
            this.columnHeaderNumberOfNodes.Width = 150;
            // 
            // cboSelect
            // 
            this.cboSelect.AccessibleName = "";
            this.cboSelect.FormattingEnabled = true;
            this.cboSelect.Items.AddRange(new object[] {
            "Sve glavne stanice",
            "Broj <=1",
            "Broj >1"});
            this.cboSelect.Location = new System.Drawing.Point(237, 28);
            this.cboSelect.Margin = new System.Windows.Forms.Padding(4);
            this.cboSelect.Name = "cboSelect";
            this.cboSelect.Size = new System.Drawing.Size(132, 24);
            this.cboSelect.TabIndex = 1;
            this.cboSelect.SelectedIndexChanged += new System.EventHandler(this.cboSelect_SelectedIndexChanged);
            // 
            // grpCommunicationNodes
            // 
            this.grpCommunicationNodes.Controls.Add(this.lstCommunicationNodes);
            this.grpCommunicationNodes.Location = new System.Drawing.Point(18, 475);
            this.grpCommunicationNodes.Margin = new System.Windows.Forms.Padding(4);
            this.grpCommunicationNodes.Name = "grpCommunicationNodes";
            this.grpCommunicationNodes.Padding = new System.Windows.Forms.Padding(4);
            this.grpCommunicationNodes.Size = new System.Drawing.Size(870, 195);
            this.grpCommunicationNodes.TabIndex = 5;
            this.grpCommunicationNodes.TabStop = false;
            this.grpCommunicationNodes.Text = "Komunikacioni čvorovi";
            // 
            // lstCommunicationNodes
            // 
            this.lstCommunicationNodes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderSerialNu,
            this.columnHeaderAddress,
            this.columnHeaderLocationNum,
            this.columnHeaderDescription});
            this.lstCommunicationNodes.FullRowSelect = true;
            this.lstCommunicationNodes.GridLines = true;
            this.lstCommunicationNodes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstCommunicationNodes.HideSelection = false;
            this.lstCommunicationNodes.Location = new System.Drawing.Point(27, 39);
            this.lstCommunicationNodes.Margin = new System.Windows.Forms.Padding(4);
            this.lstCommunicationNodes.Name = "lstCommunicationNodes";
            this.lstCommunicationNodes.Size = new System.Drawing.Size(831, 143);
            this.lstCommunicationNodes.TabIndex = 0;
            this.lstCommunicationNodes.UseCompatibleStateImageBehavior = false;
            this.lstCommunicationNodes.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderSerialNu
            // 
            this.columnHeaderSerialNu.Text = "Serijski broj";
            this.columnHeaderSerialNu.Width = 150;
            // 
            // columnHeaderAddress
            // 
            this.columnHeaderAddress.Text = "Adresa";
            this.columnHeaderAddress.Width = 150;
            // 
            // columnHeaderLocationNum
            // 
            this.columnHeaderLocationNum.Text = "Broj lokacije";
            this.columnHeaderLocationNum.Width = 120;
            // 
            // columnHeaderDescription
            // 
            this.columnHeaderDescription.Text = "Opis";
            this.columnHeaderDescription.Width = 150;
            // 
            // btnNewMainStation
            // 
            this.btnNewMainStation.Location = new System.Drawing.Point(925, 332);
            this.btnNewMainStation.Margin = new System.Windows.Forms.Padding(4);
            this.btnNewMainStation.Name = "btnNewMainStation";
            this.btnNewMainStation.Size = new System.Drawing.Size(174, 54);
            this.btnNewMainStation.TabIndex = 6;
            this.btnNewMainStation.Text = "Nova glavna stanica";
            this.btnNewMainStation.UseVisualStyleBackColor = true;
            this.btnNewMainStation.Click += new System.EventHandler(this.btnNewMainStation_Click);
            // 
            // btnDeleteMainStation
            // 
            this.btnDeleteMainStation.Location = new System.Drawing.Point(925, 394);
            this.btnDeleteMainStation.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteMainStation.Name = "btnDeleteMainStation";
            this.btnDeleteMainStation.Size = new System.Drawing.Size(174, 54);
            this.btnDeleteMainStation.TabIndex = 8;
            this.btnDeleteMainStation.Text = "Obrišite glavnu stanicu";
            this.btnDeleteMainStation.UseVisualStyleBackColor = true;
            this.btnDeleteMainStation.Click += new System.EventHandler(this.btnDeleteMainStation_Click);
            // 
            // btnDeleteCommNode
            // 
            this.btnDeleteCommNode.Location = new System.Drawing.Point(925, 597);
            this.btnDeleteCommNode.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteCommNode.Name = "btnDeleteCommNode";
            this.btnDeleteCommNode.Size = new System.Drawing.Size(174, 54);
            this.btnDeleteCommNode.TabIndex = 10;
            this.btnDeleteCommNode.Text = "Obrišite komunikacioni čvor";
            this.btnDeleteCommNode.UseVisualStyleBackColor = true;
            this.btnDeleteCommNode.Click += new System.EventHandler(this.btnDeleteCommNode_Click);
            // 
            // btnNewCommunicationNode
            // 
            this.btnNewCommunicationNode.Location = new System.Drawing.Point(925, 536);
            this.btnNewCommunicationNode.Margin = new System.Windows.Forms.Padding(4);
            this.btnNewCommunicationNode.Name = "btnNewCommunicationNode";
            this.btnNewCommunicationNode.Size = new System.Drawing.Size(174, 54);
            this.btnNewCommunicationNode.TabIndex = 3;
            this.btnNewCommunicationNode.Text = "Novi komunikacioni čvor";
            this.btnNewCommunicationNode.UseVisualStyleBackColor = true;
            this.btnNewCommunicationNode.Click += new System.EventHandler(this.btnNewCommunicationNode_Click);
            // 
            // frmDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1116, 677);
            this.Controls.Add(this.btnDeleteCommNode);
            this.Controls.Add(this.btnDeleteMainStation);
            this.Controls.Add(this.btnNewMainStation);
            this.Controls.Add(this.grpCommunicationNodes);
            this.Controls.Add(this.grpMainStations);
            this.Controls.Add(this.btnNewCommunicationNode);
            this.Controls.Add(this.btnRegionalHubs);
            this.Controls.Add(this.grpDevices);
            this.Controls.Add(this.linkMainForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDevice";
            this.Text = "Prikaz svih uređaja";
            this.Load += new System.EventHandler(this.frmDevice_Load);
            this.grpDevices.ResumeLayout(false);
            this.grpMainStations.ResumeLayout(false);
            this.grpMainStations.PerformLayout();
            this.grpCommunicationNodes.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkMainForm;
        private System.Windows.Forms.GroupBox grpDevices;
        private System.Windows.Forms.ListView lstDevices;
        private System.Windows.Forms.Button btnRegionalHubs;
        private System.Windows.Forms.ColumnHeader columnHeaderSerialNumber;
        private System.Windows.Forms.ColumnHeader columnHeaderStartDate;
        private System.Windows.Forms.ColumnHeader columnHeaderServiceDate;
        private System.Windows.Forms.ColumnHeader columnHeaderReason;
        private System.Windows.Forms.GroupBox grpMainStations;
        private System.Windows.Forms.GroupBox grpCommunicationNodes;
        private System.Windows.Forms.ListView lstMainStations;
        private System.Windows.Forms.ListView lstCommunicationNodes;
        private System.Windows.Forms.ColumnHeader columnHeaderSerialNum;
        private System.Windows.Forms.ColumnHeader columnHeaderNumberOfNodes;
        private System.Windows.Forms.ColumnHeader columnHeaderSerialNu;
        private System.Windows.Forms.ColumnHeader columnHeaderAddress;
        private System.Windows.Forms.ColumnHeader columnHeaderLocationNum;
        private System.Windows.Forms.ColumnHeader columnHeaderDescription;
        private System.Windows.Forms.ColumnHeader columnHeaderPib;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.Button btnNewMainStation;
        private System.Windows.Forms.Button btnDeleteMainStation;
        private System.Windows.Forms.Button btnDeleteCommNode;
        private System.Windows.Forms.Button btnNewCommunicationNode;
        private System.Windows.Forms.Label lblPretraga;
        private System.Windows.Forms.ComboBox cboSelect;
    }
}