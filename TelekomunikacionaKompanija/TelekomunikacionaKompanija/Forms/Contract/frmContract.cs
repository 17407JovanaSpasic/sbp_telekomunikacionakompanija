﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOManagers.Contract;
using TelekomunikacionaKompanija.DTOs.Contract;
using TelekomunikacionaKompanija.DTOs.Package;
using TelekomunikacionaKompanija.DTOs.User;
using TelekomunikacionaKompanija.Forms.Contract.Service;
using TelekomunikacionaKompanija.Forms.User;

namespace TelekomunikacionaKompanija.Forms.Contract
{
    public partial class frmContract : Form
    {
        #region Attribute(s)
        private List<PackageDTO> packagesList;
        public DialogResult alredyUser;
        List<string> packagesWhichUserPoses = new List<string>();
        List<string> additionalPackagesWhichUserWants = new List<string>();
        private List<ContractByUserDTO> contracts;

        public int userId;
        UserDTO user = new UserDTO();

        PackageDTO package = new PackageDTO();
        public string packageName;

        public static int serviceId = 0;

        DateTime startDateContract;
        DateTime endDateOfContract;
        #endregion Attribute(s)

        #region Method(s)
        public void GettingInfomationForUser()
        {
            if (this.txtKontakTelefon.Text.Length < 1)
            {
                MessageBox.Show("Proverite da li ste popunili sva polja.",
                    "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (this.IsDigitsOnly(txtKontakTelefon.Text) == false)
            {
                MessageBox.Show("Proverite da li ste ispravno uneli broj telefona.",
                    "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (UserDTOManager.CheckIfExists(this.txtKontakTelefon.Text) == false)
            {
                MessageBox.Show("Proverite ispravnost unetih podataka, korisnik sa datim brojem telefona trenutno ne postoji.", "Greška",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            UserDTOManager.GetUserByContactPhoneForContract(this.txtKontakTelefon.Text, ref this.userId, ref this.user);
            if (this.userId == 0 || this.user == null)
            {
                if (userId.ToString() == this.txtKontakTelefon.Text)
                {
                    var user = UserDTOManager.GetUserByID(userId);
                    if (user.UsersContracts.Count == 0)
                    {
                        MessageBox.Show("Korisnik ipak ne poseduje ni jednu nasu uslug, ali mozete je sada kreirati!", "Obavestenje",
                         MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Greška prilikom pribavljanja podataka o korisniku, molimo Vas proverite da " +
                        "li ste ispravno uneli broj telefona.", "Greška",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (this.alredyUser == DialogResult.Yes) //ovaj deo izvrsva ukoliko korisnik vec postoji u bazi, odnosno vec je korisnik nekih od uslga
            {
                foreach (var con in user.UsersContracts)
                {
                    this.packagesWhichUserPoses.Add(con.PackageName.Name);
                }
                if (this.packagesWhichUserPoses.Count > 0)
                {
                    MessageBox.Show("Uspešno pribavljeni podaci o korisniku.", "Obaveštenje",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Korisnik ne koristi nijednu uslugu ili je korisnik tek kreiran i " +
                        "nije mu dodeljen nijedan paket!", "Obaveštenje",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            //nakon uspeno pribavljenih podataka, moguce je izabrati zeljeni paket kao i dodatne kanale za odabrani paket
            this.grpIzborPaketa.Enabled = true;
            this.lstIzborPaketa.Enabled = true;
        }

        public bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }
        #endregion Method(s)

        #region Constructor(s)
        public frmContract()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmContract_Load(object sender, EventArgs e)
        {
            this.radioButton2Years.Checked = true;
            this.radioButton1year.Checked = false;
            this.btnServiceSelection.Enabled = false;

            //popunjavanje tabela u vezi sa paketima
            this.packagesList = PackageDTOManager.GetAllPackages();
            this.lstIzborPaketa.Items.Clear();

            foreach (var package in packagesList)
            {
                var item = new ListViewItem(new string[] { package.Name, package.Price.ToString("N2"),
                    package.AdditionalPackagePrice.ToString("N2"), package.InternetSpeed.ToString(),
                    package.PricePerMB.ToString("N2"), package.PricePerAddress.ToString("N2"),
                    package.NumberOfMinutes.ToString() });

                this.lstIzborPaketa.Items.Add(item);
            }
            this.lstIzborPaketa.Refresh();

            this.lblKontaktTelefon.Enabled = false;
            this.txtKontakTelefon.Enabled = false;
            this.btnGetUser.Enabled = false;

            this.grpIzborPaketa.Enabled = false;
            this.lstIzborPaketa.Enabled = false;

        }

        private void btnCreateUser_Click(object sender, EventArgs e)
        {
            alredyUser = MessageBox.Show("Da li ste već korisnik nekih od naših usluga?", "Upitnik",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (alredyUser == DialogResult.Yes) //Ukoliko korisnik koristi neku od usluga,
            {                                  //to znaci da vec postoji u bazi, iz toga razloga nije potrebno kreirati novg korisnika
                MessageBox.Show("Unesite broj telefona korisnika, kako bi se pribavili neophodni podaci.",
                    "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.lblKontaktTelefon.Enabled = true;
                this.txtKontakTelefon.Enabled = true;
                this.btnGetUser.Enabled = true;
                this.Refresh();
            }
            else if (alredyUser == DialogResult.No)
            {
                bool isIndividual = false;
                DialogResult res = MessageBox.Show("Ukoliko kreirate: \n" +
                                "\"Pravno lice\" kliknite na \"Yes\" \n" +
                                 "\"Fizičko lice\" kliknite na \"No\"", "Upitnik",
                                 MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (res == DialogResult.Cancel)
                    return;

                if (res == DialogResult.No)
                    isIndividual = true;

                var newFrmForCreatingUser = new frmCreateUser()
                {
                    isItIndividual = isIndividual
                };
                newFrmForCreatingUser.ShowDialog();

                MessageBox.Show("Unesite broj telefona novokreiranog korisnika, kako bi se pribavili neophodni podaci.",
                    "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.lblKontaktTelefon.Enabled = true;
                this.txtKontakTelefon.Enabled = true;
                this.btnGetUser.Enabled = true;
                this.Refresh();
            }
        }

        private void lstIzborPaketa_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnServiceSelection.Enabled = true;
        }

        private void btnGetUser_Click(object sender, EventArgs e)
        {
                this.GettingInfomationForUser();
            
        }

        private void btnKreirajUgovor_Click(object sender, EventArgs e)
        {
            if (this.radioButton1year.Checked)
            {
                this.startDateContract = DateTime.Now;
                this.endDateOfContract = startDateContract.AddYears(1);
            }

            if (this.radioButton2Years.Checked)
            {
                this.startDateContract = DateTime.Now;
                this.endDateOfContract = startDateContract.AddYears(2);
            }

            //proveravanje da li je izabran paket
            if (this.lstIzborPaketa.SelectedItems.Count == 0)
                return;

            if (this.user == null)
            {
                MessageBox.Show("Greska prilikom pribavljanja korisnika, molimo vas pokusajte ponovo", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //uzimanje imena paketa i provera da li korisnik vec ima takav paket
            this.packageName = this.lstIzborPaketa.SelectedItems[0].SubItems[0].Text;

            if (this.packagesList.Count > 0) //ako je ispunjen ovaj uslov, to znaci da korisnik vec poseduje odredjene pakete, medjutim
            {                              //on ne moze da poseduje 2 ista paketa, pa se to ovde proverava
                foreach (var i in this.packagesWhichUserPoses)
                {
                    if (packageName == i)
                    {
                        MessageBox.Show("Korisnik je izabrao paket koji već poseduje. " +
                            "Potrebno je izabrati neki drugi.", "Obaveštenje",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                }
            }

            //pribavljanje izabranog paketa
            this.package = PackageDTOManager.GetPackageByName(packageName);
            if (this.package == null)
            {
                MessageBox.Show("Greška prilikom pribavljanja paketa.", "Greška",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }




            var newContract = new ContractDTO(this.startDateContract, this.endDateOfContract, this.user, this.package, null);

            if (ContractDTOManager.CreateContract(newContract, frmContract.serviceId))
            {
                MessageBox.Show("Uspesno kreiran ugovor!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            else
                MessageBox.Show("Greska prilikom kreiranja ugovora!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void radioButton1year_Click(object sender, EventArgs e)
        {
            this.radioButton2Years.Checked = false;
            this.radioButton1year.Checked = true;
        }

        private void radioButton2Years_Click(object sender, EventArgs e)
        {
            this.radioButton2Years.Checked = true;
            this.radioButton1year.Checked = false;
        }

        private void btnServiceSelection_Click(object sender, EventArgs e)
        {
            if (this.alredyUser == DialogResult.Yes)
            {
                this.contracts = ContractDTOManager.GetContractsByUsersPhoneNumber(this.txtKontakTelefon.Text);

                if (contracts.Count == 0)
                {
                    MessageBox.Show("Za sada, korisnik sa datim brojem telefona ne poseduje ni jedan ugovor!.",
                        "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    bool userHasPackage = false;
                    foreach (var p in this.contracts)
                    {
                        if (p.PackageName == this.lstIzborPaketa.SelectedItems[0].SubItems[0].Text)
                            userHasPackage = true;
                    }
                    if (userHasPackage == true)
                    {
                        MessageBox.Show("Korisnik vec poseduje ovaj paket!",
                       "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            if (this.alredyUser == DialogResult.No) //Ukoliko je korisnik kreiran pre kreiranje prvog ugovora onda ce ovaj kod da se izvrsi
            {
                if (UserDTOManager.CheckIfExists(this.txtKontakTelefon.Text)==false)
                {
                    MessageBox.Show("Korisnik nije ispravno kreiran, pokusajte ponovo.",
                        "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.contracts = ContractDTOManager.GetContractsByUsersPhoneNumber(this.txtKontakTelefon.Text);

                if (contracts.Count == 0)
                {
                    MessageBox.Show("Za sada, korisnik sa datim brojem telefona ne poseduje ni jedan ugovor!.",
                        "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    bool userHasPackage = false;
                    foreach (var p in this.contracts)
                    {
                        if (p.PackageName == this.lstIzborPaketa.SelectedItems[0].SubItems[0].Text)
                            userHasPackage = true;
                    }
                    if (userHasPackage == true)
                    {
                        MessageBox.Show("Korisnik vec poseduje ovaj paket!",
                       "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            var newCreateServiceFrm = new frmCreateService
            {
                PackageName = this.lstIzborPaketa.SelectedItems[0].SubItems[0].Text
            };
            newCreateServiceFrm.ShowDialog();

            if (frmContract.serviceId == 0)
            {
                MessageBox.Show("Greska prilikom kreiranja servisa!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

        }
        #endregion Form Event Method(s)
    }
}
