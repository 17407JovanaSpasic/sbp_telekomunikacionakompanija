﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers.Contract;
using TelekomunikacionaKompanija.DTOs.Contract;
using TelekomunikacionaKompanija.Forms.Contract.Service;

namespace TelekomunikacionaKompanija.Forms.Contract
{
    public partial class frmShowContractsByUser : Form
    {
        #region Attribute(s)
        private List<ContractByUserDTO> contracts;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmShowContractsByUser()
        {
            this.InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmShowContractsByUser_Load(object sender, EventArgs e)
        {
            this.lstContracts.Items.Clear();
            this.btnUpdateService.Enabled = false;
        }

        private void btnShowContracts_Click(object sender, EventArgs e)
        {
            this.lstContracts.Items.Clear();

            string phoneNumber = this.txtPhoneNumber.Text;

            if (phoneNumber != string.Empty)
            {
                if (!this.ContainsDigitsOnly(phoneNumber))
                {
                    MessageBox.Show("Broj telefona mora da sadrži samo cifre.", "Upozorenje",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    this.contracts = ContractDTOManager.GetContractsByUsersPhoneNumber(phoneNumber);

                    if (contracts.Count == 0)
                    {
                        MessageBox.Show("Nije pronađen nijedan ugovor za dati broj telefona.",
                            "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        foreach (var contract in this.contracts)
                        {
                            var item = new ListViewItem(new string[] { contract.ContractNumber,
                                contract.StartDate.ToString(), contract.EndDate.ToString(),
                                contract.PackageName });

                            this.lstContracts.Items.Add(item);
                        }

                        this.lstContracts.Refresh();
                    }
                }
            }
        }

        private void lstContracts_SelectedIndexChanged(object sender, EventArgs e) =>
            this.btnUpdateService.Enabled = this.lstContracts.SelectedItems.Count != 0;

        private void btnUpdateService_Click(object sender, EventArgs e)
        {
            var selectedIndex = this.lstContracts.SelectedIndices[0];

            var updateServiceForm = new frmUpdateService()
            {
                PackageName = this.contracts[selectedIndex].PackageName,
                ServiceID = this.contracts[selectedIndex].ServiceID
            };

            updateServiceForm.Show();

            this.lstContracts.SelectedIndices.Clear();
        }
        #endregion Form Event Method(s)

        #region Helper Method(s)
        private bool ContainsDigitsOnly(string text)
        {
            bool isDigit = true;

            for (int i = 0; i < text.Length && isDigit; i++)
            {
                if (text[i] < '0' || text[i] > '9')
                {
                    isDigit = false;
                }
            }

            return isDigit;
        }
        #endregion Helper Method(s)
    }
}
