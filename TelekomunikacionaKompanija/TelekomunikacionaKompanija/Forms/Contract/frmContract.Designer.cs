﻿
namespace TelekomunikacionaKompanija.Forms.Contract
{
    partial class frmContract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCreateUser = new System.Windows.Forms.Button();
            this.lblKontaktTelefon = new System.Windows.Forms.Label();
            this.txtKontakTelefon = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.btnGetUser = new System.Windows.Forms.Button();
            this.grpKorisnik = new System.Windows.Forms.GroupBox();
            this.grpIzborPaketa = new System.Windows.Forms.GroupBox();
            this.btnServiceSelection = new System.Windows.Forms.Button();
            this.lstIzborPaketa = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnKreirajUgovor = new System.Windows.Forms.Button();
            this.radioButton1year = new System.Windows.Forms.RadioButton();
            this.radioButton2Years = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpKorisnik.SuspendLayout();
            this.grpIzborPaketa.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCreateUser
            // 
            this.btnCreateUser.Location = new System.Drawing.Point(15, 39);
            this.btnCreateUser.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCreateUser.Name = "btnCreateUser";
            this.btnCreateUser.Size = new System.Drawing.Size(293, 62);
            this.btnCreateUser.TabIndex = 0;
            this.btnCreateUser.Text = "Kreiranje/Pribavljanje korisnika";
            this.btnCreateUser.UseVisualStyleBackColor = true;
            this.btnCreateUser.Click += new System.EventHandler(this.btnCreateUser_Click);
            // 
            // lblKontaktTelefon
            // 
            this.lblKontaktTelefon.AutoSize = true;
            this.lblKontaktTelefon.Location = new System.Drawing.Point(11, 140);
            this.lblKontaktTelefon.Name = "lblKontaktTelefon";
            this.lblKontaktTelefon.Size = new System.Drawing.Size(107, 17);
            this.lblKontaktTelefon.TabIndex = 1;
            this.lblKontaktTelefon.Text = "Kontakt telefon:";
            // 
            // txtKontakTelefon
            // 
            this.txtKontakTelefon.Location = new System.Drawing.Point(125, 137);
            this.txtKontakTelefon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtKontakTelefon.MaxLength = 10;
            this.txtKontakTelefon.Name = "txtKontakTelefon";
            this.txtKontakTelefon.Size = new System.Drawing.Size(100, 22);
            this.txtKontakTelefon.TabIndex = 2;
            // 
            // btnGetUser
            // 
            this.btnGetUser.Location = new System.Drawing.Point(52, 199);
            this.btnGetUser.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnGetUser.Name = "btnGetUser";
            this.btnGetUser.Size = new System.Drawing.Size(211, 36);
            this.btnGetUser.TabIndex = 3;
            this.btnGetUser.Text = "Pribavi podatke o korisniku";
            this.btnGetUser.UseVisualStyleBackColor = true;
            this.btnGetUser.Click += new System.EventHandler(this.btnGetUser_Click);
            // 
            // grpKorisnik
            // 
            this.grpKorisnik.Controls.Add(this.txtKontakTelefon);
            this.grpKorisnik.Controls.Add(this.btnCreateUser);
            this.grpKorisnik.Controls.Add(this.lblKontaktTelefon);
            this.grpKorisnik.Controls.Add(this.btnGetUser);
            this.grpKorisnik.Location = new System.Drawing.Point(12, 12);
            this.grpKorisnik.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpKorisnik.Name = "grpKorisnik";
            this.grpKorisnik.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpKorisnik.Size = new System.Drawing.Size(324, 279);
            this.grpKorisnik.TabIndex = 0;
            this.grpKorisnik.TabStop = false;
            this.grpKorisnik.Text = "Korisnik";
            // 
            // grpIzborPaketa
            // 
            this.grpIzborPaketa.Controls.Add(this.btnServiceSelection);
            this.grpIzborPaketa.Controls.Add(this.lstIzborPaketa);
            this.grpIzborPaketa.Location = new System.Drawing.Point(341, 12);
            this.grpIzborPaketa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpIzborPaketa.Name = "grpIzborPaketa";
            this.grpIzborPaketa.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpIzborPaketa.Size = new System.Drawing.Size(1268, 279);
            this.grpIzborPaketa.TabIndex = 1;
            this.grpIzborPaketa.TabStop = false;
            this.grpIzborPaketa.Text = "Izbor paketa";
            // 
            // btnServiceSelection
            // 
            this.btnServiceSelection.Location = new System.Drawing.Point(1064, 199);
            this.btnServiceSelection.Name = "btnServiceSelection";
            this.btnServiceSelection.Size = new System.Drawing.Size(184, 66);
            this.btnServiceSelection.TabIndex = 1;
            this.btnServiceSelection.Text = "Odabir usluge";
            this.btnServiceSelection.UseVisualStyleBackColor = true;
            this.btnServiceSelection.Click += new System.EventHandler(this.btnServiceSelection_Click);
            // 
            // lstIzborPaketa
            // 
            this.lstIzborPaketa.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.lstIzborPaketa.FullRowSelect = true;
            this.lstIzborPaketa.GridLines = true;
            this.lstIzborPaketa.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstIzborPaketa.HideSelection = false;
            this.lstIzborPaketa.Location = new System.Drawing.Point(17, 39);
            this.lstIzborPaketa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstIzborPaketa.MultiSelect = false;
            this.lstIzborPaketa.Name = "lstIzborPaketa";
            this.lstIzborPaketa.Size = new System.Drawing.Size(1027, 226);
            this.lstIzborPaketa.TabIndex = 0;
            this.lstIzborPaketa.UseCompatibleStateImageBehavior = false;
            this.lstIzborPaketa.View = System.Windows.Forms.View.Details;
            this.lstIzborPaketa.SelectedIndexChanged += new System.EventHandler(this.lstIzborPaketa_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Naziv paketa";
            this.columnHeader1.Width = 117;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Cena";
            this.columnHeader2.Width = 68;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Cena dodatnog paketa";
            this.columnHeader3.Width = 156;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Brzina interneta";
            this.columnHeader4.Width = 113;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Cena po MB";
            this.columnHeader5.Width = 99;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Cena po adresi";
            this.columnHeader6.Width = 118;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Broj minuta";
            this.columnHeader7.Width = 95;
            // 
            // btnKreirajUgovor
            // 
            this.btnKreirajUgovor.Location = new System.Drawing.Point(665, 340);
            this.btnKreirajUgovor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnKreirajUgovor.Name = "btnKreirajUgovor";
            this.btnKreirajUgovor.Size = new System.Drawing.Size(380, 65);
            this.btnKreirajUgovor.TabIndex = 3;
            this.btnKreirajUgovor.Text = "Kreirajte ugovor";
            this.btnKreirajUgovor.UseVisualStyleBackColor = true;
            this.btnKreirajUgovor.Click += new System.EventHandler(this.btnKreirajUgovor_Click);
            // 
            // radioButton1year
            // 
            this.radioButton1year.AutoSize = true;
            this.radioButton1year.Location = new System.Drawing.Point(40, 38);
            this.radioButton1year.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton1year.Name = "radioButton1year";
            this.radioButton1year.Size = new System.Drawing.Size(105, 21);
            this.radioButton1year.TabIndex = 0;
            this.radioButton1year.TabStop = true;
            this.radioButton1year.Text = "godinu dana";
            this.radioButton1year.UseVisualStyleBackColor = true;
            this.radioButton1year.Click += new System.EventHandler(this.radioButton1year_Click);
            // 
            // radioButton2Years
            // 
            this.radioButton2Years.AutoSize = true;
            this.radioButton2Years.Location = new System.Drawing.Point(40, 64);
            this.radioButton2Years.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButton2Years.Name = "radioButton2Years";
            this.radioButton2Years.Size = new System.Drawing.Size(96, 21);
            this.radioButton2Years.TabIndex = 1;
            this.radioButton2Years.TabStop = true;
            this.radioButton2Years.Text = "dve godine";
            this.radioButton2Years.UseVisualStyleBackColor = true;
            this.radioButton2Years.Click += new System.EventHandler(this.radioButton2Years_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton1year);
            this.groupBox1.Controls.Add(this.radioButton2Years);
            this.groupBox1.Location = new System.Drawing.Point(12, 298);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(207, 112);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ugovor na:";
            // 
            // frmContract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1700, 420);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnKreirajUgovor);
            this.Controls.Add(this.grpIzborPaketa);
            this.Controls.Add(this.grpKorisnik);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmContract";
            this.Text = "Novi ugovor";
            this.Load += new System.EventHandler(this.frmContract_Load);
            this.grpKorisnik.ResumeLayout(false);
            this.grpKorisnik.PerformLayout();
            this.grpIzborPaketa.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateUser;
        private System.Windows.Forms.Label lblKontaktTelefon;
        private System.Windows.Forms.TextBox txtKontakTelefon;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Button btnGetUser;
        private System.Windows.Forms.GroupBox grpKorisnik;
        private System.Windows.Forms.GroupBox grpIzborPaketa;
        private System.Windows.Forms.ListView lstIzborPaketa;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Button btnKreirajUgovor;
        private System.Windows.Forms.RadioButton radioButton1year;
        private System.Windows.Forms.RadioButton radioButton2Years;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnServiceSelection;
    }
}