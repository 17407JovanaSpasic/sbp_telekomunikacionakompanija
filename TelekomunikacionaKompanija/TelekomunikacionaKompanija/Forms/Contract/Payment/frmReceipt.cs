﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers.Contract;
using TelekomunikacionaKompanija.DTOs.Contract;
using TelekomunikacionaKompanija.DTOs.Contract.Payment;
using TelekomunikacionaKompanija.DTOs.Contract.Service;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Internet;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Telephony;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Television;
using TelekomunikacionaKompanija.DTOs.User;

namespace TelekomunikacionaKompanija.Forms.Contract.Payment
{
    public partial class frmReceipt : Form
    {
        #region Attribute(s)
        public string contactPhone;
        public string packageName;
        public UserDTO user = new UserDTO();
        public ContractDTO contract = new ContractDTO();
        public ServiceDTO service = new ServiceDTO();
        public List<AdditionalChannelPackageDTO> listOfAdditionalPackages = new List<AdditionalChannelPackageDTO>();
        public List<PhoneNumberDTO> listOfAllPhoneNumbers = new List<PhoneNumberDTO>();

        float totalPricePerFlow = 0;
        float totalPriceFlatRate = 0;
        float priceOfAdditionalPackages = 0;
        float totalCost = 0;

        bool isFlateRateInternet;
        bool isRealizedInternetFlow;
        #endregion Attribute(s)

        #region Constructor(s)
        public frmReceipt()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmReceipt_Load(object sender, EventArgs e)
        {
            this.lblBrojDodatnihPaketa.Text = "0";
            this.lblIznosZaTeleviziju.Text = "0";
            this.lblBrojMinuta.Text = "0";
            this.lblIznosZaInternet.Text = "0";
            this.lblCenaFlatRateAdrese.Text = "0";
            this.lblBrojAdresa.Text = "0";
            this.lblCenaPoMB.Text = "0";
            this.lblKolicina.Text = "0";
            this.lblIznosZaInternet.Text = "0";
            this.lblUkupanIznosZaPlacanje.Text = "0";

            //pribavljanje kompletnih podataka o korisniku, ukljucujuci listu ugovora i pakete koje ima za te ugovore
            //UserDTOManager.GetUserByContactPhoneForPayment(this.contactPhone, this.packageName, ref user, ref contract);
            ContractDTOManager.GetInformationUsersByContactPhoneForPayment(this.contactPhone, this.packageName, ref user, ref contract);
            this.service = ServiceDTOManager.GetServiceByID(contract.ServiceId.ServiceID);

            //popunjavanje opstih informacija o korisniku
            this.lblIme.Text = user.Name;
            this.lblPrezime.Text = user.LastName;
            this.lblNazivPaketa.Text = contract.PackageName.Name;
            string cenaOsnovnogPaketa = contract.PackageName.Price.ToString();
            this.lblOsnovnaCenaPaketa.Text = cenaOsnovnogPaketa;
            this.lblPocetakUgovora.Text = contract.ContractStartDate.ToString();
            this.lblKrajUgovora.Text = contract.ContractEndDate.ToString();

            if (service.ContainsTelevision == true)
            {
                if (service.AdditionalChannelPackages.Count > 0)
                {
                    listOfAdditionalPackages = (List<AdditionalChannelPackageDTO>)service.AdditionalChannelPackages;
                    this.lstDodatniPaketi.Items.Clear();
                    foreach (var i in listOfAdditionalPackages)
                    {
                        var item = new ListViewItem(new string[] { i.Name });

                        this.lstDodatniPaketi.Items.Add(item);
                    }
                    this.lstDodatniPaketi.Refresh();

                    this.lblBrojDodatnihPaketa.Text = listOfAdditionalPackages.Count.ToString();

                    this.priceOfAdditionalPackages = listOfAdditionalPackages.Count * contract.PackageName.AdditionalPackagePrice;
                    string iznosCeneZaTeleviziju = priceOfAdditionalPackages.ToString();
                    this.lblIznosZaTeleviziju.Text = iznosCeneZaTeleviziju;
                }
            }
            else //ako paket ne sadrzi televiziju onda se nista ne popunjava
            {
                this.grpTelevizija.Enabled = false;
                this.lblNumberAdditionalPackages.Enabled = false;
                this.lblBrojDodatnihPaketa.Enabled = false;

                this.lstDodatniPaketi.Enabled = false;

                this.lblPriceForTv.Enabled = false;
                this.lblIznosZaTeleviziju.Enabled = false;
            }

            if (service.ContainsTelephony == true)
            {
                if (service.PhoneNumbers.Count > 0)
                {
                    this.listOfAllPhoneNumbers = (List<PhoneNumberDTO>)service.PhoneNumbers;
                    int totalNumberOfMinutes = 0;
                    this.lstTelefonija.Items.Clear();
                    foreach (var i in listOfAllPhoneNumbers)
                    {
                        var item = new ListViewItem(new string[] { i.PhoneNumber, i.NumberOfMinutes.ToString() });
                        totalNumberOfMinutes += i.NumberOfMinutes;
                        this.lstTelefonija.Items.Add(item);
                    }
                    this.lstTelefonija.Refresh();

                    this.lblBrojMinuta.Text = totalNumberOfMinutes.ToString();
                }
            }
            else
            {
                this.grpTelefonija.Enabled = false;
                this.lblNumberOfMinutes.Enabled = false;
                this.lblBrojMinuta.Enabled = false;
                this.lstTelefonija.Enabled = false;
            }



            this.grpOstvareniProtok.Enabled = false;
            this.lblPricePerFlow.Enabled = false;
            this.lblCenaPoMB.Enabled = false;
            this.lblAmount.Enabled = false;
            this.lblKolicina.Enabled = false;

            this.grpFlatRate.Enabled = false;
            this.lblCenaFlatRateAdrese.Enabled = false;
            this.lblCostOfFlatRate.Enabled = false;
            this.lblNumberOfAddres.Enabled = false;
            this.lblBrojAdresa.Enabled = false;

            if (service.ContainsInternet == true)
            {
                if (service.Internet.InternetType == DTOs.Contract.Service.Internet.InternetType.Prepaid)
                {
                    this.isRealizedInternetFlow = true;

                    this.grpOstvareniProtok.Enabled = true;
                    this.lblPricePerFlow.Enabled = true;
                    this.lblCenaPoMB.Enabled = true;
                    this.lblAmount.Enabled = true;
                    this.lblKolicina.Enabled = true; ;

                    var realizedInternetFlowObj = service.Internet as PrepaidInternetDTO;
                    float pricePerFlow = contract.PackageName.PricePerMB;
                    this.totalPricePerFlow = pricePerFlow * realizedInternetFlowObj.Balance;

                    this.lblCenaPoMB.Text = pricePerFlow.ToString();
                    this.lblKolicina.Text = realizedInternetFlowObj.Balance.ToString();
                    this.lblIznosZaInternet.Text = totalPricePerFlow.ToString();
                }
                else //Ovo je postpaid
                {
                    this.isFlateRateInternet = true;

                    this.grpFlatRate.Enabled = true;
                    this.lblCenaFlatRateAdrese.Enabled = true;
                    this.lblCostOfFlatRate.Enabled = true;
                    this.lblNumberOfAddres.Enabled = true;
                    this.lblBrojAdresa.Enabled = true;

                    //kalkulacija cene
                    float pricePerAddres = contract.PackageName.PricePerAddress;
                    int numberOfAddreses = 0;

                    if (service.Internet.Address1 != null)
                        numberOfAddreses++;

                    if (service.Internet.Address2 != null)
                        numberOfAddreses++;

                    this.totalPriceFlatRate = pricePerAddres * numberOfAddreses;

                    this.lblCenaFlatRateAdrese.Text = pricePerAddres.ToString();
                    this.lblBrojAdresa.Text = numberOfAddreses.ToString();
                    string iznosCeneZaInternet = totalPriceFlatRate.ToString();
                    this.lblIznosZaInternet.Text = iznosCeneZaInternet;

                }
            }
            this.totalCost = contract.PackageName.Price + this.totalPriceFlatRate + this.totalPricePerFlow + this.priceOfAdditionalPackages;
            string iznosUkupneCeneZaPlacanje = totalCost.ToString();
            this.lblUkupanIznosZaPlacanje.Text = iznosUkupneCeneZaPlacanje;
        }

        private void btnIzvrsiPlacanje_Click(object sender, EventArgs e)
        {
            var contract = ContractDTOManager.GetContractById(this.contract.ContractId);

            RealizedInternetFlowDTO realizedIF = null;
            FlatRateInternetDTO flatRateI = null;
            if (isRealizedInternetFlow)
            {
                realizedIF = new RealizedInternetFlowDTO(DateTime.Now, float.Parse(this.lblIznosZaTeleviziju.Text), float.Parse(this.lblUkupanIznosZaPlacanje.Text), contract,
                float.Parse(this.lblKolicina.Text), float.Parse(lblCenaPoMB.Text));
            }
            else if (isFlateRateInternet)
            {
                flatRateI = new FlatRateInternetDTO(DateTime.Now, float.Parse(this.lblIznosZaTeleviziju.Text),
                float.Parse(this.lblUkupanIznosZaPlacanje.Text), contract, float.Parse(this.lblCenaFlatRateAdrese.Text));

            }

            if (PaymentDTOManager.CreatePayment(flatRateI, realizedIF))
            {
                MessageBox.Show("Uspešno kreirana transakcija!", "Obaveštenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            else
                MessageBox.Show("Greška prilikom kreiranja transakcije!", "Greška",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        #endregion Form Event Method(s)
    }
}
