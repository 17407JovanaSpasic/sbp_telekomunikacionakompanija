﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers.Contract;
using TelekomunikacionaKompanija.DTOs.Contract;
using TelekomunikacionaKompanija.DTOs.Contract.Payment;
using TelekomunikacionaKompanija.DTOs.Package;
using TelekomunikacionaKompanija.DTOs.User;

namespace TelekomunikacionaKompanija.Forms.Contract.Payment
{
    public partial class frmPayment : Form
    {
        #region Attribute(s)
        private List<PaymentDTO> listOfAllPayements;
        private List<ContractDTO> listOfContracts = new List<ContractDTO>();
        private List<UserDTO> listOfUsers = new List<UserDTO>();
        private List<PackageDTO> listOfPackages = new List<PackageDTO>();
        #endregion Attribute(s)

        #region Constructor(s)
        public frmPayment()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void lnkPocetnaStrana_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void frmPayment_Load(object sender, EventArgs e)
        {
            this.lstPlacanje.Items.Clear();
            this.listOfAllPayements = PaymentDTOManager.GetAllPayments();

            foreach (var p in listOfAllPayements)
            {
                var item = new ListViewItem(new string[]
                {
                    p.Ime,
                    p.Prezime,
                    p.KontaktTelefon,
                    p.PaymentNumber.ToString(),
                    p.PaymentDate.ToString(),
                    p.AdditionalPackagePrice.ToString(),
                    p.TotalCost.ToString(),
                    p.BrojUgovora,
                    p.NazivPaketa

                });
                this.lstPlacanje.Items.Add(item);
            }
            this.lstPlacanje.Refresh();
            this.btnBrisanjeNarudzbine.Enabled = false;
        }

        private void btnBrisanjeNarudzbine_Click(object sender, EventArgs e)
        {
            int paymentId = int.Parse(this.lstPlacanje.SelectedItems[0].SubItems[3].Text);
            DialogResult result = MessageBox.Show("Da li ste sigurni da želite da obrišete transakciju " +
                "\"" + paymentId + "\"" + "?", "Potvrda brisanja",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No)
                return;
            if (PaymentDTOManager.DeletePayement(paymentId))
            {
                MessageBox.Show("Uspešno obrisana transakcija!", "Obaveštenje", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                var newFrmPayment = new frmPayment();
                newFrmPayment.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Dogodila se greška prilikom brisanja transakcije, molimo Vas pokusajte operaciju kasnije!",
                    "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnIzmenaNarudzbinu_Click(object sender, EventArgs e) { }

        private void btnPokreniPlacanje_Click(object sender, EventArgs e)
        {
            var newTypeNumber = new frmTypeNumber();
            newTypeNumber.Show();
            this.Close();
        }

        private void lstPlacanje_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lstPlacanje.SelectedItems.Count == 0)
            {
                this.btnBrisanjeNarudzbine.Enabled = false;
            }
            else
            {
                this.btnBrisanjeNarudzbine.Enabled = true;
            }
        }
        #endregion Form Event Method(s)
    }
}
