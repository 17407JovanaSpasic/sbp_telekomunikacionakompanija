﻿
namespace TelekomunikacionaKompanija.Forms.Contract.Payment
{
    partial class frmPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lnkPocetnaStrana = new System.Windows.Forms.LinkLabel();
            this.btnPokreniPlacanje = new System.Windows.Forms.Button();
            this.btnBrisanjeNarudzbine = new System.Windows.Forms.Button();
            this.grpOstvoreniProtok = new System.Windows.Forms.GroupBox();
            this.lstPlacanje = new System.Windows.Forms.ListView();
            this.clmIme = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmPrezime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmKontaktTelefon = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmBrRacuna = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmDatumPlacanja = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmCenaDodatnihPaketa = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmUkupnaCena = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmBrUgovora = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmNazivPaketa = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Operacije = new System.Windows.Forms.GroupBox();
            this.grpOstvoreniProtok.SuspendLayout();
            this.Operacije.SuspendLayout();
            this.SuspendLayout();
            // 
            // lnkPocetnaStrana
            // 
            this.lnkPocetnaStrana.AutoSize = true;
            this.lnkPocetnaStrana.Location = new System.Drawing.Point(15, 11);
            this.lnkPocetnaStrana.Name = "lnkPocetnaStrana";
            this.lnkPocetnaStrana.Size = new System.Drawing.Size(104, 17);
            this.lnkPocetnaStrana.TabIndex = 0;
            this.lnkPocetnaStrana.TabStop = true;
            this.lnkPocetnaStrana.Text = "Početna strana";
            this.lnkPocetnaStrana.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkPocetnaStrana_LinkClicked);
            // 
            // btnPokreniPlacanje
            // 
            this.btnPokreniPlacanje.Location = new System.Drawing.Point(29, 34);
            this.btnPokreniPlacanje.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPokreniPlacanje.Name = "btnPokreniPlacanje";
            this.btnPokreniPlacanje.Size = new System.Drawing.Size(156, 46);
            this.btnPokreniPlacanje.TabIndex = 0;
            this.btnPokreniPlacanje.Text = "Pokretanje transkacije";
            this.btnPokreniPlacanje.UseVisualStyleBackColor = true;
            this.btnPokreniPlacanje.Click += new System.EventHandler(this.btnPokreniPlacanje_Click);
            // 
            // btnBrisanjeNarudzbine
            // 
            this.btnBrisanjeNarudzbine.Enabled = false;
            this.btnBrisanjeNarudzbine.Location = new System.Drawing.Point(211, 34);
            this.btnBrisanjeNarudzbine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBrisanjeNarudzbine.Name = "btnBrisanjeNarudzbine";
            this.btnBrisanjeNarudzbine.Size = new System.Drawing.Size(187, 46);
            this.btnBrisanjeNarudzbine.TabIndex = 2;
            this.btnBrisanjeNarudzbine.Text = "Brisanje narudžbine";
            this.btnBrisanjeNarudzbine.UseVisualStyleBackColor = true;
            this.btnBrisanjeNarudzbine.Click += new System.EventHandler(this.btnBrisanjeNarudzbine_Click);
            // 
            // grpOstvoreniProtok
            // 
            this.grpOstvoreniProtok.Controls.Add(this.lstPlacanje);
            this.grpOstvoreniProtok.Location = new System.Drawing.Point(15, 164);
            this.grpOstvoreniProtok.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpOstvoreniProtok.Name = "grpOstvoreniProtok";
            this.grpOstvoreniProtok.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpOstvoreniProtok.Size = new System.Drawing.Size(1364, 372);
            this.grpOstvoreniProtok.TabIndex = 2;
            this.grpOstvoreniProtok.TabStop = false;
            this.grpOstvoreniProtok.Text = "Plaćanja";
            // 
            // lstPlacanje
            // 
            this.lstPlacanje.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clmIme,
            this.clmPrezime,
            this.clmKontaktTelefon,
            this.clmBrRacuna,
            this.clmDatumPlacanja,
            this.clmCenaDodatnihPaketa,
            this.clmUkupnaCena,
            this.clmBrUgovora,
            this.clmNazivPaketa});
            this.lstPlacanje.FullRowSelect = true;
            this.lstPlacanje.GridLines = true;
            this.lstPlacanje.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstPlacanje.HideSelection = false;
            this.lstPlacanje.Location = new System.Drawing.Point(5, 33);
            this.lstPlacanje.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstPlacanje.MultiSelect = false;
            this.lstPlacanje.Name = "lstPlacanje";
            this.lstPlacanje.Size = new System.Drawing.Size(1341, 315);
            this.lstPlacanje.TabIndex = 0;
            this.lstPlacanje.UseCompatibleStateImageBehavior = false;
            this.lstPlacanje.View = System.Windows.Forms.View.Details;
            this.lstPlacanje.SelectedIndexChanged += new System.EventHandler(this.lstPlacanje_SelectedIndexChanged);
            // 
            // clmIme
            // 
            this.clmIme.Text = "Ime";
            this.clmIme.Width = 76;
            // 
            // clmPrezime
            // 
            this.clmPrezime.Text = "Prezime";
            this.clmPrezime.Width = 95;
            // 
            // clmKontaktTelefon
            // 
            this.clmKontaktTelefon.Text = "Kontakt telefon";
            this.clmKontaktTelefon.Width = 118;
            // 
            // clmBrRacuna
            // 
            this.clmBrRacuna.Text = "Broj računa";
            this.clmBrRacuna.Width = 115;
            // 
            // clmDatumPlacanja
            // 
            this.clmDatumPlacanja.Text = "Datum plaćanja";
            this.clmDatumPlacanja.Width = 123;
            // 
            // clmCenaDodatnihPaketa
            // 
            this.clmCenaDodatnihPaketa.Text = "Cena dodatnih paketa";
            this.clmCenaDodatnihPaketa.Width = 150;
            // 
            // clmUkupnaCena
            // 
            this.clmUkupnaCena.Text = "Ukupna cena";
            this.clmUkupnaCena.Width = 103;
            // 
            // clmBrUgovora
            // 
            this.clmBrUgovora.Text = "Broj ugovora";
            this.clmBrUgovora.Width = 93;
            // 
            // clmNazivPaketa
            // 
            this.clmNazivPaketa.Text = "Naziv paketa";
            this.clmNazivPaketa.Width = 117;
            // 
            // Operacije
            // 
            this.Operacije.Controls.Add(this.btnBrisanjeNarudzbine);
            this.Operacije.Controls.Add(this.btnPokreniPlacanje);
            this.Operacije.Location = new System.Drawing.Point(15, 44);
            this.Operacije.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Operacije.Name = "Operacije";
            this.Operacije.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Operacije.Size = new System.Drawing.Size(430, 102);
            this.Operacije.TabIndex = 1;
            this.Operacije.TabStop = false;
            this.Operacije.Text = "Operacije";
            // 
            // frmPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1391, 549);
            this.Controls.Add(this.Operacije);
            this.Controls.Add(this.grpOstvoreniProtok);
            this.Controls.Add(this.lnkPocetnaStrana);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPayment";
            this.Text = "Prikaz svih plaćanja";
            this.Load += new System.EventHandler(this.frmPayment_Load);
            this.grpOstvoreniProtok.ResumeLayout(false);
            this.Operacije.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel lnkPocetnaStrana;

        private System.Windows.Forms.Button btnPokreniPlacanje;
        private System.Windows.Forms.Button btnBrisanjeNarudzbine;
        private System.Windows.Forms.GroupBox grpOstvoreniProtok;
        private System.Windows.Forms.GroupBox Operacije;
        private System.Windows.Forms.ListView lstPlacanje;
        private System.Windows.Forms.ColumnHeader clmIme;
        private System.Windows.Forms.ColumnHeader clmPrezime;
        private System.Windows.Forms.ColumnHeader clmKontaktTelefon;
        private System.Windows.Forms.ColumnHeader clmBrRacuna;
        private System.Windows.Forms.ColumnHeader clmDatumPlacanja;
        private System.Windows.Forms.ColumnHeader clmCenaDodatnihPaketa;
        private System.Windows.Forms.ColumnHeader clmUkupnaCena;
        private System.Windows.Forms.ColumnHeader clmBrUgovora;
        private System.Windows.Forms.ColumnHeader clmNazivPaketa;
    }
}