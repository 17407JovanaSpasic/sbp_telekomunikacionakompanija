﻿
namespace TelekomunikacionaKompanija.Forms.Contract.Payment
{
    partial class frmReceipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpKorisnik = new System.Windows.Forms.GroupBox();
            this.lblOsnovnaCenaPaketa = new System.Windows.Forms.Label();
            this.lblKrajUgovora = new System.Windows.Forms.Label();
            this.lblBasicPrice = new System.Windows.Forms.Label();
            this.lblPocetakUgovora = new System.Windows.Forms.Label();
            this.lblNazivPaketa = new System.Windows.Forms.Label();
            this.lblPrezime = new System.Windows.Forms.Label();
            this.lblIme = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.lblPackage = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.grpTelevizija = new System.Windows.Forms.GroupBox();
            this.lblIznosZaTeleviziju = new System.Windows.Forms.Label();
            this.lblPriceForTv = new System.Windows.Forms.Label();
            this.lstDodatniPaketi = new System.Windows.Forms.ListView();
            this.clmNazivDodatnihPaketa = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblBrojDodatnihPaketa = new System.Windows.Forms.Label();
            this.lblNumberAdditionalPackages = new System.Windows.Forms.Label();
            this.grpTelefonija = new System.Windows.Forms.GroupBox();
            this.lstTelefonija = new System.Windows.Forms.ListView();
            this.clmBrojTelefona = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmBrojMinuta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblBrojMinuta = new System.Windows.Forms.Label();
            this.lblNumberOfMinutes = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblIznosZaInternet = new System.Windows.Forms.Label();
            this.lblPriceForInternet = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grpOstvareniProtok = new System.Windows.Forms.GroupBox();
            this.lblCenaPoMB = new System.Windows.Forms.Label();
            this.lblKolicina = new System.Windows.Forms.Label();
            this.lblPricePerFlow = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.grpFlatRate = new System.Windows.Forms.GroupBox();
            this.lblCenaFlatRateAdrese = new System.Windows.Forms.Label();
            this.lblBrojAdresa = new System.Windows.Forms.Label();
            this.lblNumberOfAddres = new System.Windows.Forms.Label();
            this.lblCostOfFlatRate = new System.Windows.Forms.Label();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.lblUkupanIznosZaPlacanje = new System.Windows.Forms.Label();
            this.btnIzvrsiPlacanje = new System.Windows.Forms.Button();
            this.lblRsdPaketa = new System.Windows.Forms.Label();
            this.lblRsd2 = new System.Windows.Forms.Label();
            this.lblRsd3 = new System.Windows.Forms.Label();
            this.lblRsd4 = new System.Windows.Forms.Label();
            this.grpKorisnik.SuspendLayout();
            this.grpTelevizija.SuspendLayout();
            this.grpTelefonija.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.grpOstvareniProtok.SuspendLayout();
            this.grpFlatRate.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpKorisnik
            // 
            this.grpKorisnik.Controls.Add(this.lblRsdPaketa);
            this.grpKorisnik.Controls.Add(this.lblOsnovnaCenaPaketa);
            this.grpKorisnik.Controls.Add(this.lblKrajUgovora);
            this.grpKorisnik.Controls.Add(this.lblBasicPrice);
            this.grpKorisnik.Controls.Add(this.lblPocetakUgovora);
            this.grpKorisnik.Controls.Add(this.lblNazivPaketa);
            this.grpKorisnik.Controls.Add(this.lblPrezime);
            this.grpKorisnik.Controls.Add(this.lblIme);
            this.grpKorisnik.Controls.Add(this.lblEndDate);
            this.grpKorisnik.Controls.Add(this.lblStartDate);
            this.grpKorisnik.Controls.Add(this.lblPackage);
            this.grpKorisnik.Controls.Add(this.lblLastName);
            this.grpKorisnik.Controls.Add(this.lblName);
            this.grpKorisnik.Location = new System.Drawing.Point(15, 17);
            this.grpKorisnik.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpKorisnik.Name = "grpKorisnik";
            this.grpKorisnik.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpKorisnik.Size = new System.Drawing.Size(387, 270);
            this.grpKorisnik.TabIndex = 0;
            this.grpKorisnik.TabStop = false;
            this.grpKorisnik.Text = "Informacije o korisniku";
            // 
            // lblOsnovnaCenaPaketa
            // 
            this.lblOsnovnaCenaPaketa.AutoSize = true;
            this.lblOsnovnaCenaPaketa.Location = new System.Drawing.Point(216, 145);
            this.lblOsnovnaCenaPaketa.Name = "lblOsnovnaCenaPaketa";
            this.lblOsnovnaCenaPaketa.Size = new System.Drawing.Size(46, 17);
            this.lblOsnovnaCenaPaketa.TabIndex = 7;
            this.lblOsnovnaCenaPaketa.Text = "label3";
            // 
            // lblKrajUgovora
            // 
            this.lblKrajUgovora.AutoSize = true;
            this.lblKrajUgovora.Location = new System.Drawing.Point(216, 215);
            this.lblKrajUgovora.Name = "lblKrajUgovora";
            this.lblKrajUgovora.Size = new System.Drawing.Size(54, 17);
            this.lblKrajUgovora.TabIndex = 11;
            this.lblKrajUgovora.Text = "label10";
            // 
            // lblBasicPrice
            // 
            this.lblBasicPrice.AutoSize = true;
            this.lblBasicPrice.Location = new System.Drawing.Point(21, 145);
            this.lblBasicPrice.Name = "lblBasicPrice";
            this.lblBasicPrice.Size = new System.Drawing.Size(151, 17);
            this.lblBasicPrice.TabIndex = 6;
            this.lblBasicPrice.Text = "Osnovna cena paketa:";
            // 
            // lblPocetakUgovora
            // 
            this.lblPocetakUgovora.AutoSize = true;
            this.lblPocetakUgovora.Location = new System.Drawing.Point(216, 178);
            this.lblPocetakUgovora.Name = "lblPocetakUgovora";
            this.lblPocetakUgovora.Size = new System.Drawing.Size(46, 17);
            this.lblPocetakUgovora.TabIndex = 9;
            this.lblPocetakUgovora.Text = "label9";
            // 
            // lblNazivPaketa
            // 
            this.lblNazivPaketa.AutoSize = true;
            this.lblNazivPaketa.Location = new System.Drawing.Point(216, 114);
            this.lblNazivPaketa.Name = "lblNazivPaketa";
            this.lblNazivPaketa.Size = new System.Drawing.Size(46, 17);
            this.lblNazivPaketa.TabIndex = 5;
            this.lblNazivPaketa.Text = "label8";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Location = new System.Drawing.Point(216, 82);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(46, 17);
            this.lblPrezime.TabIndex = 3;
            this.lblPrezime.Text = "label7";
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(216, 47);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(46, 17);
            this.lblIme.TabIndex = 1;
            this.lblIme.Text = "label6";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(21, 215);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(150, 17);
            this.lblEndDate.TabIndex = 10;
            this.lblEndDate.Text = "Datum isteka ugovora:";
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(21, 178);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(188, 17);
            this.lblStartDate.TabIndex = 8;
            this.lblStartDate.Text = "Datum potpisivanja ugovora:";
            // 
            // lblPackage
            // 
            this.lblPackage.AutoSize = true;
            this.lblPackage.Location = new System.Drawing.Point(21, 114);
            this.lblPackage.Name = "lblPackage";
            this.lblPackage.Size = new System.Drawing.Size(94, 17);
            this.lblPackage.TabIndex = 4;
            this.lblPackage.Text = "Naziv paketa:";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(21, 82);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(63, 17);
            this.lblLastName.TabIndex = 2;
            this.lblLastName.Text = "Prezime:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(21, 47);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(34, 17);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Ime:";
            // 
            // grpTelevizija
            // 
            this.grpTelevizija.Controls.Add(this.lblRsd2);
            this.grpTelevizija.Controls.Add(this.lblIznosZaTeleviziju);
            this.grpTelevizija.Controls.Add(this.lblPriceForTv);
            this.grpTelevizija.Controls.Add(this.lstDodatniPaketi);
            this.grpTelevizija.Controls.Add(this.lblBrojDodatnihPaketa);
            this.grpTelevizija.Controls.Add(this.lblNumberAdditionalPackages);
            this.grpTelevizija.Location = new System.Drawing.Point(407, 17);
            this.grpTelevizija.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpTelevizija.Name = "grpTelevizija";
            this.grpTelevizija.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpTelevizija.Size = new System.Drawing.Size(284, 270);
            this.grpTelevizija.TabIndex = 1;
            this.grpTelevizija.TabStop = false;
            this.grpTelevizija.Text = "Televizija";
            // 
            // lblIznosZaTeleviziju
            // 
            this.lblIznosZaTeleviziju.AutoSize = true;
            this.lblIznosZaTeleviziju.Location = new System.Drawing.Point(68, 236);
            this.lblIznosZaTeleviziju.Name = "lblIznosZaTeleviziju";
            this.lblIznosZaTeleviziju.Size = new System.Drawing.Size(46, 17);
            this.lblIznosZaTeleviziju.TabIndex = 4;
            this.lblIznosZaTeleviziju.Text = "label3";
            // 
            // lblPriceForTv
            // 
            this.lblPriceForTv.AutoSize = true;
            this.lblPriceForTv.Location = new System.Drawing.Point(16, 236);
            this.lblPriceForTv.Name = "lblPriceForTv";
            this.lblPriceForTv.Size = new System.Drawing.Size(45, 17);
            this.lblPriceForTv.TabIndex = 3;
            this.lblPriceForTv.Text = "Iznos:";
            // 
            // lstDodatniPaketi
            // 
            this.lstDodatniPaketi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clmNazivDodatnihPaketa});
            this.lstDodatniPaketi.GridLines = true;
            this.lstDodatniPaketi.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstDodatniPaketi.HideSelection = false;
            this.lstDodatniPaketi.Location = new System.Drawing.Point(20, 52);
            this.lstDodatniPaketi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstDodatniPaketi.Name = "lstDodatniPaketi";
            this.lstDodatniPaketi.Size = new System.Drawing.Size(219, 166);
            this.lstDodatniPaketi.TabIndex = 2;
            this.lstDodatniPaketi.UseCompatibleStateImageBehavior = false;
            this.lstDodatniPaketi.View = System.Windows.Forms.View.Details;
            // 
            // clmNazivDodatnihPaketa
            // 
            this.clmNazivDodatnihPaketa.Text = "Dodatni paketi";
            this.clmNazivDodatnihPaketa.Width = 117;
            // 
            // lblBrojDodatnihPaketa
            // 
            this.lblBrojDodatnihPaketa.AutoSize = true;
            this.lblBrojDodatnihPaketa.Location = new System.Drawing.Point(165, 30);
            this.lblBrojDodatnihPaketa.Name = "lblBrojDodatnihPaketa";
            this.lblBrojDodatnihPaketa.Size = new System.Drawing.Size(46, 17);
            this.lblBrojDodatnihPaketa.TabIndex = 1;
            this.lblBrojDodatnihPaketa.Text = "label2";
            // 
            // lblNumberAdditionalPackages
            // 
            this.lblNumberAdditionalPackages.AutoSize = true;
            this.lblNumberAdditionalPackages.Location = new System.Drawing.Point(16, 30);
            this.lblNumberAdditionalPackages.Name = "lblNumberAdditionalPackages";
            this.lblNumberAdditionalPackages.Size = new System.Drawing.Size(143, 17);
            this.lblNumberAdditionalPackages.TabIndex = 0;
            this.lblNumberAdditionalPackages.Text = "Broj dodatnih paketa:";
            // 
            // grpTelefonija
            // 
            this.grpTelefonija.Controls.Add(this.lstTelefonija);
            this.grpTelefonija.Controls.Add(this.lblBrojMinuta);
            this.grpTelefonija.Controls.Add(this.lblNumberOfMinutes);
            this.grpTelefonija.Location = new System.Drawing.Point(696, 17);
            this.grpTelefonija.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpTelefonija.Name = "grpTelefonija";
            this.grpTelefonija.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpTelefonija.Size = new System.Drawing.Size(365, 270);
            this.grpTelefonija.TabIndex = 2;
            this.grpTelefonija.TabStop = false;
            this.grpTelefonija.Text = "Telefonija";
            // 
            // lstTelefonija
            // 
            this.lstTelefonija.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clmBrojTelefona,
            this.clmBrojMinuta});
            this.lstTelefonija.GridLines = true;
            this.lstTelefonija.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstTelefonija.HideSelection = false;
            this.lstTelefonija.Location = new System.Drawing.Point(11, 52);
            this.lstTelefonija.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstTelefonija.Name = "lstTelefonija";
            this.lstTelefonija.Size = new System.Drawing.Size(344, 166);
            this.lstTelefonija.TabIndex = 2;
            this.lstTelefonija.UseCompatibleStateImageBehavior = false;
            this.lstTelefonija.View = System.Windows.Forms.View.Details;
            // 
            // clmBrojTelefona
            // 
            this.clmBrojTelefona.Text = "Broj telefona";
            this.clmBrojTelefona.Width = 92;
            // 
            // clmBrojMinuta
            // 
            this.clmBrojMinuta.Text = "Broj ostvarenih minuta";
            this.clmBrojMinuta.Width = 152;
            // 
            // lblBrojMinuta
            // 
            this.lblBrojMinuta.AutoSize = true;
            this.lblBrojMinuta.Location = new System.Drawing.Point(217, 30);
            this.lblBrojMinuta.Name = "lblBrojMinuta";
            this.lblBrojMinuta.Size = new System.Drawing.Size(46, 17);
            this.lblBrojMinuta.TabIndex = 1;
            this.lblBrojMinuta.Text = "label2";
            // 
            // lblNumberOfMinutes
            // 
            this.lblNumberOfMinutes.AutoSize = true;
            this.lblNumberOfMinutes.Location = new System.Drawing.Point(7, 30);
            this.lblNumberOfMinutes.Name = "lblNumberOfMinutes";
            this.lblNumberOfMinutes.Size = new System.Drawing.Size(205, 17);
            this.lblNumberOfMinutes.TabIndex = 0;
            this.lblNumberOfMinutes.Text = "Ukupan broj ostvarenih minuta:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblRsd3);
            this.groupBox4.Controls.Add(this.lblIznosZaInternet);
            this.groupBox4.Controls.Add(this.lblPriceForInternet);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.grpOstvareniProtok);
            this.groupBox4.Controls.Add(this.grpFlatRate);
            this.groupBox4.Location = new System.Drawing.Point(1068, 17);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(407, 270);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Internet";
            // 
            // lblIznosZaInternet
            // 
            this.lblIznosZaInternet.AutoSize = true;
            this.lblIznosZaInternet.Location = new System.Drawing.Point(316, 236);
            this.lblIznosZaInternet.Name = "lblIznosZaInternet";
            this.lblIznosZaInternet.Size = new System.Drawing.Size(46, 17);
            this.lblIznosZaInternet.TabIndex = 1;
            this.lblIznosZaInternet.Text = "label7";
            // 
            // lblPriceForInternet
            // 
            this.lblPriceForInternet.AutoSize = true;
            this.lblPriceForInternet.Location = new System.Drawing.Point(316, 215);
            this.lblPriceForInternet.Name = "lblPriceForInternet";
            this.lblPriceForInternet.Size = new System.Drawing.Size(45, 17);
            this.lblPriceForInternet.TabIndex = 0;
            this.lblPriceForInternet.Text = "Iznos:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Plaćanje po:";
            // 
            // grpOstvareniProtok
            // 
            this.grpOstvareniProtok.Controls.Add(this.lblCenaPoMB);
            this.grpOstvareniProtok.Controls.Add(this.lblKolicina);
            this.grpOstvareniProtok.Controls.Add(this.lblPricePerFlow);
            this.grpOstvareniProtok.Controls.Add(this.lblAmount);
            this.grpOstvareniProtok.Location = new System.Drawing.Point(19, 156);
            this.grpOstvareniProtok.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpOstvareniProtok.Name = "grpOstvareniProtok";
            this.grpOstvareniProtok.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpOstvareniProtok.Size = new System.Drawing.Size(292, 96);
            this.grpOstvareniProtok.TabIndex = 2;
            this.grpOstvareniProtok.TabStop = false;
            this.grpOstvareniProtok.Text = "Ostvarenom protoku";
            // 
            // lblCenaPoMB
            // 
            this.lblCenaPoMB.AutoSize = true;
            this.lblCenaPoMB.Location = new System.Drawing.Point(113, 31);
            this.lblCenaPoMB.Name = "lblCenaPoMB";
            this.lblCenaPoMB.Size = new System.Drawing.Size(46, 17);
            this.lblCenaPoMB.TabIndex = 1;
            this.lblCenaPoMB.Text = "label3";
            // 
            // lblKolicina
            // 
            this.lblKolicina.AutoSize = true;
            this.lblKolicina.Location = new System.Drawing.Point(113, 59);
            this.lblKolicina.Name = "lblKolicina";
            this.lblKolicina.Size = new System.Drawing.Size(46, 17);
            this.lblKolicina.TabIndex = 3;
            this.lblKolicina.Text = "label4";
            // 
            // lblPricePerFlow
            // 
            this.lblPricePerFlow.AutoSize = true;
            this.lblPricePerFlow.Location = new System.Drawing.Point(16, 31);
            this.lblPricePerFlow.Name = "lblPricePerFlow";
            this.lblPricePerFlow.Size = new System.Drawing.Size(89, 17);
            this.lblPricePerFlow.TabIndex = 0;
            this.lblPricePerFlow.Text = "Cena po MB:";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(16, 59);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(61, 17);
            this.lblAmount.TabIndex = 2;
            this.lblAmount.Text = "Količina:";
            // 
            // grpFlatRate
            // 
            this.grpFlatRate.Controls.Add(this.lblCenaFlatRateAdrese);
            this.grpFlatRate.Controls.Add(this.lblBrojAdresa);
            this.grpFlatRate.Controls.Add(this.lblNumberOfAddres);
            this.grpFlatRate.Controls.Add(this.lblCostOfFlatRate);
            this.grpFlatRate.Location = new System.Drawing.Point(19, 52);
            this.grpFlatRate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpFlatRate.Name = "grpFlatRate";
            this.grpFlatRate.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpFlatRate.Size = new System.Drawing.Size(292, 94);
            this.grpFlatRate.TabIndex = 1;
            this.grpFlatRate.TabStop = false;
            this.grpFlatRate.Text = "Flat rate internetu";
            // 
            // lblCenaFlatRateAdrese
            // 
            this.lblCenaFlatRateAdrese.AutoSize = true;
            this.lblCenaFlatRateAdrese.Location = new System.Drawing.Point(180, 31);
            this.lblCenaFlatRateAdrese.Name = "lblCenaFlatRateAdrese";
            this.lblCenaFlatRateAdrese.Size = new System.Drawing.Size(46, 17);
            this.lblCenaFlatRateAdrese.TabIndex = 1;
            this.lblCenaFlatRateAdrese.Text = "label2";
            // 
            // lblBrojAdresa
            // 
            this.lblBrojAdresa.AutoSize = true;
            this.lblBrojAdresa.Location = new System.Drawing.Point(180, 63);
            this.lblBrojAdresa.Name = "lblBrojAdresa";
            this.lblBrojAdresa.Size = new System.Drawing.Size(46, 17);
            this.lblBrojAdresa.TabIndex = 3;
            this.lblBrojAdresa.Text = "label2";
            // 
            // lblNumberOfAddres
            // 
            this.lblNumberOfAddres.AutoSize = true;
            this.lblNumberOfAddres.Location = new System.Drawing.Point(16, 62);
            this.lblNumberOfAddres.Name = "lblNumberOfAddres";
            this.lblNumberOfAddres.Size = new System.Drawing.Size(85, 17);
            this.lblNumberOfAddres.TabIndex = 2;
            this.lblNumberOfAddres.Text = "Broj adresa:";
            // 
            // lblCostOfFlatRate
            // 
            this.lblCostOfFlatRate.AutoSize = true;
            this.lblCostOfFlatRate.Location = new System.Drawing.Point(16, 31);
            this.lblCostOfFlatRate.Name = "lblCostOfFlatRate";
            this.lblCostOfFlatRate.Size = new System.Drawing.Size(160, 17);
            this.lblCostOfFlatRate.TabIndex = 0;
            this.lblCostOfFlatRate.Text = "Cena po flat rate adresi:";
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.AutoSize = true;
            this.lblTotalPrice.Location = new System.Drawing.Point(613, 310);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(174, 17);
            this.lblTotalPrice.TabIndex = 4;
            this.lblTotalPrice.Text = "Ukupan iznos za plaćanje:";
            // 
            // lblUkupanIznosZaPlacanje
            // 
            this.lblUkupanIznosZaPlacanje.AutoSize = true;
            this.lblUkupanIznosZaPlacanje.Location = new System.Drawing.Point(793, 310);
            this.lblUkupanIznosZaPlacanje.Name = "lblUkupanIznosZaPlacanje";
            this.lblUkupanIznosZaPlacanje.Size = new System.Drawing.Size(46, 17);
            this.lblUkupanIznosZaPlacanje.TabIndex = 5;
            this.lblUkupanIznosZaPlacanje.Text = "label9";
            // 
            // btnIzvrsiPlacanje
            // 
            this.btnIzvrsiPlacanje.Location = new System.Drawing.Point(617, 337);
            this.btnIzvrsiPlacanje.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnIzvrsiPlacanje.Name = "btnIzvrsiPlacanje";
            this.btnIzvrsiPlacanje.Size = new System.Drawing.Size(228, 57);
            this.btnIzvrsiPlacanje.TabIndex = 6;
            this.btnIzvrsiPlacanje.Text = "Izvršite plaćanje";
            this.btnIzvrsiPlacanje.UseVisualStyleBackColor = true;
            this.btnIzvrsiPlacanje.Click += new System.EventHandler(this.btnIzvrsiPlacanje_Click);
            // 
            // lblRsdPaketa
            // 
            this.lblRsdPaketa.AutoSize = true;
            this.lblRsdPaketa.Location = new System.Drawing.Point(268, 145);
            this.lblRsdPaketa.Name = "lblRsdPaketa";
            this.lblRsdPaketa.Size = new System.Drawing.Size(37, 17);
            this.lblRsdPaketa.TabIndex = 7;
            this.lblRsdPaketa.Text = "RSD";
            // 
            // lblRsd2
            // 
            this.lblRsd2.AutoSize = true;
            this.lblRsd2.Location = new System.Drawing.Point(120, 236);
            this.lblRsd2.Name = "lblRsd2";
            this.lblRsd2.Size = new System.Drawing.Size(37, 17);
            this.lblRsd2.TabIndex = 5;
            this.lblRsd2.Text = "RSD";
            // 
            // lblRsd3
            // 
            this.lblRsd3.AutoSize = true;
            this.lblRsd3.Location = new System.Drawing.Point(368, 236);
            this.lblRsd3.Name = "lblRsd3";
            this.lblRsd3.Size = new System.Drawing.Size(37, 17);
            this.lblRsd3.TabIndex = 3;
            this.lblRsd3.Text = "RSD";
            // 
            // lblRsd4
            // 
            this.lblRsd4.AutoSize = true;
            this.lblRsd4.Location = new System.Drawing.Point(845, 310);
            this.lblRsd4.Name = "lblRsd4";
            this.lblRsd4.Size = new System.Drawing.Size(37, 17);
            this.lblRsd4.TabIndex = 7;
            this.lblRsd4.Text = "RSD";
            // 
            // frmReceipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1487, 407);
            this.Controls.Add(this.lblRsd4);
            this.Controls.Add(this.btnIzvrsiPlacanje);
            this.Controls.Add(this.lblUkupanIznosZaPlacanje);
            this.Controls.Add(this.lblTotalPrice);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.grpTelefonija);
            this.Controls.Add(this.grpTelevizija);
            this.Controls.Add(this.grpKorisnik);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmReceipt";
            this.Text = "Račun";
            this.Load += new System.EventHandler(this.frmReceipt_Load);
            this.grpKorisnik.ResumeLayout(false);
            this.grpKorisnik.PerformLayout();
            this.grpTelevizija.ResumeLayout(false);
            this.grpTelevizija.PerformLayout();
            this.grpTelefonija.ResumeLayout(false);
            this.grpTelefonija.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.grpOstvareniProtok.ResumeLayout(false);
            this.grpOstvareniProtok.PerformLayout();
            this.grpFlatRate.ResumeLayout(false);
            this.grpFlatRate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpKorisnik;
        private System.Windows.Forms.Label lblKrajUgovora;
        private System.Windows.Forms.Label lblPocetakUgovora;
        private System.Windows.Forms.Label lblNazivPaketa;
        private System.Windows.Forms.Label lblPrezime;
        private System.Windows.Forms.Label lblIme;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Label lblPackage;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.GroupBox grpTelevizija;
        private System.Windows.Forms.GroupBox grpTelefonija;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblBrojDodatnihPaketa;
        private System.Windows.Forms.Label lblNumberAdditionalPackages;
        private System.Windows.Forms.ListView lstDodatniPaketi;
        private System.Windows.Forms.ColumnHeader clmNazivDodatnihPaketa;
        private System.Windows.Forms.Label lblOsnovnaCenaPaketa;
        private System.Windows.Forms.Label lblBasicPrice;
        private System.Windows.Forms.Label lblIznosZaTeleviziju;
        private System.Windows.Forms.Label lblPriceForTv;
        private System.Windows.Forms.Label lblBrojMinuta;
        private System.Windows.Forms.Label lblNumberOfMinutes;
        private System.Windows.Forms.Label lblIznosZaInternet;
        private System.Windows.Forms.Label lblPriceForInternet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpOstvareniProtok;
        private System.Windows.Forms.Label lblKolicina;
        private System.Windows.Forms.Label lblPricePerFlow;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.GroupBox grpFlatRate;
        private System.Windows.Forms.Label lblBrojAdresa;
        private System.Windows.Forms.Label lblNumberOfAddres;
        private System.Windows.Forms.Label lblCostOfFlatRate;
        private System.Windows.Forms.Label lblTotalPrice;
        private System.Windows.Forms.Label lblUkupanIznosZaPlacanje;
        private System.Windows.Forms.Button btnIzvrsiPlacanje;
        private System.Windows.Forms.ListView lstTelefonija;
        private System.Windows.Forms.ColumnHeader clmBrojTelefona;
        private System.Windows.Forms.ColumnHeader clmBrojMinuta;
        private System.Windows.Forms.Label lblCenaPoMB;
        private System.Windows.Forms.Label lblCenaFlatRateAdrese;
        private System.Windows.Forms.Label lblRsdPaketa;
        private System.Windows.Forms.Label lblRsd2;
        private System.Windows.Forms.Label lblRsd3;
        private System.Windows.Forms.Label lblRsd4;
    }
}