﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TelekomunikacionaKompanija.DTOManagers.Contract;
using TelekomunikacionaKompanija.DTOs.Contract;

namespace TelekomunikacionaKompanija.Forms.Contract.Payment
{
    public partial class frmTypeNumber : Form
    {
        private List<ContractByUserDTO> contracts;
        #region Method(s)
        public bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }
        #endregion Method(s)

        #region Constructor(s)
        public frmTypeNumber()
        {
            InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void btnDalje_Click(object sender, EventArgs e)
        {
            string userContactPhone = this.txtBrojTelefona.Text;
            string userPackageName = "";

            if (this.txtPackageName.Text.Length > 0)
                userPackageName = this.txtPackageName.Text;

            if (DTOManagers.UserDTOManager.CheckIfExists(userContactPhone) == false)
            {
                MessageBox.Show("Uneli ste broj korisnika koji ne postoji!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                var newTypeNumber = new frmTypeNumber();
                newTypeNumber.Show();
                this.Close();
                return;

            }

            if (this.IsDigitsOnly(userContactPhone))
            {
                this.contracts = ContractDTOManager.GetContractsByUsersPhoneNumber(userContactPhone);

                if (contracts.Count == 0)
                {
                    MessageBox.Show("Nije pronađen nijedan ugovor za dati broj telefona.",
                        "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    bool userHasPackage = false;
                    foreach(var p in this.contracts)
                    {
                        if (p.PackageName == this.txtPackageName.Text)
                            userHasPackage = true;
                    }
                    if (userHasPackage == false)
                    {
                        MessageBox.Show("Nije pronađen nijedan ugovor za dati broj telefona.",
                       "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                
            }

            if (userContactPhone != null )
            {
                var newReceipt = new frmReceipt()
                {
                    contactPhone = userContactPhone,
                    packageName = userPackageName
                };

                newReceipt.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Greška prilikom unosa broja telefona. Možete uneti samo brojeve.", "Obaveštenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            } 
        }

        private void frmTypeNumber_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Unesite broj telefona i naziv paketa koji zelite da platite. \n \n " +
                "Obavestenje za profesora, automatski su stavljenje neke vrednosti, slobodno izbrisite i stavite vase podatke o broju telefona i nazivu paketa koje ste prethodno kreirali u formi za ugovor!", "Uputstvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        #endregion Form Event Method(s)
    }
}
