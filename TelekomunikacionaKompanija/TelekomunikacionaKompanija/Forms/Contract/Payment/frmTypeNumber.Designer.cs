﻿
namespace TelekomunikacionaKompanija.Forms.Contract.Payment
{
    partial class frmTypeNumber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDalje = new System.Windows.Forms.Button();
            this.lblBrojTelefona = new System.Windows.Forms.Label();
            this.txtBrojTelefona = new System.Windows.Forms.TextBox();
            this.lblIzborPaket = new System.Windows.Forms.Label();
            this.txtPackageName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnDalje
            // 
            this.btnDalje.Location = new System.Drawing.Point(369, 121);
            this.btnDalje.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDalje.Name = "btnDalje";
            this.btnDalje.Size = new System.Drawing.Size(141, 57);
            this.btnDalje.TabIndex = 4;
            this.btnDalje.Text = "Dalje";
            this.btnDalje.UseVisualStyleBackColor = true;
            this.btnDalje.Click += new System.EventHandler(this.btnDalje_Click);
            // 
            // lblBrojTelefona
            // 
            this.lblBrojTelefona.AutoSize = true;
            this.lblBrojTelefona.Location = new System.Drawing.Point(15, 17);
            this.lblBrojTelefona.Name = "lblBrojTelefona";
            this.lblBrojTelefona.Size = new System.Drawing.Size(143, 17);
            this.lblBrojTelefona.TabIndex = 0;
            this.lblBrojTelefona.Text = "Unesite broj telefona:";
            // 
            // txtBrojTelefona
            // 
            this.txtBrojTelefona.Location = new System.Drawing.Point(163, 14);
            this.txtBrojTelefona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBrojTelefona.MaxLength = 10;
            this.txtBrojTelefona.Name = "txtBrojTelefona";
            this.txtBrojTelefona.Size = new System.Drawing.Size(103, 22);
            this.txtBrojTelefona.TabIndex = 1;
            this.txtBrojTelefona.Text = "0660000856";
            // 
            // lblIzborPaket
            // 
            this.lblIzborPaket.AutoSize = true;
            this.lblIzborPaket.Location = new System.Drawing.Point(15, 52);
            this.lblIzborPaket.Name = "lblIzborPaket";
            this.lblIzborPaket.Size = new System.Drawing.Size(253, 17);
            this.lblIzborPaket.TabIndex = 2;
            this.lblIzborPaket.Text = "Uneti naziv paketa koji želite da platite:";
            // 
            // txtPackageName
            // 
            this.txtPackageName.Location = new System.Drawing.Point(19, 84);
            this.txtPackageName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPackageName.MaxLength = 15;
            this.txtPackageName.Name = "txtPackageName";
            this.txtPackageName.Size = new System.Drawing.Size(103, 22);
            this.txtPackageName.TabIndex = 3;
            this.txtPackageName.Text = "EON1";
            // 
            // frmTypeNumber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 193);
            this.Controls.Add(this.txtPackageName);
            this.Controls.Add(this.lblIzborPaket);
            this.Controls.Add(this.txtBrojTelefona);
            this.Controls.Add(this.lblBrojTelefona);
            this.Controls.Add(this.btnDalje);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTypeNumber";
            this.Text = "Unos broja";
            this.Load += new System.EventHandler(this.frmTypeNumber_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDalje;
        private System.Windows.Forms.Label lblBrojTelefona;
        private System.Windows.Forms.TextBox txtBrojTelefona;
        private System.Windows.Forms.Label lblIzborPaket;
        private System.Windows.Forms.TextBox txtPackageName;
    }
}