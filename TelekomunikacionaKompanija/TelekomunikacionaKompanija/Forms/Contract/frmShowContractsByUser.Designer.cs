﻿
namespace TelekomunikacionaKompanija.Forms.Contract
{
    partial class frmShowContractsByUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.btnShowContracts = new System.Windows.Forms.Button();
            this.lstContracts = new System.Windows.Forms.ListView();
            this.btnUpdateService = new System.Windows.Forms.Button();
            this.columnHeaderContractNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStartDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderEndDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPackageName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(12, 17);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(114, 13);
            this.lblPhoneNumber.TabIndex = 0;
            this.lblPhoneNumber.Text = "Broj telefona korisnika:";
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Location = new System.Drawing.Point(132, 14);
            this.txtPhoneNumber.MaxLength = 10;
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(100, 20);
            this.txtPhoneNumber.TabIndex = 1;
            // 
            // btnShowContracts
            // 
            this.btnShowContracts.Location = new System.Drawing.Point(238, 12);
            this.btnShowContracts.Name = "btnShowContracts";
            this.btnShowContracts.Size = new System.Drawing.Size(100, 23);
            this.btnShowContracts.TabIndex = 2;
            this.btnShowContracts.Text = "Prikažite ugovore";
            this.btnShowContracts.UseVisualStyleBackColor = true;
            this.btnShowContracts.Click += new System.EventHandler(this.btnShowContracts_Click);
            // 
            // lstContracts
            // 
            this.lstContracts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderContractNumber,
            this.columnHeaderStartDate,
            this.columnHeaderEndDate,
            this.columnHeaderPackageName});
            this.lstContracts.FullRowSelect = true;
            this.lstContracts.GridLines = true;
            this.lstContracts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstContracts.HideSelection = false;
            this.lstContracts.Location = new System.Drawing.Point(12, 40);
            this.lstContracts.MultiSelect = false;
            this.lstContracts.Name = "lstContracts";
            this.lstContracts.Size = new System.Drawing.Size(495, 255);
            this.lstContracts.TabIndex = 3;
            this.lstContracts.UseCompatibleStateImageBehavior = false;
            this.lstContracts.View = System.Windows.Forms.View.Details;
            this.lstContracts.SelectedIndexChanged += new System.EventHandler(this.lstContracts_SelectedIndexChanged);
            // 
            // btnUpdateService
            // 
            this.btnUpdateService.Enabled = false;
            this.btnUpdateService.Location = new System.Drawing.Point(521, 149);
            this.btnUpdateService.Name = "btnUpdateService";
            this.btnUpdateService.Size = new System.Drawing.Size(75, 35);
            this.btnUpdateService.TabIndex = 4;
            this.btnUpdateService.Text = "Ažurirajte uslugu";
            this.btnUpdateService.UseVisualStyleBackColor = true;
            this.btnUpdateService.Click += new System.EventHandler(this.btnUpdateService_Click);
            // 
            // columnHeaderContractNumber
            // 
            this.columnHeaderContractNumber.Text = "Broj ugovora";
            this.columnHeaderContractNumber.Width = 90;
            // 
            // columnHeaderStartDate
            // 
            this.columnHeaderStartDate.Text = "Datum početka trajanja";
            this.columnHeaderStartDate.Width = 130;
            // 
            // columnHeaderEndDate
            // 
            this.columnHeaderEndDate.Text = "Datum kraja trajanja";
            this.columnHeaderEndDate.Width = 120;
            // 
            // columnHeaderPackageName
            // 
            this.columnHeaderPackageName.Text = "Naziv paketa";
            this.columnHeaderPackageName.Width = 80;
            // 
            // frmShowContractsByUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 307);
            this.Controls.Add(this.btnUpdateService);
            this.Controls.Add(this.lstContracts);
            this.Controls.Add(this.btnShowContracts);
            this.Controls.Add(this.txtPhoneNumber);
            this.Controls.Add(this.lblPhoneNumber);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmShowContractsByUser";
            this.Text = "Prikaz ugovora za korisnika";
            this.Load += new System.EventHandler(this.frmShowContractsByUser_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.TextBox txtPhoneNumber;
        private System.Windows.Forms.Button btnShowContracts;
        private System.Windows.Forms.ListView lstContracts;
        private System.Windows.Forms.Button btnUpdateService;
        private System.Windows.Forms.ColumnHeader columnHeaderContractNumber;
        private System.Windows.Forms.ColumnHeader columnHeaderStartDate;
        private System.Windows.Forms.ColumnHeader columnHeaderEndDate;
        private System.Windows.Forms.ColumnHeader columnHeaderPackageName;
    }
}