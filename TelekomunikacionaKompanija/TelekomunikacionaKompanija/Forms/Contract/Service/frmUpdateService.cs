﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOManagers.Contract;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Internet;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Telephony;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Television;

namespace TelekomunikacionaKompanija.Forms.Contract.Service
{
    public partial class frmUpdateService : Form
    {
        #region Properties
        public string PackageName { get; set; }
        public int ServiceID { get; set; }
        #endregion Properties

        #region Constructor(s)
        public frmUpdateService()
        {
            this.InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmUpdateService_Load(object sender, EventArgs e)
        {
            var service = ServiceDTOManager.GetServiceByID(this.ServiceID);

            if (service.ContainsTelephony)
            {
                foreach (var phoneNumber in service.PhoneNumbers)
                {
                    var item = new ListViewItem(new string[] { phoneNumber.PhoneNumber,
                        phoneNumber.NumberOfMinutes.ToString() });

                    this.lstPhoneNumbers.Items.Add(item);
                }

                this.lstPhoneNumbers.Refresh();

                if (this.lstPhoneNumbers.Items.Count == 4)
                {
                    this.btnAddPhoneNumber.Enabled = false;
                }
            }
            else
            {
                this.lstPhoneNumbers.Enabled = false;
                this.lblPhoneNumber.ForeColor = Color.Gray;
                this.txtPhoneNumber.Enabled = false;
                this.lblNumberOfMinutes.ForeColor = Color.Gray;
                this.nudNumberOfMinutes.Enabled = false;
                this.btnAddPhoneNumber.Enabled = false;
            }

            this.btnDeletePhoneNumber.Enabled = false;
            this.btnUpdateNumberOfMinutes.Enabled = false;

            if (service.ContainsTelevision)
            {
                var additionalChannelPackagesList =
                    PackageDTOManager.GetAdditionalPackagesByPackage(this.PackageName);

                if (additionalChannelPackagesList.Count == 0)
                {
                    this.btnAddChannelPackages.Enabled = false;
                }
                else
                {
                    if (service.AdditionalChannelPackages.Count == 0)
                    {
                        foreach (var additionalChannelPackage in additionalChannelPackagesList)
                        {
                            this.lstAdditionalChannelPackages.Items.Add(additionalChannelPackage.Name);
                        }

                        this.lstAdditionalChannelPackages.Refresh();
                    }
                    else
                    {
                        var addedChannelPackagesList = service.AdditionalChannelPackages.ToList();

                        foreach (var additionalChannelPackage in additionalChannelPackagesList)
                        {
                            if (addedChannelPackagesList
                                .Find(x => x.Name == additionalChannelPackage.Name) is null)
                            {
                                this.lstAdditionalChannelPackages.Items.Add(additionalChannelPackage.Name);
                            }
                        }

                        this.lstAdditionalChannelPackages.Refresh();

                        if (this.lstAdditionalChannelPackages.Items.Count == 0)
                        {
                            this.btnAddChannelPackages.Enabled = false;
                        }

                        foreach (var additionalChannelPackage in addedChannelPackagesList)
                        {
                            this.lstAddedChannelPackages.Items.Add(additionalChannelPackage.Name);
                        }

                        this.lstAddedChannelPackages.Refresh();
                    }
                }
            }
            else
            {
                this.lblChooseChannelPackages.ForeColor = Color.Gray;
                this.lstAdditionalChannelPackages.Enabled = false;
                this.lblAddedChannelPackages.ForeColor = Color.Gray;
                this.lstAddedChannelPackages.Enabled = false;
            }

            this.btnAddChannelPackages.Enabled = false;
            this.btnDeleteChannelPackages.Enabled = false;

            if (service.ContainsInternet)
            {
                if (service.Internet.Address1 != null)
                {
                    this.lstStaticAddresses.Items.Add(service.Internet.Address1);
                }

                if (service.Internet.Address2 != null)
                {
                    this.lstStaticAddresses.Items.Add(service.Internet.Address2);
                }

                this.lstStaticAddresses.Refresh();

                if (this.lstStaticAddresses.Items.Count == 2)
                {
                    this.btnAddStaticAddress.Enabled = false;
                }

                this.rbPrepaid.Checked = service.Internet.InternetType == InternetType.Prepaid;

                if (service.Internet.InternetType == InternetType.Prepaid)
                {
                    var prepaidInternet = service.Internet as PrepaidInternetDTO;

                    if (prepaidInternet.PaymentDate != null &&
                        prepaidInternet.PaymentDate != new DateTime())
                    {
                        this.dtpPaymentDate.Value = (DateTime)prepaidInternet.PaymentDate;
                        this.txtBalance.Text = prepaidInternet.Balance.ToString("N2");
                    }
                }
                else
                {
                    this.lblPaymentDate.ForeColor = Color.Gray;
                    this.dtpPaymentDate.Enabled = false;
                    this.lblBalance.ForeColor = Color.Gray;
                    this.txtBalance.Enabled = false;
                }
            }
            else
            {
                this.lstStaticAddresses.Enabled = false;
                this.lblStaticAddress.ForeColor = Color.Gray;
                this.txtStaticAddress.Enabled = false;
                this.btnAddStaticAddress.Enabled = false;

                this.lblPaymentDate.ForeColor = Color.Gray;
                this.dtpPaymentDate.Enabled = false;
                this.lblBalance.ForeColor = Color.Gray;
                this.txtBalance.Enabled = false;
            }

            this.btnDeleteStaticAddress.Enabled = false;
            this.rbPrepaid.Enabled = false;
            this.rbPostpaid.Enabled = false;
        }

        private void lstPhoneNumbers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lstPhoneNumbers.SelectedItems.Count == 0)
            {
                this.btnDeletePhoneNumber.Enabled = false;
                this.txtPhoneNumber.Enabled = true;
                this.txtPhoneNumber.Text = string.Empty;
                this.nudNumberOfMinutes.Value = 0;
                this.btnAddPhoneNumber.Enabled = this.lstPhoneNumbers.Items.Count != 4;
                this.btnUpdateNumberOfMinutes.Enabled = false;
            }
            else
            {
                this.btnDeletePhoneNumber.Enabled = true;

                int selectedIndex = this.lstPhoneNumbers.SelectedIndices[0];

                this.txtPhoneNumber.Enabled = false;
                this.txtPhoneNumber.Text = this.lstPhoneNumbers.Items[selectedIndex].SubItems[0].Text;
                this.nudNumberOfMinutes.Value =
                    Convert.ToDecimal(this.lstPhoneNumbers.Items[selectedIndex].SubItems[1].Text);

                this.btnAddPhoneNumber.Enabled = false;
                this.btnUpdateNumberOfMinutes.Enabled = true;
            }
        }

        private void btnDeletePhoneNumber_Click(object sender, EventArgs e)
        {
            int selectedIndex = this.lstPhoneNumbers.SelectedIndices[0];
            string phoneNumber = this.lstPhoneNumbers.Items[selectedIndex].SubItems[0].Text;

            if (ServiceDTOManager.DeletePhoneNumber(this.ServiceID, phoneNumber))
            {
                this.lstPhoneNumbers.Items.RemoveAt(selectedIndex);

                this.btnAddPhoneNumber.Enabled = true;
            }
            else
            {
                MessageBox.Show("Broj telefona nije obrisan.", "Greška",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAddPhoneNumber_Click(object sender, EventArgs e)
        {
            string phoneNumber = this.txtPhoneNumber.Text;

            if (phoneNumber != string.Empty)
            {
                if (!this.ContainsDigitsOnly(phoneNumber))
                {
                    MessageBox.Show("Broj telefona mora da sadrži samo cifre.", "Upozorenje",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    var phoneNumberObj = new PhoneNumberDTO(phoneNumber,
                        Convert.ToInt32(this.nudNumberOfMinutes.Value));

                    if (ServiceDTOManager.CreatePhoneNumber(this.ServiceID, phoneNumberObj))
                    {
                        var item = new ListViewItem(new string[]
                            { phoneNumberObj.PhoneNumber, phoneNumberObj.NumberOfMinutes.ToString() });

                        this.lstPhoneNumbers.Items.Add(item);

                        if (this.lstPhoneNumbers.Items.Count == 4)
                        {
                            this.btnAddPhoneNumber.Enabled = false;
                        }

                        this.txtPhoneNumber.Text = string.Empty;
                    }
                    else
                    {
                        MessageBox.Show("Broj telefona nije dodat.", "Greška",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnUpdateNumberOfMinutes_Click(object sender, EventArgs e)
        {
            int selectedIndex = this.lstPhoneNumbers.SelectedIndices[0];

            var phoneNumber = new PhoneNumberDTO(this.lstPhoneNumbers.Items[selectedIndex].SubItems[0].Text,
                Convert.ToInt32(this.nudNumberOfMinutes.Value));

            if (ServiceDTOManager.UpdateNumberOfMinutes(this.ServiceID, phoneNumber))
            {
                this.lstPhoneNumbers.Items[selectedIndex].SubItems[1].Text =
                    phoneNumber.NumberOfMinutes.ToString();

                this.lstPhoneNumbers.SelectedItems.Clear();
            }
        }

        private void lstAdditionalChannelPackages_SelectedIndexChanged(object sender, EventArgs e) =>
            this.btnAddChannelPackages.Enabled = this.lstAdditionalChannelPackages.SelectedItems.Count != 0;

        private void btnAddChannelPackages_Click(object sender, EventArgs e)
        {
            var newChannelPackages = new List<AdditionalChannelPackageDTO>();

            foreach (ListViewItem item in this.lstAdditionalChannelPackages.SelectedItems)
            {
                var additionalChannelPackageName = item.SubItems[0].Text;

                newChannelPackages.Add(new AdditionalChannelPackageDTO(additionalChannelPackageName));

                this.lstAdditionalChannelPackages.Items.Remove(item);
                this.lstAddedChannelPackages.Items.Add(additionalChannelPackageName);
            }

            this.lstAdditionalChannelPackages.Refresh();
            this.lstAddedChannelPackages.Refresh();

            this.btnAddChannelPackages.Enabled = false;

            ServiceDTOManager.CreateAdditionalChannelPackages(this.ServiceID, newChannelPackages);
        }

        private void lstAddedChannelPackages_SelectedIndexChanged(object sender, EventArgs e) =>
            this.btnDeleteChannelPackages.Enabled = this.lstAddedChannelPackages.SelectedItems.Count != 0;

        private void btnDeleteChannelPackages_Click(object sender, EventArgs e)
        {
            var packagesToBeRemoved = new List<AdditionalChannelPackageDTO>();

            foreach (ListViewItem item in this.lstAddedChannelPackages.SelectedItems)
            {
                var additionalChannelPackageName = item.SubItems[0].Text;

                packagesToBeRemoved.Add(new AdditionalChannelPackageDTO(additionalChannelPackageName));

                this.lstAdditionalChannelPackages.Items.Add(additionalChannelPackageName);
                this.lstAddedChannelPackages.Items.Remove(item);
            }

            this.btnDeleteChannelPackages.Enabled = false;

            this.lstAdditionalChannelPackages.Refresh();
            this.lstAddedChannelPackages.Refresh();

            ServiceDTOManager.DeleteAdditionalChannelPackages(this.ServiceID, packagesToBeRemoved);
        }

        private void lstStaticAddresses_SelectedIndexChanged(object sender, EventArgs e) =>
            this.btnDeleteStaticAddress.Enabled = this.lstStaticAddresses.SelectedItems.Count != 0;

        private void btnAddStaticAddress_Click(object sender, EventArgs e)
        {
            string staticAddress = this.txtStaticAddress.Text;

            if (staticAddress != string.Empty)
            {
                if (!this.IsValidIPAddress(staticAddress))
                {
                    MessageBox.Show("Statička adresa mora biti u formatu xxx.xxx.xxx.xxx",
                        "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    this.lstStaticAddresses.Items.Add(staticAddress);

                    if (this.lstStaticAddresses.Items.Count == 2)
                    {
                        this.btnAddStaticAddress.Enabled = false;
                    }

                    this.txtStaticAddress.Text = string.Empty;
                }
            }
        }

        private void btnDeleteStaticAddress_Click(object sender, EventArgs e)
        {
            int selectedIndex = this.lstStaticAddresses.SelectedIndices[0];

            this.lstStaticAddresses.Items.RemoveAt(selectedIndex);

            this.btnAddStaticAddress.Enabled = true;
            this.btnDeleteStaticAddress.Enabled = false;
        }

        private void btnUpdateService_Click(object sender, EventArgs e)
        {
            if (ServiceDTOManager.GetServiceByID(this.ServiceID).ContainsInternet)
            {
                string address1 = (this.lstStaticAddresses.Items.Count != 0) ?
                    this.lstStaticAddresses.Items[0].SubItems[0].Text : null;
                string address2 = (this.lstStaticAddresses.Items.Count == 2) ?
                    this.lstStaticAddresses.Items[1].SubItems[0].Text : null;

                InternetDTO internet;
                if (this.rbPrepaid.Checked)
                {
                    if (this.txtBalance.Text != string.Empty)
                    {
                        internet = new PrepaidInternetDTO(address1, address2, InternetType.Prepaid,
                            this.dtpPaymentDate.Value, Convert.ToSingle(this.txtBalance.Text));
                    }
                    else
                    {
                        MessageBox.Show("Stanje ne sme da bude prazno.", "Upozorenje",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        return;
                    }
                }
                else
                {
                    internet = new PostpaidInternetDTO(address1, address2, InternetType.Postpaid);
                }

                if (!ServiceDTOManager.UpdateInternetService(this.ServiceID, internet))
                {
                    MessageBox.Show("Usluga nije ažurirana.", "Greška",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    return;
                }
            }

            MessageBox.Show("Usluga je uspešno ažurirana.", "Obaveštenje",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Close();
        }
        #endregion Form Event Method(s)

        #region Helper Method(s)
        private bool ContainsDigitsOnly(string text)
        {
            bool isDigit = true;

            for (int i = 0; i < text.Length && isDigit; i++)
            {
                if (text[i] < '0' || text[i] > '9')
                {
                    isDigit = false;
                }
            }

            return isDigit;
        }

        private bool IsValidIPAddress(string ipAddress)
        {
            string addressRegEx = "(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}" +
                "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])";

            return Regex.IsMatch(ipAddress, addressRegEx);
        }
        #endregion Helper Method(s)
    }
}
