﻿
namespace TelekomunikacionaKompanija.Forms.Contract.Service
{
    partial class frmCreateService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpTelephony = new System.Windows.Forms.GroupBox();
            this.btnDeletePhoneNumber = new System.Windows.Forms.Button();
            this.btnAddPhoneNumber = new System.Windows.Forms.Button();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.lstPhoneNumbers = new System.Windows.Forms.ListView();
            this.columnHeaderPhoneNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpTelevision = new System.Windows.Forms.GroupBox();
            this.lstAdditionalChannelPackages = new System.Windows.Forms.ListView();
            this.columnHeaderPackageName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblChooseChannelPackages = new System.Windows.Forms.Label();
            this.grpInternet = new System.Windows.Forms.GroupBox();
            this.grpInternetType = new System.Windows.Forms.GroupBox();
            this.rbPostpaid = new System.Windows.Forms.RadioButton();
            this.rbPrepaid = new System.Windows.Forms.RadioButton();
            this.btnDeleteStaticAddress = new System.Windows.Forms.Button();
            this.txtStaticAddress = new System.Windows.Forms.TextBox();
            this.btnAddStaticAddress = new System.Windows.Forms.Button();
            this.lblStaticAddress = new System.Windows.Forms.Label();
            this.lstStaticAddresses = new System.Windows.Forms.ListView();
            this.columnHeaderStaticAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnCreateService = new System.Windows.Forms.Button();
            this.grpTelephony.SuspendLayout();
            this.grpTelevision.SuspendLayout();
            this.grpInternet.SuspendLayout();
            this.grpInternetType.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpTelephony
            // 
            this.grpTelephony.Controls.Add(this.btnDeletePhoneNumber);
            this.grpTelephony.Controls.Add(this.btnAddPhoneNumber);
            this.grpTelephony.Controls.Add(this.txtPhoneNumber);
            this.grpTelephony.Controls.Add(this.lblPhoneNumber);
            this.grpTelephony.Controls.Add(this.lstPhoneNumbers);
            this.grpTelephony.Location = new System.Drawing.Point(12, 12);
            this.grpTelephony.Name = "grpTelephony";
            this.grpTelephony.Size = new System.Drawing.Size(363, 148);
            this.grpTelephony.TabIndex = 0;
            this.grpTelephony.TabStop = false;
            this.grpTelephony.Text = "Telefonija";
            // 
            // btnDeletePhoneNumber
            // 
            this.btnDeletePhoneNumber.Enabled = false;
            this.btnDeletePhoneNumber.Location = new System.Drawing.Point(175, 116);
            this.btnDeletePhoneNumber.Name = "btnDeletePhoneNumber";
            this.btnDeletePhoneNumber.Size = new System.Drawing.Size(75, 23);
            this.btnDeletePhoneNumber.TabIndex = 4;
            this.btnDeletePhoneNumber.Text = "Obrišite broj";
            this.btnDeletePhoneNumber.UseVisualStyleBackColor = true;
            this.btnDeletePhoneNumber.Click += new System.EventHandler(this.btnDeletePhoneNumber_Click);
            // 
            // btnAddPhoneNumber
            // 
            this.btnAddPhoneNumber.Location = new System.Drawing.Point(260, 51);
            this.btnAddPhoneNumber.Name = "btnAddPhoneNumber";
            this.btnAddPhoneNumber.Size = new System.Drawing.Size(75, 23);
            this.btnAddPhoneNumber.TabIndex = 3;
            this.btnAddPhoneNumber.Text = "Dodajte broj";
            this.btnAddPhoneNumber.UseVisualStyleBackColor = true;
            this.btnAddPhoneNumber.Click += new System.EventHandler(this.btnAddPhoneNumber_Click);
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Location = new System.Drawing.Point(247, 25);
            this.txtPhoneNumber.MaxLength = 10;
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(100, 20);
            this.txtPhoneNumber.TabIndex = 2;
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(172, 28);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(69, 13);
            this.lblPhoneNumber.TabIndex = 1;
            this.lblPhoneNumber.Text = "Broj telefona:";
            // 
            // lstPhoneNumbers
            // 
            this.lstPhoneNumbers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderPhoneNumber});
            this.lstPhoneNumbers.FullRowSelect = true;
            this.lstPhoneNumbers.GridLines = true;
            this.lstPhoneNumbers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstPhoneNumbers.HideSelection = false;
            this.lstPhoneNumbers.Location = new System.Drawing.Point(6, 19);
            this.lstPhoneNumbers.MultiSelect = false;
            this.lstPhoneNumbers.Name = "lstPhoneNumbers";
            this.lstPhoneNumbers.Size = new System.Drawing.Size(160, 120);
            this.lstPhoneNumbers.TabIndex = 0;
            this.lstPhoneNumbers.UseCompatibleStateImageBehavior = false;
            this.lstPhoneNumbers.View = System.Windows.Forms.View.Details;
            this.lstPhoneNumbers.SelectedIndexChanged += new System.EventHandler(this.lstPhoneNumbers_SelectedIndexChanged);
            // 
            // columnHeaderPhoneNumber
            // 
            this.columnHeaderPhoneNumber.Text = "Broj telefona";
            this.columnHeaderPhoneNumber.Width = 120;
            // 
            // grpTelevision
            // 
            this.grpTelevision.Controls.Add(this.lstAdditionalChannelPackages);
            this.grpTelevision.Controls.Add(this.lblChooseChannelPackages);
            this.grpTelevision.Location = new System.Drawing.Point(12, 166);
            this.grpTelevision.Name = "grpTelevision";
            this.grpTelevision.Size = new System.Drawing.Size(363, 233);
            this.grpTelevision.TabIndex = 1;
            this.grpTelevision.TabStop = false;
            this.grpTelevision.Text = "Televizija";
            // 
            // lstAdditionalChannelPackages
            // 
            this.lstAdditionalChannelPackages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderPackageName});
            this.lstAdditionalChannelPackages.FullRowSelect = true;
            this.lstAdditionalChannelPackages.GridLines = true;
            this.lstAdditionalChannelPackages.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstAdditionalChannelPackages.HideSelection = false;
            this.lstAdditionalChannelPackages.Location = new System.Drawing.Point(175, 26);
            this.lstAdditionalChannelPackages.Name = "lstAdditionalChannelPackages";
            this.lstAdditionalChannelPackages.Size = new System.Drawing.Size(182, 201);
            this.lstAdditionalChannelPackages.TabIndex = 1;
            this.lstAdditionalChannelPackages.UseCompatibleStateImageBehavior = false;
            this.lstAdditionalChannelPackages.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderPackageName
            // 
            this.columnHeaderPackageName.Text = "Dodatni paket kanala";
            this.columnHeaderPackageName.Width = 150;
            // 
            // lblChooseChannelPackages
            // 
            this.lblChooseChannelPackages.AutoSize = true;
            this.lblChooseChannelPackages.Location = new System.Drawing.Point(6, 26);
            this.lblChooseChannelPackages.Name = "lblChooseChannelPackages";
            this.lblChooseChannelPackages.Size = new System.Drawing.Size(163, 13);
            this.lblChooseChannelPackages.TabIndex = 0;
            this.lblChooseChannelPackages.Text = "Izaberite dodatne pakete kanala:";
            // 
            // grpInternet
            // 
            this.grpInternet.Controls.Add(this.grpInternetType);
            this.grpInternet.Controls.Add(this.btnDeleteStaticAddress);
            this.grpInternet.Controls.Add(this.txtStaticAddress);
            this.grpInternet.Controls.Add(this.btnAddStaticAddress);
            this.grpInternet.Controls.Add(this.lblStaticAddress);
            this.grpInternet.Controls.Add(this.lstStaticAddresses);
            this.grpInternet.Location = new System.Drawing.Point(381, 12);
            this.grpInternet.Name = "grpInternet";
            this.grpInternet.Size = new System.Drawing.Size(391, 232);
            this.grpInternet.TabIndex = 2;
            this.grpInternet.TabStop = false;
            this.grpInternet.Text = "Internet";
            // 
            // grpInternetType
            // 
            this.grpInternetType.Controls.Add(this.rbPostpaid);
            this.grpInternetType.Controls.Add(this.rbPrepaid);
            this.grpInternetType.Location = new System.Drawing.Point(6, 122);
            this.grpInternetType.Name = "grpInternetType";
            this.grpInternetType.Size = new System.Drawing.Size(178, 100);
            this.grpInternetType.TabIndex = 5;
            this.grpInternetType.TabStop = false;
            this.grpInternetType.Text = "Tip";
            // 
            // rbPostpaid
            // 
            this.rbPostpaid.AutoSize = true;
            this.rbPostpaid.Location = new System.Drawing.Point(56, 58);
            this.rbPostpaid.Name = "rbPostpaid";
            this.rbPostpaid.Size = new System.Drawing.Size(66, 17);
            this.rbPostpaid.TabIndex = 1;
            this.rbPostpaid.Text = "Postpaid";
            this.rbPostpaid.UseVisualStyleBackColor = true;
            // 
            // rbPrepaid
            // 
            this.rbPrepaid.AutoSize = true;
            this.rbPrepaid.Checked = true;
            this.rbPrepaid.Location = new System.Drawing.Point(56, 32);
            this.rbPrepaid.Name = "rbPrepaid";
            this.rbPrepaid.Size = new System.Drawing.Size(61, 17);
            this.rbPrepaid.TabIndex = 0;
            this.rbPrepaid.TabStop = true;
            this.rbPrepaid.Text = "Prepaid";
            this.rbPrepaid.UseVisualStyleBackColor = true;
            // 
            // btnDeleteStaticAddress
            // 
            this.btnDeleteStaticAddress.Enabled = false;
            this.btnDeleteStaticAddress.Location = new System.Drawing.Point(193, 93);
            this.btnDeleteStaticAddress.Name = "btnDeleteStaticAddress";
            this.btnDeleteStaticAddress.Size = new System.Drawing.Size(100, 23);
            this.btnDeleteStaticAddress.TabIndex = 4;
            this.btnDeleteStaticAddress.Text = "Obrišite adresu";
            this.btnDeleteStaticAddress.UseVisualStyleBackColor = true;
            this.btnDeleteStaticAddress.Click += new System.EventHandler(this.btnDeleteStaticAddress_Click);
            // 
            // txtStaticAddress
            // 
            this.txtStaticAddress.Location = new System.Drawing.Point(280, 25);
            this.txtStaticAddress.MaxLength = 15;
            this.txtStaticAddress.Name = "txtStaticAddress";
            this.txtStaticAddress.Size = new System.Drawing.Size(100, 20);
            this.txtStaticAddress.TabIndex = 2;
            // 
            // btnAddStaticAddress
            // 
            this.btnAddStaticAddress.Location = new System.Drawing.Point(280, 51);
            this.btnAddStaticAddress.Name = "btnAddStaticAddress";
            this.btnAddStaticAddress.Size = new System.Drawing.Size(100, 23);
            this.btnAddStaticAddress.TabIndex = 3;
            this.btnAddStaticAddress.Text = "Dodajte adresu";
            this.btnAddStaticAddress.UseVisualStyleBackColor = true;
            this.btnAddStaticAddress.Click += new System.EventHandler(this.btnAddStaticAddress_Click);
            // 
            // lblStaticAddress
            // 
            this.lblStaticAddress.AutoSize = true;
            this.lblStaticAddress.Location = new System.Drawing.Point(190, 28);
            this.lblStaticAddress.Name = "lblStaticAddress";
            this.lblStaticAddress.Size = new System.Drawing.Size(84, 13);
            this.lblStaticAddress.TabIndex = 1;
            this.lblStaticAddress.Text = "Statička adresa:";
            // 
            // lstStaticAddresses
            // 
            this.lstStaticAddresses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderStaticAddress});
            this.lstStaticAddresses.FullRowSelect = true;
            this.lstStaticAddresses.GridLines = true;
            this.lstStaticAddresses.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstStaticAddresses.HideSelection = false;
            this.lstStaticAddresses.Location = new System.Drawing.Point(6, 19);
            this.lstStaticAddresses.MultiSelect = false;
            this.lstStaticAddresses.Name = "lstStaticAddresses";
            this.lstStaticAddresses.Size = new System.Drawing.Size(178, 97);
            this.lstStaticAddresses.TabIndex = 0;
            this.lstStaticAddresses.UseCompatibleStateImageBehavior = false;
            this.lstStaticAddresses.View = System.Windows.Forms.View.Details;
            this.lstStaticAddresses.SelectedIndexChanged += new System.EventHandler(this.lstStaticAddresses_SelectedIndexChanged);
            // 
            // columnHeaderStaticAddress
            // 
            this.columnHeaderStaticAddress.Text = "Statička adresa";
            this.columnHeaderStaticAddress.Width = 150;
            // 
            // btnCreateService
            // 
            this.btnCreateService.Location = new System.Drawing.Point(647, 349);
            this.btnCreateService.Name = "btnCreateService";
            this.btnCreateService.Size = new System.Drawing.Size(125, 50);
            this.btnCreateService.TabIndex = 3;
            this.btnCreateService.Text = "Kreirajte uslugu";
            this.btnCreateService.UseVisualStyleBackColor = true;
            this.btnCreateService.Click += new System.EventHandler(this.btnCreateService_Click);
            // 
            // frmCreateService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 411);
            this.Controls.Add(this.btnCreateService);
            this.Controls.Add(this.grpInternet);
            this.Controls.Add(this.grpTelevision);
            this.Controls.Add(this.grpTelephony);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCreateService";
            this.Text = "Nova usluga";
            this.Load += new System.EventHandler(this.frmCreateService_Load);
            this.grpTelephony.ResumeLayout(false);
            this.grpTelephony.PerformLayout();
            this.grpTelevision.ResumeLayout(false);
            this.grpTelevision.PerformLayout();
            this.grpInternet.ResumeLayout(false);
            this.grpInternet.PerformLayout();
            this.grpInternetType.ResumeLayout(false);
            this.grpInternetType.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpTelephony;
        private System.Windows.Forms.Button btnDeletePhoneNumber;
        private System.Windows.Forms.Button btnAddPhoneNumber;
        private System.Windows.Forms.TextBox txtPhoneNumber;
        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.ListView lstPhoneNumbers;
        private System.Windows.Forms.ColumnHeader columnHeaderPhoneNumber;
        private System.Windows.Forms.GroupBox grpTelevision;
        private System.Windows.Forms.ListView lstAdditionalChannelPackages;
        private System.Windows.Forms.ColumnHeader columnHeaderPackageName;
        private System.Windows.Forms.Label lblChooseChannelPackages;
        private System.Windows.Forms.GroupBox grpInternet;
        private System.Windows.Forms.Button btnDeleteStaticAddress;
        private System.Windows.Forms.TextBox txtStaticAddress;
        private System.Windows.Forms.Button btnAddStaticAddress;
        private System.Windows.Forms.Label lblStaticAddress;
        private System.Windows.Forms.ListView lstStaticAddresses;
        private System.Windows.Forms.ColumnHeader columnHeaderStaticAddress;
        private System.Windows.Forms.Button btnCreateService;
        private System.Windows.Forms.GroupBox grpInternetType;
        private System.Windows.Forms.RadioButton rbPostpaid;
        private System.Windows.Forms.RadioButton rbPrepaid;
    }
}