﻿
namespace TelekomunikacionaKompanija.Forms.Contract.Service
{
    partial class frmUpdateService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpdateService = new System.Windows.Forms.Button();
            this.btnDeleteStaticAddress = new System.Windows.Forms.Button();
            this.txtStaticAddress = new System.Windows.Forms.TextBox();
            this.btnAddStaticAddress = new System.Windows.Forms.Button();
            this.lblStaticAddress = new System.Windows.Forms.Label();
            this.lstStaticAddresses = new System.Windows.Forms.ListView();
            this.columnHeaderStaticAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rbPostpaid = new System.Windows.Forms.RadioButton();
            this.rbPrepaid = new System.Windows.Forms.RadioButton();
            this.lstAdditionalChannelPackages = new System.Windows.Forms.ListView();
            this.columnHeaderPackageName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpInternetType = new System.Windows.Forms.GroupBox();
            this.grpInternet = new System.Windows.Forms.GroupBox();
            this.grpPrepaid = new System.Windows.Forms.GroupBox();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.lblBalance = new System.Windows.Forms.Label();
            this.dtpPaymentDate = new System.Windows.Forms.DateTimePicker();
            this.lblPaymentDate = new System.Windows.Forms.Label();
            this.lblChooseChannelPackages = new System.Windows.Forms.Label();
            this.grpTelevision = new System.Windows.Forms.GroupBox();
            this.btnAddChannelPackages = new System.Windows.Forms.Button();
            this.btnDeleteChannelPackages = new System.Windows.Forms.Button();
            this.lstAddedChannelPackages = new System.Windows.Forms.ListView();
            this.columnHeaderChannelName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblAddedChannelPackages = new System.Windows.Forms.Label();
            this.columnHeaderPhoneNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDeletePhoneNumber = new System.Windows.Forms.Button();
            this.btnAddPhoneNumber = new System.Windows.Forms.Button();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.lstPhoneNumbers = new System.Windows.Forms.ListView();
            this.columnHeaderNumberOfMinutes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpTelephony = new System.Windows.Forms.GroupBox();
            this.btnUpdateNumberOfMinutes = new System.Windows.Forms.Button();
            this.nudNumberOfMinutes = new System.Windows.Forms.NumericUpDown();
            this.lblNumberOfMinutes = new System.Windows.Forms.Label();
            this.grpInternetType.SuspendLayout();
            this.grpInternet.SuspendLayout();
            this.grpPrepaid.SuspendLayout();
            this.grpTelevision.SuspendLayout();
            this.grpTelephony.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfMinutes)).BeginInit();
            this.SuspendLayout();
            // 
            // btnUpdateService
            // 
            this.btnUpdateService.Location = new System.Drawing.Point(663, 628);
            this.btnUpdateService.Name = "btnUpdateService";
            this.btnUpdateService.Size = new System.Drawing.Size(125, 50);
            this.btnUpdateService.TabIndex = 7;
            this.btnUpdateService.Text = "Ažurirajte uslugu";
            this.btnUpdateService.UseVisualStyleBackColor = true;
            this.btnUpdateService.Click += new System.EventHandler(this.btnUpdateService_Click);
            // 
            // btnDeleteStaticAddress
            // 
            this.btnDeleteStaticAddress.Enabled = false;
            this.btnDeleteStaticAddress.Location = new System.Drawing.Point(193, 93);
            this.btnDeleteStaticAddress.Name = "btnDeleteStaticAddress";
            this.btnDeleteStaticAddress.Size = new System.Drawing.Size(100, 23);
            this.btnDeleteStaticAddress.TabIndex = 4;
            this.btnDeleteStaticAddress.Text = "Obrišite adresu";
            this.btnDeleteStaticAddress.UseVisualStyleBackColor = true;
            this.btnDeleteStaticAddress.Click += new System.EventHandler(this.btnDeleteStaticAddress_Click);
            // 
            // txtStaticAddress
            // 
            this.txtStaticAddress.Location = new System.Drawing.Point(280, 25);
            this.txtStaticAddress.MaxLength = 15;
            this.txtStaticAddress.Name = "txtStaticAddress";
            this.txtStaticAddress.Size = new System.Drawing.Size(100, 20);
            this.txtStaticAddress.TabIndex = 2;
            // 
            // btnAddStaticAddress
            // 
            this.btnAddStaticAddress.Location = new System.Drawing.Point(280, 51);
            this.btnAddStaticAddress.Name = "btnAddStaticAddress";
            this.btnAddStaticAddress.Size = new System.Drawing.Size(100, 23);
            this.btnAddStaticAddress.TabIndex = 3;
            this.btnAddStaticAddress.Text = "Dodajte adresu";
            this.btnAddStaticAddress.UseVisualStyleBackColor = true;
            this.btnAddStaticAddress.Click += new System.EventHandler(this.btnAddStaticAddress_Click);
            // 
            // lblStaticAddress
            // 
            this.lblStaticAddress.AutoSize = true;
            this.lblStaticAddress.Location = new System.Drawing.Point(190, 28);
            this.lblStaticAddress.Name = "lblStaticAddress";
            this.lblStaticAddress.Size = new System.Drawing.Size(84, 13);
            this.lblStaticAddress.TabIndex = 1;
            this.lblStaticAddress.Text = "Statička adresa:";
            // 
            // lstStaticAddresses
            // 
            this.lstStaticAddresses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderStaticAddress});
            this.lstStaticAddresses.FullRowSelect = true;
            this.lstStaticAddresses.GridLines = true;
            this.lstStaticAddresses.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstStaticAddresses.HideSelection = false;
            this.lstStaticAddresses.Location = new System.Drawing.Point(6, 19);
            this.lstStaticAddresses.MultiSelect = false;
            this.lstStaticAddresses.Name = "lstStaticAddresses";
            this.lstStaticAddresses.Size = new System.Drawing.Size(178, 97);
            this.lstStaticAddresses.TabIndex = 0;
            this.lstStaticAddresses.UseCompatibleStateImageBehavior = false;
            this.lstStaticAddresses.View = System.Windows.Forms.View.Details;
            this.lstStaticAddresses.SelectedIndexChanged += new System.EventHandler(this.lstStaticAddresses_SelectedIndexChanged);
            // 
            // columnHeaderStaticAddress
            // 
            this.columnHeaderStaticAddress.Text = "Statička adresa";
            this.columnHeaderStaticAddress.Width = 150;
            // 
            // rbPostpaid
            // 
            this.rbPostpaid.AutoSize = true;
            this.rbPostpaid.Location = new System.Drawing.Point(56, 46);
            this.rbPostpaid.Name = "rbPostpaid";
            this.rbPostpaid.Size = new System.Drawing.Size(66, 17);
            this.rbPostpaid.TabIndex = 1;
            this.rbPostpaid.Text = "Postpaid";
            this.rbPostpaid.UseVisualStyleBackColor = true;
            // 
            // rbPrepaid
            // 
            this.rbPrepaid.AutoSize = true;
            this.rbPrepaid.Checked = true;
            this.rbPrepaid.Location = new System.Drawing.Point(56, 20);
            this.rbPrepaid.Name = "rbPrepaid";
            this.rbPrepaid.Size = new System.Drawing.Size(61, 17);
            this.rbPrepaid.TabIndex = 0;
            this.rbPrepaid.TabStop = true;
            this.rbPrepaid.Text = "Prepaid";
            this.rbPrepaid.UseVisualStyleBackColor = true;
            // 
            // lstAdditionalChannelPackages
            // 
            this.lstAdditionalChannelPackages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderPackageName});
            this.lstAdditionalChannelPackages.FullRowSelect = true;
            this.lstAdditionalChannelPackages.GridLines = true;
            this.lstAdditionalChannelPackages.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstAdditionalChannelPackages.HideSelection = false;
            this.lstAdditionalChannelPackages.Location = new System.Drawing.Point(175, 26);
            this.lstAdditionalChannelPackages.Name = "lstAdditionalChannelPackages";
            this.lstAdditionalChannelPackages.Size = new System.Drawing.Size(182, 201);
            this.lstAdditionalChannelPackages.TabIndex = 1;
            this.lstAdditionalChannelPackages.UseCompatibleStateImageBehavior = false;
            this.lstAdditionalChannelPackages.View = System.Windows.Forms.View.Details;
            this.lstAdditionalChannelPackages.SelectedIndexChanged += new System.EventHandler(this.lstAdditionalChannelPackages_SelectedIndexChanged);
            // 
            // columnHeaderPackageName
            // 
            this.columnHeaderPackageName.Text = "Dodatni paket kanala";
            this.columnHeaderPackageName.Width = 150;
            // 
            // grpInternetType
            // 
            this.grpInternetType.Controls.Add(this.rbPostpaid);
            this.grpInternetType.Controls.Add(this.rbPrepaid);
            this.grpInternetType.Location = new System.Drawing.Point(6, 122);
            this.grpInternetType.Name = "grpInternetType";
            this.grpInternetType.Size = new System.Drawing.Size(178, 75);
            this.grpInternetType.TabIndex = 5;
            this.grpInternetType.TabStop = false;
            this.grpInternetType.Text = "Tip";
            // 
            // grpInternet
            // 
            this.grpInternet.Controls.Add(this.grpPrepaid);
            this.grpInternet.Controls.Add(this.grpInternetType);
            this.grpInternet.Controls.Add(this.btnDeleteStaticAddress);
            this.grpInternet.Controls.Add(this.txtStaticAddress);
            this.grpInternet.Controls.Add(this.btnAddStaticAddress);
            this.grpInternet.Controls.Add(this.lblStaticAddress);
            this.grpInternet.Controls.Add(this.lstStaticAddresses);
            this.grpInternet.Location = new System.Drawing.Point(381, 12);
            this.grpInternet.Name = "grpInternet";
            this.grpInternet.Size = new System.Drawing.Size(407, 266);
            this.grpInternet.TabIndex = 2;
            this.grpInternet.TabStop = false;
            this.grpInternet.Text = "Internet";
            // 
            // grpPrepaid
            // 
            this.grpPrepaid.Controls.Add(this.txtBalance);
            this.grpPrepaid.Controls.Add(this.lblBalance);
            this.grpPrepaid.Controls.Add(this.dtpPaymentDate);
            this.grpPrepaid.Controls.Add(this.lblPaymentDate);
            this.grpPrepaid.Location = new System.Drawing.Point(193, 122);
            this.grpPrepaid.Name = "grpPrepaid";
            this.grpPrepaid.Size = new System.Drawing.Size(208, 133);
            this.grpPrepaid.TabIndex = 6;
            this.grpPrepaid.TabStop = false;
            this.grpPrepaid.Text = "Prepaid";
            // 
            // txtBalance
            // 
            this.txtBalance.Location = new System.Drawing.Point(87, 91);
            this.txtBalance.MaxLength = 7;
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Size = new System.Drawing.Size(100, 20);
            this.txtBalance.TabIndex = 3;
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Location = new System.Drawing.Point(19, 94);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(40, 13);
            this.lblBalance.TabIndex = 2;
            this.lblBalance.Text = "Stanje:";
            // 
            // dtpPaymentDate
            // 
            this.dtpPaymentDate.Location = new System.Drawing.Point(22, 55);
            this.dtpPaymentDate.Name = "dtpPaymentDate";
            this.dtpPaymentDate.Size = new System.Drawing.Size(179, 20);
            this.dtpPaymentDate.TabIndex = 1;
            // 
            // lblPaymentDate
            // 
            this.lblPaymentDate.AutoSize = true;
            this.lblPaymentDate.Location = new System.Drawing.Point(19, 32);
            this.lblPaymentDate.Name = "lblPaymentDate";
            this.lblPaymentDate.Size = new System.Drawing.Size(73, 13);
            this.lblPaymentDate.TabIndex = 0;
            this.lblPaymentDate.Text = "Datum uplate:";
            // 
            // lblChooseChannelPackages
            // 
            this.lblChooseChannelPackages.AutoSize = true;
            this.lblChooseChannelPackages.Location = new System.Drawing.Point(6, 26);
            this.lblChooseChannelPackages.Name = "lblChooseChannelPackages";
            this.lblChooseChannelPackages.Size = new System.Drawing.Size(163, 13);
            this.lblChooseChannelPackages.TabIndex = 0;
            this.lblChooseChannelPackages.Text = "Izaberite dodatne pakete kanala:";
            // 
            // grpTelevision
            // 
            this.grpTelevision.Controls.Add(this.btnAddChannelPackages);
            this.grpTelevision.Controls.Add(this.btnDeleteChannelPackages);
            this.grpTelevision.Controls.Add(this.lstAddedChannelPackages);
            this.grpTelevision.Controls.Add(this.lblAddedChannelPackages);
            this.grpTelevision.Controls.Add(this.lstAdditionalChannelPackages);
            this.grpTelevision.Controls.Add(this.lblChooseChannelPackages);
            this.grpTelevision.Location = new System.Drawing.Point(12, 282);
            this.grpTelevision.Name = "grpTelevision";
            this.grpTelevision.Size = new System.Drawing.Size(363, 400);
            this.grpTelevision.TabIndex = 1;
            this.grpTelevision.TabStop = false;
            this.grpTelevision.Text = "Televizija";
            // 
            // btnAddChannelPackages
            // 
            this.btnAddChannelPackages.Location = new System.Drawing.Point(48, 113);
            this.btnAddChannelPackages.Name = "btnAddChannelPackages";
            this.btnAddChannelPackages.Size = new System.Drawing.Size(75, 50);
            this.btnAddChannelPackages.TabIndex = 2;
            this.btnAddChannelPackages.Text = "Dodajte pakete kanala";
            this.btnAddChannelPackages.UseVisualStyleBackColor = true;
            this.btnAddChannelPackages.Click += new System.EventHandler(this.btnAddChannelPackages_Click);
            // 
            // btnDeleteChannelPackages
            // 
            this.btnDeleteChannelPackages.Enabled = false;
            this.btnDeleteChannelPackages.Location = new System.Drawing.Point(204, 371);
            this.btnDeleteChannelPackages.Name = "btnDeleteChannelPackages";
            this.btnDeleteChannelPackages.Size = new System.Drawing.Size(125, 23);
            this.btnDeleteChannelPackages.TabIndex = 5;
            this.btnDeleteChannelPackages.Text = "Obrišite pakete kanala";
            this.btnDeleteChannelPackages.UseVisualStyleBackColor = true;
            this.btnDeleteChannelPackages.Click += new System.EventHandler(this.btnDeleteChannelPackages_Click);
            // 
            // lstAddedChannelPackages
            // 
            this.lstAddedChannelPackages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderChannelName});
            this.lstAddedChannelPackages.FullRowSelect = true;
            this.lstAddedChannelPackages.GridLines = true;
            this.lstAddedChannelPackages.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstAddedChannelPackages.HideSelection = false;
            this.lstAddedChannelPackages.Location = new System.Drawing.Point(175, 238);
            this.lstAddedChannelPackages.Name = "lstAddedChannelPackages";
            this.lstAddedChannelPackages.Size = new System.Drawing.Size(182, 129);
            this.lstAddedChannelPackages.TabIndex = 4;
            this.lstAddedChannelPackages.UseCompatibleStateImageBehavior = false;
            this.lstAddedChannelPackages.View = System.Windows.Forms.View.Details;
            this.lstAddedChannelPackages.SelectedIndexChanged += new System.EventHandler(this.lstAddedChannelPackages_SelectedIndexChanged);
            // 
            // columnHeaderChannelName
            // 
            this.columnHeaderChannelName.Text = "Dodatni paket kanala";
            this.columnHeaderChannelName.Width = 150;
            // 
            // lblAddedChannelPackages
            // 
            this.lblAddedChannelPackages.AutoSize = true;
            this.lblAddedChannelPackages.Location = new System.Drawing.Point(6, 238);
            this.lblAddedChannelPackages.Name = "lblAddedChannelPackages";
            this.lblAddedChannelPackages.Size = new System.Drawing.Size(131, 13);
            this.lblAddedChannelPackages.TabIndex = 3;
            this.lblAddedChannelPackages.Text = "Već dodati dodatni paketi:";
            // 
            // columnHeaderPhoneNumber
            // 
            this.columnHeaderPhoneNumber.Text = "Broj telefona";
            this.columnHeaderPhoneNumber.Width = 120;
            // 
            // btnDeletePhoneNumber
            // 
            this.btnDeletePhoneNumber.Enabled = false;
            this.btnDeletePhoneNumber.Location = new System.Drawing.Point(274, 68);
            this.btnDeletePhoneNumber.Name = "btnDeletePhoneNumber";
            this.btnDeletePhoneNumber.Size = new System.Drawing.Size(75, 23);
            this.btnDeletePhoneNumber.TabIndex = 1;
            this.btnDeletePhoneNumber.Text = "Obrišite broj";
            this.btnDeletePhoneNumber.UseVisualStyleBackColor = true;
            this.btnDeletePhoneNumber.Click += new System.EventHandler(this.btnDeletePhoneNumber_Click);
            // 
            // btnAddPhoneNumber
            // 
            this.btnAddPhoneNumber.Location = new System.Drawing.Point(60, 216);
            this.btnAddPhoneNumber.Name = "btnAddPhoneNumber";
            this.btnAddPhoneNumber.Size = new System.Drawing.Size(75, 23);
            this.btnAddPhoneNumber.TabIndex = 6;
            this.btnAddPhoneNumber.Text = "Dodajte broj";
            this.btnAddPhoneNumber.UseVisualStyleBackColor = true;
            this.btnAddPhoneNumber.Click += new System.EventHandler(this.btnAddPhoneNumber_Click);
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Location = new System.Drawing.Point(141, 151);
            this.txtPhoneNumber.MaxLength = 10;
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(100, 20);
            this.txtPhoneNumber.TabIndex = 3;
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(21, 154);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(69, 13);
            this.lblPhoneNumber.TabIndex = 2;
            this.lblPhoneNumber.Text = "Broj telefona:";
            // 
            // lstPhoneNumbers
            // 
            this.lstPhoneNumbers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderPhoneNumber,
            this.columnHeaderNumberOfMinutes});
            this.lstPhoneNumbers.FullRowSelect = true;
            this.lstPhoneNumbers.GridLines = true;
            this.lstPhoneNumbers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstPhoneNumbers.HideSelection = false;
            this.lstPhoneNumbers.Location = new System.Drawing.Point(6, 19);
            this.lstPhoneNumbers.MultiSelect = false;
            this.lstPhoneNumbers.Name = "lstPhoneNumbers";
            this.lstPhoneNumbers.Size = new System.Drawing.Size(256, 120);
            this.lstPhoneNumbers.TabIndex = 0;
            this.lstPhoneNumbers.UseCompatibleStateImageBehavior = false;
            this.lstPhoneNumbers.View = System.Windows.Forms.View.Details;
            this.lstPhoneNumbers.SelectedIndexChanged += new System.EventHandler(this.lstPhoneNumbers_SelectedIndexChanged);
            // 
            // columnHeaderNumberOfMinutes
            // 
            this.columnHeaderNumberOfMinutes.Text = "Broj ostvarenih minuta";
            this.columnHeaderNumberOfMinutes.Width = 120;
            // 
            // grpTelephony
            // 
            this.grpTelephony.Controls.Add(this.btnUpdateNumberOfMinutes);
            this.grpTelephony.Controls.Add(this.nudNumberOfMinutes);
            this.grpTelephony.Controls.Add(this.lblNumberOfMinutes);
            this.grpTelephony.Controls.Add(this.btnDeletePhoneNumber);
            this.grpTelephony.Controls.Add(this.btnAddPhoneNumber);
            this.grpTelephony.Controls.Add(this.txtPhoneNumber);
            this.grpTelephony.Controls.Add(this.lblPhoneNumber);
            this.grpTelephony.Controls.Add(this.lstPhoneNumbers);
            this.grpTelephony.Location = new System.Drawing.Point(12, 12);
            this.grpTelephony.Name = "grpTelephony";
            this.grpTelephony.Size = new System.Drawing.Size(363, 255);
            this.grpTelephony.TabIndex = 0;
            this.grpTelephony.TabStop = false;
            this.grpTelephony.Text = "Telefonija";
            // 
            // btnUpdateNumberOfMinutes
            // 
            this.btnUpdateNumberOfMinutes.Enabled = false;
            this.btnUpdateNumberOfMinutes.Location = new System.Drawing.Point(190, 216);
            this.btnUpdateNumberOfMinutes.Name = "btnUpdateNumberOfMinutes";
            this.btnUpdateNumberOfMinutes.Size = new System.Drawing.Size(125, 23);
            this.btnUpdateNumberOfMinutes.TabIndex = 7;
            this.btnUpdateNumberOfMinutes.Text = "Ažurirajte broj minuta";
            this.btnUpdateNumberOfMinutes.UseVisualStyleBackColor = true;
            this.btnUpdateNumberOfMinutes.Click += new System.EventHandler(this.btnUpdateNumberOfMinutes_Click);
            // 
            // nudNumberOfMinutes
            // 
            this.nudNumberOfMinutes.Location = new System.Drawing.Point(141, 177);
            this.nudNumberOfMinutes.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudNumberOfMinutes.Name = "nudNumberOfMinutes";
            this.nudNumberOfMinutes.Size = new System.Drawing.Size(100, 20);
            this.nudNumberOfMinutes.TabIndex = 5;
            // 
            // lblNumberOfMinutes
            // 
            this.lblNumberOfMinutes.AutoSize = true;
            this.lblNumberOfMinutes.Location = new System.Drawing.Point(21, 182);
            this.lblNumberOfMinutes.Name = "lblNumberOfMinutes";
            this.lblNumberOfMinutes.Size = new System.Drawing.Size(114, 13);
            this.lblNumberOfMinutes.TabIndex = 4;
            this.lblNumberOfMinutes.Text = "Broj ostvarenih minuta:";
            // 
            // frmUpdateService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 690);
            this.Controls.Add(this.btnUpdateService);
            this.Controls.Add(this.grpInternet);
            this.Controls.Add(this.grpTelevision);
            this.Controls.Add(this.grpTelephony);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUpdateService";
            this.Text = "Ažuriranje podataka o usluzi";
            this.Load += new System.EventHandler(this.frmUpdateService_Load);
            this.grpInternetType.ResumeLayout(false);
            this.grpInternetType.PerformLayout();
            this.grpInternet.ResumeLayout(false);
            this.grpInternet.PerformLayout();
            this.grpPrepaid.ResumeLayout(false);
            this.grpPrepaid.PerformLayout();
            this.grpTelevision.ResumeLayout(false);
            this.grpTelevision.PerformLayout();
            this.grpTelephony.ResumeLayout(false);
            this.grpTelephony.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfMinutes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnUpdateService;
        private System.Windows.Forms.Button btnDeleteStaticAddress;
        private System.Windows.Forms.TextBox txtStaticAddress;
        private System.Windows.Forms.Button btnAddStaticAddress;
        private System.Windows.Forms.Label lblStaticAddress;
        private System.Windows.Forms.ListView lstStaticAddresses;
        private System.Windows.Forms.ColumnHeader columnHeaderStaticAddress;
        private System.Windows.Forms.RadioButton rbPostpaid;
        private System.Windows.Forms.RadioButton rbPrepaid;
        private System.Windows.Forms.ListView lstAdditionalChannelPackages;
        private System.Windows.Forms.ColumnHeader columnHeaderPackageName;
        private System.Windows.Forms.GroupBox grpInternetType;
        private System.Windows.Forms.GroupBox grpInternet;
        private System.Windows.Forms.Label lblChooseChannelPackages;
        private System.Windows.Forms.GroupBox grpTelevision;
        private System.Windows.Forms.ColumnHeader columnHeaderPhoneNumber;
        private System.Windows.Forms.Button btnDeletePhoneNumber;
        private System.Windows.Forms.Button btnAddPhoneNumber;
        private System.Windows.Forms.TextBox txtPhoneNumber;
        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.ListView lstPhoneNumbers;
        private System.Windows.Forms.GroupBox grpTelephony;
        private System.Windows.Forms.ColumnHeader columnHeaderNumberOfMinutes;
        private System.Windows.Forms.NumericUpDown nudNumberOfMinutes;
        private System.Windows.Forms.Label lblNumberOfMinutes;
        private System.Windows.Forms.GroupBox grpPrepaid;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.DateTimePicker dtpPaymentDate;
        private System.Windows.Forms.Label lblPaymentDate;
        private System.Windows.Forms.Button btnAddChannelPackages;
        private System.Windows.Forms.Button btnDeleteChannelPackages;
        private System.Windows.Forms.ListView lstAddedChannelPackages;
        private System.Windows.Forms.ColumnHeader columnHeaderChannelName;
        private System.Windows.Forms.Label lblAddedChannelPackages;
        private System.Windows.Forms.Button btnUpdateNumberOfMinutes;
        private System.Windows.Forms.TextBox txtBalance;
    }
}