﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using TelekomunikacionaKompanija.DTOManagers;
using TelekomunikacionaKompanija.DTOManagers.Contract;
using TelekomunikacionaKompanija.DTOs.Contract.Service;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Internet;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Telephony;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Television;

namespace TelekomunikacionaKompanija.Forms.Contract.Service
{
    public partial class frmCreateService : Form
    {
        #region Attribute(s)
        private bool containsTelephony;
        private bool containsTelevision;
        private bool containsInternet;
        #endregion Attribute(s)

        #region Properties
        public string PackageName { get; set; }
        #endregion Properties

        #region Constructor(s)
        public frmCreateService()
        {
            this.InitializeComponent();
        }
        #endregion Constructor(s)

        #region Form Event Method(s)
        private void frmCreateService_Load(object sender, EventArgs e)
        {
            var package = PackageDTOManager.GetPackageByName(this.PackageName);

            if (package.NumberOfMinutes == 0)
            {
                this.containsTelephony = false;

                this.lstPhoneNumbers.Enabled = false;
                this.lblPhoneNumber.ForeColor = Color.Gray;
                this.txtPhoneNumber.Enabled = false;
                this.btnAddPhoneNumber.Enabled = false;
            }
            else
            {
                this.containsTelephony = true;
            }

            this.btnDeletePhoneNumber.Enabled = false;

            if (PackageDTOManager.CheckIfPackageContainsTelevision(this.PackageName))
            {
                this.containsTelevision = true;

                if (package.AdditionalPackagePrice == 0)
                {
                    this.lblChooseChannelPackages.ForeColor = Color.Gray;
                    this.lstAdditionalChannelPackages.Enabled = false;
                }
                else
                {
                    var additionalChannelPackagesList =
                        PackageDTOManager.GetAdditionalPackagesByPackage(this.PackageName);

                    foreach (var additionalChannelPackage in additionalChannelPackagesList)
                    {
                        this.lstAdditionalChannelPackages.Items
                            .Add(additionalChannelPackage.Name);
                    }

                    this.lstAdditionalChannelPackages.Refresh();
                }
            }
            else
            {
                this.containsTelevision = false;

                this.lblChooseChannelPackages.ForeColor = Color.Gray;
                this.lstAdditionalChannelPackages.Enabled = false;
            }

            if (package.InternetSpeed == 0)
            {
                this.containsInternet = false;

                this.lstStaticAddresses.Enabled = false;
                this.lblStaticAddress.ForeColor = Color.Gray;
                this.txtStaticAddress.Enabled = false;
                this.btnAddStaticAddress.Enabled = false;
                this.rbPrepaid.Enabled = false;
                this.rbPostpaid.Enabled = false;
            }
            else
            {
                this.containsInternet = true;
            }

            this.btnDeleteStaticAddress.Enabled = false;
            this.rbPrepaid.Checked = true;
            this.rbPostpaid.Checked = false;
        }

        private void lstPhoneNumbers_SelectedIndexChanged(object sender, EventArgs e) =>
            this.btnDeletePhoneNumber.Enabled = this.lstPhoneNumbers.SelectedItems.Count != 0;

        private void btnAddPhoneNumber_Click(object sender, EventArgs e)
        {
            string phoneNumber = this.txtPhoneNumber.Text;

            if (phoneNumber != string.Empty)
            {
                if (!this.ContainsDigitsOnly(phoneNumber))
                {
                    MessageBox.Show("Broj telefona mora da sadrži samo cifre.", "Upozorenje",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    this.lstPhoneNumbers.Items.Add(phoneNumber);

                    if (this.lstPhoneNumbers.Items.Count == 4)
                    {
                        this.btnAddPhoneNumber.Enabled = false;
                    }

                    this.txtPhoneNumber.Text = string.Empty;
                }
            }
        }

        private void btnDeletePhoneNumber_Click(object sender, EventArgs e)
        {
            int selectedIndex = this.lstPhoneNumbers.SelectedIndices[0];

            this.lstPhoneNumbers.Items.RemoveAt(selectedIndex);

            this.btnAddPhoneNumber.Enabled = true;
            this.btnDeletePhoneNumber.Enabled = false;
        }

        private void lstStaticAddresses_SelectedIndexChanged(object sender, EventArgs e) =>
            this.btnDeleteStaticAddress.Enabled = this.lstStaticAddresses.SelectedItems.Count != 0;

        private void btnAddStaticAddress_Click(object sender, EventArgs e)
        {
            string staticAddress = this.txtStaticAddress.Text;

            if (staticAddress != string.Empty)
            {
                if (!this.IsValidIPAddress(staticAddress))
                {
                    MessageBox.Show("Statička adresa mora biti u formatu xxx.xxx.xxx.xxx",
                        "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    this.lstStaticAddresses.Items.Add(staticAddress);

                    if (this.lstStaticAddresses.Items.Count == 2)
                    {
                        this.btnAddStaticAddress.Enabled = false;
                    }

                    this.txtStaticAddress.Text = string.Empty;
                }
            }
        }

        private void btnDeleteStaticAddress_Click(object sender, EventArgs e)
        {
            int selectedIndex = this.lstStaticAddresses.SelectedIndices[0];

            this.lstStaticAddresses.Items.RemoveAt(selectedIndex);

            this.btnAddStaticAddress.Enabled = true;
            this.btnDeleteStaticAddress.Enabled = false;
        }

        private void btnCreateService_Click(object sender, EventArgs e)
        {
            var service = new ServiceDTO(this.containsTelephony, this.containsTelevision,
                this.containsInternet);

            if (this.containsTelephony)
            {
                foreach (ListViewItem item in this.lstPhoneNumbers.Items)
                {
                    var phoneNumber = new PhoneNumberDTO(item.SubItems[0].Text);

                    service.PhoneNumbers.Add(phoneNumber);
                }
            }

            if (this.containsTelevision && this.lstAdditionalChannelPackages.Enabled)
            {
                foreach (ListViewItem item in this.lstAdditionalChannelPackages.SelectedItems)
                {
                    var additionalChannelPackage =
                        new AdditionalChannelPackageDTO(item.SubItems[0].Text);

                    service.AdditionalChannelPackages.Add(additionalChannelPackage);
                }
            }

            if (this.containsInternet)
            {
                service.Internet = new InternetDTO();

                if (this.lstStaticAddresses.Items.Count == 1)
                {
                    service.Internet.Address1 = this.lstStaticAddresses.Items[0].SubItems[0].Text;
                }
                else if (this.lstStaticAddresses.Items.Count == 2)
                {
                    service.Internet.Address1 = this.lstStaticAddresses.Items[0].SubItems[0].Text;
                    service.Internet.Address2 = this.lstStaticAddresses.Items[1].SubItems[0].Text;
                }

                service.Internet.InternetType = (this.rbPrepaid.Checked) ? InternetType.Prepaid :
                    InternetType.Postpaid;
            }

            int serviceID = ServiceDTOManager.CreateService(service);

            if (serviceID != -1)
            {
                MessageBox.Show("Usluga je uspešno kreirana.", "Obaveštenje",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                frmContract.serviceId = serviceID;

                this.Close();
            }
            else
            {
                MessageBox.Show("Usluga nije kreirana.", "Greška",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion Form Event Method(s)

        #region Helper Method(s)
        private bool ContainsDigitsOnly(string text)
        {
            bool isDigit = true;

            for (int i = 0; i < text.Length && isDigit; i++)
            {
                if (text[i] < '0' || text[i] > '9')
                {
                    isDigit = false;
                }
            }

            return isDigit;
        }

        private bool IsValidIPAddress(string ipAddress)
        {
            string addressRegEx = "(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}" +
                "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])";

            return Regex.IsMatch(ipAddress, addressRegEx);
        }
        #endregion Helper Method(s)
    }
}
