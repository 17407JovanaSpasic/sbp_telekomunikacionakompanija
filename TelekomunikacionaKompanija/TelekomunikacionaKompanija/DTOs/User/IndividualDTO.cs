﻿namespace TelekomunikacionaKompanija.DTOs.User
{
    public class IndividualDTO : UserDTO
    {
        #region Properties
        public string IndividualJMBG { get; set; }
        #endregion Properties

        #region Constructor(s)
        public IndividualDTO() : base() { }

        public IndividualDTO(string name, string lname, string address, int addressNumber, string city,
            string contactPhone, string email, string jmbg)
           : base(name, lname, address, addressNumber, city, contactPhone, email, 1, 0)
        {
            this.IndividualJMBG = jmbg;
        }

        public IndividualDTO(int id, string name, string lname, string address, int addressNumber,
            string city, string contactPhone, string email, string jmbg)
            : base(id, name, lname, address, addressNumber, city, contactPhone, email, 1, 0)
        {
            this.IndividualJMBG = jmbg;
        }
        #endregion Constructor(s)
    }
}
