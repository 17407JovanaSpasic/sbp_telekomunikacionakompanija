﻿using System.Collections.Generic;

using TelekomunikacionaKompanija.DTOs.Contract;
using TelekomunikacionaKompanija.Entities;

namespace TelekomunikacionaKompanija.DTOs.User
{
    public class UserDTO
    {
        #region Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int AddressNumber { get; set; }
        public string City { get; set; }
        public string ContactPhone { get; set; }
        public string Email { get; set; }

        //da li navodim ovaj atribut?
        public virtual CommunicationNode NodeSerialNumber { get; set; }

        //da li treba virtual? Posto su na onom njihovom primeru stavljali tako
        public virtual IList<ContractDTO> UsersContracts { get; set; }

        //flagovi da li je user Fizicko odnosno Pravno lice
        public int Individual { get; set; }
        public int LegalEntity { get; set; }
        #endregion Properties

        #region Constructor(s)
        public UserDTO() { this.UsersContracts = new List<ContractDTO>(); }

        public UserDTO(string name, string lname, string address, int addressNumber, string city,
            string contactPhone, string email, int flagIndividual, int flagLegalEntity)
        {
            this.Name = name;
            this.LastName = lname;
            this.Address = address;
            this.AddressNumber = addressNumber;
            this.City = city;
            this.ContactPhone = contactPhone;
            this.Email = email;

            this.Individual = flagIndividual;           // Postavljamo flagove:
            this.LegalEntity = flagLegalEntity;         // 0 == false
                                                        // 1 == true                   
        }

        public UserDTO(int id, string name, string lname, string address, int addressNumber, string city,
            string contactPhone, string email, int flagIndividual, int flagLegalEntity)
        {
            this.Id = id;
            this.Name = name;
            this.LastName = lname;
            this.Address = address;
            this.AddressNumber = addressNumber;
            this.City = city;
            this.ContactPhone = contactPhone;
            this.Email = email;

            this.Individual = flagIndividual;           // Postavljamo flagove:
            this.LegalEntity = flagLegalEntity;         // 0 == false
                                                        // 1 == true                   
        }
        #endregion Constructor(s)
    }
}
