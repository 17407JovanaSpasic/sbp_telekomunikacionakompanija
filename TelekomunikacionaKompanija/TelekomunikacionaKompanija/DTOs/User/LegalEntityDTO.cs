﻿namespace TelekomunikacionaKompanija.DTOs.User
{
    public class LegalEntityDTO : UserDTO
    {
        #region Properties
        public string LegalEntityFaxNumber { get; set; }
        public string LegalEntityPIB { get; set; }
        #endregion Properties

        #region Constructor(s)
        public LegalEntityDTO() : base() { }

        //this constructor is used for representing relation in list views
        public LegalEntityDTO(string name, string lname, string address, int addressNumber, string city,
            string contactPhone, string email, string FaxNumber, string PIB)
            : base(name, lname, address, addressNumber, city, contactPhone, email, 0, 1)
        {
            this.LegalEntityFaxNumber = FaxNumber;
            this.LegalEntityPIB = PIB;
        }

        //this constructor is used for representing relation in list views
        public LegalEntityDTO(int id, string name, string lname, string address, int addressNumber,
            string city, string contactPhone, string email, string FaxNumber, string PIB)
            : base(id, name, lname, address, addressNumber, city, contactPhone, email, 0, 1)
        {
            this.LegalEntityFaxNumber = FaxNumber;
            this.LegalEntityPIB = PIB;
        }
        #endregion Constructor(s)
    }
}
