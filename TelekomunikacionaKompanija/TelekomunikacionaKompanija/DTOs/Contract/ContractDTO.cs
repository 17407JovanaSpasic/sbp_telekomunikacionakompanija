﻿using System;
using System.Collections.Generic;

using TelekomunikacionaKompanija.DTOs.Contract.Payment;
using TelekomunikacionaKompanija.DTOs.Contract.Service;
using TelekomunikacionaKompanija.DTOs.Package;
using TelekomunikacionaKompanija.DTOs.User;

namespace TelekomunikacionaKompanija.DTOs.Contract
{
    public class ContractDTO
    {
        #region Properties
        public string ContractId { get; set; }
        public DateTime ContractStartDate { get; set; }
        public DateTime ContractEndDate { get; set; }
        public UserDTO UserId { get; set; }
        public PackageDTO PackageName { get; set; }
        public ServiceDTO ServiceId { get; set; }

        public virtual IList<PaymentDTO> ServicePayment { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ContractDTO() { }

        public ContractDTO(string contractId, DateTime contractStarDate, DateTime contractEndDate,
            UserDTO userId, PackageDTO packageName, ServiceDTO serviceId)
        {
            this.ContractId = contractId;
            this.ContractStartDate = contractStarDate;
            this.ContractEndDate = contractEndDate;
            this.UserId = userId;
            this.PackageName = packageName;
            this.ServiceId = serviceId;
        }

        public ContractDTO(DateTime contractStarDate, DateTime contractEndDate, UserDTO userId,
            PackageDTO packageName, ServiceDTO serviceId)
        {
            this.ContractStartDate = contractStarDate;
            this.ContractEndDate = contractEndDate;
            this.UserId = userId;
            this.PackageName = packageName;
            this.ServiceId = serviceId;
        }

        //nisam bio siguran kako cemo da radimo sa onom listom, pa sam zato ostavio zakomentarisano
        /* public ContractDTO(string contractId, DateTime contractStarDate, DateTime contractEndDate, UserDTO userId, PackageDTO packageName, 
         ServiceDTO serviceId, List<PackageDTO> servicePayment)
         {
             this.ContractId = contractId;
             this.ContractStartDate = contractStarDate;
             this.ContractEndDate = contractEndDate;
             this.UserId = userId;
             this.PackageName = packageName;
             this.ServicePayment = ServicePayment;
             //this.ServiceId = servicePayment; ??
         }*/
        #endregion Constructor(s)
    }
}
