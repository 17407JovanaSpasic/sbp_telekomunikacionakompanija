﻿using System;

namespace TelekomunikacionaKompanija.DTOs.Contract
{
    public class ContractByUserDTO
    {
        #region Properties
        public string ContractNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PackageName { get; set; }
        public int ServiceID { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ContractByUserDTO() { }

        public ContractByUserDTO(string contractNumber, DateTime startDate, DateTime endDate,
            string packageName, int serviceID)
        {
            this.ContractNumber = contractNumber;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.PackageName = packageName;
            this.ServiceID = serviceID;
        }
        #endregion Constructor(s)
    }
}
