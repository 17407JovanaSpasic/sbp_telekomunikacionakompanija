﻿using System.Collections.Generic;

using TelekomunikacionaKompanija.DTOs.Contract.Service.Internet;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Telephony;
using TelekomunikacionaKompanija.DTOs.Contract.Service.Television;

namespace TelekomunikacionaKompanija.DTOs.Contract.Service
{
    public class ServiceDTO
    {
        #region Properties
        public int ServiceID { get; protected set; }
        public bool ContainsTelephony { get; set; }
        public bool ContainsTelevision { get; set; }
        public bool ContainsInternet { get; set; }

        public IList<PhoneNumberDTO> PhoneNumbers { get; set; }
        public IList<AdditionalChannelPackageDTO> AdditionalChannelPackages { get; set; }
        public InternetDTO Internet { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ServiceDTO()
        {
            this.PhoneNumbers = new List<PhoneNumberDTO>();
            this.AdditionalChannelPackages = new List<AdditionalChannelPackageDTO>();
            this.Internet = null;
        }

        public ServiceDTO(bool containsTelephony, bool containsTelevision, bool containsInternet)
        {
            this.ContainsTelephony = containsTelephony;
            this.ContainsTelevision = containsTelevision;
            this.ContainsInternet = containsInternet;
            this.PhoneNumbers = new List<PhoneNumberDTO>();
            this.AdditionalChannelPackages = new List<AdditionalChannelPackageDTO>();
            this.Internet = null;
        }

        public ServiceDTO(int serviceID)
        {
            this.ServiceID = serviceID;
            this.PhoneNumbers = new List<PhoneNumberDTO>();
            this.AdditionalChannelPackages = new List<AdditionalChannelPackageDTO>();
            this.Internet = null;
        }

        public ServiceDTO(int serviceID, bool containsTelephony, bool containsTelevision,
            bool containsInternet)
        {
            this.ServiceID = serviceID;
            this.ContainsTelephony = containsTelephony;
            this.ContainsTelevision = containsTelevision;
            this.ContainsInternet = containsInternet;
            this.PhoneNumbers = new List<PhoneNumberDTO>();
            this.AdditionalChannelPackages = new List<AdditionalChannelPackageDTO>();
            this.Internet = null;
        }
        #endregion Constructor(s)
    }
}
