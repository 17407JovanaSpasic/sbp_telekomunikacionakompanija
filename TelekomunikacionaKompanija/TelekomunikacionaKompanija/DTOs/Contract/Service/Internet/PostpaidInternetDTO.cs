﻿namespace TelekomunikacionaKompanija.DTOs.Contract.Service.Internet
{
    public class PostpaidInternetDTO : InternetDTO
    {
        #region Constructor(s)
        public PostpaidInternetDTO() : base() { }

        public PostpaidInternetDTO(string address1, string address2, InternetType internetType)
            : base(address1, address2, internetType) { }
        #endregion Constructor(s)
    }
}
