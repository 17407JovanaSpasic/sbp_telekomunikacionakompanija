﻿namespace TelekomunikacionaKompanija.DTOs.Contract.Service.Internet
{
    public class InternetDTO
    {
        #region Properties
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public InternetType InternetType { get; set; }
        #endregion Properties

        #region Constructor(s)
        public InternetDTO() { }

        public InternetDTO(string address1, string address2, InternetType internetType)
        {
            this.Address1 = address1;
            this.Address2 = address2;
            this.InternetType = internetType;
        }
        #endregion Constructor(s)
    }
}
