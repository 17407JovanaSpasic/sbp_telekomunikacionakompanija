﻿namespace TelekomunikacionaKompanija.DTOs.Contract.Service.Internet
{
    public enum InternetType
    {
        Prepaid,
        Postpaid
    };
}
