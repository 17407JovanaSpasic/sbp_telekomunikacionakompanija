﻿using System;

namespace TelekomunikacionaKompanija.DTOs.Contract.Service.Internet
{
    public class PrepaidInternetDTO : InternetDTO
    {
        #region Properties
        public DateTime? PaymentDate { get; set; }
        public float Balance { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PrepaidInternetDTO() : base() { }

        public PrepaidInternetDTO(string address1, string address2, InternetType internetType)
            : base(address1, address2, internetType) { }

        public PrepaidInternetDTO(string address1, string address2, InternetType internetType,
            DateTime? paymentDate, float balance) : base(address1, address2, internetType)
        {
            this.PaymentDate = paymentDate;
            this.Balance = balance;
        }
        #endregion Constructor(s)
    }
}
