﻿namespace TelekomunikacionaKompanija.DTOs.Contract.Service.Telephony
{
    public class PhoneNumberDTO
    {
        #region Properties
        public string PhoneNumber { get; set; }
        public int NumberOfMinutes { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PhoneNumberDTO() { }

        public PhoneNumberDTO(string phoneNumber)
        {
            this.PhoneNumber = phoneNumber;
            this.NumberOfMinutes = 0;
        }

        public PhoneNumberDTO(string phoneNumber, int numberOfMinutes)
        {
            this.PhoneNumber = phoneNumber;
            this.NumberOfMinutes = numberOfMinutes;
        }
        #endregion Constructor(s)
    }
}
