﻿namespace TelekomunikacionaKompanija.DTOs.Contract.Service.Television
{
    public class AdditionalChannelPackageDTO
    {
        #region Properties
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public AdditionalChannelPackageDTO() { }

        public AdditionalChannelPackageDTO(string name)
        {
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
