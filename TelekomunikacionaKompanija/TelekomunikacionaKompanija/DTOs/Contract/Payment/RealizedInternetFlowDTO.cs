﻿using System;

namespace TelekomunikacionaKompanija.DTOs.Contract.Payment
{
    public class RealizedInternetFlowDTO : PaymentDTO
    {
        #region Properties
        public float FlowAmount { get; set; }
        public float CostPerFlow { get; set; }
        #endregion Properties

        #region Constructor(s)
        public RealizedInternetFlowDTO() : base() { }

        public RealizedInternetFlowDTO(DateTime payementDate, float additionalPackagePrice, float totalCost,
           TelekomunikacionaKompanija.Entities.Contract contractNumber, float flowAmout, float costPerFlow)
           : base(payementDate, additionalPackagePrice, totalCost, contractNumber)
        {
            this.FlowAmount = flowAmout;
            this.CostPerFlow = costPerFlow;
        }

        public RealizedInternetFlowDTO(int payementNumber, DateTime payementDate, float additionalPackagePrice, float totalCost,
            TelekomunikacionaKompanija.Entities.Contract contractNumber, float flowAmout, float costPerFlow)
            : base(payementNumber, payementDate, additionalPackagePrice, totalCost, contractNumber)
        {
            this.FlowAmount = flowAmout;
            this.CostPerFlow = costPerFlow;
        }
        #endregion Constructor(s)
    }
}
