﻿using System;

namespace TelekomunikacionaKompanija.DTOs.Contract.Payment
{
    public class FlatRateInternetDTO : PaymentDTO
    {
        #region Properties
        public float Cost { get; set; }
        #endregion Properties

        #region Constructor(s)
        public FlatRateInternetDTO() : base() { }
        public FlatRateInternetDTO(int payementNumber, DateTime payementDate,
            float additionalPackagePrice, float totalCost, TelekomunikacionaKompanija.Entities.Contract contractNumber,
            float cost)
            : base(payementNumber, payementDate, additionalPackagePrice, totalCost, contractNumber)
        {
            this.Cost = cost;
        }

        public FlatRateInternetDTO(DateTime payementDate,
           float additionalPackagePrice, float totalCost, TelekomunikacionaKompanija.Entities.Contract contractNumber,
           float cost)
           : base( payementDate, additionalPackagePrice, totalCost, contractNumber)
        {
            this.Cost = cost;
        }
        #endregion Constructor(s)
    }
}
