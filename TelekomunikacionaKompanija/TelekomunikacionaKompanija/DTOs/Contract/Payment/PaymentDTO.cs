﻿using System;

namespace TelekomunikacionaKompanija.DTOs.Contract.Payment
{
    public class PaymentDTO
    {
        #region Properties
        public int PaymentNumber { get; set; }
        public DateTime PaymentDate { get; set; }
        public float AdditionalPackagePrice { get; set; }
        public float TotalCost { get; set; }

        public ContractDTO Contract { get; set; }

        public TelekomunikacionaKompanija.Entities.Contract ContractNumber { get; set; }

        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string KontaktTelefon { get; set; }
        public string BrojUgovora { get; set; }
        public string NazivPaketa { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PaymentDTO() { }

        public PaymentDTO(DateTime payementDate, float additionalPackagePrice, float totalCost,
            TelekomunikacionaKompanija.Entities.Contract contractNumber)
        {
            this.PaymentDate = payementDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
            this.ContractNumber = contractNumber;
        }

        public PaymentDTO(int payementNumber, DateTime payementDate, float additionalPackagePrice,
            float totalCost, TelekomunikacionaKompanija.Entities.Contract contractNumber)
        {
            this.PaymentNumber = payementNumber;
            this.PaymentDate = payementDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
            this.ContractNumber = contractNumber;
        }

        public PaymentDTO(int payementNumber, DateTime payementDate, float additionalPackagePrice,
            float totalCost, string ime, string prezime, string kontaktTelefon, string brojUgovora,
            string nazivPaketa)
        {
            this.PaymentNumber = payementNumber;
            this.PaymentDate = payementDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
            this.Ime = ime;
            this.Prezime = prezime;
            this.KontaktTelefon = kontaktTelefon;
            this.BrojUgovora = brojUgovora;
            this.NazivPaketa = nazivPaketa;
        }
        #endregion Constructor(s)
    }
}
