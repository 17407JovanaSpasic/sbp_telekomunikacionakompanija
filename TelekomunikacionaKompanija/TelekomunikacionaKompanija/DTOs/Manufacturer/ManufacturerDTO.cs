﻿namespace TelekomunikacionaKompanija.DTOs.Manufacturer
{
    public class ManufacturerDTO
    {
        #region Properties
        public string PIB { get; set; }
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ManufacturerDTO() { }

        public ManufacturerDTO(string pib, string name)
        {
            this.PIB = pib;
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
