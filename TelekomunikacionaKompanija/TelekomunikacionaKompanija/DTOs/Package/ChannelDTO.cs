﻿namespace TelekomunikacionaKompanija.DTOs.Package
{
    public class ChannelDTO
    {
        #region Properties
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ChannelDTO() { }

        public ChannelDTO(string name)
        {
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
