﻿namespace TelekomunikacionaKompanija.DTOs.Package
{
    public class AdditionalPackageDTO
    {
        #region Properties
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public AdditionalPackageDTO() { }

        public AdditionalPackageDTO(string name)
        {
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
