﻿using System;

namespace TelekomunikacionaKompanija.DTOs.Device
{
    public class RegionalHubDTO : MainStationDTO
    {
        #region Properties
        public string NameOfRegion { get; set; }
        #endregion Properties

        #region Constructor(s)
        public RegionalHubDTO() : base() { }

        public RegionalHubDTO(string serialNumber, DateTime startDate, DateTime serviceDate, string reason,
            TelekomunikacionaKompanija.Entities.Manufacturer pib, int numOfNodes, string nameOfRegion)
            : base(serialNumber, startDate, serviceDate, reason, pib, numOfNodes, 1)
        {
            this.NameOfRegion = nameOfRegion;
        }

        public RegionalHubDTO(string serialNumber, DateTime startDate, DateTime serviceDate, string reason,
            int numOfNodes, string nameOfRegion)
            : base(serialNumber, startDate, serviceDate, reason, numOfNodes, 1)
        {
            this.NameOfRegion = nameOfRegion;
        }
        #endregion Constructor(s)
    }
}
