﻿using System;

namespace TelekomunikacionaKompanija.DTOs.Device
{
    public class DeviceDTO
    {
        #region Properties
        public string SerialNumber { get; set; }
        public DateTime StartOfUseDate { get; set; }
        public DateTime ServiceDate { get; set; }
        public string ReasonForServicing { get; set; }
        public TelekomunikacionaKompanija.Entities.Manufacturer PIBManufacturer { get; set; }

        public string Pib { get; set; }
        public string NameP { get; set; }
        #endregion Properties

        #region Constructor(s)
        public DeviceDTO() { }
        public DeviceDTO(string serialNumber)
        {
            this.SerialNumber = serialNumber;
        }

        public DeviceDTO(string serialNumber, DateTime startDate, DateTime serviceDate, string reason,
            TelekomunikacionaKompanija.Entities.Manufacturer pib)
        {
            this.SerialNumber = serialNumber;
            this.StartOfUseDate = startDate;
            this.ServiceDate = serviceDate;
            this.ReasonForServicing = reason;
            this.PIBManufacturer = pib;
        }

        public DeviceDTO(string serialNumber, DateTime startDate, DateTime serviceDate, string reason)
        {
            this.SerialNumber = serialNumber;
            this.StartOfUseDate = startDate;
            this.ServiceDate = serviceDate;
            this.ReasonForServicing = reason;
        }

        public DeviceDTO(string serialNumber, DateTime startDate, DateTime serviceDate, string reason,
            string pib, string name)
        {
            this.SerialNumber = serialNumber;
            this.StartOfUseDate = startDate;
            this.ServiceDate = serviceDate;
            this.ReasonForServicing = reason;
            this.Pib = pib;
            this.NameP = name;
        }
        #endregion Constructor(s)
    }
}
