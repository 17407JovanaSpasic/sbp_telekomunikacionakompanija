﻿using System;

namespace TelekomunikacionaKompanija.DTOs.Device
{
    public class CommunicationNodeDTO : DeviceDTO
    {
        #region Properties
        public string Address { get; set; }
        public int LocationNumber { get; set; }
        public string Description { get; set; }
        public TelekomunikacionaKompanija.Entities.MainStation SerialNumberOfMainStation { get; set; }
        #endregion Properties

        #region Constructor(s)
        public CommunicationNodeDTO(string serialNumber, DateTime startDate, DateTime serviceDate,
            string reason, TelekomunikacionaKompanija.Entities.Manufacturer pib, string address, int locationNum,
            string description, TelekomunikacionaKompanija.Entities.MainStation serialNumOfMainStation)
            : base(serialNumber, startDate, serviceDate, reason, pib)
        {
            this.Address = address;
            this.LocationNumber = locationNum;
            this.Description = description;
            this.SerialNumberOfMainStation = serialNumOfMainStation;
        }
        public CommunicationNodeDTO(string serialNumber, string address, int locationNum, string description)
            : base(serialNumber)
        {
            this.Address = address;
            this.LocationNumber = locationNum;
            this.Description = description;
        }
        #endregion Constructor(s)
    }
}
