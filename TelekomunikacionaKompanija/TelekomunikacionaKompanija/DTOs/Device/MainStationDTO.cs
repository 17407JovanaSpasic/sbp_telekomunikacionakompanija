﻿using System;

namespace TelekomunikacionaKompanija.DTOs.Device
{
    public class MainStationDTO : DeviceDTO
    {
        #region Properties
        public int NumberOfNodes { get; set; }
        public int PRegionalHub { get; set; }
        #endregion Properties

        #region Constructor(s)
        public MainStationDTO() : base() { }

        public MainStationDTO(string serialNumber, DateTime startDate, DateTime serviceDate, string reason,
            TelekomunikacionaKompanija.Entities.Manufacturer pib, int numOfNodes, int pRegionalHub)
            : base(serialNumber, startDate, serviceDate, reason, pib)
        {
            this.NumberOfNodes = numOfNodes;
            //0 - false, 1 - true
            this.PRegionalHub = pRegionalHub;
        }

        public MainStationDTO(string serialNumber, DateTime startDate, DateTime serviceDate, string reason,
            int numOfNodes, int pRegionalHub) : base(serialNumber, startDate, serviceDate, reason)
        {
            this.NumberOfNodes = numOfNodes;
            //0 - false, 1 - true
            this.PRegionalHub = pRegionalHub;
        }

        public MainStationDTO(string serialNumber, int numOfNodes) : base(serialNumber)
        {
            this.NumberOfNodes = numOfNodes;
        }
        #endregion Constructor(s)
    }
}
