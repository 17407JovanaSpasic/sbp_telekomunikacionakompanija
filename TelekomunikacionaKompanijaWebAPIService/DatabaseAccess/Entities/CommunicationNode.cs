﻿using System.Collections.Generic;

namespace DatabaseAccess.Entities
{
    public class CommunicationNode : Device
    {
        #region Properties
        public virtual string Address { get; set; }
        public virtual int LocationNumber { get; set; }
        public virtual string Description { get; set; }

        public virtual MainStation MainStation { get; set; }
        public virtual IList<User> Users { get; set; }
        #endregion Properties

        #region Constructor(s)
        public CommunicationNode()
        {
            this.Users = new List<User>();
        }
        #endregion Constructor(s)
    }
}
