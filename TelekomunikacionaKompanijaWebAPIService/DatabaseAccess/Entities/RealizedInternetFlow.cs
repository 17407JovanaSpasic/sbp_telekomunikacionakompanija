﻿namespace DatabaseAccess.Entities
{
    public class RealizedInternetFlow : Payment
    {
        #region Properties
        public virtual float FlowAmount { get; set; }
        public virtual float CostPerFlow { get; set; }
        #endregion Properties
    }
}
