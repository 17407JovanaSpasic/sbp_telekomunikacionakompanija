﻿using System.Collections.Generic;

namespace DatabaseAccess.Entities
{
    public class MainStation : Device
    {
        #region Properties
        public virtual int NumberOfNodes { get; set; }
        public virtual bool IsRegionalHub { get; set; }
        public virtual string NameOfRegion { get; set; }

        public virtual IList<CommunicationNode> CommunicationNodes { get; set; }
        #endregion Properties

        #region Constructor(s)
        public MainStation()
        {
            this.CommunicationNodes = new List<CommunicationNode>();
        }
        #endregion Constructor(s)
    }
}
