﻿namespace DatabaseAccess.Entities
{
    #region AdditionalPackage
    public class AdditionalPackage
    {
        #region Properties
        public virtual AdditionalPackageID ID { get; set; }
        #endregion Properties

        #region Constructor(s)
        public AdditionalPackage()
        {
            this.ID = new AdditionalPackageID();
        }
        #endregion Constructor(s)
    }
    #endregion AdditionalPackage

    #region AdditionalPackageID
    public class AdditionalPackageID
    {
        #region Properties
        public virtual Package Package { get; set; }
        public virtual string AdditionalPackageName { get; set; }
        #endregion Properties

        #region Method(s)
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != typeof(AdditionalPackageID))
            {
                return false;
            }

            var additionalPackageIDObj = obj as AdditionalPackageID;

            return (this.Package.Name == additionalPackageIDObj.Package.Name) &&
                (this.AdditionalPackageName == additionalPackageIDObj.AdditionalPackageName);
        }

        public override int GetHashCode() => base.GetHashCode();
        #endregion Method(s)
    }
    #endregion AdditionalPackageID
}
