﻿namespace DatabaseAccess.Entities
{
    #region PhoneNumber
    public class PhoneNumber
    {
        #region Properties
        public virtual PhoneNumberID ID { get; set; }
        public virtual int NumberOfMinutes { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PhoneNumber()
        {
            this.ID = new PhoneNumberID();
        }
        #endregion Constructor(s)
    }
    #endregion PhoneNumber

    #region PhoneNumberID
    public class PhoneNumberID
    {
        #region Properties
        public virtual Service Service { get; set; }
        public virtual string Number { get; set; }
        #endregion Properties

        #region Method(s)
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != typeof(PhoneNumberID))
            {
                return false;
            }

            var phoneNumberIDObj = obj as PhoneNumberID;

            return (this.Service.ServiceID == phoneNumberIDObj.Service.ServiceID) &&
                (this.Number == phoneNumberIDObj.Number);
        }

        public override int GetHashCode() => base.GetHashCode();
        #endregion Method(s)
    }
    #endregion PhoneNumberID
}
