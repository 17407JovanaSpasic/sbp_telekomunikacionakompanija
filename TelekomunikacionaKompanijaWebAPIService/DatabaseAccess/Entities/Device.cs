﻿using System;

namespace DatabaseAccess.Entities
{
    public class Device
    {
        #region Properties
        public virtual string SerialNumber { get; set; }
        public virtual DateTime StartOfUseDate { get; set; }
        public virtual DateTime? ServicingDate { get; set; }
        public virtual string ReasonForServicing { get; set; }

        public virtual Manufacturer Manufacturer { get; set; }
        #endregion Properties
    }
}
