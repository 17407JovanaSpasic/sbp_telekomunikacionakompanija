﻿namespace DatabaseAccess.Entities
{
    #region AdditionalChannelPackage
    public class AdditionalChannelPackage
    {
        #region Properties
        public virtual AdditionalChannelPackageID ID { get; set; }
        #endregion Properties

        #region Constructor(s)
        public AdditionalChannelPackage()
        {
            this.ID = new AdditionalChannelPackageID();
        }
        #endregion Constructor(s)
    }
    #endregion AdditionalChannelPackage

    #region AdditionalChannelPackageID
    public class AdditionalChannelPackageID
    {
        #region Properties
        public virtual Service Service { get; set; }
        public virtual string AdditionalChannelPackageName { get; set; }
        #endregion Properties

        #region Method(s)
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != typeof(AdditionalChannelPackageID))
            {
                return false;
            }

            var additionalChannelPackageIDObj = obj as AdditionalChannelPackageID;

            return (this.Service.ServiceID == additionalChannelPackageIDObj.Service.ServiceID) &&
                (this.AdditionalChannelPackageName == additionalChannelPackageIDObj.AdditionalChannelPackageName);
        }

        public override int GetHashCode() => base.GetHashCode();
        #endregion Method(s)
    }
    #endregion AdditionalChannelPackageID
}
