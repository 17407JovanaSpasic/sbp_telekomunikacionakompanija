﻿using FluentNHibernate.Mapping;

using DatabaseAccess.Entities;

namespace DatabaseAccess.Mappings
{
    public class ContractMapping : ClassMap<Contract>
    {
        #region Constructor(s)
        public ContractMapping()
        {
            Table("UGOVOR");

            Id(x => x.ContractId).Column("Broj_Ugovora").GeneratedBy.TriggerIdentity();

            Map(x => x.ContractStartDate, "Datum_Pocetka_Trajanja");
            Map(x => x.ContractEndDate, "Datum_Kraja_Trajanja");

            References(x => x.UserId).Column("ID_Korisnika");
            References(x => x.PackageName).Column("Naziv_Paketa");
            References(x => x.ServiceId).Column("ID_Usluge").Unique();

            HasMany(x => x.ServicePayment).KeyColumn("Broj_Ugovora").Cascade.All().Inverse();
        }
        #endregion Constructor(s)
    }
}
