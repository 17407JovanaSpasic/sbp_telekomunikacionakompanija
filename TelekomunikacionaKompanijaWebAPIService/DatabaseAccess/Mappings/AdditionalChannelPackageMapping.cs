﻿using FluentNHibernate.Mapping;

using DatabaseAccess.Entities;

namespace DatabaseAccess.Mappings
{
    public class AdditionalChannelPackageMapping : ClassMap<AdditionalChannelPackage>
    {
        #region Constructor(s)
        public AdditionalChannelPackageMapping()
        {
            this.Table("DODATNI_PAKET_KANALA");

            this.CompositeId(x => x.ID)
                .KeyReference(x => x.Service, "ID_Usluge")
                .KeyProperty(x => x.AdditionalChannelPackageName, "Dodatni_Paket_Kanala");
        }
        #endregion Constructor(s)
    }
}
