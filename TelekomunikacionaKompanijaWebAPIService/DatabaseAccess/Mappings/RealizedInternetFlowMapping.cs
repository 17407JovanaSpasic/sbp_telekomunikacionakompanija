﻿using FluentNHibernate.Mapping;

using DatabaseAccess.Entities;

namespace DatabaseAccess.Mappings
{
    public class RealizedInternetFlowMapping : SubclassMap<RealizedInternetFlow>
    {
        #region Constructor(s)
        public RealizedInternetFlowMapping()
        {
            Table("OSTVARENI_PROTOK_INTERNETA");

            KeyColumn("Broj_Racuna");

            Map(x => x.FlowAmount, "Kolicina");
            Map(x => x.CostPerFlow, "Cena_Po_Protoku");
        }
        #endregion Constructor(s)
    }
}
