﻿using DatabaseAccess.Entities;

using FluentNHibernate.Mapping;

namespace DatabaseAccess.Mappings
{
    public class DeviceMapping : ClassMap<Device>
    {
        #region Constructor(s)
        public DeviceMapping()
        {
            this.Table("UREDJAJ");

            this.Id(x => x.SerialNumber).Column("Serijski_Broj").GeneratedBy.Assigned();

            this.Map(x => x.StartOfUseDate).Column("Datum_Pocetka_Upotrebe");
            this.Map(x => x.ServicingDate).Column("Datum_Servisiranja");
            this.Map(x => x.ReasonForServicing).Column("Razlog_Servisiranja");

            this.References(x => x.Manufacturer).Column("PIB_Proizvodjaca").LazyLoad();
        }
        #endregion Constructor(s)
    }
}
