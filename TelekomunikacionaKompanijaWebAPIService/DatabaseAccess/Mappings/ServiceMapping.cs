﻿using FluentNHibernate.Mapping;

using DatabaseAccess.Entities;

namespace DatabaseAccess.Mappings
{
    public class ServiceMapping : ClassMap<Service>
    {
        #region Constructor(s)
        public ServiceMapping()
        {
            this.Table("USLUGA");

            this.Id(x => x.ServiceID).Column("ID_Usluge").GeneratedBy.TriggerIdentity();

            this.Map(x => x.ContainsTelephony).Column("PTelefonija");
            this.Map(x => x.ContainsTelevision).Column("PTelevizija");
            this.Map(x => x.ContainsInternet).Column("PInternet");
            this.Map(x => x.Address1).Column("Adresa1");
            this.Map(x => x.Address2).Column("Adresa2");
            this.Map(x => x.IsPrepaidInternet).Column("PPrepaid");
            this.Map(x => x.PaymentDate).Column("Datum_Uplate");
            this.Map(x => x.Balance).Column("Stanje");
            this.Map(x => x.IsPostpaidInternet).Column("PPostpaid");

            this.HasMany(x => x.PhoneNumbers).KeyColumn("ID_Usluge").Inverse().Cascade.All();
            this.HasMany(x => x.AdditionalChannelPackages).KeyColumn("ID_Usluge").Inverse().Cascade.All();

            this.HasOne(x => x.Contract).PropertyRef(x => x.ServiceId);
        }
        #endregion Constructor(s)
    }
}
