﻿using FluentNHibernate.Mapping;

using DatabaseAccess.Entities;

namespace DatabaseAccess.Mappings
{
    public class PaymentMapping : ClassMap<Payment>
    {
        #region Constructor(s)
        public PaymentMapping()
        {
            Table("PLACANJE");

            Id(x => x.PaymentNumber, "Broj_Racuna").GeneratedBy.TriggerIdentity();

            Map(x => x.PaymentDate, "Datum_Placanja");
            Map(x => x.AdditionalPackagePrice, "Cena_Dodatnih_Paketa");
            Map(x => x.TotalCost, "Ukupna_Cena");

            References(x => x.ContractNumber).Column("Broj_Ugovora");
        }
        #endregion Constructor(s)
    }
}
