﻿using FluentNHibernate.Mapping;

using DatabaseAccess.Entities;

namespace DatabaseAccess.Mappings
{
    public class PhoneNumberMapping : ClassMap<PhoneNumber>
    {
        #region Constructor(s)
        public PhoneNumberMapping()
        {
            this.Table("BROJ_TELEFONA");

            this.CompositeId(x => x.ID)
                .KeyReference(x => x.Service, "ID_Usluge")
                .KeyProperty(x => x.Number, "Broj");

            this.Map(x => x.NumberOfMinutes, "Broj_Ostvarenih_Minuta");
        }
        #endregion Constructor(s)
    }
}
