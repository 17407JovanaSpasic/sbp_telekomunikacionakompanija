﻿using FluentNHibernate.Mapping;

using DatabaseAccess.Entities;

namespace DatabaseAccess.Mappings
{
    public class UserMapping : ClassMap<User>
    {
        #region Constructor(s)
        public UserMapping()
        {
            Table("KORISNIK");

            Id(x => x.Id, "ID_Korisnika").GeneratedBy.TriggerIdentity();

            Map(x => x.Name, "Ime");
            Map(x => x.LastName, "Prezime");
            Map(x => x.Address, "Adresa");
            Map(x => x.AddressNumber, "Broj");
            Map(x => x.City, "Grad");
            Map(x => x.ContactPhone, "Kontakt_Telefon");
            Map(x => x.Email, "Email");

            References(x => x.NodeSerialNumber).Column("Serijski_Broj_Cvora");

            HasMany(x => x.UsersContracts).KeyColumn("ID_Korisnika").Cascade.All().Inverse();

            Map(x => x.Individual, "PFizicko_Lice");
            Map(x => x.IndividualJMBG, "JMBG");
            Map(x => x.LegalEntity, "PPravno_Lice");
            Map(x => x.LegalEntityFaxNumber, "Broj_Faksa");
            Map(x => x.LegalEntityPIB, "PIB");
        }
        #endregion Constructor(s)
    }
}
