﻿using DatabaseAccess.Entities;

using FluentNHibernate.Mapping;

namespace DatabaseAccess.Mappings
{
    public class CommunicationNodeMapping : SubclassMap<CommunicationNode>
    {
        #region Constructor(s)
        public CommunicationNodeMapping()
        {
            this.Table("KOMUNIKACIONI_CVOR");

            this.KeyColumn("Serijski_Broj");

            this.Map(x => x.Address).Column("Adresa");
            this.Map(x => x.LocationNumber).Column("Broj_Lokacije");
            this.Map(x => x.Description).Column("Opis");

            this.References(x => x.MainStation).Column("Serijski_Broj_Glavne_Stanice").LazyLoad();
            this.HasMany(x => x.Users).KeyColumn("Serijski_Broj_Cvora").Inverse().Cascade.All();
        }
        #endregion Constructor(s)
    }
}
