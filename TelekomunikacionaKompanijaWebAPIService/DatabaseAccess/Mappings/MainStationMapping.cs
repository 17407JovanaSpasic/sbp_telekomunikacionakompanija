﻿using DatabaseAccess.Entities;

using FluentNHibernate.Mapping;

namespace DatabaseAccess.Mappings
{
    public class MainStationMapping : SubclassMap<MainStation>
    {
        #region Constructor(s)
        public MainStationMapping()
        {
            this.Table("GLAVNA_STANICA");

            this.KeyColumn("Serijski_Broj");

            this.Map(x => x.NumberOfNodes).Column("Broj_Cvorova");
            this.Map(x => x.IsRegionalHub).Column("PRegionalni_Hub");
            this.Map(x => x.NameOfRegion).Column("Naziv_Regiona");

            this.HasMany(x => x.CommunicationNodes).KeyColumn("Serijski_Broj_Glavne_Stanice")
                .Inverse().Cascade.All();
        }
        #endregion Constructor(s)
    }
}
