﻿using FluentNHibernate.Mapping;

using DatabaseAccess.Entities;

namespace DatabaseAccess.Mappings
{
    public class ChannelMapping : ClassMap<Channel>
    {
        #region Constructor(s)
        public ChannelMapping()
        {
            this.Table("KANAL");

            this.CompositeId(x => x.ID)
                .KeyReference(x => x.Package, "Naziv_Paketa")
                .KeyProperty(x => x.ChannelName, "Kanal");
        }
        #endregion Constructor(s)
    }
}
