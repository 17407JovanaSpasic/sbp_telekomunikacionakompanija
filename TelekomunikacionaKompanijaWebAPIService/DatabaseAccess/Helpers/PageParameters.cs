﻿namespace DatabaseAccess.Helpers
{
    public class PageParameters
    {
        #region Properties
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }
        #endregion Properties

        #region Constructor(s)
        public PageParameters() { }

        public PageParameters(int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }
        #endregion Constructor(s)
    }
}
