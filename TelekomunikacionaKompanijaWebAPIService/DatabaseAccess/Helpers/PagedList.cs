﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseAccess.Helpers
{
    public class PagedList<T> : List<T>
    {
        #region Properties
        public int CurrentPage { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }
        #endregion Properties

        #region Constructor(s)
        public PagedList(int currentPage, int pageSize, int totalCount)
        {
            this.CurrentPage = currentPage;
            this.PageSize = pageSize;
            this.TotalCount = totalCount;
        }

        public PagedList(int currentPage, int pageSize, int totalCount, List<T> records)
        {
            this.CurrentPage = currentPage;
            this.TotalPages = Convert.ToInt32(Math.Ceiling(totalCount / (double)pageSize));
            this.PageSize = pageSize;
            this.TotalCount = totalCount;

            this.AddRange(records);
        }
        #endregion Constructor(s)

        #region Method(s)
        public static PagedList<T> ToPagedList(IQueryable<T> source, PageParameters parameters)
        {
            int count = source.Count();

            var records = source.Skip((parameters.PageNumber - 1) * parameters.PageSize)
                .Take(parameters.PageSize).ToList();

            return new PagedList<T>(parameters.PageNumber, parameters.PageSize, count, records);
        }
        #endregion Method(s)
    }
}
