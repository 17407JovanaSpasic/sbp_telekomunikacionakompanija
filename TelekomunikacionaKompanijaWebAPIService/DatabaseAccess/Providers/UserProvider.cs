﻿using System;
using System.Collections.Generic;
using System.Linq;

using DatabaseAccess.Entities;
using DatabaseAccess.Views.Contract;
using DatabaseAccess.Views.Contract.Service;
using DatabaseAccess.Views.Package;
using DatabaseAccess.Views.User;

using NHibernate;

namespace DatabaseAccess.Providers
{
    public class UserProvider
    {
        #region Mutual Method(s)
        public static bool AddContractToTheUser(int userId, string contractId)
        {
            try
            {
                var s = DataLayer.GetSession();

                var contract = s.Get<Entities.Contract>(contractId);
                var user = s.Get<Entities.User>(userId);

                if (contract == null || user == null)
                    return false;

                user.UsersContracts.Add(contract);

                s.SaveOrUpdate(user);
                s.Flush();
                s.Close();
                return true;
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in ContractProvider(AddPaymentToTheContract). \n" + ex.Message); return false; }
        }

        public static bool CheckIfExists(string contactPhone)
        {
            try
            {
                var session = DataLayer.GetSession();

                IQuery que = session.CreateQuery("select u from User u where u.ContactPhone = " + "'" + contactPhone + "'");
                var user = que.UniqueResult<User>();

                if (user == null)
                    return false;
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(CheckIfExists). \n" + ex.Message); return false; }

            return true;
        }

        public static bool CheckIfUserExists(string id)
        {
            try
            {
                var session = DataLayer.GetSession();

                IQuery que = session.CreateQuery("select u from User u where u.Id = " + "'" + id + "'");
                var user = que.UniqueResult<User>();

                if (user == null)
                    return false;
                else
                    return true;
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(CheckIfExists). \n" + ex.Message); return false; }
        }

        public static void AddContract(int id, Entities.Contract con)
        {
            try
            {
                var s = DataLayer.GetSession();

                var user = s.Get<User>(id);
                user.UsersContracts.Add(con);

                s.SaveOrUpdate(user);
                s.Flush();
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(AddContract). \n" + ex.Message); }
        }

        public static bool DeleteUser(int userId)
        {
            try
            {
                var session = DataLayer.GetSession();
                var user = session.Get<User>(userId);
                session.Delete(user);
                session.Flush();
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(DeleteUser). \n" + ex.Message); return false; }
            return true;
        }



        public static void GetUserByContactPhoneForPayment(string phoneNumber, string packageName, ref UserView obj, ref ContractView contractObj)
        {
            try
            {
                var session = DataLayer.GetSession();

                IQuery que = session.CreateQuery("select u from User u where u.ContactPhone = " + "'" + phoneNumber + "'");

                var user = que.UniqueResult<User>();

                obj.Id = user.Id;
                obj.Name = user.Name;
                obj.LastName = user.LastName;
                obj.Address = user.Address;
                obj.AddressNumber = user.AddressNumber;
                obj.City = user.City;
                obj.ContactPhone = user.ContactPhone;
                obj.Email = user.Email;

                foreach (var p in user.UsersContracts)
                {
                    if (p.PackageName.Name == packageName || packageName == "")
                    {
                        var package = new PackageView(p.PackageName.Name, p.PackageName.Price, p.PackageName.AdditionalPackagePrice,
                                                     p.PackageName.InternetSpeed, p.PackageName.PricePerMB, p.PackageName.PricePerAddress,
                                                     p.PackageName.NumberOfMinutes);

                        var service = new ServiceView(p.ServiceId.ServiceID);

                        contractObj = new ContractView(p.ContractId, p.ContractStartDate, p.ContractEndDate, obj, package, service);
                    }
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(GetInformationUsersByContactPhoneForPayment). \n" + ex.Message); }
        }

        public static void GetUserByContactPhoneForContract(string phoneNumber, ref int userId, ref UserView obj)
        {
            try
            {
                var session = DataLayer.GetSession();

                IQuery que = session.CreateQuery("select u from User u where u.ContactPhone = " + "'" + phoneNumber + "'");

                var user = que.UniqueResult<User>();

                obj.Id = user.Id;
                userId = user.Id;
                obj.Name = user.Name;
                obj.LastName = user.LastName;
                obj.Address = user.Address;
                obj.AddressNumber = user.AddressNumber;
                obj.City = user.City;
                obj.ContactPhone = user.ContactPhone;
                obj.Email = user.Email;

                ContractView contractObj;
                foreach (var p in user.UsersContracts)
                {
                    var package = new PackageView(p.PackageName.Name, p.PackageName.Price, p.PackageName.AdditionalPackagePrice,
                                                 p.PackageName.InternetSpeed, p.PackageName.PricePerMB, p.PackageName.PricePerAddress,
                                                 p.PackageName.NumberOfMinutes);

                    var service = new ServiceView(p.ServiceId.ServiceID);

                    contractObj = new ContractView(p.ContractId, p.ContractStartDate, p.ContractEndDate, obj, package, service);

                    obj.UsersContracts.Add(new ContractView(p.ContractId, p.ContractStartDate, p.ContractEndDate, obj, package, service));
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(GetInformationUsersByContactPhoneForPayment). \n" + ex.Message); }
        }

        public static UserView GetUserByID(int userID)
        {
            var user = new UserView();

            try
            {
                var session = DataLayer.GetSession();

                var userEntity = session.Query<User>().Where(x => x.Id == userID).FirstOrDefault();
                if (userEntity == null)
                    return null;

                if (userEntity.Individual == 1)
                {
                    user = new IndividualView(userEntity.Id, userEntity.Name, userEntity.LastName,
                        userEntity.Address, userEntity.AddressNumber, userEntity.City,
                        userEntity.ContactPhone, userEntity.Email, userEntity.IndividualJMBG);
                }
                else
                {
                    user = new LegalEntityView(userEntity.Id, userEntity.Name, userEntity.LastName,
                        userEntity.Address, userEntity.AddressNumber, userEntity.City,
                        userEntity.ContactPhone, userEntity.Email, userEntity.LegalEntityFaxNumber,
                        userEntity.LegalEntityPIB);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return user;
        }
        #endregion Mutual Method(s)

        #region Individual Method(s)
        public static List<IndividualView> GetAllIndividualUsers()
        {
            List<IndividualView> listOfUsers = new List<IndividualView>();
            try
            {
                ISession s = DataLayer.GetSession();

                var users = (from u in s.Query<User>()
                             where (u.Individual == 1)
                             select u);

                foreach (var u in users)
                {
                    var listOfContracts = new List<ContractBriefView>();
                    foreach (var c in u.UsersContracts)
                    {
                        listOfContracts.Add(new ContractBriefView(c.ContractId, c.ContractStartDate, c.ContractEndDate));
                    }
                    listOfUsers.Add(new IndividualView(u.Id, u.Name, u.LastName, u.Address, u.AddressNumber,
                        u.City, u.ContactPhone, u.Email, u.IndividualJMBG, u.NodeSerialNumber.SerialNumber, listOfContracts));
                }
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(GetAllIndividualUsers). \n" + ex.Message); }

            return listOfUsers;
        }

        public static bool CreateIndividualUser(IndividualUserCreationView obj)
        {
            try
            {
                var s = DataLayer.GetSession();

                var IndividualEntity = new User();
                IndividualEntity.Name = obj.Name;
                IndividualEntity.LastName = obj.LastName;
                IndividualEntity.Address = obj.Address;
                IndividualEntity.AddressNumber = obj.AddressNumber;
                IndividualEntity.City = obj.City;
                IndividualEntity.ContactPhone = obj.ContactPhone;
                IndividualEntity.Email = obj.Email;

                IndividualEntity.Individual = 1;
                IndividualEntity.LegalEntity = 0;
                IndividualEntity.IndividualJMBG = obj.IndividualJMBG;

                int randomIndex = new Random()
                    .Next(0, DeviceProvider.GetNumberOfCommunicationNodes() - 1);
                string nodeSerialNumber = DeviceProvider.GetRandomCommunicationNodeSerialNumber(randomIndex);

                var ComChanel = s.Get<CommunicationNode>(nodeSerialNumber);
                IndividualEntity.NodeSerialNumber = ComChanel;

                s.SaveOrUpdate(IndividualEntity);
                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error has occured in UserDTOManager(CreateIndividualUser). \n" + ex.Message);
                return false;
            }
            return true;
        }

        public static bool UpdateIndividualUser(IndividualBriefView obj)
        {
            try
            {
                var session = DataLayer.GetSession();
                var user = session.Get<Entities.User>(obj.Id);
                if (user == null || user.LegalEntity == 1)
                    return false;

                user.Address = obj.Address;
                user.AddressNumber = obj.AddressNumber;
                user.City = obj.City;
                user.ContactPhone = obj.ContactPhone;
                user.Email = obj.Email;

                session.SaveOrUpdate(user);
                session.Flush();
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(UpdateUsers). \n" + ex.Message); return false; }
            return true;
        }
        #endregion Individual Method(s)

        #region Legal Entity Method(s)
        public static bool UpdateLegalEntity(LegalEntityBriefView obj)
        {
            try
            {
                var session = DataLayer.GetSession();
                var user = session.Get<Entities.User>(obj.Id);
                if (user == null || user.Individual == 1)
                    return false;

                user.Address = obj.Address;
                user.AddressNumber = obj.AddressNumber;
                user.City = obj.City;
                user.ContactPhone = obj.ContactPhone;
                user.Email = obj.Email;
                user.LegalEntityFaxNumber = obj.LegalEntityFaxNumber;
                user.LegalEntityPIB = obj.LegalEntityPIB;

                session.SaveOrUpdate(user);
                session.Flush();
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(UpdateUsers). \n" + ex.Message); return false; }
            return true;
        }
        public static List<LegalEntityView> GetAllLegalEntityUsers()
        {
            List<LegalEntityView> listOfUsers = new List<LegalEntityView>();
            try
            {
                ISession s = DataLayer.GetSession();

                var users = (from u in s.Query<User>()
                             where (u.Individual == 0)
                             select u);

                foreach (var u in users)
                {
                    var listOfContracts = new List<ContractBriefView>();
                    foreach (var c in u.UsersContracts)
                    {
                        listOfContracts.Add(new ContractBriefView(c.ContractId, c.ContractStartDate, c.ContractEndDate));
                    }
                    listOfUsers.Add(new LegalEntityView(u.Id, u.Name, u.LastName, u.Address, u.AddressNumber,
                        u.City, u.ContactPhone, u.Email, u.LegalEntityFaxNumber, u.LegalEntityPIB, u.NodeSerialNumber.SerialNumber, listOfContracts));
                }
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in UserDTOManager(GetAllLegalEntityUsers). \n" + ex.Message); }

            return listOfUsers;
        }

        public static bool CreateLegalEntityUser(LegalEntityUserCreationView obj)
        {
            try
            {
                var s = DataLayer.GetSession();

                var legalEntity = new User();
                legalEntity.Name = obj.Name;
                legalEntity.LastName = obj.LastName;
                legalEntity.Address = obj.Address;
                legalEntity.AddressNumber = obj.AddressNumber;
                legalEntity.City = obj.City;
                legalEntity.ContactPhone = obj.ContactPhone;
                legalEntity.Email = obj.Email;

                legalEntity.LegalEntity = 1;
                legalEntity.Individual = 0;
                legalEntity.LegalEntityFaxNumber = obj.LegalEntityFaxNumber;
                legalEntity.LegalEntityPIB = obj.LegalEntityPIB;

                int randomIndex = new Random()
                    .Next(0, DeviceProvider.GetNumberOfCommunicationNodes() - 1);
                string nodeSerialNumber = DeviceProvider.GetRandomCommunicationNodeSerialNumber(randomIndex);

                var ComChanel = s.Get<CommunicationNode>(nodeSerialNumber);

                legalEntity.NodeSerialNumber = ComChanel;

                s.SaveOrUpdate(legalEntity);
                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error has occured in UserDTOManager(CreateLegalEntityUser). \n" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion Legal Entity Method(s)
    }
}
