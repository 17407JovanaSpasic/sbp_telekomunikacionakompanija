﻿using NHibernate;
using System;
using System.Linq;
using DatabaseAccess.Views.Contract;
using DatabaseAccess.Views.Contract.Service;
using DatabaseAccess.Views.Package;
using DatabaseAccess.Views.User;
using DatabaseAccess.Entities;
using System.Collections.Generic;

namespace DatabaseAccess.Providers.Contract
{
    public class ContractProvider
    {
        #region Contract Method(s)
        public static void AddPaymentToTheContract(string contractId,int idPayment)
        {
            try 
            {
                var s = DataLayer.GetSession();

                var contract = s.Get<Entities.Contract>(contractId);
                var payment = s.Get<Entities.Payment>(idPayment);

                if (contract == null || payment == null)
                    return;

                contract.ServicePayment.Add(payment);

                s.SaveOrUpdate(contract);
                s.Flush();
                s.Close();
            }
            catch(Exception ex) { Console.WriteLine("Error has occured in ContractProvider(AddPaymentToTheContract). \n" + ex.Message); }
        }

        public static bool CreateContract(ContractView con)
        {
            try
            {
                if (con == null)
                    return false;
                var s = DataLayer.GetSession();

                var newContract = new Entities.Contract();
                newContract.ContractStartDate = con.ContractStartDate;
                newContract.ContractEndDate = con.ContractEndDate;
                var newUser = s.Get<Entities.User>(con.UserId.Id);
                var package = s.Get<Entities.Package>(con.PackageName.Name);
                var service = s.Get<Entities.Service>(con.ServiceId.ServiceID);

                newContract.UserId = newUser;
                newContract.PackageName = package;
                newContract.ServiceId = service;

                s.SaveOrUpdate(newContract);
                s.Flush();
                s.Close();
                string id = newContract.ContractId;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error has occured in ContractProvider(CreateContract). \n" + ex.Message);
                return false;
            }
        }

        public static bool UpdateContract(ContractBriefView con)
        {
            try
            {
                var s = DataLayer.GetSession();
                var contract = s.Get<Entities.Contract>(con.ContractId);
                if (contract == null)
                    return false;

                contract.ContractStartDate = con.ContractStartDate;
                contract.ContractEndDate = con.ContractEndDate;

                s.SaveOrUpdate(contract);
                s.Flush();
                s.Close();

                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error has occured in ContractProvider(UpdateContract). \n " + ex.Message);
                return false;               
            }
        }

        public static bool DeleteContract(string contractId)
        {
            try
            {
                var s = DataLayer.GetSession();
                var contract = s.Get<Entities.Contract>(contractId);
                if (contract == null)
                    return false;

                s.Delete(contract);
                s.Flush();
                s.Close();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error has occured in ContractProvider(UpdateContract). \n " + ex.Message);
                return false;
            }
        }

        public static List<ContractView> GetAllContracts()
        {
            var listOfContracts = new List<ContractView>();
            try
            {
                var s = DataLayer.GetSession();
                var contracts = (from c in s.Query<Entities.Contract>() select c);

                foreach(var c in contracts)
                {
                    var user = new UserView(c.UserId.Id, c.UserId.Name, c.UserId.LastName, c.UserId.Address, c.UserId.AddressNumber,
                        c.UserId.City, c.UserId.ContactPhone, c.UserId.Email, c.UserId.Individual, 
                        c.UserId.LegalEntity, c.UserId.NodeSerialNumber.SerialNumber, null);

                    var package = new PackageView(c.PackageName.Name, c.PackageName.Price, c.PackageName.AdditionalPackagePrice, c.PackageName.InternetSpeed, 
                        c.PackageName.PricePerMB, c.PackageName.PricePerAddress, c.PackageName.NumberOfMinutes);

                    var service = new ServiceView(c.ServiceId.ServiceID, c.ServiceId.ContainsTelephony, c.ServiceId.ContainsTelevision, c.ServiceId.ContainsInternet);

                    listOfContracts.Add(new ContractView(c.ContractId, c.ContractStartDate, c.ContractEndDate, user, package, service));
                }
                s.Close();
                return listOfContracts;
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public static Entities.Contract GetContractById(string id)
        {
            try
            {
                var s = DataLayer.GetSession();
                var contract = s.Get<Entities.Contract>(id);
                s.Close();
                return contract;
            }

            catch(Exception ex) 
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        #endregion Contract Method(s)
    }
}
