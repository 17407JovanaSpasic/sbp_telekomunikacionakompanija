﻿using System;
using System.Collections.Generic;
using System.Linq;

using DatabaseAccess.Entities;
using DatabaseAccess.Helpers;
using DatabaseAccess.Views.Contract.Service;
using DatabaseAccess.Views.Contract.Service.Internet;
using DatabaseAccess.Views.Contract.Service.Telephony;
using DatabaseAccess.Views.Contract.Service.Television;

namespace DatabaseAccess.Providers.Contract
{
    public class ServiceProvider
    {
        #region Service Method(s)
        public static void CreateService(BasicServiceView service)
        {
            var serviceEntity = new Service
            {
                ContainsTelephony = service.ContainsTelephony,
                ContainsTelevision = service.ContainsTelevision,
                ContainsInternet = service.ContainsInternet
            };

            if (service.ContainsInternet)
            {
                serviceEntity.Address1 = service.Internet.Address1;
                serviceEntity.Address2 = service.Internet.Address2;
                serviceEntity.IsPrepaidInternet = service.Internet.IsInternetPrepaid;
                serviceEntity.PaymentDate = null;
                serviceEntity.IsPostpaidInternet = !service.Internet.IsInternetPrepaid;
            }

            try
            {
                var session = DataLayer.GetSession();

                session.Save(serviceEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static PagedList<ServiceView> GetAllServices(PageParameters parameters)
        {
            PagedList<ServiceView> servicesList;
            try
            {
                var session = DataLayer.GetSession();

                var serviceEntities = session.Query<Service>();

                var records = PagedList<Service>.ToPagedList(serviceEntities, parameters);

                servicesList = new PagedList<ServiceView>(parameters.PageNumber,
                    parameters.PageSize, records.TotalCount);

                foreach (var record in records)
                {
                    var service = new ServiceView(record.ServiceID, record.ContainsTelephony,
                        record.ContainsTelevision, record.ContainsInternet);

                    foreach (var phoneNumber in record.PhoneNumbers)
                    {
                        service.PhoneNumbers.Add(new PhoneNumberView(phoneNumber.ID.Number,
                            phoneNumber.NumberOfMinutes));
                    }

                    foreach (var additionalChannelPackage in record.AdditionalChannelPackages)
                    {
                        service.AdditionalChannelPackages.Add(new AdditionalChannelPackageView(
                            additionalChannelPackage.ID.AdditionalChannelPackageName));
                    }

                    if (record.ContainsInternet)
                    {
                        service.Internet = new InternetView(record.Address1, record.Address2,
                            record.IsPrepaidInternet, record.PaymentDate, record.Balance,
                            record.IsPostpaidInternet);
                    }

                    servicesList.Add(service);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return servicesList;
        }

        public static ServiceView GetServiceByID(int serviceID)
        {
            var service = new ServiceView(serviceID);

            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Get<Service>(serviceID);

                if (serviceEntity is null)
                {
                    throw new Exception("Service does not exist.");
                }

                service.ContainsTelephony = serviceEntity.ContainsTelephony;
                service.ContainsTelevision = serviceEntity.ContainsTelevision;
                service.ContainsInternet = serviceEntity.ContainsInternet;

                foreach (var phoneNumberEntity in serviceEntity.PhoneNumbers)
                {
                    var phoneNumber = new PhoneNumberView(phoneNumberEntity.ID.Number,
                        phoneNumberEntity.NumberOfMinutes);

                    service.PhoneNumbers.Add(phoneNumber);
                }

                foreach (var additionalChannelPackageEntity in serviceEntity.AdditionalChannelPackages)
                {
                    var additionalChannelPackage = new AdditionalChannelPackageView(
                        additionalChannelPackageEntity.ID.AdditionalChannelPackageName);

                    service.AdditionalChannelPackages.Add(additionalChannelPackage);
                }

                if (serviceEntity.ContainsInternet)
                {
                    service.Internet = new InternetView(serviceEntity.Address1, serviceEntity.Address2,
                        serviceEntity.IsPrepaidInternet, serviceEntity.PaymentDate, serviceEntity.Balance,
                        serviceEntity.IsPostpaidInternet);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return service;
        }

        public static void UpdatePrepaidInternetServiceInfo(int serviceID, PrepaidInternetView internet)
        {
            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Get<Service>(serviceID);

                if (serviceEntity is null)
                {
                    throw new Exception("Service does not exist.");
                }

                if (!serviceEntity.ContainsInternet)
                {
                    throw new Exception("Service does not contain internet.");
                }

                if (!serviceEntity.IsPrepaidInternet)
                {
                    throw new Exception("Internet is not prepaid.");
                }

                serviceEntity.Address1 = internet.Address1;
                serviceEntity.Address2 = internet.Address2;
                serviceEntity.PaymentDate = internet.PaymentDate;
                serviceEntity.Balance = internet.Balance;

                session.Update(serviceEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void UpdatePostpaidInternetServiceInfo(int serviceID, PostpaidInternetView internet)
        {
            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Get<Service>(serviceID);

                if (serviceEntity is null)
                {
                    throw new Exception("Service does not exist.");
                }

                if (!serviceEntity.ContainsInternet)
                {
                    throw new Exception("Service does not contain internet.");
                }

                if (!serviceEntity.IsPostpaidInternet)
                {
                    throw new Exception("Internet is not postpaid.");
                }

                serviceEntity.Address1 = internet.Address1;
                serviceEntity.Address2 = internet.Address2;

                session.Update(serviceEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void DeleteService(int serviceID)
        {
            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Get<Service>(serviceID);

                if (serviceEntity is null)
                {
                    throw new Exception("Service does not exist.");
                }

                session.Delete(serviceEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }
        #endregion Service Method(s)

        #region Telephony Service Method(s)
        public static void CreatePhoneNumber(ServicePhoneNumberView phoneNumber)
        {
            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Get<Service>(phoneNumber.ServiceID);

                if (serviceEntity is null)
                {
                    throw new Exception("Service does not exist.");
                }

                if (serviceEntity.PhoneNumbers.Count == 4)
                {
                    throw new Exception("Maximum phone numbers for service is 4.");
                }

                var phoneNumberEntity = new PhoneNumber
                {
                    ID = new PhoneNumberID
                    {
                        Service = serviceEntity,
                        Number = phoneNumber.Number
                    }
                };

                session.Save(phoneNumberEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static List<PhoneNumberView> GetPhoneNumbersByServiceID(int serviceID)
        {
            var phoneNumbersList = new List<PhoneNumberView>();

            try
            {
                var session = DataLayer.GetSession();

                var phoneNumberEntities = session.Query<PhoneNumber>()
                    .Where(x => x.ID.Service.ServiceID == serviceID).ToList();

                foreach (var phoneNumberEntity in phoneNumberEntities)
                {
                    var phoneNumber = new PhoneNumberView(phoneNumberEntity.ID.Number,
                        phoneNumberEntity.NumberOfMinutes);

                    phoneNumbersList.Add(phoneNumber);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return phoneNumbersList;
        }

        public static void UpdateNumberOfMinutes(PhoneNumberView phoneNumber)
        {
            try
            {
                var session = DataLayer.GetSession();

                var phoneNumberEntity = session.Query<PhoneNumber>()
                    .Where(x => x.ID.Number == phoneNumber.PhoneNumber).FirstOrDefault();

                if (phoneNumberEntity is null)
                {
                    throw new Exception("Phone number does not exist.");
                }

                phoneNumberEntity.NumberOfMinutes = phoneNumber.NumberOfMinutes;

                session.Update(phoneNumberEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void DeletePhoneNumber(string phoneNumber)
        {
            try
            {
                var session = DataLayer.GetSession();

                var phoneNumberEntity = session.Query<PhoneNumber>()
                    .Where(x => x.ID.Number == phoneNumber).FirstOrDefault();

                if (phoneNumberEntity is null)
                {
                    throw new Exception("Phone number does not exist.");
                }

                session.Delete(phoneNumberEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }
        #endregion Telephony Service Method(s)

        #region Television Service Method(s)
        public static void CreateAdditionalChannelPackage(ServiceAdditionalChannelPackageView
            additionalChannelPackage)
        {
            try
            {
                var session = DataLayer.GetSession();

                var serviceEntity = session.Get<Service>(additionalChannelPackage.ServiceID);

                if (serviceEntity is null)
                {
                    throw new Exception("Service does not exist.");
                }

                var additionalChannelPackageEntity = new AdditionalChannelPackage
                {
                    ID = new AdditionalChannelPackageID
                    {
                        Service = serviceEntity,
                        AdditionalChannelPackageName = additionalChannelPackage.Name
                    }
                };

                session.Save(additionalChannelPackageEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static List<AdditionalChannelPackageView>
            GetAdditionalChannelPackagesByServiceID(int serviceID)
        {
            var additionalChannelPackagesList = new List<AdditionalChannelPackageView>();

            try
            {
                var session = DataLayer.GetSession();

                var additionalChannelPackageEntities = session.Query<AdditionalChannelPackage>()
                    .Where(x => x.ID.Service.ServiceID == serviceID).ToList();

                foreach (var additionalChannelPackageEntity in additionalChannelPackageEntities)
                {
                    var additionalChannelPackage =
                        new AdditionalChannelPackageView(additionalChannelPackageEntity
                            .ID.AdditionalChannelPackageName);

                    additionalChannelPackagesList.Add(additionalChannelPackage);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return additionalChannelPackagesList;
        }

        public static void UpdateAdditionalChannelPackageFromService(
            UpdateAdditionalChannelPackageView additionalChannelPackage)
        {
            try
            {
                var session = DataLayer.GetSession();

                var additionalChannelPackageEntity = session.Query<AdditionalChannelPackage>()
                    .Where(x => x.ID.Service.ServiceID == additionalChannelPackage.ServiceID &&
                    x.ID.AdditionalChannelPackageName == additionalChannelPackage.Name)
                    .FirstOrDefault();

                if (additionalChannelPackageEntity is null)
                {
                    throw new Exception("Additional channel package does not exist.");
                }

                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        var updatedAdditionalChannelPackageEntity = new AdditionalChannelPackage
                        {
                            ID = new AdditionalChannelPackageID
                            {
                                Service = additionalChannelPackageEntity.ID.Service,
                                AdditionalChannelPackageName = additionalChannelPackage.NewName
                            }
                        };

                        session.Delete(additionalChannelPackageEntity);
                        session.Save(updatedAdditionalChannelPackageEntity);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);

                        transaction.Rollback();

                        throw;
                    }
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void DeleteAdditionalChannelPackageFromService(
            ServiceAdditionalChannelPackageView additionalChannelPackage)
        {
            try
            {
                var session = DataLayer.GetSession();

                var additionalChannelPackageEntity = session.Query<AdditionalChannelPackage>()
                    .Where(x => x.ID.Service.ServiceID == additionalChannelPackage.ServiceID &&
                    x.ID.AdditionalChannelPackageName == additionalChannelPackage.Name)
                    .FirstOrDefault();

                if (additionalChannelPackageEntity is null)
                {
                    throw new Exception("Additional channel package does not exist.");
                }

                session.Delete(additionalChannelPackageEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }
        #endregion Television Service Method(s)
    }
}
