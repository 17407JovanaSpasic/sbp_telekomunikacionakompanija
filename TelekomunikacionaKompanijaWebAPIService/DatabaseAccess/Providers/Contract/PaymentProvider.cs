﻿using System;
using System.Collections.Generic;
using System.Linq;

using NHibernate;

using DatabaseAccess.Views.Contract.Payment;
using DatabaseAccess.Entities;

namespace DatabaseAccess.Providers.Contract
{
    public class PaymentProvider
    {
        #region Payment Method(s)
        public static List<PaymentView> GetAllPayments()
        {
            List<PaymentView> listOfAllpayments = new List<PaymentView>();
            try
            {
                var session = DataLayer.GetSession();
                var payments = (from p in session.Query<Payment>() select p);

                foreach (var p in payments)
                {
                    IQuery q = session.CreateQuery("select u.Name, u.LastName, u.ContactPhone from User u " +
                                                    "where u.Id like " + p.ContractNumber.UserId.Id);
                    IList<object[]> results = q.List<object[]>();

                    foreach (object[] o in results)
                    {
                        string ime = (string)o[0];
                        string prezime = (string)o[1];
                        string kontaktTelefon = (string)o[2];

                        string brojUgovora = p.ContractNumber.ContractId;
                        string nazivPakte = p.ContractNumber.PackageName.Name;
                        listOfAllpayments.Add(new PaymentView(p.PaymentNumber, p.PaymentDate, p.AdditionalPackagePrice, p.TotalCost,
                        ime, prezime, kontaktTelefon, brojUgovora, nazivPakte));
                    }
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PayementDTOManger(GetAllPayments). \n" + ex.Message); }
            return listOfAllpayments;
        }
        public static bool UpdatePayment(PaymentBriefView payment)
        {
            try
            {
                var s = DataLayer.GetSession();
                var p = s.Get<Payment>(payment.PaymentNumber);
                if (payment == null)
                    return false;
               
                p.AdditionalPackagePrice = payment.AdditionalPackagePrice;
                p.PaymentDate = payment.PaymentDate;
                p.TotalCost = payment.TotalCost;

                s.SaveOrUpdate(p);
                s.Flush();
                s.Close();
            }
            catch(Exception ex) { Console.WriteLine("Error has occured in PaymentProvider(UpadteFlatRateInternet)" + ex.Message); return false; }
            return true;
        }

        public static bool CreatePayment(PaymentCreationView payment)
        {
            try
            {
                var s = DataLayer.GetSession();
                var contract = s.Get<Entities.Contract>(payment.ContractNumber);
                if (contract == null)
                    return false;

                var p = new Payment();

                p.AdditionalPackagePrice = payment.AdditionalPackagePrice;
                p.PaymentDate = payment.PaymentDate;
                p.TotalCost = payment.TotalCost;

                var con = ContractProvider.GetContractById(contract.ContractId);
                p.ContractNumber = con;

                s.SaveOrUpdate(p);
                s.Flush();
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PaymentProvider(UpadteFlatRateInternet)" + ex.Message); return false; }
            return true;
        }
        public static bool CreatePaymentWithInternet(FlatRateCreationView flateRateIObj, RealizedInternetCreationView realizedInternetFlowObj)
        {
            string idContract;
            int idPayment;
            try
            {
                var s = DataLayer.GetSession();
         
                if (flateRateIObj != null)
                {
                    var contract = s.Get<Entities.Contract>(flateRateIObj.ContractNumber);
                    if (contract == null)
                        return false;

                    if (contract.ServiceId.IsPostpaidInternet == false)
                    {
                        return false;
                    }
                    var newFlateRateObj = new FlatRateInternet();
                    newFlateRateObj.PaymentDate = flateRateIObj.PaymentDate;
                    newFlateRateObj.AdditionalPackagePrice = flateRateIObj.AdditionalPackagePrice;
                    newFlateRateObj.TotalCost = flateRateIObj.TotalCost;
                    newFlateRateObj.ContractNumber = contract;
                    newFlateRateObj.Cost = flateRateIObj.Cost;
                    
                    s.SaveOrUpdate(newFlateRateObj);
                    s.Flush();
                    s.Close();

                    idContract = newFlateRateObj.ContractNumber.ContractId;
                    idPayment = newFlateRateObj.PaymentNumber;

                    ContractProvider.AddPaymentToTheContract(idContract, idPayment);

                }
                else if (realizedInternetFlowObj != null)
                {
                    var contract = s.Get<Entities.Contract>(realizedInternetFlowObj.ContractNumber);
                    if (contract == null)
                        return false;
                    if (contract.ServiceId.IsPrepaidInternet == false)
                    {
                        return false;
                    }
                    var newRealizedInternetFlowObj = new RealizedInternetFlow();
                    newRealizedInternetFlowObj.PaymentDate = realizedInternetFlowObj.PaymentDate;
                    newRealizedInternetFlowObj.AdditionalPackagePrice = realizedInternetFlowObj.AdditionalPackagePrice;
                    newRealizedInternetFlowObj.TotalCost = realizedInternetFlowObj.TotalCost;
                    newRealizedInternetFlowObj.ContractNumber = contract;
                    newRealizedInternetFlowObj.FlowAmount = realizedInternetFlowObj.FlowAmount;
                    newRealizedInternetFlowObj.CostPerFlow = realizedInternetFlowObj.CostPerFlow;

                    s.SaveOrUpdate(newRealizedInternetFlowObj);
                    s.Flush();
                    s.Close();

                    idContract = newRealizedInternetFlowObj.ContractNumber.ContractId;
                    idPayment = newRealizedInternetFlowObj.PaymentNumber;

                    ContractProvider.AddPaymentToTheContract(idContract, idPayment);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error has occured in PaymnetProvider(CreateOrUpadatePayment). \n" + ex.Message);
                return false;
            }
            return true;
        }

        public static bool DeletePayement(int paymentNumber)
        {
            try
            {
                var session = DataLayer.GetSession();
                var user = session.Get<Payment>(paymentNumber);
                session.Delete(user);
                session.Flush();
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PaymentDTOManager(DeletePayment). \n" + ex.Message); return false; }
            return true;
        }
        #endregion Payment Method(s)

        #region Flat-Rate Internet Method(s)
        public static List<RealizedInternetFlowView> GetAllRealizedInternetFlow()
        {
            List<RealizedInternetFlowView> realizedInternetFlows = new List<RealizedInternetFlowView>();
            try
            {
                var session = DataLayer.GetSession();
                var payements = (from p in session.Query<RealizedInternetFlow>() select p);

                foreach (var p in payements)
                {
                    realizedInternetFlows.Add(new RealizedInternetFlowView(p.PaymentNumber, p.PaymentDate, p.AdditionalPackagePrice, p.TotalCost,
                        p.ContractNumber.UserId.Name, p.ContractNumber.UserId.LastName, p.ContractNumber.UserId.ContactPhone,
                        p.ContractNumber.ContractId, p.ContractNumber.PackageName.Name, p.FlowAmount, p.CostPerFlow));
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PayementDTOManger(GetAllRealizedInternetFlow). \n" + ex.Message); }
            return realizedInternetFlows;
        }

        public static bool UpdateFlatRateInternet(FlatRateInternetBriefView flateRateInternet)
        {
            try
            {
                var s = DataLayer.GetSession();
                var payment = s.Get<FlatRateInternet>(flateRateInternet.PaymentNumber);
                if (payment == null)
                    return false;
                if (payment.ContractNumber.ServiceId.IsPostpaidInternet == false)
                {
                    return false;
                }

                payment.AdditionalPackagePrice = flateRateInternet.AdditionalPackagePrice;
                payment.PaymentDate = flateRateInternet.PaymentDate;
                payment.TotalCost = flateRateInternet.TotalCost;
                payment.Cost = flateRateInternet.Cost;

                s.SaveOrUpdate(payment);
                s.Flush();
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PaymentProvider(UpadteFlatRateInternet)" + ex.Message); return false; }
            return true;
        }

        #endregion Flat-Rate Internet Method(s)

        #region Realized Internet Flow Method(s)
        public static List<FlatRateInternetView> GetAllFlatRateInternet()
        {

            List<FlatRateInternetView> flatRateInternets = new List<FlatRateInternetView>();
            try
            {
                var session = DataLayer.GetSession();
                var payements = (from p in session.Query<FlatRateInternet>() select p);

                foreach (var p in payements)
                {
                    flatRateInternets.Add(new FlatRateInternetView(p.PaymentNumber, p.PaymentDate, p.AdditionalPackagePrice, p.TotalCost, p.ContractNumber.UserId.Name,
                        p.ContractNumber.UserId.LastName, p.ContractNumber.UserId.ContactPhone,p.ContractNumber.ContractId, p.ContractNumber.PackageName.Name, p.Cost)); ;
                }
                session.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PayementDTOManger(GetAllRealizedInternetFlow). \n" + ex.Message); }
            return flatRateInternets;
        }

        public static bool UpdateRealizedInternetFlow(RealizedInternetFlowBriefView realizedFlow)
        {
            try
            {
                var s = DataLayer.GetSession();
                var payment = s.Get<RealizedInternetFlow>(realizedFlow.PaymentNumber);
                if (payment == null)
                    return false;
                if (payment.ContractNumber.ServiceId.IsPrepaidInternet == false)
                {
                    return false;
                }

                payment.AdditionalPackagePrice = realizedFlow.AdditionalPackagePrice;
                payment.PaymentDate = realizedFlow.PaymentDate;
                payment.TotalCost = realizedFlow.TotalCost;
                payment.FlowAmount = realizedFlow.FlowAmount;
                payment.CostPerFlow = realizedFlow.CostPerFlow;

                s.SaveOrUpdate(payment);
                s.Flush();
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine("Error has occured in PaymentProvider(UpadteRealizedInternetFlow)" + ex.Message); return false; }
            return true;
        }
        #endregion Realized Internet Flow Method(s)
    }
}
