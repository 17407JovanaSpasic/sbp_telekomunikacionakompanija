﻿using System;
using System.Collections.Generic;
using System.Linq;

using DatabaseAccess.Entities;
using DatabaseAccess.Views.Device;
using DatabaseAccess.Views.Device.CommunicationNode;
using DatabaseAccess.Views.Device.MainStation;
using DatabaseAccess.Views.Device.MainStation.RegionalHub;
using DatabaseAccess.Views.Manufacturer;
using DatabaseAccess.Views.User;

namespace DatabaseAccess.Providers
{
    public class DeviceProvider
    {
        #region DeleteMethod
        public static bool DeleteMainStationOrRegionalHub(string id)
        {
            try
            {
                var s = DataLayer.GetSession();
                var station = s.Get<MainStation>(id);

                s.Delete(station);
                s.Flush();
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); return false; }
            return true;
        }
        #endregion DeleteMethod

        #region Main Station Method(s)

        public static List<AdvancedMainStationBriefView> GetAllMainStations()
        {
            //IZ NEKOG RAZLOGA SE NE PRIKAZUJE SVE!
            var listOfComunicationNodes = new List<AdvancedMainStationBriefView>();
            try
            {
                var s = DataLayer.GetSession();
                var mainStations = (from st in s.Query<Entities.MainStation>() select st);

                foreach (var station in mainStations)
                {
                    var comunicationNodes = (from comSt in s.Query<CommunicationNode>()
                                             where (comSt.MainStation.SerialNumber == station.SerialNumber)
                                             select comSt.SerialNumber);

                    List<String> listOfComunicatinNodesAttached = new List<String>();
                    int numberOfAttached = 0;
                    foreach (var cm in comunicationNodes)
                    {
                        listOfComunicatinNodesAttached.Add(cm);
                        numberOfAttached++;
                    }
                    if (numberOfAttached == 0)
                    {
                        List<string> emphtyList = new List<string>();
                        emphtyList.Add(" ");
                        listOfComunicationNodes.Add(new AdvancedMainStationBriefView(station.Manufacturer.Name, station.SerialNumber, station.IsRegionalHub, station.StartOfUseDate,
                            station.ServicingDate, station.ReasonForServicing, station.NumberOfNodes, emphtyList));
                    }
                    else
                        listOfComunicationNodes.Add(new AdvancedMainStationBriefView(station.Manufacturer.Name, station.SerialNumber, station.IsRegionalHub, station.StartOfUseDate,
                            station.ServicingDate, station.ReasonForServicing, station.NumberOfNodes, listOfComunicatinNodesAttached));
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return listOfComunicationNodes;
        }

        public static void CreateMainStation(BasicDeviceView mainStation)
        {
            try
            {
                var session = DataLayer.GetSession();

                var manufacturerEntity = session.Get<Manufacturer>(mainStation.ManufacturerPIB);

                if (manufacturerEntity is null)
                {
                    throw new Exception("Manufacturer does not exist.");
                }

                var mainStationEntity = new MainStation
                {
                    SerialNumber = mainStation.SerialNumber,
                    StartOfUseDate = mainStation.StartOfUseDate,
                    ServicingDate = null,
                    Manufacturer = manufacturerEntity,
                    IsRegionalHub = false
                };

                session.Save(mainStationEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static bool UpdateMainStation(string id, MainStationUpdateView station)
        {
            try
            {
                var s = DataLayer.GetSession();
                var mainStation = s.Get<MainStation>(id);

                if (mainStation == null)
                    return false;

                mainStation.ReasonForServicing = station.ReasonForServicing;
                mainStation.ServicingDate = station.ServicingDate;

                s.SaveOrUpdate(mainStation);
                s.Flush();
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); return false; }
            return true;
        }
        #endregion Main Station Method(s)

        #region Regional Hub Method(s)
        public static void CreateRegionalHub(BasicRegionalHubView regionalHub)
        {
            try
            {
                var session = DataLayer.GetSession();

                var manufacturerEntity = session.Get<Manufacturer>(regionalHub.ManufacturerPIB);

                if (manufacturerEntity is null)
                {
                    throw new Exception("Manufacturer does not exist.");
                }

                if (regionalHub.NameOfRegion == null)
                {
                    throw new Exception("Name of region can not be null.");
                }

                var regionalHubEntity = new MainStation
                {
                    SerialNumber = regionalHub.SerialNumber,
                    StartOfUseDate = regionalHub.StartOfUseDate,
                    ServicingDate = null,
                    Manufacturer = manufacturerEntity,
                    IsRegionalHub = true,
                    NameOfRegion = regionalHub.NameOfRegion
                };

                session.Save(regionalHubEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static AdvancedRegionalHubView GetRegionalHub()
        {
            try
            {
                AdvancedRegionalHubView regionalHub = null;
                var s = DataLayer.GetSession();
                var mainStations = (from st in s.Query<MainStation>() where (st.IsRegionalHub == true) select st);

                foreach (var regSt in mainStations)
                {
                    regionalHub = new AdvancedRegionalHubView(regSt.Manufacturer.Name, regSt.SerialNumber, regSt.IsRegionalHub, regSt.NameOfRegion, regSt.StartOfUseDate,
                    (DateTime)regSt.ServicingDate, regSt.ReasonForServicing, regSt.NumberOfNodes);
                    break;
                }
                return regionalHub;

            }
            catch (Exception ex) { Console.WriteLine(ex.Message); return null; }
        }

        public static bool UpdateRegionalHub(string id, RegionalHubUpdateView station)
        {
            try
            {
                var s = DataLayer.GetSession();
                var mainStation = s.Get<MainStation>(id);

                if (mainStation == null)
                    return false;

                mainStation.IsRegionalHub = station.IsRegionalHub;
                mainStation.NameOfRegion = station.NameOfRegion;
                mainStation.ReasonForServicing = station.ReasonForServicing;
                mainStation.ServicingDate = station.ServicingDate;

                s.SaveOrUpdate(mainStation);
                s.Flush();
                s.Close();
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); return false; }
            return true;
        }
        #endregion Regional Hub Method(s)

        #region Communication Node Method(s)
        public static void CreateCommunicationNode(BasicCommunicationNodeView communicationNode)
        {
            try
            {
                var session = DataLayer.GetSession();

                var manufacturerEntity = session.Get<Manufacturer>(communicationNode.ManufacturerPIB);

                if (manufacturerEntity is null)
                {
                    throw new Exception("Manufacturer does not exist.");
                }

                var mainStationEntity = session.Get<MainStation>(communicationNode.MainStationSerialNumber);

                if (mainStationEntity is null)
                {
                    throw new Exception("Main station does not exist.");
                }

                if (mainStationEntity.NumberOfNodes == 20)
                {
                    throw new Exception("Maximum number of nodes is already connected.");
                }

                var communicationNodeEntity = new CommunicationNode
                {
                    SerialNumber = communicationNode.SerialNumber,
                    StartOfUseDate = communicationNode.StartOfUseDate,
                    ServicingDate = null,
                    Manufacturer = manufacturerEntity,
                    Address = communicationNode.Address,
                    LocationNumber = communicationNode.LocationNumber,
                    Description = communicationNode.Description,
                    MainStation = mainStationEntity
                };

                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(communicationNodeEntity);
                        mainStationEntity.NumberOfNodes++;
                        session.Update(mainStationEntity);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);

                        transaction.Rollback();

                        throw;
                    }
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static AdvancedCommunicationNodeView GetCommunicationNodeBySerialNumber(string serialNumber)
        {
            var communicationNode = new AdvancedCommunicationNodeView();

            try
            {
                var session = DataLayer.GetSession();

                var communicationNodeEntity = session.Query<CommunicationNode>()
                    .Where(x => x.SerialNumber == serialNumber).FirstOrDefault();

                if (communicationNodeEntity is null)
                {
                    throw new Exception("Communication node does not exist.");
                }

                communicationNode.SerialNumber = communicationNodeEntity.SerialNumber;
                communicationNode.StartOfUseDate = communicationNodeEntity.StartOfUseDate;
                communicationNode.ServicingDate = (communicationNodeEntity.ServicingDate == new DateTime()) ?
                    null : communicationNodeEntity.ServicingDate;
                communicationNode.ReasonForServicing = communicationNodeEntity.ReasonForServicing;
                communicationNode.Address = communicationNodeEntity.Address;
                communicationNode.LocationNumber = communicationNodeEntity.LocationNumber;
                communicationNode.Description = communicationNodeEntity.Description;

                communicationNode.Manufacturer =
                    new ManufacturerView(communicationNodeEntity.Manufacturer.PIB,
                    communicationNodeEntity.Manufacturer.Name);

                communicationNode.MainStation =
                    new AdvancedMainStationView(communicationNodeEntity.MainStation.SerialNumber,
                    communicationNodeEntity.MainStation.StartOfUseDate,
                    communicationNodeEntity.MainStation.ServicingDate,
                    communicationNodeEntity.MainStation.ReasonForServicing,
                    communicationNodeEntity.MainStation.NumberOfNodes);

                foreach (var userEntity in communicationNodeEntity.Users)
                {
                    communicationNode.Users.Add(new BasicUserView(userEntity.Name,
                        userEntity.LastName, userEntity.ContactPhone));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return communicationNode;
        }

        public static List<AdvancedCommunicationNodeView> GetAllCommunicationNodes()
        {
            var communicationNodesList = new List<AdvancedCommunicationNodeView>();

            try
            {
                var session = DataLayer.GetSession();

                var communicationNodeEntities = session.Query<CommunicationNode>().ToList();

                foreach (var communicationNodeEntity in communicationNodeEntities)
                {
                    var communicationNode = new AdvancedCommunicationNodeView(
                        communicationNodeEntity.SerialNumber, communicationNodeEntity.StartOfUseDate,
                        communicationNodeEntity.ServicingDate, communicationNodeEntity.ReasonForServicing,
                        communicationNodeEntity.Address, communicationNodeEntity.LocationNumber,
                        communicationNodeEntity.Description)
                    {
                        Manufacturer = new ManufacturerView(communicationNodeEntity.Manufacturer.PIB,
                            communicationNodeEntity.Manufacturer.Name),
                        MainStation = new AdvancedMainStationView(
                            communicationNodeEntity.MainStation.SerialNumber,
                            communicationNodeEntity.MainStation.StartOfUseDate,
                            communicationNodeEntity.MainStation.ServicingDate,
                            communicationNodeEntity.MainStation.ReasonForServicing,
                            communicationNodeEntity.MainStation.NumberOfNodes)
                    };

                    foreach (var userEntity in communicationNodeEntity.Users)
                    {
                        communicationNode.Users.Add(new BasicUserView(userEntity.Name,
                            userEntity.LastName, userEntity.ContactPhone));
                    }

                    communicationNodesList.Add(communicationNode);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return communicationNodesList;
        }

        public static string GetRandomCommunicationNodeSerialNumber(int index)
        {
            string serialNumber;

            try
            {
                var session = DataLayer.GetSession();

                var communicationNodeEntities = session.Query<CommunicationNode>().ToList();

                serialNumber = communicationNodeEntities[index].SerialNumber;

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return serialNumber;
        }

        public static int GetNumberOfCommunicationNodes()
        {
            int numberOfCommunicationNodes;

            try
            {
                var session = DataLayer.GetSession();

                numberOfCommunicationNodes = session.Query<CommunicationNode>().Count();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return numberOfCommunicationNodes;
        }

        public static void UpdateCommunicationNode(string serialNumber,
            CommunicationNodeView communicationNode)
        {
            try
            {
                var session = DataLayer.GetSession();

                var communicationNodeEntity = session.Get<CommunicationNode>(serialNumber);

                if (communicationNodeEntity is null)
                {
                    throw new Exception("Communication node does not exist.");
                }

                communicationNodeEntity.StartOfUseDate = communicationNode.StartOfUseDate;
                communicationNodeEntity.ServicingDate = communicationNode.ServicingDate;
                communicationNodeEntity.ReasonForServicing = communicationNode.ReasonForServicing;
                communicationNodeEntity.Address = communicationNode.Address;
                communicationNodeEntity.LocationNumber = communicationNode.LocationNumber;
                communicationNodeEntity.Description = communicationNode.Description;

                if (communicationNodeEntity.MainStation.SerialNumber !=
                    communicationNode.MainStationSerialNumber)
                {
                    var mainStationEntity =
                        session.Get<MainStation>(communicationNode.MainStationSerialNumber);

                    if (mainStationEntity is null)
                    {
                        throw new Exception("Main station does not exist.");
                    }

                    if (mainStationEntity.NumberOfNodes == 20)
                    {
                        throw new Exception("Maximum number of nodes is already connected.");
                    }

                    using (var transaction = session.BeginTransaction())
                    {
                        try
                        {
                            communicationNodeEntity.MainStation.NumberOfNodes--;
                            session.Update(communicationNodeEntity.MainStation);
                            mainStationEntity.NumberOfNodes++;
                            session.Update(mainStationEntity);
                            communicationNodeEntity.MainStation = mainStationEntity;
                            session.Update(communicationNodeEntity);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);

                            transaction.Rollback();

                            throw;
                        }
                    }
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void DeleteCommunicationNode(string serialNumber)
        {
            try
            {
                var session = DataLayer.GetSession();

                var communicationNodeEntity = session.Get<CommunicationNode>(serialNumber);

                if (communicationNodeEntity is null)
                {
                    throw new Exception("Communication node does not exist.");
                }

                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        communicationNodeEntity.MainStation.NumberOfNodes--;
                        session.Update(communicationNodeEntity.MainStation);
                        session.Delete(communicationNodeEntity);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);

                        transaction.Rollback();

                        throw;
                    }
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }
        #endregion Communication Node Method(s)
    }
}
