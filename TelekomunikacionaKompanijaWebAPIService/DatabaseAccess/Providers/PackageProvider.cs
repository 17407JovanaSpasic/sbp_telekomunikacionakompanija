﻿using System;
using System.Collections.Generic;
using System.Linq;

using DatabaseAccess.Entities;
using DatabaseAccess.Views.Package;
using DatabaseAccess.Views.Package.AdditionalPackage;
using DatabaseAccess.Views.Package.Channel;

namespace DatabaseAccess.Providers
{
    public class PackageProvider
    {
        #region Package Method(s)
        public static List<PackageView> GetAllPackages()
        {
            var packagesList = new List<PackageView>();

            try
            {
                var session = DataLayer.GetSession();

                var packageEntities = (from package in session.Query<Package>()
                                       select package);

                foreach (var package in packageEntities)
                {
                    packagesList.Add(new PackageView(package.Name, package.Price,
                        package.AdditionalPackagePrice, package.InternetSpeed, package.PricePerMB,
                        package.PricePerAddress, package.NumberOfMinutes));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return packagesList;
        }

        public static List<AdvancedPackageView> GetAdvancedPackagesInfo()
        {
            var packagesList = new List<AdvancedPackageView>();

            try
            {
                var session = DataLayer.GetSession();

                var packageEntities = session.Query<Package>().ToList();

                foreach (var packageEntity in packageEntities)
                {
                    var package = new AdvancedPackageView(packageEntity.Name, packageEntity.Price,
                        packageEntity.AdditionalPackagePrice, packageEntity.InternetSpeed,
                        packageEntity.PricePerMB, packageEntity.PricePerAddress,
                        packageEntity.NumberOfMinutes);

                    foreach (var channelEntity in packageEntity.Channels)
                    {
                        var channel = new ChannelView(channelEntity.ID.ChannelName);

                        package.Channels.Add(channel);
                    }

                    foreach (var additionalPackageEntity in packageEntity.AdditionalPackages)
                    {
                        var additionalPackage =
                            new AdditionalPackageView(additionalPackageEntity.ID.AdditionalPackageName);

                        package.AdditionalPackages.Add(additionalPackage);
                    }

                    packagesList.Add(package);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return packagesList;
        }

        public static void CreatePackage(PackageView package)
        {
            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = new Package
                {
                    Name = package.Name,
                    Price = package.Price,
                    AdditionalPackagePrice = package.AdditionalPackagePrice,
                    InternetSpeed = package.InternetSpeed,
                    PricePerMB = package.PricePerMB,
                    PricePerAddress = package.PricePerAddress,
                    NumberOfMinutes = package.NumberOfMinutes
                };

                session.Save(packageEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void UpdatePackage(PackageView package)
        {
            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = session.Get<Package>(package.Name);

                if (packageEntity is null)
                {
                    throw new Exception("Package does not exist.");
                }

                packageEntity.Price = package.Price;
                packageEntity.AdditionalPackagePrice = package.AdditionalPackagePrice;
                packageEntity.InternetSpeed = package.InternetSpeed;
                packageEntity.PricePerMB = package.PricePerMB;
                packageEntity.PricePerAddress = package.PricePerAddress;
                packageEntity.NumberOfMinutes = package.NumberOfMinutes;

                session.Update(packageEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static PackageView GetPackageByName(string packageName)
        {
            var package = new PackageView();

            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = session.Get<Package>(packageName);

                if (packageEntity is null)
                {
                    throw new Exception("Package does not exist.");
                }

                package.Name = packageEntity.Name;
                package.Price = packageEntity.Price;
                package.AdditionalPackagePrice = packageEntity.AdditionalPackagePrice;
                package.InternetSpeed = packageEntity.InternetSpeed;
                package.PricePerMB = packageEntity.PricePerMB;
                package.PricePerAddress = packageEntity.PricePerAddress;
                package.NumberOfMinutes = packageEntity.NumberOfMinutes;

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return package;
        }

        public static AdvancedPackageView GetAdvancedPackageInfoByName(string packageName)
        {
            var package = new AdvancedPackageView();

            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = session.Get<Package>(packageName);

                if (packageEntity is null)
                {
                    throw new Exception("Package does not exist.");
                }

                package.Name = packageEntity.Name;
                package.Price = packageEntity.Price;
                package.AdditionalPackagePrice = packageEntity.AdditionalPackagePrice;
                package.InternetSpeed = packageEntity.InternetSpeed;
                package.PricePerMB = packageEntity.PricePerMB;
                package.PricePerAddress = packageEntity.PricePerAddress;
                package.NumberOfMinutes = packageEntity.NumberOfMinutes;

                foreach (var channelEntity in packageEntity.Channels)
                {
                    var channel = new ChannelView(channelEntity.ID.ChannelName);

                    package.Channels.Add(channel);
                }

                foreach (var additionalPackageEntity in packageEntity.AdditionalPackages)
                {
                    var additionalPackage =
                        new AdditionalPackageView(additionalPackageEntity.ID.AdditionalPackageName);

                    package.AdditionalPackages.Add(additionalPackage);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return package;
        }

        public static void DeletePackage(string packageName)
        {
            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = session.Get<Package>(packageName);

                if (packageEntity is null)
                {
                    throw new Exception("Package does not exist.");
                }

                session.Delete(packageEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }
        #endregion Package Method(s)

        #region Channel Method(s)
        public static List<ChannelView> GetChannelsByPackage(string packageName)
        {
            var channelsList = new List<ChannelView>();

            try
            {
                var session = DataLayer.GetSession();

                var channelNames = (from channel in session.Query<Channel>()
                                    where channel.ID.Package.Name == packageName
                                    select channel.ID.ChannelName);

                foreach (var name in channelNames)
                {
                    channelsList.Add(new ChannelView(name));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return channelsList;
        }

        public static List<ChannelView> GetAllChannels()
        {
            var channelsList = new List<ChannelView>();

            try
            {
                var session = DataLayer.GetSession();

                var channelNames = (from channel in session.Query<Channel>()
                                    select channel.ID.ChannelName).Distinct();

                foreach (var name in channelNames)
                {
                    channelsList.Add(new ChannelView(name));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return channelsList;
        }

        public static void CreateChannelForPackage(PackageChannelView channel)
        {
            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = session.Get<Package>(channel.PackageName);

                if (packageEntity is null)
                {
                    throw new Exception("Package does not exist.");
                }

                var channelEntity = new Channel
                {
                    ID = new ChannelID
                    {
                        Package = packageEntity,
                        ChannelName = channel.Name
                    }
                };

                session.Save(channelEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void UpdateChannelFromPackage(UpdatePackageChannelView channel)
        {
            try
            {
                var session = DataLayer.GetSession();

                var channelEntity = session.Query<Channel>().Where(x =>
                    x.ID.Package.Name == channel.PackageName && x.ID.ChannelName == channel.Name)
                    .FirstOrDefault();

                if (channelEntity is null)
                {
                    throw new Exception("Channel does not exist.");
                }

                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        var updatedChannelEntity = new Channel
                        {
                            ID = new ChannelID
                            {
                                Package = channelEntity.ID.Package,
                                ChannelName = channel.NewName
                            }
                        };

                        session.Delete(channelEntity);
                        session.Save(updatedChannelEntity);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);

                        transaction.Rollback();

                        throw;
                    }
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void DeleteChannelFromPackage(PackageChannelView channel)
        {
            try
            {
                var session = DataLayer.GetSession();

                var channelEntity = session.Query<Channel>().Where(x =>
                    x.ID.Package.Name == channel.PackageName && x.ID.ChannelName == channel.Name)
                    .FirstOrDefault();

                if (channelEntity is null)
                {
                    throw new Exception("Channel does not exist.");
                }

                session.Delete(channelEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }
        #endregion Channel Method(s)

        #region Additional Package Method(s)
        public static List<AdditionalPackageView> GetAdditionalPackagesByPackage(string packageName)
        {
            var additionalPackagesList = new List<AdditionalPackageView>();

            try
            {
                var session = DataLayer.GetSession();

                var additionalPackageNames = (from additionalPackage in session.Query<AdditionalPackage>()
                                              where additionalPackage.ID.Package.Name == packageName
                                              select additionalPackage.ID.AdditionalPackageName);

                foreach (var name in additionalPackageNames)
                {
                    additionalPackagesList.Add(new AdditionalPackageView(name));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return additionalPackagesList;
        }

        public static List<AdditionalPackageView> GetAllAdditionalPackages()
        {
            var additionalPackagesList = new List<AdditionalPackageView>();

            try
            {
                var session = DataLayer.GetSession();

                var additionalPackageNames = (from additionalPackage in session.Query<AdditionalPackage>()
                                              select additionalPackage.ID.AdditionalPackageName).Distinct();

                foreach (var name in additionalPackageNames)
                {
                    additionalPackagesList.Add(new AdditionalPackageView(name));
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return additionalPackagesList;
        }

        public static void CreateAdditionalPackageForPackage(PackageAdditionalPackageView additionalPackage)
        {
            try
            {
                var session = DataLayer.GetSession();

                var packageEntity = session.Get<Package>(additionalPackage.PackageName);

                if (packageEntity is null)
                {
                    throw new Exception("Package does not exist.");
                }

                var additionalPackageEntity = new AdditionalPackage
                {
                    ID = new AdditionalPackageID
                    {
                        Package = packageEntity,
                        AdditionalPackageName = additionalPackage.Name
                    }
                };

                session.Save(additionalPackageEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void UpdateAdditionalPackageFromPackage(UpdateAdditionalPackageView
            additionalPackage)
        {
            try
            {
                var session = DataLayer.GetSession();

                var additionalPackageEntity = session.Query<AdditionalPackage>()
                    .Where(x => x.ID.Package.Name == additionalPackage.PackageName &&
                    x.ID.AdditionalPackageName == additionalPackage.Name).FirstOrDefault();

                if (additionalPackageEntity is null)
                {
                    throw new Exception("Additional package does not exist.");
                }

                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        var updatedAdditionalPackageEntity = new AdditionalPackage
                        {
                            ID = new AdditionalPackageID
                            {
                                Package = additionalPackageEntity.ID.Package,
                                AdditionalPackageName = additionalPackage.NewName
                            }
                        };

                        session.Delete(additionalPackageEntity);
                        session.Save(updatedAdditionalPackageEntity);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);

                        transaction.Rollback();

                        throw;
                    }
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void DeleteAdditionalPackageFromPackage(PackageAdditionalPackageView additionalPackage)
        {
            try
            {
                var session = DataLayer.GetSession();

                var additionalPackageEntity = session.Query<AdditionalPackage>()
                    .Where(x => x.ID.Package.Name == additionalPackage.PackageName &&
                    x.ID.AdditionalPackageName == additionalPackage.Name).FirstOrDefault();

                if (additionalPackageEntity is null)
                {
                    throw new Exception("Additional package does not exist.");
                }

                session.Delete(additionalPackageEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }
        #endregion Additional Package Method(s)
    }
}
