﻿using System;
using System.Collections.Generic;
using System.Linq;

using DatabaseAccess.Entities;
using DatabaseAccess.Views.Manufacturer;

namespace DatabaseAccess.Providers
{
    public class ManufacturerProvider
    {
        #region Manufacturer Method(s)
        public static void CreateManufacturer(ManufacturerView manufacturer)
        {
            try
            {
                var session = DataLayer.GetSession();

                var manufacturerEntity = new Manufacturer
                {
                    PIB = manufacturer.PIB,
                    Name = manufacturer.Name
                };

                session.Save(manufacturerEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static List<ManufacturerView> GetAllManufacturers()
        {
            var manufacturersList = new List<ManufacturerView>();

            try
            {
                var session = DataLayer.GetSession();

                var manufacturerEntities = session.Query<Manufacturer>().ToList();

                foreach (var manufacturerEntity in manufacturerEntities)
                {
                    var manufacturer = new ManufacturerView(manufacturerEntity.PIB,
                        manufacturerEntity.Name);

                    manufacturersList.Add(manufacturer);
                }

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return manufacturersList;
        }

        public static void UpdateManufacturer(ManufacturerView manufacturer)
        {
            try
            {
                var session = DataLayer.GetSession();

                var manufacturerEntity = session.Get<Manufacturer>(manufacturer.PIB);

                if (manufacturerEntity is null)
                {
                    throw new Exception("Manufacturer does not exist.");
                }

                manufacturerEntity.Name = manufacturer.Name;

                session.Update(manufacturerEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }

        public static void DeleteManufacturer(string pib)
        {
            try
            {
                var session = DataLayer.GetSession();

                var manufacturerEntity = session.Get<Manufacturer>(pib);

                if (manufacturerEntity is null)
                {
                    throw new Exception("Manufacturer does not exist.");
                }

                session.Delete(manufacturerEntity);

                session.Flush();

                session.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }
        #endregion Manufacturer Method(s)
    }
}
