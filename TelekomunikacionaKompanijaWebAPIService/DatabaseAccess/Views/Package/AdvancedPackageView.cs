﻿using System.Collections.Generic;

using DatabaseAccess.Views.Package.AdditionalPackage;
using DatabaseAccess.Views.Package.Channel;

namespace DatabaseAccess.Views.Package
{
    public class AdvancedPackageView
    {
        #region Properties
        public string Name { get; set; }
        public float Price { get; set; }
        public float AdditionalPackagePrice { get; set; }
        public int InternetSpeed { get; set; }
        public float PricePerMB { get; set; }
        public float PricePerAddress { get; set; }
        public int NumberOfMinutes { get; set; }

        public IList<ChannelView> Channels { get; set; }
        public IList<AdditionalPackageView> AdditionalPackages { get; set; }
        #endregion Properties

        #region Constructor(s)
        public AdvancedPackageView()
        {
            this.Channels = new List<ChannelView>();
            this.AdditionalPackages = new List<AdditionalPackageView>();
        }

        public AdvancedPackageView(string name, float price, float additionalPackagePrice,
            int internetSpeed, float pricePerMB, float pricePerAddress, int numberOfMinutes)
        {
            this.Name = name;
            this.Price = price;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.InternetSpeed = internetSpeed;
            this.PricePerMB = pricePerMB;
            this.PricePerAddress = pricePerAddress;
            this.NumberOfMinutes = numberOfMinutes;

            this.Channels = new List<ChannelView>();
            this.AdditionalPackages = new List<AdditionalPackageView>();
        }
        #endregion Constructor(s)
    }
}
