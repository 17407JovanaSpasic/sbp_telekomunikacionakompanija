﻿namespace DatabaseAccess.Views.Package.AdditionalPackage
{
    public class UpdateAdditionalPackageView
    {
        #region Properties
        public string PackageName { get; set; }
        public string Name { get; set; }
        public string NewName { get; set; }
        #endregion Properties

        #region Constructor(s)
        public UpdateAdditionalPackageView() { }

        public UpdateAdditionalPackageView(string packageName, string name, string newName)
        {
            this.PackageName = packageName;
            this.Name = name;
            this.NewName = newName;
        }
        #endregion Constructor(s)
    }
}
