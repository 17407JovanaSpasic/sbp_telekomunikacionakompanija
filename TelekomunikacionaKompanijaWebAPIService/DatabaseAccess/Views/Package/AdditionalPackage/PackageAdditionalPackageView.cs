﻿namespace DatabaseAccess.Views.Package.AdditionalPackage
{
    public class PackageAdditionalPackageView
    {
        #region Properties
        public string PackageName { get; set; }
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PackageAdditionalPackageView() { }

        public PackageAdditionalPackageView(string packageName, string name)
        {
            this.PackageName = packageName;
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
