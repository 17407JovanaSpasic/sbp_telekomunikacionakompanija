﻿namespace DatabaseAccess.Views.Package.AdditionalPackage
{
    public class AdditionalPackageView
    {
        #region Properties
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public AdditionalPackageView() { }

        public AdditionalPackageView(string name)
        {
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
