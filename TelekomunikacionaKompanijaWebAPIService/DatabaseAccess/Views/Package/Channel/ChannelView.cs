﻿namespace DatabaseAccess.Views.Package.Channel
{
    public class ChannelView
    {
        #region Properties
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ChannelView() { }

        public ChannelView(string name)
        {
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
