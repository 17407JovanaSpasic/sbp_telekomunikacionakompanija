﻿namespace DatabaseAccess.Views.Package.Channel
{
    public class UpdatePackageChannelView
    {
        #region Properties
        public string PackageName { get; set; }
        public string Name { get; set; }
        public string NewName { get; set; }
        #endregion Properties

        #region Constructor(s)
        public UpdatePackageChannelView() { }

        public UpdatePackageChannelView(string packageName, string name, string newName)
        {
            this.PackageName = packageName;
            this.Name = name;
            this.NewName = newName;
        }
        #endregion Constructor(s)
    }
}
