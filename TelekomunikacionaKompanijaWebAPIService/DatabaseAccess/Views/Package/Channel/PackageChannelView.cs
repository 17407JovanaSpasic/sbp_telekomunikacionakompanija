﻿namespace DatabaseAccess.Views.Package.Channel
{
    public class PackageChannelView
    {
        #region Properties
        public string PackageName { get; set; }
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PackageChannelView() { }

        public PackageChannelView(string packageName, string name)
        {
            this.PackageName = packageName;
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
