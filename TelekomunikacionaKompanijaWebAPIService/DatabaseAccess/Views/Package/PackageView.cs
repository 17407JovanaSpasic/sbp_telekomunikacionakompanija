﻿namespace DatabaseAccess.Views.Package
{
    public class PackageView
    {
        #region Properties
        public string Name { get; set; }
        public float Price { get; set; }
        public float AdditionalPackagePrice { get; set; }
        public int InternetSpeed { get; set; }
        public float PricePerMB { get; set; }
        public float PricePerAddress { get; set; }
        public int NumberOfMinutes { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PackageView() { }

        public PackageView(string name, float price, float additionalPackagePrice, int internetSpeed,
            float pricePerMB, float pricePerAddress, int numberOfMinutes)
        {
            this.Name = name;
            this.Price = price;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.InternetSpeed = internetSpeed;
            this.PricePerMB = pricePerMB;
            this.PricePerAddress = pricePerAddress;
            this.NumberOfMinutes = numberOfMinutes;
        }
        #endregion Constructor(s)
    }
}
