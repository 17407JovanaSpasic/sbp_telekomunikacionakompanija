﻿using System;

namespace DatabaseAccess.Views.Device
{
    public class BasicDeviceView
    {
        #region Properties
        public string SerialNumber { get; set; }
        public DateTime StartOfUseDate { get; set; }
        public string ManufacturerPIB { get; set; }
        #endregion Properties

        #region Constructor(s)
        public BasicDeviceView() { }

        public BasicDeviceView(string serialNumber, DateTime startOfUseDate, string manufacturerPIB)
        {
            this.SerialNumber = serialNumber;
            this.StartOfUseDate = startOfUseDate;
            this.ManufacturerPIB = manufacturerPIB;
        }
        #endregion Constructor(s)
    }
}
