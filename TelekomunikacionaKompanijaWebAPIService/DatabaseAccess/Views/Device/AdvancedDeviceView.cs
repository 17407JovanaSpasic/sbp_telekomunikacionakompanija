﻿using System;

using DatabaseAccess.Views.Manufacturer;

namespace DatabaseAccess.Views.Device
{
    public class AdvancedDeviceView
    {
        #region Properties
        public string SerialNumber { get; set; }
        public DateTime StartOfUseDate { get; set; }
        public DateTime? ServicingDate { get; set; }
        public string ReasonForServicing { get; set; }

        public ManufacturerView Manufacturer { get; set; }
        #endregion Properties

        #region Constructor(s)
        public AdvancedDeviceView()
        {
            this.Manufacturer = null;
        }

        public AdvancedDeviceView(string serialNumber, DateTime startOfUseDate, DateTime? servicingDate,
            string reasonForServicing)
        {
            this.SerialNumber = serialNumber;
            this.StartOfUseDate = startOfUseDate;
            this.ServicingDate = (servicingDate == new DateTime()) ? null : servicingDate;
            this.ReasonForServicing = reasonForServicing;
        }
        #endregion Constructor(s)
    }
}
