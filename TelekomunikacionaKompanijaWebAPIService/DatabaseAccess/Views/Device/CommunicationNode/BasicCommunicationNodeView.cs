﻿using System;

namespace DatabaseAccess.Views.Device.CommunicationNode
{
    public class BasicCommunicationNodeView : BasicDeviceView
    {
        #region Properties
        public string Address { get; set; }
        public int LocationNumber { get; set; }
        public string Description { get; set; }
        public string MainStationSerialNumber { get; set; }
        #endregion Properties

        #region Constructor(s)
        public BasicCommunicationNodeView() : base() { }

        public BasicCommunicationNodeView(string serialNumber, DateTime startOfUseDate,
            string manufacturerPIB, string address, int locationNumber, string description,
            string mainStationSerialNumber)
            : base(serialNumber, startOfUseDate, manufacturerPIB)
        {
            this.Address = address;
            this.LocationNumber = locationNumber;
            this.Description = description;
            this.MainStationSerialNumber = mainStationSerialNumber;
        }
        #endregion Constructor(s)
    }
}
