﻿using System;
using System.Collections.Generic;

using DatabaseAccess.Views.Device.MainStation;
using DatabaseAccess.Views.User;

namespace DatabaseAccess.Views.Device.CommunicationNode
{
    public class AdvancedCommunicationNodeView : AdvancedDeviceView
    {
        #region Properties
        public string Address { get; set; }
        public int LocationNumber { get; set; }
        public string Description { get; set; }

        public AdvancedMainStationView MainStation { get; set; }
        public IList<BasicUserView> Users { get; set; }
        #endregion Properties

        #region Constructor(s)
        public AdvancedCommunicationNodeView() : base()
        {
            this.MainStation = null;
            this.Users = new List<BasicUserView>();
        }

        public AdvancedCommunicationNodeView(string serialNumber, DateTime startOfUseDate,
            DateTime? servicingDate, string reasonForServicing, string address, int locationNumber,
            string description)
            : base(serialNumber, startOfUseDate, servicingDate, reasonForServicing)
        {
            this.Address = address;
            this.LocationNumber = locationNumber;
            this.Description = description;

            this.MainStation = null;
            this.Users = new List<BasicUserView>();
        }
        #endregion Constructor(s)
    }
}
