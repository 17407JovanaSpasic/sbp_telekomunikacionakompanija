﻿using System;

namespace DatabaseAccess.Views.Device.CommunicationNode
{
    public class CommunicationNodeView : DeviceView
    {
        #region Properties
        public string Address { get; set; }
        public int LocationNumber { get; set; }
        public string Description { get; set; }
        public string MainStationSerialNumber { get; set; }
        #endregion Properties

        #region Constructor(s)
        public CommunicationNodeView() : base() { }

        public CommunicationNodeView(DateTime startOfUseDate, DateTime? servicingDate,
            string reasonForServicing, string address, int locationNumber, string description,
            string mainStationSerialNumber)
            : base(startOfUseDate, servicingDate, reasonForServicing)
        {
            this.Address = address;
            this.LocationNumber = locationNumber;
            this.Description = description;
            this.MainStationSerialNumber = mainStationSerialNumber;
        }
        #endregion Constructor(s)
    }
}
