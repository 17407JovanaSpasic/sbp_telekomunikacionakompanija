﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Views.Device.MainStation
{
    public class MainStationUpdateView
    {
        #region Attributes
        public DateTime? ServicingDate { get; set; }
        public string ReasonForServicing { get; set; }
        #endregion Attributes

        #region Constructor
        public MainStationUpdateView(DateTime? servicingDate, string reasonForServicing)
        {
            this.ServicingDate = servicingDate;
            this.ReasonForServicing = reasonForServicing;
        }
        #endregion Constructor
    }
}
