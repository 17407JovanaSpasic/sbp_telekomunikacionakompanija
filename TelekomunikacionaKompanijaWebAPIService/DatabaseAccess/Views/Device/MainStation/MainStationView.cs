﻿using System;

namespace DatabaseAccess.Views.Device.MainStation
{
    public class MainStationView : DeviceView
    {
        #region Properties
        public int NumberOfNodes { get; private set; }
        #endregion Properties

        #region Constructor(s)
        public MainStationView() : base() { }

        public MainStationView(DateTime startOfUseDate, DateTime? servicingDate,
            string reasonForServicing, int numberOfNodes)
            : base(startOfUseDate, servicingDate, reasonForServicing)
        {
            this.NumberOfNodes = numberOfNodes;
        }
        #endregion Constructor(s)
    }
}
