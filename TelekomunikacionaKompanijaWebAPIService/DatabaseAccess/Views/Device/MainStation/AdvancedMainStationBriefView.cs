﻿using System;
using System.Collections.Generic;

namespace DatabaseAccess.Views.Device.MainStation
{
    public class AdvancedMainStationBriefView
    {
        #region Attributes
        public string Manufacturer { get; set; }
        public bool IsRegionalHub { get; set; }
        public string SerialNumber { get; set; }
        public DateTime StartOfUseDate { get; set; }
        public DateTime? ServicingDate { get; set; }
        public string ReasonForServicing { get; set; }
        public int NumberOfNodes { get; set; }
        public List<String> SerialNumberOfComunicationNodes { get; set; }
        #endregion Attributes

        #region Constructor
        public AdvancedMainStationBriefView(string manufacturer, string serialNumber, bool isRegionalHub, DateTime startOfUseDate,
            DateTime? servicingDate, string reasonForServicing, int numberOfNodes, List<String> serialNumberOfComunicationNodes)
        {
            this.Manufacturer = manufacturer;
            this.SerialNumber = serialNumber;
            this.IsRegionalHub = isRegionalHub;
            //this.NameOfRegion = nameOfRegion; //mislim da je ovo suvisno
            this.StartOfUseDate = startOfUseDate;
            this.ServicingDate = (servicingDate == new DateTime()) ? null : servicingDate;
            this.NumberOfNodes = numberOfNodes;
            this.ReasonForServicing = reasonForServicing;
            this.SerialNumberOfComunicationNodes = new List<String>();
            this.SerialNumberOfComunicationNodes = serialNumberOfComunicationNodes;
        }
        #endregion Constructor

    }
}
