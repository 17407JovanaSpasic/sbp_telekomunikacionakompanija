﻿using System;

namespace DatabaseAccess.Views.Device.MainStation
{
    public class AdvancedMainStationView : AdvancedDeviceView
    {
        #region Properties
        public int NumberOfNodes { get; set; }
        #endregion Properties

        #region Constructor(s)
        public AdvancedMainStationView(string serialNumber, DateTime startOfUseDate,
            DateTime? servicingDate, string reasonForServicing, int numberOfNodes)
            : base(serialNumber, startOfUseDate, servicingDate, reasonForServicing)
        {
            this.NumberOfNodes = numberOfNodes;
        }
        #endregion Constructor(s)
    }
}
