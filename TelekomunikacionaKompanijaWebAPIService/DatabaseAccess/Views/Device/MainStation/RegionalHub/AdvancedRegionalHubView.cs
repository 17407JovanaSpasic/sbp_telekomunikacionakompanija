﻿using System;

namespace DatabaseAccess.Views.Device.MainStation.RegionalHub
{
    public class AdvancedRegionalHubView
    {
        #region Attributes
        public string Manufacturer { get; set; }
        public string SerialNumber { get; set; }
        public bool IsRegionalHub { get; set; }
        public string NameOfRegion { get; set; }
        public DateTime StartOfUseDate { get; set; }
        public DateTime? ServicingDate { get; set; }
        public string ReasonForServicing { get; set; }
        public int NumberOfNodes { get; set; }
        #endregion Attributes

        #region Constructor
        public AdvancedRegionalHubView(string manufacturer, string serialNumber, bool isRegionalHub, string nameOfRegion, DateTime startOfUseDate,
            DateTime? servicingDate, string reasonForServicing, int numberOfNodes)
        {
            this.Manufacturer = manufacturer;
            this.SerialNumber = serialNumber;
            this.IsRegionalHub = isRegionalHub;
            this.NameOfRegion = nameOfRegion;
            this.StartOfUseDate = startOfUseDate;
            this.ServicingDate = (servicingDate == new DateTime()) ? null : servicingDate;
            this.NumberOfNodes = numberOfNodes;
            this.ReasonForServicing = reasonForServicing;
        }
        #endregion Constructor
    }
}
