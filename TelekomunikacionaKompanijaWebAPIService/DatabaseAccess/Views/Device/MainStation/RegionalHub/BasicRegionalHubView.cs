﻿using System;

namespace DatabaseAccess.Views.Device.MainStation.RegionalHub
{
    public class BasicRegionalHubView : BasicDeviceView
    {
        #region Properties
        public string NameOfRegion { get; set; }
        #endregion Properties

        #region Constructor(s)
        public BasicRegionalHubView() : base() { }

        public BasicRegionalHubView(string serialNumber, DateTime startOfUseDate,
            string manufacturerPIB, string nameOfRegion)
            : base(serialNumber, startOfUseDate, manufacturerPIB)
        {
            this.NameOfRegion = nameOfRegion;
        }
        #endregion Constructor(s)
    }
}
