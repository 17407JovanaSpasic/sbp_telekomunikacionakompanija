﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Views.Device.MainStation.RegionalHub
{
    public class RegionalHubUpdateView
    {
        #region Attributes
        public bool IsRegionalHub { get; set; }
        public string NameOfRegion { get; set; }
        public DateTime? ServicingDate { get; set; }
        public string ReasonForServicing { get; set; }
        #endregion Attributes

        #region Constructor
        public RegionalHubUpdateView(bool isRegionalHub, string nameOfRegion, DateTime? servicingDate, string reasonForServicing, int numberOfNodes)
        {
            this.IsRegionalHub = isRegionalHub;
            this.NameOfRegion = nameOfRegion;
            this.ServicingDate = servicingDate;
            this.ReasonForServicing = reasonForServicing;
        }
        #endregion Constructor
    }
}
