﻿using System;

namespace DatabaseAccess.Views.Device
{
    public class DeviceView
    {
        #region Properties
        public DateTime StartOfUseDate { get; set; }
        public DateTime? ServicingDate { get; set; }
        public string ReasonForServicing { get; set; }
        #endregion Properties

        #region Constructor(s)
        public DeviceView() { }

        public DeviceView(DateTime startOfUseDate, DateTime? servicingDate,
            string reasonForServicing)
        {
            this.StartOfUseDate = startOfUseDate;
            this.ServicingDate = (servicingDate == new DateTime()) ? null : servicingDate;
            this.ReasonForServicing = reasonForServicing;
        }
        #endregion Constructor(s)
    }
}
