﻿using System.Collections.Generic;

using DatabaseAccess.Views.Contract;
using DatabaseAccess.Entities;

namespace DatabaseAccess.Views.User
{
    public class IndividualBriefView //Ovaj View se koristi za azuriranja fizicko lica
    {
        #region Properties
        public int Id { get; set; }
        public string Address { get; set; }
        public int AddressNumber { get; set; }
        public string City { get; set; }
        public string ContactPhone { get; set; }
        public string Email { get; set; }

        #endregion Properties

        #region Constructor(s)
        public IndividualBriefView() {}

        public IndividualBriefView(string address, int addressNumber, string city,
            string contactPhone, string email)
        {
            this.Address = address;
            this.AddressNumber = addressNumber;
            this.City = city;
            this.ContactPhone = contactPhone;
            this.Email = email;
        }
        #endregion Constructor(s)
    }
}
