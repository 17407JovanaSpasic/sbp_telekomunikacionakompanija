﻿using DatabaseAccess.Views.Contract;
using System.Collections.Generic;

namespace DatabaseAccess.Views.User
{
    public class IndividualView : UserView
    {
        #region Properties
        public string IndividualJMBG { get; set; }
        #endregion Properties

        #region Constructor(s)
        public IndividualView() : base() { }

        public IndividualView(string name, string lname, string address, int addressNumber, string city,
            string contactPhone, string email, string jmbg)
           : base(name, lname, address, addressNumber, city, contactPhone, email, 1, 0)
        {
            this.IndividualJMBG = jmbg;
        }

        public IndividualView(int id, string name, string lname, string address, int addressNumber,
            string city, string contactPhone, string email, string jmbg)
            : base(id, name, lname, address, addressNumber, city, contactPhone, email, 1, 0)
        {
            this.IndividualJMBG = jmbg;
        }

        public IndividualView(int id, string name, string lname, string address, int addressNumber,
           string city, string contactPhone, string email, string jmbg,string serialNumber, List<ContractBriefView> contracts)
           : base(id, name, lname, address, addressNumber, city, contactPhone, email, 1, 0,serialNumber,contracts )
        {
            this.IndividualJMBG = jmbg;
        }
        #endregion Constructor(s)
    }
}
