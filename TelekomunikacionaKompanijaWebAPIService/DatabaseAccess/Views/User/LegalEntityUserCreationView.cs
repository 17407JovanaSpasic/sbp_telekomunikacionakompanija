﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Views.User
{
    public class LegalEntityUserCreationView //Ovaj View se koristi za kreiranja pravnog lica
    {
        #region Properties
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int AddressNumber { get; set; }
        public string City { get; set; }
        public string ContactPhone { get; set; }
        public string Email { get; set; }

        public string LegalEntityFaxNumber { get; set; }
        public string LegalEntityPIB { get; set; }

        #endregion Properties

        #region Constructor(s)
        public LegalEntityUserCreationView() { }

        public LegalEntityUserCreationView(string name, string lname, string address, int addressNumber, string city,
            string contactPhone, string email, int flagIndividual, int flagLegalEntity, string legalEntityFaxNumber, string legalEntityPib)
        {
            this.Name = name;
            this.LastName = lname;
            this.Address = address;
            this.AddressNumber = addressNumber;
            this.City = city;
            this.ContactPhone = contactPhone;
            this.Email = email;
            this.LegalEntityFaxNumber = legalEntityFaxNumber;
            this.LegalEntityPIB = legalEntityPib;
        }
        #endregion Constructor(s)
    }
}
