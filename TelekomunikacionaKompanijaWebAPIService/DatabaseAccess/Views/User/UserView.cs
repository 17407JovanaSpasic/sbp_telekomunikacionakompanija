﻿using System.Collections.Generic;

using DatabaseAccess.Views.Contract;
using DatabaseAccess.Entities;

namespace DatabaseAccess.Views.User
{
    public class UserView
    {
        #region Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int AddressNumber { get; set; }
        public string City { get; set; }
        public string ContactPhone { get; set; }
        public string Email { get; set; }
        public string NoderSerialNumber { get; set; }
        public virtual IList<ContractBriefView> userBriefContract { get; set; }

        //da li navodim ovaj atribut?
        //public virtual CommunicationNode NodeSerialNumber { get; set; }

        public virtual IList<ContractView> UsersContracts { get; set; }

        //flagovi da li je user Fizicko odnosno Pravno lice
        public int Individual { get; set; }
        public int LegalEntity { get; set; }
        #endregion Properties

        #region Constructor(s)
        public UserView() { this.UsersContracts = new List<ContractView>(); }

        public UserView(string name, string lname, string address, int addressNumber, string city,
            string contactPhone, string email, int flagIndividual, int flagLegalEntity)
        {
            this.Name = name;
            this.LastName = lname;
            this.Address = address;
            this.AddressNumber = addressNumber;
            this.City = city;
            this.ContactPhone = contactPhone;
            this.Email = email;

            this.Individual = flagIndividual;           // Postavljamo flagove:
            this.LegalEntity = flagLegalEntity;         // 0 == false
                                                        // 1 == true                   
        }

        public UserView(int id, string name, string lname, string address, int addressNumber, string city,
            string contactPhone, string email, int flagIndividual, int flagLegalEntity)
        {
            this.Id = id;
            this.Name = name;
            this.LastName = lname;
            this.Address = address;
            this.AddressNumber = addressNumber;
            this.City = city;
            this.ContactPhone = contactPhone;
            this.Email = email;

            this.Individual = flagIndividual;           // Postavljamo flagove:
            this.LegalEntity = flagLegalEntity;         // 0 == false
                                                        // 1 == true                   
        }

        public UserView(int id, string name, string lname, string address, int addressNumber, string city,
            string contactPhone, string email, int flagIndividual, int flagLegalEntity, string serialNumber, List<ContractBriefView> contracts)
        {
            this.Id = id;
            this.Name = name;
            this.LastName = lname;
            this.Address = address;
            this.AddressNumber = addressNumber;
            this.City = city;
            this.ContactPhone = contactPhone;
            this.Email = email;

            this.Individual = flagIndividual;           
            this.LegalEntity = flagLegalEntity;
            this.NoderSerialNumber = serialNumber;
            this.userBriefContract = contracts;
            
        }
        #endregion Constructor(s)
    }
}
