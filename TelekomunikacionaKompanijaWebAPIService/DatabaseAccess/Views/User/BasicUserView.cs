﻿namespace DatabaseAccess.Views.User
{
    public class BasicUserView
    {
        #region Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        #endregion Properties

        #region Constructor(s)
        public BasicUserView() { }

        public BasicUserView(string firstName, string lastName, string phoneNumber)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.PhoneNumber = phoneNumber;
        }
        #endregion Constructor(s)
    }
}
