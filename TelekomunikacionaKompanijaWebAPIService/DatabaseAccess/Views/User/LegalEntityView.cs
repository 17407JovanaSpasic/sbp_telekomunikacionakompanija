﻿using DatabaseAccess.Views.Contract;
using System.Collections.Generic;

namespace DatabaseAccess.Views.User
{
    public class LegalEntityView : UserView
    {
        #region Properties
        public string LegalEntityFaxNumber { get; set; }
        public string LegalEntityPIB { get; set; }
        #endregion Properties

        #region Constructor(s)
        public LegalEntityView() : base() { }

        //this constructor is used for representing relation in list views
        public LegalEntityView(string name, string lname, string address, int addressNumber, string city,
            string contactPhone, string email, string FaxNumber, string PIB)
            : base(name, lname, address, addressNumber, city, contactPhone, email, 0, 1)
        {
            this.LegalEntityFaxNumber = FaxNumber;
            this.LegalEntityPIB = PIB;
        }

        //this constructor is used for representing relation in list views
        public LegalEntityView(int id, string name, string lname, string address, int addressNumber,
            string city, string contactPhone, string email, string FaxNumber, string PIB)
            : base(id, name, lname, address, addressNumber, city, contactPhone, email, 0, 1)
        {
            this.LegalEntityFaxNumber = FaxNumber;
            this.LegalEntityPIB = PIB;
        }

        public LegalEntityView(int id, string name, string lname, string address, int addressNumber,
            string city, string contactPhone, string email, string FaxNumber, string PIB,string serialNumber, List<ContractBriefView> contracts)
            : base(id, name, lname, address, addressNumber, city, contactPhone, email, 0, 1,serialNumber,contracts)
        {
            this.LegalEntityFaxNumber = FaxNumber;
            this.LegalEntityPIB = PIB;
        }
        #endregion Constructor(s)
    }
}
