﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Views.Contract
{
    public class ContractCreationView
    {

        #region Propertie
        public int UserId { get; set; }
        public string PackageName { get; set; }
        public int ServiceId { get; set; }
        public int Duration { get; set; }
        #endregion

        #region constructors
        public ContractCreationView(int userId, string packageName, int serviceId, int duration)
        {
            this.UserId = userId;
            this.PackageName = packageName;
            this.ServiceId = serviceId;
            this.Duration = duration;
        }
        #endregion constuctors

    }
}
