﻿namespace DatabaseAccess.Views.Contract.Service.Television
{
    public class AdditionalChannelPackageView
    {
        #region Properties
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public AdditionalChannelPackageView() { }

        public AdditionalChannelPackageView(string name)
        {
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
