﻿namespace DatabaseAccess.Views.Contract.Service.Television
{
    public class ServiceAdditionalChannelPackageView
    {
        #region Properties
        public int ServiceID { get; set; }
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ServiceAdditionalChannelPackageView() { }

        public ServiceAdditionalChannelPackageView(int serviceID, string name)
        {
            this.ServiceID = serviceID;
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
