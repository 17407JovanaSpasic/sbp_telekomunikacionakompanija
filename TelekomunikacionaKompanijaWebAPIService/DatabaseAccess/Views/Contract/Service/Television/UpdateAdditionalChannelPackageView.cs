﻿namespace DatabaseAccess.Views.Contract.Service.Television
{
    public class UpdateAdditionalChannelPackageView
    {
        #region Properties
        public int ServiceID { get; set; }
        public string Name { get; set; }
        public string NewName { get; set; }
        #endregion Properties

        #region Constructor(s)
        public UpdateAdditionalChannelPackageView() { }

        public UpdateAdditionalChannelPackageView(int serviceID, string name, string newName)
        {
            this.ServiceID = serviceID;
            this.Name = name;
            this.NewName = newName;
        }
        #endregion Constructor(s)
    }
}
