﻿namespace DatabaseAccess.Views.Contract.Service.Telephony
{
    public class PhoneNumberView
    {
        #region Properties
        public string PhoneNumber { get; set; }
        public int NumberOfMinutes { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PhoneNumberView() { }

        public PhoneNumberView(string phoneNumber)
        {
            this.PhoneNumber = phoneNumber;
            this.NumberOfMinutes = 0;
        }

        public PhoneNumberView(string phoneNumber, int numberOfMinutes)
        {
            this.PhoneNumber = phoneNumber;
            this.NumberOfMinutes = numberOfMinutes;
        }
        #endregion Constructor(s)
    }
}
