﻿namespace DatabaseAccess.Views.Contract.Service.Telephony
{
    public class ServicePhoneNumberView
    {
        #region Properties
        public int ServiceID { get; set; }
        public string Number { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ServicePhoneNumberView() { }

        public ServicePhoneNumberView(int serviceID, string number)
        {
            this.ServiceID = serviceID;
            this.Number = number;
        }
        #endregion Constructor(s)
    }
}
