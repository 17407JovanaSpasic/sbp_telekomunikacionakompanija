﻿namespace DatabaseAccess.Views.Contract.Service.Internet
{
    public class PostpaidInternetView
    {
        #region Properties
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PostpaidInternetView() { }

        public PostpaidInternetView(string address1, string address2)
        {
            this.Address1 = address1;
            this.Address2 = address2;
        }
        #endregion Constructor(s)
    }
}
