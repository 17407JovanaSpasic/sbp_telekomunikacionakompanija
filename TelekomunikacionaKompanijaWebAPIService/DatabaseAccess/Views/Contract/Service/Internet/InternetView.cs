﻿using System;

namespace DatabaseAccess.Views.Contract.Service.Internet
{
    public class InternetView
    {
        #region Properties
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public bool IsInternetPrepaid { get; set; }
        public DateTime? PaymentDate { get; set; }
        public float Balance { get; set; }
        public bool IsInternetPostpaid { get; set; }
        #endregion Properties

        #region Constructor(s)
        public InternetView() { }

        public InternetView(string address1, string address2, bool isInternetPrepaid,
            DateTime? paymentDate, float balance, bool isInternetPostpaid)
        {
            this.Address1 = address1;
            this.Address2 = address2;
            this.IsInternetPrepaid = isInternetPrepaid;
            this.PaymentDate = (paymentDate == new DateTime()) ? null : paymentDate;
            this.Balance = balance;
            this.IsInternetPostpaid = isInternetPostpaid;
        }
        #endregion Constructor(s)
    }
}
