﻿using System;

namespace DatabaseAccess.Views.Contract.Service.Internet
{
    public class PrepaidInternetView
    {
        #region Properties
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public DateTime? PaymentDate { get; set; }
        public float Balance { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PrepaidInternetView() { }

        public PrepaidInternetView(string address1, string address2, DateTime? paymentDate,
            float balance)
        {
            this.Address1 = address1;
            this.Address2 = address2;
            this.PaymentDate = paymentDate;
            this.Balance = balance;
        }
        #endregion Constructor(s)
    }
}
