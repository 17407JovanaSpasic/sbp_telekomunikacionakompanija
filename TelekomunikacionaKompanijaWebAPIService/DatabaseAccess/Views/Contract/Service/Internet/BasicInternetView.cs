﻿namespace DatabaseAccess.Views.Contract.Service.Internet
{
    public class BasicInternetView
    {
        #region Properties
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public bool IsInternetPrepaid { get; set; }
        #endregion Properties

        #region Constructor(s)
        public BasicInternetView() { }

        public BasicInternetView(string address1, string address2, bool isInternetPrepaid)
        {
            this.Address1 = address1;
            this.Address2 = address2;
            this.IsInternetPrepaid = isInternetPrepaid;
        }
        #endregion Constructor(s)
    }
}
