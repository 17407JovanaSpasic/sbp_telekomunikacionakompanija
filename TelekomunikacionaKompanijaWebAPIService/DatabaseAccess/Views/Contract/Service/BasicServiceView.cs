﻿using DatabaseAccess.Views.Contract.Service.Internet;

namespace DatabaseAccess.Views.Contract.Service
{
    public class BasicServiceView
    {
        #region Properties
        public bool ContainsTelephony { get; set; }
        public bool ContainsTelevision { get; set; }
        public bool ContainsInternet { get; set; }

        public BasicInternetView Internet { get; set; }
        #endregion Properties

        #region Constructor(s)
        public BasicServiceView()
        {
            this.Internet = null;
        }

        public BasicServiceView(bool containsTelephony, bool containsTelevision, bool containsInternet)
        {
            this.ContainsTelephony = containsTelephony;
            this.ContainsTelevision = containsTelevision;
            this.ContainsInternet = containsInternet;

            this.Internet = null;
        }
        #endregion Constructor(s)
    }
}
