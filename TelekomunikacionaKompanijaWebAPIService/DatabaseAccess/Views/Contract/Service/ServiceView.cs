﻿using System.Collections.Generic;

using DatabaseAccess.Views.Contract.Service.Internet;
using DatabaseAccess.Views.Contract.Service.Telephony;
using DatabaseAccess.Views.Contract.Service.Television;

namespace DatabaseAccess.Views.Contract.Service
{
    public class ServiceView
    {
        #region Properties
        public int ServiceID { get; protected set; }
        public bool ContainsTelephony { get; set; }
        public bool ContainsTelevision { get; set; }
        public bool ContainsInternet { get; set; }

        public IList<PhoneNumberView> PhoneNumbers { get; set; }
        public IList<AdditionalChannelPackageView> AdditionalChannelPackages { get; set; }
        public InternetView Internet { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ServiceView()
        {
            this.PhoneNumbers = new List<PhoneNumberView>();
            this.AdditionalChannelPackages = new List<AdditionalChannelPackageView>();
            this.Internet = null;
        }

        public ServiceView(bool containsTelephony, bool containsTelevision, bool containsInternet)
        {
            this.ContainsTelephony = containsTelephony;
            this.ContainsTelevision = containsTelevision;
            this.ContainsInternet = containsInternet;
            this.PhoneNumbers = new List<PhoneNumberView>();
            this.AdditionalChannelPackages = new List<AdditionalChannelPackageView>();
            this.Internet = null;
        }

        public ServiceView(int serviceID)
        {
            this.ServiceID = serviceID;
            this.PhoneNumbers = new List<PhoneNumberView>();
            this.AdditionalChannelPackages = new List<AdditionalChannelPackageView>();
            this.Internet = null;
        }

        public ServiceView(int serviceID, bool containsTelephony, bool containsTelevision,
            bool containsInternet)
        {
            this.ServiceID = serviceID;
            this.ContainsTelephony = containsTelephony;
            this.ContainsTelevision = containsTelevision;
            this.ContainsInternet = containsInternet;
            this.PhoneNumbers = new List<PhoneNumberView>();
            this.AdditionalChannelPackages = new List<AdditionalChannelPackageView>();
            this.Internet = null;
        }
        #endregion Constructor(s)
    }
}
