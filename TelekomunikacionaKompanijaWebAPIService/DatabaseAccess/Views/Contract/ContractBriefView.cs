﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Views.Contract
{
    public class ContractBriefView
    {
        #region Properties
        public string ContractId { get; set; }
        public DateTime ContractStartDate { get; set; }
        public DateTime ContractEndDate { get; set; }
        #endregion

        #region constructors
        public ContractBriefView(string contractId, DateTime contractStartDate, DateTime contractEndDate)
        {
            this.ContractId = contractId;
            this.ContractStartDate = contractStartDate;
            this.ContractEndDate = contractEndDate;
        }
        #endregion constuctors
    }
}
