﻿using System;
using System.Collections.Generic;
using DatabaseAccess.Views.Contract.Service;
using DatabaseAccess.Views.Contract.Payment;
using DatabaseAccess.Views.Package;
using DatabaseAccess.Views.User;

namespace DatabaseAccess.Views.Contract
{
    public class ContractView
    {
        #region Properties
        public string ContractId { get; set; }
        public DateTime ContractStartDate { get; set; }
        public DateTime ContractEndDate { get; set; }
        public UserView UserId { get; set; }
        public PackageView PackageName { get; set; }
        public ServiceView ServiceId { get; set; }

        public virtual IList<PaymentView> ServicePayment { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ContractView() { }

        public ContractView(string contractId, DateTime contractStarDate, DateTime contractEndDate,
            UserView userId, PackageView packageName, ServiceView serviceId)
        {
            this.ContractId = contractId;
            this.ContractStartDate = contractStarDate;
            this.ContractEndDate = contractEndDate;
            this.UserId = userId;
            this.PackageName = packageName;
            this.ServiceId = serviceId;
        }

        public ContractView(DateTime contractStarDate, DateTime contractEndDate, UserView userId,
            PackageView packageName, ServiceView serviceId)
        {
            this.ContractStartDate = contractStarDate;
            this.ContractEndDate = contractEndDate;
            this.UserId = userId;
            this.PackageName = packageName;
            this.ServiceId = serviceId;
        }

        //nisam bio siguran kako cemo da radimo sa onom listom, pa sam zato ostavio zakomentarisano
        /* public ContractDTO(string contractId, DateTime contractStarDate, DateTime contractEndDate, UserDTO userId, PackageDTO packageName, 
         ServiceDTO serviceId, List<PackageDTO> servicePayment)
         {
             this.ContractId = contractId;
             this.ContractStartDate = contractStarDate;
             this.ContractEndDate = contractEndDate;
             this.UserId = userId;
             this.PackageName = packageName;
             this.ServicePayment = ServicePayment;
             //this.ServiceId = servicePayment; ??
         }*/
        #endregion Constructor(s)
    }
}
