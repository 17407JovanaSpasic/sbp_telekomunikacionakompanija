﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Views.Contract.Payment
{
    public class PaymentCreationView
    {
        #region Properties
        public DateTime PaymentDate { get; set; }
        public float AdditionalPackagePrice { get; set; }
        public float TotalCost { get; set; }
        public string ContractNumber { get; set; }
        #endregion Properites

        #region Construcor
        public PaymentCreationView(DateTime paymentDate, float additionalPackagePrice, float totalCost, string contractNumber)
        {
            this.PaymentDate = paymentDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
            this.ContractNumber = contractNumber;
        }
        #endregion Constructor
    }
}
