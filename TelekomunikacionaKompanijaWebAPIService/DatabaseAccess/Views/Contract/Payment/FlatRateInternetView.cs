﻿using System;

namespace DatabaseAccess.Views.Contract.Payment
{
    public class FlatRateInternetView : PaymentView
    {
        #region Properties
        public float Cost { get; set; }
        #endregion Properties

        #region Constructor(s)
        public FlatRateInternetView() : base() { }
        public FlatRateInternetView(int payementNumber, DateTime payementDate,
            float additionalPackagePrice, float totalCost, string contractNumber,
            float cost)
            : base(payementNumber, payementDate, additionalPackagePrice, totalCost, contractNumber)
        {
            this.Cost = cost;
        }

        public FlatRateInternetView(DateTime payementDate,
           float additionalPackagePrice, float totalCost, string contractNumber,
           float cost)
           : base( payementDate, additionalPackagePrice, totalCost, contractNumber)
        {
            this.Cost = cost;
        }

        public FlatRateInternetView(int payementNumber, DateTime payementDate, float additionalPackagePrice,
           float totalCost, string ime, string prezime, string kontaktTelefon, string brojUgovora,
           string nazivPaketa,float cost)
            :base(payementNumber,payementDate,additionalPackagePrice,totalCost,ime,prezime,kontaktTelefon,brojUgovora,nazivPaketa)
        {
            this.Cost = cost;
        }
        #endregion Constructor(s)
    }
}
