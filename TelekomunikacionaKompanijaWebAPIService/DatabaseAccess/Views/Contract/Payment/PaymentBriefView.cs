﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Views.Contract.Payment
{
    public class PaymentBriefView
    {
        #region Properties
        public int PaymentNumber { get; set; }
        public DateTime PaymentDate { get; set; }
        public float AdditionalPackagePrice { get; set; }
        public float TotalCost { get; set; }
        #endregion Properites

        #region Construcor
        public PaymentBriefView(int paymentNumber, DateTime paymentDate, float additionalPackagePrice, float totalCost)
        {
            this.PaymentNumber = paymentNumber;
            this.PaymentDate = paymentDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
        }
        #endregion Constructor
    }
}
