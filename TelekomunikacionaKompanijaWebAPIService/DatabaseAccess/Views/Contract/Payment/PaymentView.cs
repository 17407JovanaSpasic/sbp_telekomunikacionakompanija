﻿using System;

namespace DatabaseAccess.Views.Contract.Payment

{
    public class PaymentView
    {
        #region Properties
        public int PaymentNumber { get; set; }
        public DateTime PaymentDate { get; set; }
        public float AdditionalPackagePrice { get; set; }
        public float TotalCost { get; set; }

        //public ContractView Contract { get; set; }

        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string KontaktTelefon { get; set; }
        public string BrojUgovora { get; set; }
        public string NazivPaketa { get; set; }
        #endregion Properties

        #region Constructor(s)
        public PaymentView() { }

        public PaymentView(DateTime payementDate, float additionalPackagePrice, float totalCost,
            string brojUgovora)
        {
            this.PaymentDate = payementDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
            this.BrojUgovora = brojUgovora;
        }

        public PaymentView(int payementNumber, DateTime payementDate, float additionalPackagePrice,
            float totalCost, string brojUgovora)
        {
            this.PaymentNumber = payementNumber;
            this.PaymentDate = payementDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
            this.BrojUgovora = brojUgovora;
        }

        public PaymentView(int payementNumber, DateTime payementDate, float additionalPackagePrice,
            float totalCost, string ime, string prezime, string kontaktTelefon, string brojUgovora,
            string nazivPaketa)
        {
            this.PaymentNumber = payementNumber;
            this.PaymentDate = payementDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
            this.Ime = ime;
            this.Prezime = prezime;
            this.KontaktTelefon = kontaktTelefon;
            this.BrojUgovora = brojUgovora;
            this.NazivPaketa = nazivPaketa;
        }
        #endregion Constructor(s)
    }
}
