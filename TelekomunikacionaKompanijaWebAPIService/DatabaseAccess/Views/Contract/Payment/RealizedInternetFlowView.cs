﻿using System;

namespace DatabaseAccess.Views.Contract.Payment
{
    public class RealizedInternetFlowView : PaymentView
    {
        #region Properties
        public float FlowAmount { get; set; }
        public float CostPerFlow { get; set; }
        #endregion Properties

        #region Constructor(s)
        public RealizedInternetFlowView() : base() { }

        public RealizedInternetFlowView(DateTime payementDate, float additionalPackagePrice, float totalCost,
           string contractNumber, float flowAmout, float costPerFlow)
           : base(payementDate, additionalPackagePrice, totalCost, contractNumber)
        {
            this.FlowAmount = flowAmout;
            this.CostPerFlow = costPerFlow;
        }

        public RealizedInternetFlowView(int payementNumber, DateTime payementDate, float additionalPackagePrice, float totalCost,
           string contractNumber, float flowAmout, float costPerFlow)
            : base(payementNumber, payementDate, additionalPackagePrice, totalCost, contractNumber)
        {
            this.FlowAmount = flowAmout;
            this.CostPerFlow = costPerFlow;
        }

        public RealizedInternetFlowView(int payementNumber, DateTime payementDate, float additionalPackagePrice,
           float totalCost, string ime, string prezime, string kontaktTelefon, string brojUgovora,
           string nazivPaketa, float flowAmount, float costPerFlow)
            : base(payementNumber, payementDate, additionalPackagePrice, totalCost, ime, prezime, kontaktTelefon, brojUgovora, nazivPaketa)
        {
            this.FlowAmount = flowAmount;
            this.CostPerFlow = costPerFlow;
        }
        #endregion Constructor(s)
    }
}
