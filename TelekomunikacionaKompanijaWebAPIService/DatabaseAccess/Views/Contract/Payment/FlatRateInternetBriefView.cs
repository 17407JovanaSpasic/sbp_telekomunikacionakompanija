﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Views.Contract.Payment
{
    public class FlatRateInternetBriefView
    {
        #region Properties
        public int PaymentNumber { get; set; }
        public DateTime PaymentDate { get; set; }
        public float AdditionalPackagePrice { get; set; }
        public float TotalCost { get; set; }
        //public string ContractNumber { get; set; }
        public float Cost { get; set; }
        #endregion Properties

        #region Constructor
        public FlatRateInternetBriefView(int paymentNumber, DateTime paymentDate, float additionalPackagePrice,
           float totalCost, float cost)
        {
            this.PaymentNumber = paymentNumber;
            this.PaymentDate = paymentDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
           // this.ContractNumber = contractNumber;
            this.Cost = cost;
        }
        #endregion Constructor
    }
}
