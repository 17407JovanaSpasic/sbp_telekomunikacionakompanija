﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Views.Contract.Payment
{
    public class FlatRateCreationView //Ovaj View se koristi za kreiranje placanja po Flat Rate-u
    {
        #region Properties
        public DateTime PaymentDate { get; set; }
        public float AdditionalPackagePrice { get; set; }
        public float TotalCost { get; set; }
        public string ContractNumber { get; set; }
        public float Cost { get; set; }
        #endregion Properties

        #region Constructor
        public FlatRateCreationView(DateTime paymentDate, float additionalPackagePrice,
           float totalCost, string contractNumber, float cost)
        {
            this.PaymentDate = paymentDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
            this.ContractNumber = contractNumber;
            this.Cost = cost;
        }
        #endregion Constructor
    }
}
