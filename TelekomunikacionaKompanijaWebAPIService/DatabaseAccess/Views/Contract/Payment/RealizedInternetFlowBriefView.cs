﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Views.Contract.Payment
{
    public class RealizedInternetFlowBriefView //Ovaj View se koristi za azuriranje placanja po protoku
    {
        #region Properties
        public int PaymentNumber { get; set; }
        public DateTime PaymentDate { get; set; }
        public float AdditionalPackagePrice { get; set; }
        public float TotalCost { get; set; }
        //public string ContractNumber { get; set; }
        public float FlowAmount { get; set; }
        public float CostPerFlow { get; set; }

        #endregion Properties

        #region Constructor
        public RealizedInternetFlowBriefView(int paymentNumber, DateTime paymentDate, float additionalPackagePrice,
           float totalCost, float flowAmount, float costPerFlow)
        {
            this.PaymentNumber = paymentNumber;
            this.PaymentDate = paymentDate;
            this.AdditionalPackagePrice = additionalPackagePrice;
            this.TotalCost = totalCost;
            this.FlowAmount = flowAmount;
            //this.ContractNumber = contractNumber;
            this.CostPerFlow = costPerFlow;
        }
        #endregion Constructor
    }
}
