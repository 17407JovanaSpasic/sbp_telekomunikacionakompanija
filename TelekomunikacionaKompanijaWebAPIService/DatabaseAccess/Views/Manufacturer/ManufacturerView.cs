﻿namespace DatabaseAccess.Views.Manufacturer
{
    public class ManufacturerView
    {
        #region Properties
        public string PIB { get; set; }
        public string Name { get; set; }
        #endregion Properties

        #region Constructor(s)
        public ManufacturerView() { }

        public ManufacturerView(string pib, string name)
        {
            this.PIB = pib;
            this.Name = name;
        }
        #endregion Constructor(s)
    }
}
