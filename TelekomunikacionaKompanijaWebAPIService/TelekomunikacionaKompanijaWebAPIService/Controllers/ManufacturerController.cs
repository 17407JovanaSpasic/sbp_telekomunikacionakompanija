﻿using System;

using DatabaseAccess.Providers;
using DatabaseAccess.Views.Manufacturer;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TelekomunikacionaKompanijaWebAPIService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ManufacturerController : ControllerBase
    {
        #region Manufacturer Method(s)
        [HttpPost]
        [Route("CreateManufacturer")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateManufacturer([FromBody] ManufacturerView manufacturer)
        {
            try
            {
                ManufacturerProvider.CreateManufacturer(manufacturer);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllManufacturers")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetAllManufacturers()
        {
            try
            {
                return new JsonResult(ManufacturerProvider.GetAllManufacturers());
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdateManufacturer")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateManufacturer([FromBody] ManufacturerView manufacturer)
        {
            try
            {
                ManufacturerProvider.UpdateManufacturer(manufacturer);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteManufacturer/{pib}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteManufacturer(string pib)
        {
            try
            {
                ManufacturerProvider.DeleteManufacturer(pib);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Manufacturer Method(s)
    }
}
