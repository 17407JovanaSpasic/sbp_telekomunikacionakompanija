﻿using System;

using DatabaseAccess.Providers;
using DatabaseAccess.Views.Package;
using DatabaseAccess.Views.Package.AdditionalPackage;
using DatabaseAccess.Views.Package.Channel;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TelekomunikacionaKompanijaWebAPIService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PackageController : ControllerBase
    {
        #region Package Method(s)
        [HttpPost]
        [Route("CreatePackage")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreatePackage([FromBody] PackageView package)
        {
            try
            {
                PackageProvider.CreatePackage(package);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetPackageByName/{packageName}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetPackageByName(string packageName)
        {
            try
            {
                var package = PackageProvider.GetPackageByName(packageName);

                return new JsonResult(package);
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetPackageByNameWithAdditionalInfo/{packageName}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetPackageByNameWithAdditionalInfo(string packageName)
        {
            try
            {
                return new JsonResult(PackageProvider.GetAdvancedPackageInfoByName(packageName));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllPackages")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetAllPackages()
        {
            try
            {
                return new JsonResult(PackageProvider.GetAllPackages());
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllPackagesWithAdditionalInfo")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetAllPackagesWithAdditionalInfo()
        {
            try
            {
                return new JsonResult(PackageProvider.GetAdvancedPackagesInfo());
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdatePackage")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdatePackage([FromBody] PackageView package)
        {
            try
            {
                PackageProvider.UpdatePackage(package);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeletePackage/{packageName}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeletePackage(string packageName)
        {
            try
            {
                PackageProvider.DeletePackage(packageName);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Package Method(s)

        #region Channel Method(s)
        [HttpPost]
        [Route("CreateChannelForPackage")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateChannelForPackage([FromBody] PackageChannelView channel)
        {
            try
            {
                PackageProvider.CreateChannelForPackage(channel);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetChannelsByPackage/{packageName}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetChannelsByPackage(string packageName)
        {
            try
            {
                return new JsonResult(PackageProvider.GetChannelsByPackage(packageName));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllChannels")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetAllChannels()
        {
            try
            {
                return new JsonResult(PackageProvider.GetAllChannels());
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdateChannelFromPackage")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateChannelFromPackage([FromBody] UpdatePackageChannelView channel)
        {
            try
            {
                PackageProvider.UpdateChannelFromPackage(channel);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteChannelByPackage/{channelName}/{packageName}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteChannelByPackage(string channelName, string packageName)
        {
            try
            {
                var channel = new PackageChannelView(packageName, channelName);

                PackageProvider.DeleteChannelFromPackage(channel);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Channel Method(s)

        #region Additional Package Method(s)
        [HttpPost]
        [Route("CreateAdditionalPackageForPackage")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateAdditionalPackageForPackage
            ([FromBody] PackageAdditionalPackageView additionalPackage)
        {
            try
            {
                PackageProvider.CreateAdditionalPackageForPackage(additionalPackage);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAdditionalPackagesByPackage/{packageName}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetAdditionalPackagesByPackage(string packageName)
        {
            try
            {
                return new JsonResult(PackageProvider.GetAdditionalPackagesByPackage(packageName));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllAdditionalPackages")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetAllAdditionalPackages()
        {
            try
            {
                return new JsonResult(PackageProvider.GetAllAdditionalPackages());
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdateAdditionalPackageFromPackage")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateAdditionalPackageFromPackage([FromBody]
            UpdateAdditionalPackageView additionalPackage)
        {
            try
            {
                PackageProvider.UpdateAdditionalPackageFromPackage(additionalPackage);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteAdditionalPackageByPackage/{additionalPackageName}/{packageName}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteAdditionalPackageByPackage(string additionalPackageName,
            string packageName)
        {
            try
            {
                var additionalPackage = new PackageAdditionalPackageView(packageName,
                    additionalPackageName);

                PackageProvider.DeleteAdditionalPackageFromPackage(additionalPackage);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Additional Package Method(s)
    }
}
