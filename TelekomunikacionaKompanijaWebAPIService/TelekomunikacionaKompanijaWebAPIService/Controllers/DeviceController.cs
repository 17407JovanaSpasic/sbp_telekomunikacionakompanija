﻿using System;

using DatabaseAccess.Providers;
using DatabaseAccess.Views.Device;
using DatabaseAccess.Views.Device.CommunicationNode;
using DatabaseAccess.Views.Device.MainStation;
using DatabaseAccess.Views.Device.MainStation.RegionalHub;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TelekomunikacionaKompanijaWebAPIService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        #region Main Station Method(s)
        [HttpPost]
        [Route("CreateMainStation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateMainStation([FromBody] BasicDeviceView mainStation)
        {
            try
            {
                DeviceProvider.CreateMainStation(mainStation);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllMainStations")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetMainStationStations()
        {
            try
            {
                return new JsonResult(DeviceProvider.GetAllMainStations());
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdateMainStations/{serialId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateMainStationStations(string serialId, [FromBody] MainStationUpdateView mainStation)
        {
            try
            {
                if (DeviceProvider.UpdateMainStation(serialId, mainStation))
                    return Ok();
                else
                    return BadRequest(new String("Uneli ste pogresan serijski broj cvora"));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteMainStation/{serialId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult MainStationHub(string serialId)
        {
            try
            {
                if (DeviceProvider.DeleteMainStationOrRegionalHub(serialId))
                    return Ok();
                else
                    return BadRequest(new String("Pokusali ste da obriste cvor koji ne postoji"));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Main Station Method(s)

        #region Regional Hub Method(s)
        [HttpPost]
        [Route("CreateRegionalHub")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateRegionalHub([FromBody] BasicRegionalHubView regionalHub)
        {
            try
            {
                DeviceProvider.CreateRegionalHub(regionalHub);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetRegionalHub")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetRegionalHub()
        {
            try
            {
                return new JsonResult(DeviceProvider.GetRegionalHub());
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdateRegionalHub/{serialId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateRegionalHub(string serialId, [FromBody] RegionalHubUpdateView regionalHub)
        {
            try
            {
                if (DeviceProvider.UpdateRegionalHub(serialId, regionalHub))
                    return Ok();
                else
                    return BadRequest(new String("Uneli ste pogresan serijski broj cvora"));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteRegionalHub/{serialId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteRegionalHub(string serialId)
        {
            try
            {
                if (DeviceProvider.DeleteMainStationOrRegionalHub(serialId))
                    return Ok();
                else
                    return BadRequest(new String("Pokusali ste da obriste cvor koji ne postoji"));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Regional Hub Method(s)

        #region Communication Node Method(s)
        [HttpPost]
        [Route("CreateCommunicationNode")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateCommunicationNode
            ([FromBody] BasicCommunicationNodeView communicationNode)
        {
            try
            {
                DeviceProvider.CreateCommunicationNode(communicationNode);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetCommunicationNodeBySerialNumber/{serialNumber}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetCommunicationNodeBySerialNumber(string serialNumber)
        {
            try
            {
                return new JsonResult(DeviceProvider.GetCommunicationNodeBySerialNumber(serialNumber));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllCommunicationNodes")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetAllCommunicationNodes()
        {
            try
            {
                return new JsonResult(DeviceProvider.GetAllCommunicationNodes());
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdateCommunicationNode/{serialNumber}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateCommunicationNode(string serialNumber,
            [FromBody] CommunicationNodeView communicationNode)
        {
            try
            {
                DeviceProvider.UpdateCommunicationNode(serialNumber, communicationNode);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteCommunicationNode/{serialNumber}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteCommunicationNode(string serialNumber)
        {
            try
            {
                DeviceProvider.DeleteCommunicationNode(serialNumber);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Communication Node Method(s)
    }
}
