﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatabaseAccess.Providers;
using DatabaseAccess.Views.User;
using DatabaseAccess.Providers.Contract;

namespace TelekomunikacionaKompanijaWebAPIService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region IndividualUser

        [HttpPost]
        [Route("CreateIndividualUser")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateIndividualUser([FromBody] IndividualUserCreationView user)
        {
            try
            {
                if (UserProvider.CreateIndividualUser(user))
                    return Ok();
                else
                    return BadRequest(new String("Greska prilikom kreiranja korisnika, najcesci razlozi su da ste pokusali da kreirate korisnika" +
                        " sa istim brojem telefona i email i time narusili column constraint"));
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpGet]
        [Route("GetIndividualUsers")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetIndividualUsers()
        {
            try
            {
                return new JsonResult(UserProvider.GetAllIndividualUsers());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPut]
        [Route("UpdateIndividualUser")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateIndividualUser([FromBody] IndividualBriefView user)
        {
            try
            {
                if(UserProvider.UpdateIndividualUser(user))
                    return Ok();

                return BadRequest(new String("Greska prilikom azuriranja fizickog lica!" +
                " \nNajcesci razlozi: uneli ste ID korisnika koji ne postoji ili ste pokusali da azurirate korisnika koji je pravno lice."));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
        #endregion IndividualUser

        #region LegalEntity

        [HttpPost]
        [Route("CreateLegalEntity")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateLegalEntity([FromBody] LegalEntityUserCreationView user)
        {
            try
            {
                if (UserProvider.CreateLegalEntityUser(user))
                    return Ok();
                else
                    return BadRequest(new String("Greska prilikom kreiranja korisnika, najcesci razlozi su da ste pokusali da kreirate korisnika" +
                        " sa istim brojem telefona i email i time narusili column constraint"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpGet]
        [Route("GetLegalEntityUsers")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetLegalEntity()
        {
            try
            {
                return new JsonResult(UserProvider.GetAllLegalEntityUsers());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPut]
        [Route("UpdateLegalEntity")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateLegalEntity([FromBody] LegalEntityBriefView user)
        {
            try
            {
                if(UserProvider.UpdateLegalEntity(user))
                    return Ok();

                return BadRequest(new String("Greska prilikom azuriranja pravnog lica!" +
                " \nNajcesci razlozi: uneli ste ID korisnika koji ne postoji ili ste pokusali da azurirate korisnika koji je fizicko lice."));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
        #endregion LegalEntity

        #region MutualMethods
        [HttpDelete]
        [Route("DeleteUser/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteUser(int id)
        {
            try
            {
                if (UserProvider.DeleteUser(id))
                {
                    return Ok();
                }
                else return BadRequest();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
        #endregion MutualMethods
    }
}
