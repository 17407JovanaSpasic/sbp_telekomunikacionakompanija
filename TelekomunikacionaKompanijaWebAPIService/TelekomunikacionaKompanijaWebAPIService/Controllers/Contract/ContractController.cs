﻿using DatabaseAccess.Providers;
using DatabaseAccess.Providers.Contract;
using DatabaseAccess.Views.Contract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TelekomunikacionaKompanijaWebAPIService.Controllers.Contract
{
    [Route("[controller]")]
    [ApiController]
    public class ContractController : ControllerBase
    {
        [HttpPost]
        [Route("CreateContract")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateContract([FromBody] ContractCreationView con)
        {
            try
            {

                var user = UserProvider.GetUserByID(con.UserId);
                if (user == null)
                    return BadRequest(new String("Korisnik sa datim Id-em ne postoji, proverite da li ste uneli ispravan Id"));

                var package = PackageProvider.GetPackageByName(con.PackageName);
                if (package == null)
                    return BadRequest(new String("Ne postoji paket sa datim imenom"));

                var service = ServiceProvider.GetServiceByID(con.ServiceId);
                if (service == null)
                    return BadRequest(new String("Proverite da li ste uneli ispravan ID servisa koji ste prethodno kreirali"));

                var contract = new ContractView(DateTime.Now, DateTime.Now.AddYears(con.Duration), user, package, service);

                if (ContractProvider.CreateContract(contract) == false)
                    return BadRequest(new String("Gresak prilikom kreiranja ugovora"));

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpGet]
        [Route("GetAllContracts")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetContracts()
        {
            try
            {
                return new JsonResult(ContractProvider.GetAllContracts());
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPut]
        [Route("UpdateContract")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateIndividualUser([FromBody] ContractBriefView con)
        {
            try
            {
                if(ContractProvider.UpdateContract(con)==false)
                    return BadRequest(new String("Greska prilikom azuriranja ugovora! \n Proverite da li ste ispravno uneli ID"));
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpDelete]
        [Route("DeleteContract/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteContract(string id)
        {
            try
            {
                if(ContractProvider.DeleteContract(id)==false)
                    return BadRequest(new String("Ugovor sa datim ID-em vec ne postoji!"));
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}
