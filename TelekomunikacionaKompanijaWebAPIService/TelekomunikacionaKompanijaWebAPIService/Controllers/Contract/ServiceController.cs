﻿using System;

using DatabaseAccess.Helpers;
using DatabaseAccess.Providers.Contract;
using DatabaseAccess.Views.Contract.Service;
using DatabaseAccess.Views.Contract.Service.Internet;
using DatabaseAccess.Views.Contract.Service.Telephony;
using DatabaseAccess.Views.Contract.Service.Television;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;

namespace TelekomunikacionaKompanijaWebAPIService.Controllers.Contract
{
    [Route("[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        #region Service Method(s)
        [HttpPost]
        [Route("CreateService")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateService([FromBody] BasicServiceView service)
        {
            try
            {
                ServiceProvider.CreateService(service);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetServiceByID/{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetServiceByID(int id)
        {
            try
            {
                return new JsonResult(ServiceProvider.GetServiceByID(id));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllServices")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetAllServices([FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            try
            {
                var parameters = new PageParameters(pageNumber, pageSize);

                var services = ServiceProvider.GetAllServices(parameters);

                var metadata = new
                {
                    services.TotalCount,
                    services.PageSize,
                    services.CurrentPage,
                    services.TotalPages
                };

                this.Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

                return new JsonResult(services);
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdatePrepaidInternetServiceInfo/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdatePrepaidInternetServiceInfo(int id,
            [FromBody] PrepaidInternetView internet)
        {
            try
            {
                ServiceProvider.UpdatePrepaidInternetServiceInfo(id, internet);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdatePostpaidInternetServiceInfo/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdatePostpaidInternetServiceInfo(int id,
            [FromBody] PostpaidInternetView internet)
        {
            try
            {
                ServiceProvider.UpdatePostpaidInternetServiceInfo(id, internet);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteService/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteService(int id)
        {
            try
            {
                ServiceProvider.DeleteService(id);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Service Method(s)

        #region Phone Number Method(s)
        [HttpPost]
        [Route("CreatePhoneNumberForService")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreatePhoneNumberForService([FromBody] ServicePhoneNumberView phoneNumber)
        {
            try
            {
                ServiceProvider.CreatePhoneNumber(phoneNumber);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetPhoneNumbersByServiceID/{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetPhoneNumbersByServiceID(int id)
        {
            try
            {
                return new JsonResult(ServiceProvider.GetPhoneNumbersByServiceID(id));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdateNumberOfMinutes")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateNumberOfMinutes([FromBody] PhoneNumberView phoneNumber)
        {
            try
            {
                ServiceProvider.UpdateNumberOfMinutes(phoneNumber);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeletePhoneNumber/{phoneNumber}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeletePhoneNumber(string phoneNumber)
        {
            try
            {
                ServiceProvider.DeletePhoneNumber(phoneNumber);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Phone Number Method(s)

        #region Additional Channel Package Method(s)
        [HttpPost]
        [Route("CreateAdditionalChannelPackageForService")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateAdditionalChannelPackageForService([FromBody]
            ServiceAdditionalChannelPackageView additionalChannelPackage)
        {
            try
            {
                ServiceProvider.CreateAdditionalChannelPackage(additionalChannelPackage);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAdditionalChannelPackagesByServiceID/{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetAdditionalChannelPackagesByServiceID(int id)
        {
            try
            {
                return new JsonResult(ServiceProvider.GetAdditionalChannelPackagesByServiceID(id));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdateAdditionalChannelPackageFromService")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateAdditionalChannelPackageFromService([FromBody]
            UpdateAdditionalChannelPackageView additionalChannelPackage)
        {
            try
            {
                ServiceProvider.UpdateAdditionalChannelPackageFromService(additionalChannelPackage);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteAdditionalChannelPackageFromService/{id}/{additionalPackageName}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteAdditionalChannelPackageFromService(int id,
            string additionalPackageName)
        {
            try
            {
                var additionalChannelName = new ServiceAdditionalChannelPackageView(id,
                    additionalPackageName);

                ServiceProvider.DeleteAdditionalChannelPackageFromService(additionalChannelName);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Additional Channel Package Method(s)
    }
}
