﻿using DatabaseAccess.Providers.Contract;
using DatabaseAccess.Views.Contract.Payment;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TelekomunikacionaKompanijaWebAPIService.Controllers.Contract
{
    [Route("[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        #region FlatRatePayment


        [HttpPost]
        [Route("CreatePaymentFlateRateInternet")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateFlateRateInternetPayment([FromBody] FlatRateCreationView payment)
        {
            try
            {
                if (PaymentProvider.CreatePaymentWithInternet(payment,null))
                    return Ok();
                else
                    return BadRequest(new String("Proverite da li ste uneli ispravan ID ugovora! " +
                        "\nDrugi vid greske je da ste pokusali da platite racun za po protoku."));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpGet]
        [Route("GetAllFlateRatePayments")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetFlateRatePayments()
        {
            try
            {
                return new JsonResult(PaymentProvider.GetAllFlatRateInternet());

            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPut]
        [Route("UpdateFlatRateInternet")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateFlatRateInternet([FromBody] FlatRateInternetBriefView payment)
        {
            try
            {
                if (PaymentProvider.UpdateFlatRateInternet(payment))
                    return Ok();

                return BadRequest(new String("Proverite da li ste uneli ispravan ID ugovora! " +
                    "\nDrugi vid greske je da ste pokusali da platite racun za po protoku."));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpDelete]
        [Route("DeleteFlatRateInternetPayment/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteFlatRateInternetPaymentPayment(string id)
        {
            try
            {
                if (PaymentProvider.DeletePayement(int.Parse(id)))
                    return Ok();
                else
                {
                    return BadRequest(new String("Ugovor sa datim ID-em vec ne postoji!"));
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
        #endregion PaymentInGeneral

        #region RealizedInternetFlowPayment

        [HttpPost]
        [Route("CreateRealizedInternetFlowInternet")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateRealizedInternetFlowPayment([FromBody] RealizedInternetCreationView payment)
        {
            try
            {
                if (PaymentProvider.CreatePaymentWithInternet(null, payment))
                    return Ok();
                else
                    return BadRequest(new String("Proverite da li ste uneli ispravan ID ugovora! \nDrugi vid greske je da ste pokusali da platite racun za placanje po Flat Rate Adresi."));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpGet]
        [Route("GetAllRealizedInternetFlowPayments")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult RealizedInternetFlowPayments()
        {
            try
            {
                return new JsonResult(PaymentProvider.GetAllRealizedInternetFlow());

            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPut]
        [Route("UpdateRealizedInternetFlow")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateRealizedInternetFlow([FromBody] RealizedInternetFlowBriefView payment)
        {
            try
            {
                if (PaymentProvider.UpdateRealizedInternetFlow(payment))
                    return Ok();

                return BadRequest(new String("Proverite da li ste uneli ispravan ID ugovora! " +
                    "\nDrugi vid greske je da ste pokusali da platite racun za po protoku."));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpDelete]
        [Route("DeleteRealizedInternetFlowPaymnet/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeleteRealizedInternetFlowPayment(string id)
        {
            try
            {
                if (PaymentProvider.DeletePayement(int.Parse(id)))
                    return Ok();
                else
                {
                    return BadRequest(new String("Ugovor sa datim ID-em vec ne postoji!"));
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
        #endregion RealizedInternetFlowPayment

        #region PaymentInGeneral
        [HttpPost]
        [Route("CreatePayment")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreatePayment([FromBody] PaymentCreationView payment)
        {
            try
            {
                if (PaymentProvider.CreatePayment(payment))
                    return Ok();
                else
                    return BadRequest(new String("Proverite da li ste uneli ispravan ID ugovora!"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpGet]
        [Route("GetAllPayments")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetPayments()
        {
            try
            {
                return new JsonResult(PaymentProvider.GetAllPayments());

            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPut]
        [Route("UpdatePaymentInGeneral")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdatePayment([FromBody] PaymentBriefView payment)
        {
            try
            {
                if (PaymentProvider.UpdatePayment(payment))
                    return Ok();

                return BadRequest(new String("Proverite da li ste uneli ispravan ID ugovora!"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpDelete]
        [Route("DeletePaymnet/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult DeletePayment(string id)
        {
            try
            {
                if (PaymentProvider.DeletePayement(int.Parse(id)))
                    return Ok();
                else
                {
                    return BadRequest(new String("Ugovor sa datim ID-em vec ne postoji!"));
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
        #endregion PaymentInGeneral
    }
}

